# Charts Library Release Notes

# 4.8.2

- Fixed similar range issue([OW-20482](https://oliasoft.atlassian.net/browse/OW-20482))

# 4.8.1

- Removed `isEqual` check to improve performance for huge DSs

# 4.8.0

- Upgrade dependencies([OW-20242](https://oliasoft.atlassian.net/browse/OW-20242))

# 4.7.9

- Fixed getting `Line Chart` initial range ([OW-19966](https://oliasoft.atlassian.net/browse/OW-19966))

# 4.7.8

- Fixed resetting in `Line Chart` when multi axes ([OW-19953](https://oliasoft.atlassian.net/browse/OW-19953))

# 4.7.7

- Dragger plugin, lose context when external re-render([OW-19887](https://oliasoft.atlassian.net/browse/OW-19887))

# 4.7.6

- Attempt to fix issue - `Cannot read properties of undefined (reading 'handleEvent')`([OW-19467](https://oliasoft.atlassian.net/browse/OW-19467)) ([WELLDESIGN-4M5](https://oliasoft-as.sentry.io/issues/5878512851/))

# 4.7.5

- Fixed set styles in Scatter Chart

# 4.7.4

- Enabled  support multi axes for `Dragging Plugin`([OW-19346](https://oliasoft.atlassian.net/browse/OW-19346))

# 4.7.3

- Added guards([OW-19467](https://oliasoft.atlassian.net/browse/OW-19467))

# 4.7.2

- Bump version of `oliasoft-open-source/units` to `4.2.2`
- Bump version of `oliasoft-open-source/react-ui-library` to `4.18.3`
- ([OW-19280](https://oliasoft.atlassian.net/browse/OW-19280))

# 4.7.1

- Enabled passing line tension for each DSs separately([OW-19335](https://oliasoft.atlassian.net/browse/OW-19335))

# 4.7.0

- Added feature to restrict annotation dragging within specified bounds or chart area([OW-19201](https://oliasoft.atlassian.net/browse/OW-19201))

# 4.6.6

- Update packages

# 4.6.5

- Upgrade to latest version of @oliasoft-open-source/units and @oliasoft-open-source/react-ui-library packages

# 4.6.4

- Added Support for Custom Shapes in Point Annotations([OW-18898](https://oliasoft.atlassian.net/browse/OW-18898))

# 4.6.3

- `LineChart` menu collapses when there isn't space ([OW-18599](https://oliasoft.atlassian.net/browse/OW-18599))

# 4.6.2

- Update legend dropzone position and size when chart size changes ([OW-18770](https://oliasoft.atlassian.net/browse/OW-18770))

# 4.6.1

- Added several guards(WELLDESIGN-4M5)

# 4.6.0

- Upgrade base image to Node.js v22 ([OW-18527](https://oliasoft.atlassian.net/browse/OW-18527))

# 4.5.3

- Used default dra time for annotation([OW-18472](https://oliasoft.atlassian.net/browse/OW-18472))

# 4.5.2

- Single dataset `BarChart` should have same color for all bars by default ([OW-18326](https://oliasoft.atlassian.net/browse/OW-18326))

# 4.5.1

- Bar chart legend icon uses `backgroundColor` instead of `borderColor` ([OW-18304](https://oliasoft.atlassian.net/browse/OW-18304))

# 4.5.0

- Allowed dragging annotation to drag only one axis([OW-18145](https://oliasoft.atlassian.net/browse/OW-18145))

# 4.4.6

- added `scientificNotation` prop to tooltips options ([OW-18050](https://oliasoft.atlassian.net/browse/OW-18050))

# 4.4.5

- upgrade `react-ui-library` package

# 4.4.4

- make the `peerDepencency` version on Immer less strict (accept `v9` or `v10`)
- upgrade `react-ui-library` and `units` packages

# 4.4.3

- Fixed chart crash bug when trying to edit annotations very fast with "auto-recalc-trajectories" feature enabled in WellDesign ([OW-17857](https://oliasoft.atlassian.net/browse/OW-17857)), ([OW-17824](https://oliasoft.atlassian.net/browse/OW-17824))

# 4.4.2

- Enabled datapoints label (not just the dataset label) to be displayed on hover. ([OW-17713](https://oliasoft.atlassian.net/browse/OW-17713?focusedCommentId=41109))

# 4.4.1

- Fix Safari sizing bug with legend lines/points ([OW-17764](https://oliasoft.atlassian.net/browse/OW-17764))

# 4.4.0

- Added custom gradient plugin, applied for `Scatter Chart`([OW-17704](https://oliasoft.atlassian.net/browse/OW-17704))

# 4.3.12

- Added portal for `Bar Chart`([OW-17702](https://oliasoft.atlassian.net/browse/OW-17702))

# 4.3.11

- fixed the animation problem (jumping/rerendering) of some data points

# 4.3.10

- Fixed warnings on console([OW-17673](https://oliasoft.atlassian.net/browse/OW-17673))

# 4.3.9

- Fixed formatting value in bar chart data labels

# 4.3.8

- Fixed data labels and tooltips for waterfall bar charts and added story ([OW-17382](https://oliasoft.atlassian.net/browse/OW-17382))

# 4.3.7

- added ajv packages as peerDependency

# 4.3.6

- Fixed set styling for `Scatter`([OW-17485](https://oliasoft.atlassian.net/browse/OW-17485))

# 4.3.5

- Fixed Published storybook build([OW-17497](https://oliasoft.atlassian.net/browse/OW-17497))

# 4.3.4

- Add `display` property to `labelConfig` to control label display in point annotations

# 4.3.3

- Fixed minor build errors

# 4.3.2

- Turn off tooltips when drag annotation

# 4.3.1

- Made drag annotation plugin works with dynamic values

# 4.3.0

- Created custom plugin for drag specific annotation([OW-17088](https://oliasoft.atlassian.net/browse/OW-17088))

# 4.2.3

- Added to `Bar Chart` support stacking on selected axis([OW-17014](https://oliasoft.atlassian.net/browse/OW-17014))

# 4.2.2

- Fix register issue([WELLDESIGN-49R](https://oliasoft-as.sentry.io/issues/5448639578/))

# 4.2.1

- Added Scatter Chart to exports

# 4.2.0

- Implemented `Scatter Chart`([OW-16963](https://oliasoft.atlassian.net/browse/OW-16963))

# 4.1.11

- Fixed annotation type ([OW-17047](https://oliasoft.atlassian.net/browse/OW-17047))

# 4.1.10

- Fixed tooltip labels in `Bar Chart`([OW-16626](https://oliasoft.atlassian.net/browse/OW-16626))

# 4.1.9

- Allow hide specific `Annotation` in legend([OW-16137](https://oliasoft.atlassian.net/browse/OW-16137))

# 4.1.8

- Fixed similar value case with controlled annotation([OW-15951](https://oliasoft.atlassian.net/browse/OW-15951))

# 4.1.7

- Minor fixes and improvements([WELLDESIGN-42T](https://oliasoft-as.sentry.io/issues/5249899045))

# 4.1.6

- Improved `Annotation`, added show/hide prop, remove redundant indexes for showing annotations([OW-16493](https://oliasoft.atlassian.net/browse/OW-16493))

# 4.1.5

- Fixed tsconfig and eslint alias config([OW-15542](https://oliasoft.atlassian.net/browse/OW-OW-15542))

# 4.1.4

- Added guards for `getUnitsFromLabel`

# 4.1.3

- Fixed toggle annotation externally

# 4.1.2

- Fixed formatting value in tooltips

# 4.1.1

- Added minor guards

# 4.1.0

- Improved custom legend component and applied to BarChart([OW-10671](https://oliasoft.atlassian.net/browse/OW-10671))

# 4.0.5

- Added option to disable scientific notation in line chart tooltips [OW-15239](https://oliasoft.atlassian.net/browse/OW-15239)

# 4.0.4

- Improved custom legend component and applied to BarChart([OW-10671](https://oliasoft.atlassian.net/browse/OW-10671))

# 4.0.4

- Updated logic for `showPoints` and `showLine` and added story to test variations [OW-15191](https://oliasoft.atlassian.net/browse/OW-15191)

# 4.0.3

- Fix ESModule type [OW-14064](https://oliasoft.atlassian.net/browse/OW-14064)

# 4.0.2

- Fixed sentry bug `WELLDESIGN-3GK`

# 4.0.1

- Fix mattermost url and changed channel name to Release bots [OW-14755](https://oliasoft.atlassian.net/browse/OW-14755)

# 4.0.0

- Rebranded `charts-library` with fonts and styles for Oliasoft's new brand identity [OW-13041](https://oliasoft.atlassian.net/browse/OW-13041)

## 3.8.0

- Cleanup any types & removed redundant logic([OW-13895](https://oliasoft.atlassian.net/browse/OW-13895))

## 3.7.6

- Enabled global options when custom option defined in DS

## 3.7.5

- Added story for `lineTension` in line chart

## 3.7.4

- Add option to hide datasets from legend ([OW-14273](https://oliasoft.atlassian.net/browse/OW-14273))

## 3.7.3

- Hide legend when DSs are empty([OW-14258](https://oliasoft.atlassian.net/browse/OW-14258))

## 3.7.2

- Fixed logic to update initAxes when DS change([OW-14254](https://oliasoft.atlassian.net/browse/OW-14254))

## 3.7.1

- Bumped react-ui-library version to 3.13.9 ([OW-13887](https://oliasoft.atlassian.net/browse/OW-13887))

## 3.7.0

- Removed scatter chart ([OW-13980](https://oliasoft.atlassian.net/browse/OW-13980))

## 3.6.10

- Fixed labels in BarChart for not logarithmic chart type([OW-13914](https://oliasoft.atlassian.net/browse/OW-13914))

## 3.6.9

- Fixed DTS plugin config

## 3.6.8

- Clean legend state when datasets changing([OW-13747](https://oliasoft.atlassian.net/browse/OW-13747))

## 3.6.7

- Fixed pan([OW-13728](https://oliasoft.atlassian.net/browse/OW-13728))

## 3.6.6

- Fixed legend panel([OW-13694](https://oliasoft.atlassian.net/browse/OW-13694))

## 3.6.5

- fixed weird range after reseting([OW-13670](https://oliasoft.atlassian.net/browse/OW-13670))

## 3.6.4

- Allow point and line combined in same chart([OW-13561](https://oliasoft.atlassian.net/browse/OW-13561))

## 3.6.3

- Migrated annotation plugin to 3.x([OW-13617](https://oliasoft.atlassian.net/browse/OW-13617))

## 3.6.2

- Build Alert Resolution fixing Errors([OW-12847](https://oliasoft.atlassian.net/browse/OW-12847))

## 3.6.1

- Handle partial range([OW-13161](https://oliasoft.atlassian.net/browse/OW-13161))

## 3.6.0

- Updated all packages to their latest versions

## 3.5.5

- Fixed out values issue and axes menu([OW-13318](https://oliasoft.atlassian.net/browse/OW-13318))

## 3.5.4

- Updated react-chartjs-2 for support ChartJS ver4+

## 3.5.3

- Allowed continue pipelines when betta falls([OW-13420](https://oliasoft.atlassian.net/browse/OW-13420))

## 3.5.2

- Fix check range and def value for it([OW-13037](https://oliasoft.atlassian.net/browse/OW-13037))

## 3.5.1

- Added logic for update axes range when datasets changed([OW-12861](https://oliasoft.atlassian.net/browse/OW-12861))

## 3.5.0

- Added Beta release label and added rules to pipelines to run beta release when added

## 3.4.11

- Always show points for single point datasets in LineChart ([OW-12874](https://oliasoft.atlassian.net/browse/OW-12874))

## 3.4.10

- Used rounding in tooltips ([OW-12385](https://oliasoft.atlassian.net/browse/OW-12385))

## 3.4.9

- Fixed dnd conflict ([OW-12724](https://oliasoft.atlassian.net/browse/OW-12724))

## 3.4.8

- Fixed dnd conflict ([OW-12724](https://oliasoft.atlassian.net/browse/OW-12724))

## 3.4.7

- Fixed types and interfaces ([OW-12579](https://oliasoft.atlassian.net/browse/OW-12579))

## 3.4.6

- Simplified getAxesData, used common properties ([OW-12533](https://oliasoft.atlassian.net/browse/OW-12533))

## 3.4.5

- Display different point styles in tooltip and reduce label length; show units in tooltip if present ([OW-11906](https://oliasoft.atlassian.net/browse/OW-11906))

## 3.4.4

- Fixed active icon when switch from DragData ([OW-12649](https://oliasoft.atlassian.net/browse/OW-12649))

## 3.4.3

- Fixed reset btn ([OW-12648](https://oliasoft.atlassian.net/browse/OW-12648))

## 3.4.2

- Fixed pass props for dragData ([OW-12593](https://oliasoft.atlassian.net/browse/OW-12593))

## 3.4.1

- Replaced Object.freeze with enums ([OW-12596](https://oliasoft.atlassian.net/browse/OW-12596))

## 3.4.0

- Remove PropTypes, switch to TypeScript ([OW-9591](https://oliasoft.atlassian.net/browse/OW-9591))

## 3.3.8

- Fix crash bug in multi-axis charts with implicit default axis IDs ([OW-12527](https://oliasoft.atlassian.net/browse/OW-12527))

## 3.3.7

- Improved display value view in tooltips ([OW-12385](https://oliasoft.atlassian.net/browse/OW-12385))

## 3.3.6

- Fixed get data for axes([OW-12461](https://oliasoft.atlassian.net/browse/OW-12461))

## 3.3.5

- Upgrade version of units package to resolve number display bugs

## 3.3.4

- Fixed legends hidden by default

## 3.3.3

- Update legend layout on chart resize ([OW-12426](https://oliasoft.atlassian.net/browse/OW-12426))

## 3.3.2

- Fixed rounding in pie chart tooltips [OW-12129](https://oliasoft.atlassian.net/browse/OW-12129)

## 3.3.1

- Fix annotation error in PieChart

## 3.3.0

- Draggable on-canvas legend [OW-11861](https://oliasoft.atlassian.net/browse/OW-11861)

## 3.2.5

- Fix bug in where reseting axes in linechart made the units selectors disappear. ([OW-12370](https://oliasoft.atlassian.net/browse/OW-12370))

## 3.2.4

- improve axis tick label number formatting (upstream fixes from units package) ([OW-12287](https://oliasoft.atlassian.net/browse/OW-12287))

## 3.2.3

- Updated zoom/pan button style to match other buttons [OW-11866](https://oliasoft.atlassian.net/browse/OW-11866)

## 3.2.2

- Added option to portal controls outside of chart ([OW-11865](https://oliasoft.atlassian.net/browse/OW-11865))

## 3.2.1

- Fix "ghost"-axis bug in multi-axis charts [OW-12061](https://oliasoft.atlassian.net/browse/OW-12061)

## 3.2.0

- Added sourcemap for building the package ([OW-12175](https://oliasoft.atlassian.net/browse/OW-12175))

## 3.1.0

- Improve axis tick number formatting ([OW-9589](https://oliasoft.atlassian.net/browse/OW-9589))

## 3.0.4

- Updated UI Library to fix custom SVG handling ([OW-12065](https://oliasoft.atlassian.net/browse/OW-12065))

## 3.0.3

- add axis label formatting test case stories

## 3.0.2

- Fix `dependencies` and `peerDependencies` ([OW-12046](https://oliasoft.atlassian.net/browse/OW-12046))

## 3.0.1

- upgrade version of `react-ui-library` to resolve a `NumberInput` bug ([OW-12040](https://oliasoft.atlassian.net/browse/OW-12040))

## 3.0.0

- Switched over to Vite and Vitest.
  ([OW-11983](https://oliasoft.atlassian.net/browse/OW-11983))

## 2.17.6

- Fix `NumberInput` bug that prevented users typing decimal values with trailing zeros in charts controls
  ([OW-11920](https://oliasoft.atlassian.net/browse/OW-11920))

## 2.17.5

- Support disabling `usePointStyle` to show dashed lines in legend ([OW-11786](https://oliasoft.atlassian.net/browse/OW-11786))

## 2.17.4

- Use new `<NumberInput>` component from GUI Library in charts controls, to simplify validation ([OW-11773](https://oliasoft.atlassian.net/browse/OW-11773))

## 2.17.3

- Replace legacy internal implementation of `isEqualWithTolerance` with `isCloseTo` from units package ([OW-11767](https://oliasoft.atlassian.net/browse/OW-11767))

## 2.17.2

- Fix release CI/CD pipeline ([OW-11691](https://oliasoft.atlassian.net/browse/OW-11691))

## 2.17.1

- Added debounce to prevent maximum call stack size exceeded

## 2.17.0

- Added initialize logic, for improve translations

## 2.16.0

- Added common chart area text plugin

## 2.15.0

- Added translation, by provider handled([OW-11237](https://oliasoft.atlassian.net/browse/OW-11237))

## 2.14.1

- Fix save initAxesRange when dataset changed by parent component([OW-11332](https://oliasoft.atlassian.net/browse/OW-11332))

## 2.14.0

- Refactored line chart, for better performance, readable and reduce not needed re-renders

## 2.13.5

- Fixed reset range when subComponent update data, prevent extra re-renders when range have same deep values but different reference

## 2.13.4

- Fixed legend state

## 2.13.3

- Added story for point styling to support ([OW-11107](https://oliasoft.atlassian.net/browse/OW-11107))

## 2.13.2

- Changed default value for zoom to true

## 2.13.1

- Text label for drag options trigger, to make current behaviour more obvious ([OW-11102](https://oliasoft.atlassian.net/browse/OW-11102))

## 2.13.0

- Added feature to save state to localStorage and clean when expired, fixed panEnable(state) bug.

## 2.12.0

- Add unit selector and depth selector to axes options ([OW-9496](https://oliasoft.atlassian.net/browse/OW-9496))

## 2.11.3

- Submit Axes option on keyboard Enter ([OW-10995](https://oliasoft.atlassian.net/browse/OW-10995))

## 2.11.2

- remove number handling functions that were duplicated from units package
  ([OW-11062](https://oliasoft.atlassian.net/browse/OW-11062))

## 2.11.1

- updated icon import

## 2.11.0

- added `dragData` feature ([chartjs-plugin-dragdata](https://github.com/chrispahm/chartjs-plugin-dragdata))

## 2.10.2

- remove dead `less-vars-to-js` package ([OW-11017](https://oliasoft.atlassian.net/browse/OW-11017))

## 2.10.1

- Use `peerDependencies` for common versions of shared packages like `units` and `react-ui-library` so reduce double
  installs and conflicting versions in parent apps ([OW-10974](https://oliasoft.atlassian.net/browse/OW-10974))

## 2.10.0

- Switch to standard `round()` function from units repo [OW-10972](https://oliasoft.atlassian.net/browse/OW-10972)

## 2.9.4

- Updated version of UI Library

## 2.9.3

- Add @oliasoft-open-source/units package and update pase method in BarChart ([OW-10670](https://oliasoft.atlassian.net/browse/OW-10670))

## 2.9.2

- Fix missing axis scale when in dataset is one point with negative values ([OW-10616](https://oliasoft.atlassian.net/browse/OW-10616))

## 2.9.1

- Fix decimal values in BarChart in tooltips for locales with comma decimal ([OW-10632](https://oliasoft.atlassian.net/browse/OW-10632))

## 2.9.0

- Add double-click event on chart area, change reset chart ranges from one click to double click ([OW-10412](https://oliasoft.atlassian.net/browse/OW-10412))

## 2.8.0

- Simplify dist-tag strategy for beta version releases (to reduce cleanup effort) ([OW-10003](https://oliasoft.atlassian.net/browse/OW-10003))

## 2.7.0

- Add optional CI pipeline that automates publishing of beta releases ([OW-10003](https://oliasoft.atlassian.net/browse/OW-10003))

## 2.6.2

- Fix asis range update correctly ([OW-10534](https://oliasoft.atlassian.net/browse/OW-10534), and [OW-10457](https://oliasoft.atlassian.net/browse/OW-10457))

## 2.6.1

- Added Box and Ellipse annotations ([OW-10519](https://oliasoft.atlassian.net/browse/OW-10519))

## 2.6.0

- Add support for optional `autoAxisPadding` prop, which autoscales 5% padding around data ([OW-10398](https://oliasoft.atlassian.net/browse/OW-10398))
- Fix bug in default scales when all data points similar values ([OW-4327](https://oliasoft.atlassian.net/browse/OW-4327))

## 2.5.27

- Add guard to useEffect in line-chart

## 2.5.26

- Add guard to getLineChartAxis method

## 2.5.25

- Fix axis ranges updating when new chart data and ranges is passed

## 2.5.24

- Add performance test case

## 2.5.23

- Patch minor regression in axis limits introduced in 2.5.22

## 2.5.22

- Improve UX and input validation for axes range inputs ([OW-10305](https://oliasoft.atlassian.net/browse/OW-10305))

## 2.5.21

- Revert changes from `2.5.12` / [OW-10320](https://oliasoft.atlassian.net/browse/OW-10320)
- Changing thousands separators broke decimal separators in some locales
- Resolves [OW-10320](https://oliasoft.atlassian.net/browse/OW-10320)

## 2.5.20

- Minor refactor (code cleanup)

## 2.5.19

- Hide irregular major axis ticks ([OW-10088](https://oliasoft.atlassian.net/browse/OW-10088))

## 2.5.18

- Fixed minor gridlines when last major tick does not reach axis bounds ([OW-10296](https://oliasoft.atlassian.net/browse/OW-10296))

## 2.5.17

- Added some (realistic) test case stories from our app

## 2.5.16

- Remove test artefacts from published package

## 2.5.15

- Changed GUI library version to `^3.1`

## 2.5.14

- Sync axis labels in chart Controls with props

## 2.5.13

- fixed custom range inputs reset, multi-axis range placeholder values missing

## 2.5.12

- change thousand separators in axis label from comma to space

## 2.5.11

- Allow headerComponent and chart title to coexist

## 2.5.10

- Fix zoom / pan bug at < 0.1 scale on charts

## 2.5.9

- Fix validation for two and more decimal places in charts settings

## 2.5.8

- Disable tooltip animations in `performanceMode`

## 2.5.7

- Allow stretchy headerComponent in controls

## 2.5.6

- Removed resizeDelay, fixed double data labels set position

## 2.5.5

- Removed empty label annotations

## 2.5.4

- Fixed onPointHover event

## 2.5.3

- Fixed datalabels position, set labels position inside chart area

## 2.5.2

- Fix crashes when type a non number into the axis range - add validation in getLinechartAxis method

## 2.5.1

- Added resize delay for performance

## 2.5.0

- Separate controls for points/lines, axes options, legend, download
- Standard control for table
- Box zoom

## 2.4.6

- Fix Multiple X Axes example in LineChart

## 2.4.5

- Fix zooming issue from OW-9960

## 2.4.4

- Updated GUI Library version

## 2.4.3

- Fix crashes when type a non-number into the axis range OW-9924

## 2.4.2

- Square aspect ratio charts can fill height/width

## 2.4.1

- Remove dependency on http-server (moved to dev dependencies)

## 2.4.0

- Switch package manager from NPM to Yarn

## 2.3.4

- RollBack changes regarding OW-4327

## 2.3.3

- restrict suggestedMin/Max by X axe OW-4327

## 2.3.2

- added default values for axis ticks (suggestedMin/Max) OW-4327

## 2.3.0

- Text annotations in lin
