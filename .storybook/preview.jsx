import React from 'react';
import { useDarkMode } from 'storybook-dark-mode';
import { themes } from '@storybook/theming';
import { DocsContainer } from '@storybook/addon-docs';
import '@oliasoft-open-source/react-ui-library/dist/global.css';
import './storybook.less';

/**
 * Dark mode for GUI Library components (i.e. Controls)
 */
export const decorators = [
  (Story) => {
    document.documentElement.setAttribute(
      'data-theme',
      useDarkMode() ? 'dark' : 'default',
    );
    return <Story />;
  },
];

export const parameters = {
  /**
   * Updated fix for S7 dark mode docs from https://github.com/hipstersmoothie/storybook-dark-mode/issues/180
   */
  docs: {
    container: (props) => {
      const isDark = useDarkMode();
      const currentProps = { ...props };
      currentProps.theme = isDark ? themes.dark : themes.light;
      document.documentElement.setAttribute(
        'data-theme',
        isDark ? 'dark' : 'default',
      );
      return React.createElement(DocsContainer, currentProps);
    },
  },
  darkMode: {
    // Apply dark mode to preview iframe https://storybook.js.org/addons/storybook-dark-mode
    stylePreview: true,
  },
};
