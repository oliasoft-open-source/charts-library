const {mergeConfig} = require('vite');

module.exports = {
  stories: ['../src/components/**/*.stories.@(js|jsx|ts|tsx|mdx)'],
  addons: ['@storybook/addon-actions', {
    name: '@storybook/addon-docs',
    options: {
      sourceLoaderOptions: {
        injectStoryParameters: false
      }
    }
  }, 'storybook-dark-mode', '@storybook/addon-mdx-gfm'],
  framework: {
    name: '@storybook/react-vite',
    options: {}
  },
  features: {
    "storyStoreV7": true
  },
  async viteFinal(config) {
    // Merge custom configuration into the default config
    return mergeConfig(config, {
      base: '',
      build: {
        publicDir: false,
        sourcemap: false,
      },
      optimizeDeps: {
        include: ['storybook-dark-mode'],
      },
    });
  },
  docs: {
    autodocs: true
  }
};
