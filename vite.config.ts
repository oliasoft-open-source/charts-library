import react from '@vitejs/plugin-react-swc';
import cssInjectedByJsPlugin from 'vite-plugin-css-injected-by-js';
import peerDeepExternal from 'rollup-plugin-peer-deps-external';
import tsconfigPaths from 'vite-tsconfig-paths';
import dts from 'vite-plugin-dts';
import svgr from 'vite-plugin-svgr';

// https://vitejs.dev/config/
export default () => {
  const root = process.cwd();

  return {
    base: '/',
    root,
    assetsInclude: ['**/*.md'],
    define: {
      globalThis: 'globalThis',
    },
    publicDir: false,
    emptyOutDir: true,
    server: {
      port: 9001,
      host: true,
    },
    build: {
      outDir: 'dist',
      minify: false,
      sourcemap: true,
      lib: {
        entry: 'index.js',
        formats: ['es'],
        fileName: () => 'index.js',
      },
      rollupOptions: {
        plugins: [peerDeepExternal()],
      },
    },
    plugins: [
      react(),
      dts({ insertTypesEntry: true }),
      tsconfigPaths(),
      cssInjectedByJsPlugin({ topExecutionPriority: false }),
      svgr({ include: '**/*.svg' }),
    ],
  };
};
