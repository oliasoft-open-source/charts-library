import React, { useState } from 'react';
import { Button } from '@oliasoft-open-source/react-ui-library';
import { LineChart } from './line-chart';

const basicChart = {
  data: {
    datasets: [
      {
        label: 'Dataset 1',
        data: [
          {
            x: 0,
            y: 0,
          },
          {
            x: 5,
            y: 25,
          },
        ],
      },
    ],
  },
  options: {
    title: 'Chart title',
    chartStyling: {
      height: '100%',
    },
  },
};

export default {
  title: 'LineChart / Test Cases',
  component: LineChart,
  args: {
    chart: basicChart,
  },
};

export const TestLegendDropzoneResizing = () => {
  const [isSmall, setIsSmall] = useState(false);
  return (
    <>
      <Button onClick={() => setIsSmall(!isSmall)} label="Toggle size" />
      <div
        style={{
          height: isSmall ? 300 : null,
          width: isSmall ? 300 : null,
          padding: 'var(--padding)',
          overflow: 'auto',
        }}
      >
        <LineChart chart={basicChart} />
      </div>
    </>
  );
};

export const PartialRangeWithSimilarValues = () => {
  return (
    <LineChart
      chart={{
        data: {
          datasets: [
            {
              label: 'Duration %',
              lineTension: '0',
              pointHitRadius: 10,
              borderColor: '#dc3912',
              pointBackgroundColor: '#dc3912',
              borderWidth: 2,
              pointRadius: 4,
              data: [
                {
                  x: 3,
                  y: 0,
                },
                {
                  x: 3,
                  y: 100,
                },
              ],
              yAxisID: 'y2',
            },
          ],
        },
        options: {
          direction: 'vertical',
          axes: {
            x: [
              {
                label: 'Duration (TempSim long) [d]',
                position: 'bottom',
              },
            ],
            y: [
              {
                label: 'Probability',
              },
              {
                label: 'Percentage [%]',
              },
            ],
          },
          annotations: {
            annotationsData: [
              {
                annotationAxis: 'y2',
                color: 'darkgrey',
                value: 16,
              },
              {
                displayDragCoordinates: false,
                color: '#ff9900',
                label: 'P16',
                dragAxis: 'y',
                enableDrag: true,
                resizable: false,
                type: 'box',
                xMax: 3,
                xMin: 3,
                yMax: 17,
                yMin: 15,
                yScaleID: 'y2',
              },
              {
                annotationAxis: 'y2',
                color: 'darkgrey',
                value: 66,
              },
              {
                displayDragCoordinates: false,
                color: '#109618',
                label: 'P66',
                dragAxis: 'y',
                enableDrag: true,
                resizable: false,
                type: 'box',
                xMax: 3,
                xMin: 3,
                yMax: 67,
                yMin: 65,
                yScaleID: 'y2',
              },
              {
                annotationAxis: 'y2',
                color: 'darkgrey',
                value: 78,
              },
              {
                displayDragCoordinates: false,
                color: '#990099',
                label: 'P78',
                dragAxis: 'y',
                enableDrag: true,
                resizable: false,
                type: 'box',
                xMax: 3,
                xMin: 3,
                yMax: 79,
                yMin: 77,
                yScaleID: 'y2',
              },
              {
                annotationAxis: 'y2',
                color: 'darkgrey',
                value: 90,
              },
              {
                displayDragCoordinates: false,
                color: '#0099c6',
                label: 'P90',
                dragAxis: 'y',
                enableDrag: true,
                resizable: false,
                type: 'box',
                xMax: 3,
                xMin: 3,
                yMax: 91,
                yMin: 89,
                yScaleID: 'y2',
              },
            ],
            enableDragAnnotation: true,
            showAnnotations: true,
          },
          additionalAxesOptions: {
            range: {
              x: {
                min: 3,
              },
            },
            chartScaleType: 'linear',
            reverse: false,
          },
          graph: {
            showMinorGridlines: true,
          },
          chartStyling: {
            height: '100%',
          },
          chartOptions: {
            showPoints: false,
            enableDragAnnotation: true,
          },
        },
      }}
    />
  );
};
