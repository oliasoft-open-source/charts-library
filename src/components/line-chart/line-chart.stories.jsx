import React, { useCallback, useMemo, useRef, useState } from 'react';
import {
  ButtonGroup,
  CheckBox,
  Divider,
  Drawer,
  Flex,
  FormRow,
  Menu,
  Message,
  Select,
  Slider,
  Spacer,
  Table,
  Text,
  Toggle,
} from '@oliasoft-open-source/react-ui-library';
import { sample } from 'lodash';
import { unum } from '@oliasoft-open-source/units';
import { DEFAULT_POINT_RADIUS } from 'components/line-chart/constants/line-chart-consts';
import { getCustomLegendPlugin } from '../common/helpers/get-custom-legend-plugin-example';
import { LineChart } from './line-chart';
import appTestCase1 from './__tests__/test-data/app-test-case-1.json';
import appTestCase2 from './__tests__/test-data/app-test-case-2.json';
import appTestCase3 from './__tests__/test-data/app-test-case-3.json';
import appTestCase4 from './__tests__/test-data/app-test-case-4.json';
import appTestCase5 from './__tests__/test-data/app-test-case-5.json';
import appTestCase6 from './__tests__/test-data/app-test-case-6.json';
import { defaultTranslations } from './constants/default-translations';
import { initializeLineChart } from './initialize/initialize-line-chart';
import { DEFAULT_FONT_FAMILY } from '../common/helpers/chart-consts';

const pointStyles = ['triangle', 'rect', 'rectRot', 'circle'];

const dataset1 = {
  label: 'Dataset 1',
  data: [
    {
      x: 0,
      y: 0,
    },
    {
      x: 5,
      y: 25,
    },
    {
      x: 10,
      y: 60,
    },
    {
      x: 15,
      y: 90,
    },
    {
      x: 20,
      y: 120,
    },
    {
      x: 25,
      y: 150,
    },
  ],
};

const datasetLabelled1 = {
  ...dataset1,
  data: dataset1.data.map((item) => ({ ...item, label: ['Label'] })),
};

const dataset2 = {
  label: 'Dataset 2',
  data: [
    {
      x: 0,
      y: 0,
    },
    {
      x: 3,
      y: 15,
    },
    {
      x: 6,
      y: 30,
    },
    {
      x: 9,
      y: 60,
    },
    {
      x: 12,
      y: 120,
    },
    {
      x: 15,
      y: 240,
    },
  ],
};

const dataset3 = {
  label: 'Dataset 3',
  data: [
    {
      x: 1,
      y: 10,
    },
    {
      x: 4,
      y: 20,
    },
    {
      x: 7,
      y: 30,
    },
    {
      x: 10,
      y: 40,
    },
    {
      x: 13,
      y: 50,
    },
    {
      x: 16,
      y: 60,
    },
  ],
};

const datasetMissingValues = {
  label: 'Dataset with missing (null) values',
  data: [
    {
      x: 0,
      y: 0,
    },
    {
      x: 3,
      y: 0,
    },
    null, //reproduce OW-9855
    undefined, //reproduce OW-9855
    {
      x: 3,
      y: 30,
    },
    {
      x: null,
      y: 60,
    },
    {
      x: 12,
      y: 120,
    },
    {
      x: 15,
      y: 240,
    },
  ],
};

const datasetLabelled2 = {
  ...dataset2,
  data: dataset2.data.map((item) => ({ ...item, label: ['Label'] })),
};

const basicChart = {
  data: {
    datasets: [dataset1, dataset2],
  },
  options: {
    title: 'Chart title',
    chartStyling: {
      height: 400,
    },
  },
};

export default {
  title: 'LineChart',
  component: LineChart,
  args: {
    chart: basicChart,
  },
};

const Template = (args) => {
  return (
    <LineChart
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
    />
  );
};

const customLegendContainerID = 'custom-legend-container';

const TemplateWithCustomLegendContainer = (args) => {
  return (
    <>
      <LineChart
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...args}
      />
      <div id={customLegendContainerID}></div>
    </>
  );
};

export const Default = Template.bind({});

export const HideDatasetInLegend = Template.bind({});
HideDatasetInLegend.args = {
  chart: {
    data: {
      datasets: [
        {
          ...dataset1,
          hideLegend: true,
        },
        {
          ...dataset2,
        },
      ],
    },
  },
};

export const OnePoint = Template.bind({});
OnePoint.args = {
  chart: {
    data: {
      datasets: [
        {
          label: 'Dataset 1',
          data: [
            {
              x: 0.5,
              y: -1.8,
            },
          ],
        },
      ],
    },
  },
};

export const PointStyles = Template.bind({});
PointStyles.args = {
  chart: {
    ...basicChart,
    data: {
      datasets: pointStyles.map((shape, index) => ({
        label: shape,
        pointStyle: shape,
        pointRadius: 4,
        pointHoverRadius: 5,
        data: Array.from({ length: 5 }).map((_, i) => ({
          x: i,
          y: i * 10 - index * 10,
        })),
      })),
    },
  },
};

export const LineStyles = Template.bind({});
LineStyles.args = {
  chart: {
    ...basicChart,
    data: {
      datasets: [
        [4, 4],
        [8, 4],
        [12, 4],
        [16, 4],
      ].map((borderDash, index) => ({
        borderDash,
        label: 'Label',
        data: Array.from({ length: 5 }).map((_, i) => ({
          x: i,
          y: i * 10 - index * 10,
        })),
      })),
    },
    options: {
      ...basicChart.options,
      legend: {
        usePointStyle: false,
      },
    },
  },
};

export const FillContainer = Template.bind({});
FillContainer.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      chartStyling: {
        ...basicChart.options.chartStyling,
        height: '100%',
      },
    },
  },
};
FillContainer.decorators = [
  (Story) => (
    <div style={{ height: 400, width: 500 }}>
      <Story />
    </div>
  ),
];

export const AutoAxisPadding = Template.bind({});
AutoAxisPadding.args = {
  chart: {
    ...basicChart,
    data: {
      datasets: [
        dataset1,
        {
          label: 'Negative values',
          data: [
            {
              x: 0,
              y: 0,
            },
            {
              x: -3,
              y: -15,
            },
            {
              x: -6,
              y: -30,
            },
            {
              x: -9,
              y: -60,
            },
            {
              x: -12,
              y: -120,
            },
            {
              x: -15,
              y: -240,
            },
          ],
        },
      ],
    },
    options: {
      ...basicChart.options,
      title: 'Optional autoAxisPadding property adds 5% padding around data',
      additionalAxesOptions: {
        autoAxisPadding: true,
      },
    },
  },
};

export const NoTitle = Template.bind({});
NoTitle.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      title: undefined,
    },
  },
};

export const DataGaps = Template.bind({});
DataGaps.args = {
  chart: {
    ...basicChart,
    data: {
      datasets: [datasetMissingValues],
    },
  },
};

export const TooltipUnits = Template.bind({});
TooltipUnits.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      axes: {
        x: [
          {
            label: 'x-axis title here [m]',
          },
        ],
        y: [
          {
            label: 'y-axis title here [bar]',
          },
        ],
      },
    },
  },
};

export const MinorGridlines = Template.bind({});
MinorGridlines.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      graph: {
        showMinorGridlines: true,
      },
    },
  },
};

export const AxesLabels = Template.bind({});
AxesLabels.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      axes: {
        x: [
          {
            label: 'x-axis title here',
          },
        ],
        y: [
          {
            label: 'y-axis title here',
          },
        ],
      },
    },
  },
};

export const MultipleYAxes = Template.bind({});
MultipleYAxes.args = {
  chart: {
    data: {
      datasets: [
        {
          label: 'Rainfall',
          data: [
            {
              x: 1,
              y: 49.9,
            },
            {
              x: 2,
              y: 71.5,
            },
            {
              x: 3,
              y: 106.4,
            },
            {
              x: 4,
              y: 129.2,
            },
            {
              x: 5,
              y: 144.0,
            },
            {
              x: 6,
              y: 176.0,
            },
            {
              x: 7,
              y: 135.6,
            },
            {
              x: 8,
              y: 148.5,
            },
            {
              x: 9,
              y: 216.4,
            },
            {
              x: 10,
              y: 194.1,
            },
            {
              x: 11,
              y: 95.6,
            },
            {
              x: 12,
              y: 54.4,
            },
          ],
        },
        {
          label: 'Temperature',
          data: [
            {
              x: 1,
              y: 7.0,
            },
            {
              x: 2,
              y: 6.9,
            },
            {
              x: 3,
              y: 9.5,
            },
            {
              x: 4,
              y: 14.5,
            },
            {
              x: 5,
              y: 18.2,
            },
            {
              x: 6,
              y: 21.5,
            },
            {
              x: 7,
              y: 25.2,
            },
            {
              x: 8,
              y: 26.5,
            },
            {
              x: 9,
              y: 23.3,
            },
            {
              x: 10,
              y: 18.3,
            },
            {
              x: 11,
              y: 13.9,
            },
            {
              x: 12,
              y: 9.6,
            },
          ],
          yAxisID: 'y2',
        },
      ],
    },
    options: {
      title: 'Multiple y-axes',
      additionalAxesOptions: {
        range: {
          x: {
            min: 0,
            max: 12,
          },
          y: {
            min: 40,
            max: 220,
          },
          y2: {
            min: 5,
            max: 30,
          },
        },
      },
      chartStyling: {
        height: 400,
      },
      axes: {
        x: [
          {
            label: 'Month',
          },
        ],
        y: [
          {
            label: 'Rainfall [mm]',
            position: 'left',
          },
          {
            label: 'Temperature [C]',
            position: 'right',
          },
        ],
      },
    },
  },
};

export const MultipleXAxes = Template.bind({});
MultipleXAxes.args = {
  chart: {
    data: {
      datasets: [
        dataset1,
        {
          ...dataset2,
          xAxisID: 'x2',
        },
      ],
    },
    options: {
      title: 'Chart title',
      chartStyling: {
        height: 400,
      },
      axes: {
        x: [
          {
            label: 'The X',
            position: 'bottom',
          },
          {
            label: 'The X 2',
            position: 'top',
          },
        ],
      },
    },
  },
};

export const ReversedYAxis = Template.bind({});
ReversedYAxis.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      additionalAxesOptions: {
        reverse: true,
      },
    },
  },
};

export const LogarithmicScale = Template.bind({});
LogarithmicScale.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      additionalAxesOptions: {
        chartScaleType: 'logarithmic',
      },
    },
  },
};

export const PresetRange = Template.bind({});
PresetRange.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      additionalAxesOptions: {
        range: {
          x: {
            min: -10,
            max: 40,
          },
          y: {
            min: -10,
            max: 180,
          },
        },
      },
    },
  },
};

export const DataLabels = Template.bind({});
DataLabels.args = {
  chart: {
    ...basicChart,
    data: {
      datasets: [datasetLabelled1, datasetLabelled2],
    },
    options: {
      ...basicChart.options,
      graph: {
        showDataLabels: true,
      },
    },
  },
};

export const DataLabelsInTooltips = Template.bind({});
DataLabelsInTooltips.args = {
  chart: {
    ...basicChart,
    data: {
      datasets: [datasetLabelled1, datasetLabelled2],
    },
    options: {
      ...basicChart.options,
      tooltip: {
        showLabelsInTooltips: true,
      },
    },
  },
};

export const LegendOnRight = Template.bind({});
LegendOnRight.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      legend: {
        position: 'bottom-right',
      },
    },
  },
};

export const HideLegend = Template.bind({});
HideLegend.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      legend: {
        display: false,
      },
    },
  },
};

export const Annotations = Template.bind({});
Annotations.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      legend: {
        display: false,
      },
      annotations: {
        showAnnotations: true,
        labelAnnotation: {
          showLabel: true,
          text: 'Pipe Rating is shown without temperature derating',
        },
        annotationsData: [
          {
            annotationAxis: 'y',
            label: 'Annotation on y-axis',
            value: 50,
          },
          {
            annotationAxis: 'x',
            label: 'Annotation on x-axis',
            value: 10,
          },
          {
            annotationAxis: 'y',
            label: 'Diagonal annotation',
            value: 20,
            endValue: 170,
          },
        ],
      },
    },
  },
};

export const AnnotationsBox = Template.bind({});
AnnotationsBox.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      annotations: {
        showAnnotations: true,
        annotationsData: [
          {
            type: 'box',
            yMin: 0,
            yMax: 70,
            color: 'rgba(200, 20, 20, 0.15)',
            adjustScaleRange: false,
            label: 'BAD',
          },
          {
            type: 'box',
            yMin: 180,
            yMax: 250,
            color: 'rgba(20, 200, 20, 0.15)',
            adjustScaleRange: false,
            label: 'GOOD',
          },
        ],
      },
    },
  },
};

export const AnnotationsEllipse = Template.bind({});
AnnotationsEllipse.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      annotations: {
        showAnnotations: true,
        annotationsData: [
          {
            type: 'ellipse',
            xMin: 17.5,
            xMax: 30,
            yMin: -50,
            yMax: 300,
            color: 'rgba(200, 20, 20, 0.15)',
            adjustScaleRange: false,
          },
        ],
      },
    },
  },
};

export const AnnotationsPoint = Template.bind({});
AnnotationsPoint.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      annotations: {
        showAnnotations: true,
        annotationsData: [
          {
            type: 'point',
            xValue: 12,
            yValue: 120,
            color: 'rgba(200, 20, 20, 0.25)',
            radius: 16,
          },
        ],
      },
    },
  },
};

export const AnnotationsInLegend = Template.bind({});
AnnotationsInLegend.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      annotations: {
        controlAnnotation: true,
        showAnnotations: true,
        annotationsData: [
          {
            type: 'line',
            annotationAxis: 'x',
            label: 'Line Annotation',
            value: 20,
          },
          {
            type: 'ellipse',
            xMin: 17.5,
            xMax: 30,
            yMin: -50,
            yMax: 300,
            color: 'rgba(200, 20, 20, 0.15)',
            adjustScaleRange: false,
            label: 'Ellipse Annotation',
          },
          {
            type: 'box',
            yMin: 180,
            yMax: 250,
            color: 'rgba(20, 200, 20, 0.15)',
            adjustScaleRange: false,
            label: 'Box Annotation',
          },
        ],
      },
    },
  },
};

export const CustomLegend = TemplateWithCustomLegendContainer.bind({});
CustomLegend.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      title: 'Custom HTML legend',
      legend: {
        customLegend: {
          customLegendPlugin: getCustomLegendPlugin(customLegendContainerID),
          customLegendContainerID,
        },
      },
    },
  },
};

export const Animation = Template.bind({});
Animation.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      chartStyling: {
        ...basicChart.options.chartStyling,
        performanceMode: false,
      },
    },
  },
};

export const SquareAspectRatio = Template.bind({});
SquareAspectRatio.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      title: 'Square aspect ratio',
      chartStyling: {
        squareAspectRatio: true,
      },
    },
  },
};

export const HeaderComponentNoTitle = (args) => {
  const options = ['Category A', 'Category B', 'Category C'];

  const suboptions = ['Chart 1', 'Chart 2', 'Chart 3'];

  const charts = options.map((option) => ({
    label: option,
    options: suboptions.map((suboption) => ({
      label: suboption,
      options: {
        label: `${option} - ${suboption}`,
      },
    })),
  }));

  const [selectedChartTitle, setSelectedChartTitle] = useState(
    `${options[0]} - ${suboptions[0]}`,
  );

  const chart = {
    ...basicChart,
    options: { ...basicChart.options, title: null },
  };

  return (
    <LineChart
      chart={chart}
      headerComponent={
        <Flex gap="10px" alignItems="center">
          <Text bold>
            <Menu
              menu={{
                label: selectedChartTitle,
                sections: charts.map((option) => ({
                  type: 'Menu',
                  trigger: 'Text',
                  menu: {
                    label: option.label,
                    trigger: 'Text',
                    sections: option.options.map((suboption) => ({
                      label: suboption.label,
                      type: 'Option',
                      onClick: () =>
                        setSelectedChartTitle(
                          `${option.label} - ${suboption.label}`,
                        ),
                    })),
                  },
                })),
                trigger: 'Text',
                small: true,
              }}
            />
          </Text>
          <Toggle label="Toggle" onChange={() => {}} noMargin />
        </Flex>
      }
    />
  );
};
HeaderComponentNoTitle.parameters = {
  docs: {
    description: {
      story:
        'If no chart title is set, a `headerComponent` can take its place.',
    },
  },
};

const testComponent = (
  <div
    style={{
      flexGrow: 1,
      minWidth: 280,
      margin: '-9px 0',
      display: 'flex',
      alignItems: 'center',
    }}
  >
    <Select
      options={['Aardvarks', 'Kangaroos']}
      value="Aardvarks"
      small
      width="90px"
      autoLayerWidth
    />
    <Slider max={100} min={0} onChange={() => {}} showArrows value={50} />
    <Text>180s</Text>
  </div>
);

export const HeaderComponentWithTitle = Template.bind({});
HeaderComponentWithTitle.args = {
  headerComponent: testComponent,
};
HeaderComponentWithTitle.parameters = {
  docs: {
    description: {
      story:
        "If a chart title is set, the `headerComponent` will be displayed to the right of it. Setting a `minWidth` on the `headerComponent` will make sure it isn't squashed.",
    },
  },
};
export const SubheaderComponent = () => {
  const [dataset, setDataset] = useState(dataset1);
  const { data } = dataset;
  const [selectedAxes, setSelectedAxes] = useState('x');
  const maxSliderValue = useMemo(
    () => Math.max(...data.map((el) => el[selectedAxes])),
    [data, selectedAxes],
  );
  const [sliderValue, setSliderValue] = useState(maxSliderValue / 2);
  const prevSliderValueRef = useRef(null);
  const pointIndexRef = useRef(0);

  const onSliderChange = useCallback(
    (value) => {
      setSliderValue(value);
      if (prevSliderValueRef.current !== null) {
        const newData = [...data];
        const sign = value >= prevSliderValueRef.current ? '+' : '-';
        // Updating the specific point in a round-robin fashion
        const index = pointIndexRef.current % newData.length;
        newData[index][selectedAxes] += parseInt(`${sign}${1}`);
        setDataset((prevDataset) => ({ ...prevDataset, data: newData }));
        pointIndexRef.current += 1;
      }
      prevSliderValueRef.current = value;
    },
    [data, selectedAxes],
  );

  return (
    <>
      <div
        style={{
          flexGrow: 1,
          minWidth: 280,
          margin: '-9px 0',
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <Select
          options={['x', 'y']}
          value={selectedAxes}
          small
          width="90px"
          onChange={({ target: { value } }) => setSelectedAxes(value)}
          autoLayerWidth
        />
        <Slider
          max={maxSliderValue}
          min={0}
          onChange={({ target: { value } }) => onSliderChange(value)}
          showArrows
          value={sliderValue}
        />
        <Text>180s</Text>
      </div>
      <LineChart
        chart={{
          data: {
            datasets: [dataset],
          },
          options: {
            additionalAxesOptions: {
              chartScaleType: 'linear',
              range: {
                x: {
                  min: 0,
                  max: 25,
                },
                y: {
                  min: 0,
                  max: 150,
                },
              },
            },
            chartStyling: {
              height: 400,
            },
            axes: {
              x: [
                {
                  label: 'X',
                },
              ],
              y: [
                {
                  label: 'Y',
                },
              ],
            },
          },
        }}
      />
    </>
  );
};

const table = {
  headers: [
    {
      cells: [
        { value: 'Name' },
        { value: 'Weight' },
        { value: 'Energy' },
        { value: 'Origin' },
      ],
    },
  ],
  rows: [
    {
      cells: [
        { value: 'Brown rice' },
        { value: 100 },
        { value: 361 },
        { value: 'Vietnam' },
      ],
    },
    {
      cells: [
        { value: 'Buckwheat' },
        { value: 50 },
        { value: 358 },
        { value: 'Poland' },
      ],
    },
    {
      cells: [
        { value: 'Couscous' },
        { value: 10 },
        { value: 368 },
        { value: 'France' },
      ],
    },
  ],
};
SubheaderComponent.parameters = {
  docs: {
    description: {
      story:
        'This will be displayed between the title/controls and the chart itself.',
    },
  },
};

export const ControlsPortal = (args) => {
  const controlsPortalId = 'controls-portal-container';
  return (
    <>
      <div
        style={{
          display: 'flex',
          border: '1px solid red',
        }}
      >
        <div>This div is outside the LineChart component</div>
        <div style={{ flex: 1 }} id={controlsPortalId} />
      </div>

      <LineChart
        chart={{
          ...basicChart,
          controlsPortalId,
          options: {
            ...basicChart.options,
            title: null,
          },
        }}
      />
    </>
  );
};

export const WithTable = (args) => {
  return <LineChart chart={basicChart} table={<Table table={table} />} />;
};

export const SquareAspectRatioFillContainer = Template.bind({});
SquareAspectRatioFillContainer.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      title: 'Square aspect ratio',
      chartStyling: {
        squareAspectRatio: true,
        height: '100%',
      },
    },
  },
};
SquareAspectRatioFillContainer.decorators = [
  (Story) => (
    <div style={{ display: 'flex', height: 400, width: 600 }}>
      <Story />
    </div>
  ),
];

export const OnPointHover = Template.bind({});
OnPointHover.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      interactions: {
        onHover: (evt, datasetIndex, pointIndex, datasets) => {
          console.log({ evt });
          console.log(datasets[datasetIndex].data[pointIndex]);
        },
        onUnhover: () => {
          console.log('No point hovered');
        },
      },
    },
  },
};

export const ZeroValues = Template.bind({});
ZeroValues.args = {
  chart: {
    data: {
      datasets: [
        {
          label: 'Values close to 0',
          data: [
            {
              x: 3.552713678800501e-15,
              y: 100,
            },
            {
              x: 2.842170943040401e-14,
              y: 200,
            },
            {
              x: 0,
              y: 300,
            },
            {
              x: 5.84217033040401e-14,
              y: 400,
            },
          ],
        },
      ],
    },
    options: {
      ...basicChart.options,
      title: 'Values close to 0 (OW-4327)',
    },
  },
};

export const SimilarValues = Template.bind({});
SimilarValues.args = {
  chart: {
    data: {
      datasets: [
        {
          label: 'Similar values',
          data: [
            {
              x: 15.599680785028,
              y: 5,
            },
            {
              x: 15.599680785026,
              y: 10,
            },
            {
              x: 15.599680785024,
              y: 15,
            },
            {
              x: 15.599680785022,
              y: 20,
            },
          ],
        },
      ],
    },
    options: {
      ...basicChart.options,
      title: 'Similar values (OW-4327)',
    },
  },
};

/*
  Test case for hiding irregular major axis ticks (OW-10088)
  This is controlled by the includeBounds property which is always set to false
  internally (not exposed, since we want this behaviour at all times)
 */
export const HideIrregularMajorAxisTicks = Template.bind({});
HideIrregularMajorAxisTicks.args = {
  chart: {
    data: {
      datasets: [
        {
          label: 'Hide irregular major axis ticks',
          data: [
            {
              x: 1,
              y: 2.4,
            },
            {
              x: 2,
              y: 3.5,
            },
            {
              x: 3,
              y: 3.9,
            },
            {
              x: 4,
              y: 4.1,
            },
          ],
        },
      ],
    },
    options: {
      ...basicChart.options,
      title: 'Irregular major axis ticks are always hidden (OW-10088)',
      graph: {
        showMinorGridlines: true,
      },
      additionalAxesOptions: {
        range: {
          x: {
            min: 0.6,
            max: 4.2,
          },
          y: {
            min: 0.7,
            max: 4.1,
          },
        },
      },
    },
  },
};

export const TestNumberSeparators = Template.bind({});
TestNumberSeparators.args = {
  chart: {
    data: {
      datasets: [
        {
          label: 'Separators test case',
          data: [
            {
              x: 0.1,
              y: 1000,
            },
            {
              x: 0.2,
              y: 2000,
            },
            {
              x: 0.3,
              y: 3000,
            },
            {
              x: 0.4,
              y: 4000,
            },
          ],
        },
      ],
    },
    options: {
      ...basicChart.options,
      title:
        'Test thousand separates (spaces) and decimal separators (dots) in labels',
    },
  },
};

export const PerformanceTestCase = () => {
  const [timeElapsed, setTimeElapsed] = useState(null);
  const startTimeStamp = performance.now();
  const content = timeElapsed ? `Render time: ${timeElapsed} ms` : '';
  return (
    <>
      <Message
        message={{
          visible: true,
          heading: 'This test case is for regression testing performance',
          content,
        }}
      />
      <Spacer />
      <LineChart
        chart={{
          data: {
            datasets: [
              {
                label: 'Performance',
                data: [...Array(5000).keys()].map((a, i) => ({ x: i, y: i })),
              },
              {
                label: 'Performance 2',
                data: [...Array(5000).keys()].map((a, i) => ({
                  x: i,
                  y: i % 900,
                })),
              },
            ],
          },
          options: {
            title: 'Chart title',
            chartStyling: {
              height: 400,
              performanceMode: false,
            },
            interactions: {
              onAnimationComplete: () => {
                const endTimeStamp = performance.now();
                if (!timeElapsed) {
                  setTimeElapsed(endTimeStamp - startTimeStamp);
                }
              },
            },
          },
        }}
      />
      <Divider />
    </>
  );
};

export const ExtraTestCases = () => {
  return (
    <>
      <Message
        message={{
          visible: true,
          content: 'Extra test cases (realistic examples for regression tests)',
        }}
      />
      <Spacer />
      <LineChart
        chart={{
          ...appTestCase1,
        }}
      />
      <Divider />
      <LineChart
        chart={{
          ...appTestCase2,
        }}
      />
      <Divider />
      <LineChart
        chart={{
          ...appTestCase3,
        }}
      />
      <Divider />
      <LineChart
        chart={{
          ...appTestCase4,
        }}
      />
      <Divider />
      <LineChart
        chart={{
          ...appTestCase5,
        }}
      />
      <Divider />
      <LineChart
        chart={{
          ...appTestCase6,
        }}
      />
    </>
  );
};

export const AxisLabelFormatTestCases = () => {
  return (
    <>
      <Message
        message={{
          visible: true,
          content: 'Test cases for axis label formatting',
        }}
      />
      <Spacer />
      <LineChart
        chart={{
          data: {
            datasets: [
              {
                data: [
                  {
                    x: 1.12345e16,
                    y: 1.12345e16,
                  },
                  {
                    x: 1.22345e16,
                    y: 1.22345e16,
                  },
                  {
                    x: 1.32345e16,
                    y: 1.32345e16,
                  },
                ],
                label: 'Dataset 1',
              },
            ],
          },
          options: {
            chartStyling: {
              height: 400,
            },
            title: 'Large numbers > 1e15 show in scientific notation',
            tooltip: {},
          },
        }}
      />
      <Divider />
      <LineChart
        chart={{
          data: {
            datasets: [
              {
                data: [
                  {
                    x: 1.747693134862315e308,
                    y: 1.747693134862315e308,
                  },
                  {
                    x: 1.757693134862315e308,
                    y: 1.757693134862315e308,
                  },
                  {
                    x: 1.767693134862315e308,
                    y: 1.767693134862315e308,
                  },
                ],
                label: 'Dataset 1',
              },
            ],
          },
          options: {
            chartStyling: {
              height: 400,
            },
            title: 'Extremely large numbers',
            tooltip: {},
          },
        }}
      />
      <Divider />
      <LineChart
        chart={{
          data: {
            datasets: [
              {
                data: [
                  {
                    x: 1.1234e-5,
                    y: 1.1234e-5,
                  },
                  {
                    x: 1.1235e-5,
                    y: 1.1235e-5,
                  },
                  {
                    x: 1.1236e-5,
                    y: 1.1236e-5,
                  },
                ],
                label: 'Dataset 1',
              },
            ],
          },
          options: {
            chartStyling: {
              height: 400,
            },
            title: 'Small numbers < 1e-4 show in scientific notation',
            tooltip: {},
          },
        }}
      />
      <LineChart
        chart={{
          data: {
            datasets: [
              {
                data: [
                  {
                    x: 0.00134567891011,
                    y: 1.1234567891011,
                  },
                  {
                    x: 0.00134567891011,
                    y: 2.1334567891011,
                  },
                  {
                    x: 0.00384567891011,
                    y: 3.1484567891011,
                  },
                ],
                label: 'Dataset 1',
              },
              {
                data: [
                  {
                    x: -0.00004,
                    y: 1,
                  },
                  {
                    x: -0.00005,
                    y: 2,
                  },
                  {
                    x: 0.0,
                    y: 3,
                  },
                ],
                label: 'Zero never has decimal places',
              },
            ],
          },
          options: {
            chartStyling: {
              height: 400,
            },
            title: 'Decimals get rounded (depends on axis range delta)',
            tooltip: {},
          },
        }}
      />
      <Divider />
      <LineChart
        chart={{
          data: {
            datasets: [
              {
                data: [
                  {
                    x: 123456789.12345,
                    y: 123456789,
                  },
                  {
                    x: 223456789.12345,
                    y: 223456789,
                  },
                  {
                    x: 323456789.12345,
                    y: 323456789,
                  },
                ],
                label: 'Dataset 1',
              },
            ],
          },
          options: {
            chartStyling: {
              height: 400,
            },
            title: 'Thousand separators (note decimals are rounded)',
            tooltip: {},
          },
        }}
      />
      <Divider />
      <LineChart
        chart={{
          data: {
            datasets: [
              {
                data: [
                  {
                    x: 1.0,
                    y: 1.0,
                  },
                  {
                    x: 2.0,
                    y: 2.0,
                  },
                  {
                    x: 3.0,
                    y: 3.0,
                  },
                ],
                label: 'Dataset 1',
              },
            ],
          },
          options: {
            chartStyling: {
              height: 400,
            },
            title: 'With custom formatting options',
            tooltip: {},
            axes: {
              x: [
                {
                  label: `X-axis`,
                  position: 'bottom',
                },
              ],
              y: [
                {
                  label: `Left y-axis`,
                },
                {
                  label: `Right y-axis`,
                  position: 'right',
                },
              ],
            },
            scales: {
              x: {
                ticks: {
                  callback: (val) => {
                    return `${val}%`;
                  },
                  color: 'red',
                },
              },
              y1: {
                ticks: {
                  callback: (val) => {
                    return `${val}%`;
                  },
                  color: 'purple',
                },
              },
            },
          },
        }}
      />
      <Divider />
    </>
  );
};

export const DataSetWithToggle = () => {
  const [val, setVal] = useState(1);

  const d1 = {
    data: [
      { x: 0, y: 280 },
      { x: 280, y: 7000 },
    ],
  };
  const d2 = {
    data: [
      { x: 0, y: 0 },
      { x: 4, y: 48 },
    ],
  };
  const range1 = {
    x: { min: 0, max: 285 },
    y: { min: 35, max: 7500 },
  };
  const range2 = {
    x: { min: 0, max: 4 },
    y: { min: 0, max: 48 },
  };

  return (
    <>
      <ButtonGroup
        items={[
          {
            key: 1,
            label: 'Dataset with custom range 1',
          },
          {
            key: 2,
            label: 'Dataset with custom range 2',
          },
        ]}
        onSelected={(ev) => {
          setVal(ev);
        }}
        value={val}
      />

      <LineChart
        chart={{
          data: {
            datasets: val === 1 ? [d1] : [d2],
          },
          options: {
            additionalAxesOptions: {
              range: val === 1 ? range1 : range2,
            },
          },
        }}
      />
    </>
  );
};

export const AxesDepthTypes = () => {
  const [selectedXUnit, setSelectedXUnit] = useState('sg');
  const [selectedYUnit, setSelectedYUnit] = useState('m');
  const [selectedDepthType, setSelectedDepthType] = useState('TVD');

  const defaultPoints = [
    { x: 0, y: 35 },
    { x: 4, y: 600 },
    { x: 3, y: 685 },
    { x: 5, y: 700 },
    { x: 4, y: 800 },
  ];

  return (
    <LineChart
      chart={{
        data: {
          datasets: [
            {
              data: defaultPoints.map((point) => {
                return {
                  x: unum(point.x, selectedXUnit, 'sg'),
                  y: unum(
                    selectedDepthType === 'TVD' ? point.y : point.y + 200,
                    selectedYUnit,
                    'm',
                  ),
                };
              }),
            },
          ],
        },
        options: {
          ...basicChart.options,
          depthType: {
            options: ['MD', 'TVD'],
            selectedDepthType,
            setSelectedDepthType,
          },
          axes: {
            x: [
              {
                label: `Pressure Gradient [${selectedXUnit}]`,
                position: 'bottom',
                unit: {
                  options: ['sg', 'ppg'],
                  selectedUnit: selectedXUnit,
                  setSelectedUnit: setSelectedXUnit,
                },
              },
            ],
            y: [
              {
                label: `Depth (${selectedDepthType}) [${selectedYUnit}]`,
                unit: {
                  options: ['m', 'ft', 'in'],
                  selectedUnit: selectedYUnit,
                  setSelectedUnit: setSelectedYUnit,
                },
              },
              {
                label: `Right Axis`,
                position: 'right',
              },
            ],
          },
        },
      }}
    />
  );
};

export const DragDataChart = () => {
  const [dragDataDatasets, setDragDataDatasets] = useState([
    dataset1,
    dataset2,
  ]);

  return (
    <LineChart
      chart={{
        data: {
          datasets: dragDataDatasets,
        },
        options: {
          title: 'Chart title',
          dragData: {
            enableDragData: true,
            showTooltip: true,
            roundPoints: true,
            onDragStart: (evt, element) => {
              // evt = event, element = datapoint that was dragged
              // you may use this callback to prohibit dragging certain datapoints
              // by returning false in this callback
              // if (element.datasetIndex === 0 && element.index === 0) {
              // this would prohibit dragging the first datapoint in the first
              // dataset entirely
              // return false
              // }
              // console.log('onDragStart =>', evt, element)
            },
            onDrag: (evt, datasetIndex, index, value) => {
              // you may control the range in which datapoints are allowed to be
              // dragged by returning `false` in this callback
              // if (value < 0) return false // this only allows positive values
              // if (datasetIndex === 0 && index === 0 && value > 20) return false
              //  console.log('onDrag =>', evt, datasetIndex, index, value)
            },
            onDragEnd: (evt, datasetIndex, index, value) => {
              const newDragDataDatasets = [...dragDataDatasets];
              newDragDataDatasets[datasetIndex].data[index] = value;
              setDragDataDatasets(newDragDataDatasets);
              // you may use this callback to store the final datapoint value
              // (after dragging) in a database, or update other UI elements that
              // dependent on it
              // console.log('onDragEnd =>', evt, datasetIndex, index, value)
            },
          },
        },
      }}
    />
  );
};

export const StorageState = Template.bind({});
StorageState.args = {
  chart: {
    data: {
      datasets: [dataset1],
    },
    options: {
      title: 'Save state to localStorage',
      chartStyling: {
        height: 400,
      },
    },
    persistenceId: 'storageStateID',
  },
};

export const WithTranslations = () => {
  initializeLineChart({
    translations: {
      ...defaultTranslations,
      axesOptions: 'My Axes options',
    },
  });
  return (
    <LineChart
      chart={{
        ...basicChart,
      }}
    />
  );
};

export const TestLongTooltip = Template.bind({});
TestLongTooltip.args = {
  chart: {
    ...basicChart,
    data: {
      datasets: [
        {
          label:
            'Long dataset label goes here lorem ipsum dolor est lorem ipsum dolor est',
          data: Array.from({ length: 5 }).map((_, i) => ({
            x: i,
            y: i * 10,
          })),
        },
      ],
    },
    options: {
      ...basicChart.options,
      chartStyling: {
        height: 600,
      },
      axes: {
        x: [
          {
            label:
              'Long axis label goes here lorem ipsum dolor est lorem ipsum dolor est',
          },
        ],
        y: [
          {
            label:
              'Long axis label goes here lorem ipsum dolor est lorem ipsum dolor est',
          },
        ],
      },
    },
  },
};
export const TestManyLines = Template.bind({});
TestManyLines.args = {
  chart: {
    ...basicChart,
    data: {
      datasets: Array.from({ length: 20 }).map((_, index) => {
        return {
          label: `Label ${index}`,
          pointStyle: sample(pointStyles),
          pointRadius: 3,
          data: Array.from({ length: 5 }).map((_, i) => ({
            x: i,
            y: i * 10 - index * 10,
          })),
        };
      }),
    },
  },
};

export const TestLongLegendLabels = Template.bind({});
TestLongLegendLabels.args = {
  chart: {
    ...basicChart,
    data: {
      datasets: Array.from({ length: 5 }).map((_, index) => {
        return {
          label: `Label goes here lorem ipsum dolor est lorem ipsum dolor est lorem ipsum dolor est lorem ipsum dolor est lorem ipsum dolor est ${index}`,
          data: Array.from({ length: 5 }).map((_, i) => ({
            x: i,
            y: i * 10 - index * 10,
          })),
        };
      }),
    },
  },
};

export const TestResizingContainer = () => {
  const chart = {
    ...basicChart,
    data: {
      datasets: Array.from({ length: 5 }).map((_, index) => {
        return {
          label: `Label goes here lorem ipsum dolor est lorem ipsum dolor est lorem ipsum dolor est lorem ipsum dolor est lorem ipsum dolor est ${index}`,
          data: Array.from({ length: 5 }).map((_, i) => ({
            x: i,
            y: i * 10 - index * 10,
          })),
        };
      }),
    },
  };
  return (
    <Flex height="500px">
      <div style={{ flex: 1, overflow: 'auto' }}>
        <LineChart chart={chart} />
      </div>

      <Drawer right border open button closedWidth={50} />
    </Flex>
  );
};

export const PartialRange = Template.bind({});
PartialRange.args = {
  chart: {
    data: {
      datasets: [
        {
          label: 'Dataset 1',
          data: [
            {
              x: -10,
              y: -10,
            },
            {
              x: 5,
              y: 25,
            },
            {
              x: 10,
              y: 60,
            },
            {
              x: 15,
              y: 90,
            },
            {
              x: 20,
              y: 120,
            },
            {
              x: 25,
              y: 150,
            },
          ],
        },
      ],
    },
    options: {
      title: 'Partial Range',
      ...basicChart.options,
      additionalAxesOptions: {
        range: {
          x: {
            // min: -5,
            max: 25,
          },
          y: {
            min: -20,
            // max: 160,
          },
        },
      },
    },
  },
};

export const CombiningTypes = Template.bind({});
CombiningTypes.args = {
  chart: {
    data: {
      datasets: [
        {
          label: 'Normal DS',
          data: [
            {
              x: 0,
              y: -10,
            },
            {
              x: 5,
              y: -10,
            },
            {
              x: 10,
              y: -10,
            },
            {
              x: 15,
              y: -10,
            },
            {
              x: 20,
              y: -10,
            },
            {
              x: 25,
              y: -10,
            },
          ],
        },
        {
          label: 'Line',
          showPoints: false,
          data: [
            {
              x: -10,
              y: -10,
            },
            {
              x: 5,
              y: 25,
            },
            {
              x: 10,
              y: 60,
            },
            {
              x: 15,
              y: 90,
            },
            {
              x: 20,
              y: 120,
            },
            {
              x: 25,
              y: 150,
            },
          ],
        },
        {
          label: 'Dots',
          showLine: false,
          data: [
            {
              x: 0,
              y: 0,
            },
            {
              x: 3,
              y: 15,
            },
            {
              x: 6,
              y: 30,
            },
            {
              x: 9,
              y: 60,
            },
            {
              x: 12,
              y: 120,
            },
            {
              x: 15,
              y: 240,
            },
          ],
        },
      ],
    },
    options: {
      title: 'Partial Range',
      ...basicChart.options,
    },
  },
};

export const ExternalSelect = () => {
  const [selectedDatasets, setSelectedDatasets] = useState([]);
  const [selectedValues, setSelectedValues] = useState([]);
  const chart = {
    data: {
      datasets: selectedDatasets,
    },
    options: {
      chartStyling: {
        height: 400,
      },
    },
  };
  const selectOptions = [
    {
      label: 'Dataset 1',
      value: 'dataset1',
    },
    {
      label: 'Dataset 2',
      value: 'dataset2',
    },
    {
      label: 'Dataset 3',
      value: 'dataset3',
    },
  ];

  const handleChange = ({ target: { value } }) => {
    const selectedLabels = value.map((selectedOption) => selectedOption.label);
    const newSelectedDatasets = [dataset1, dataset2, dataset3].filter(
      (dataset) => selectedLabels.includes(dataset.label),
    );

    setSelectedDatasets(newSelectedDatasets);
    setSelectedValues(value);
  };

  return (
    <>
      <Select
        multi
        value={selectedValues}
        onChange={handleChange}
        options={selectOptions}
        placeholder="Auto width"
        width="auto"
      />
      <LineChart chart={chart} />
    </>
  );
};

export const SetDSDynamically = () => {
  const [val, setVal] = useState(1);
  const DS = {
    1: {
      data: {
        datasets: [],
      },
    },
    2: appTestCase1,
    3: appTestCase2,
  }[val];

  return (
    <>
      <ButtonGroup
        items={[
          {
            key: 1,
            label: 'Empty Dataset',
          },
          {
            key: 2,
            label: 'Dataset 1',
          },
          {
            key: 3,
            label: 'Dataset 2',
          },
        ]}
        onSelected={(ev) => {
          setVal(ev);
        }}
        value={val}
      />

      <LineChart
        chart={{
          ...basicChart,
          ...DS,
        }}
      />
    </>
  );
};

export const EmptyDS = Template.bind({});
EmptyDS.args = {
  chart: {
    ...basicChart,
    data: {
      datasets: [],
    },
  },
};

export const Tension = Template.bind({});
Tension.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      graph: {
        lineTension: 0.4,
      },
    },
    data: {
      datasets: [
        {
          label: 'Line with tension',
          showPoints: false,
          data: [
            {
              x: 0,
              y: -10,
            },
            {
              x: 1,
              y: 100,
            },
            {
              x: 2,
              y: 60,
            },
            {
              x: 3,
              y: 80,
            },
            {
              x: 4,
              y: 40,
            },
            {
              x: 5,
              y: 20,
            },
          ],
        },
      ],
    },
  },
};

export const SeparateTensionsForDSs = Template.bind({});
SeparateTensionsForDSs.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      graph: {
        lineTension: 0.4,
      },
      additionalAxesOptions: {
        range: {
          x: {
            min: 1,
            max: 15,
          },
          y: {
            min: 0,
            max: 100,
          },
        },
      },
    },
    data: {
      datasets: [
        {
          label: 'Line with general tension',
          showPoints: false,
          data: [
            {
              x: 4,
              y: 20,
            },
            {
              x: 6,
              y: 70,
            },
            {
              x: 8,
              y: 20,
            },
            {
              x: 12,
              y: 70,
            },
          ],
        },
        {
          label: 'Line with zero tension',
          showPoints: false,
          lineTension: 0,
          data: [
            {
              x: 5,
              y: 60,
            },
            {
              x: 8,
              y: 60,
            },
            {
              x: 8,
              y: 30,
            },
            {
              x: 5,
              y: 30,
            },
          ],
        },
        {
          label: 'Line with tension',
          showPoints: false,
          lineTension: 0.5,
          data: [
            {
              x: 5,
              y: 80,
            },
            {
              x: 10,
              y: 80,
            },
            {
              x: 10,
              y: 10,
            },
            {
              x: 5,
              y: 10,
            },
          ],
        },
      ],
    },
  },
};

export const TestCustomLinePointsLogic = Template.bind({});
TestCustomLinePointsLogic.args = {
  chart: {
    data: {
      datasets: [
        {
          label: 'Default',
          data: [
            {
              x: 0,
              y: 25,
            },
            {
              x: 12.5,
              y: 25,
            },
            {
              x: 25,
              y: 25,
            },
          ],
        },
        {
          label: 'Always show line',
          showLine: true,
          data: [
            {
              x: 0,
              y: 20,
            },
            {
              x: 12.5,
              y: 20,
            },
            {
              x: 25,
              y: 20,
            },
          ],
        },
        {
          label: 'Always show points',
          showPoints: true,
          data: [
            {
              x: 0,
              y: 15,
            },
            {
              x: 12.5,
              y: 15,
            },
            {
              x: 25,
              y: 15,
            },
          ],
        },
        {
          label: 'Never show line',
          showLine: false,
          data: [
            {
              x: 0,
              y: 10,
            },
            {
              x: 12.5,
              y: 10,
            },
            {
              x: 25,
              y: 10,
            },
          ],
        },
        {
          label: 'Never show points',
          showPoints: false,
          data: [
            {
              x: 0,
              y: 5,
            },
            {
              x: 12.5,
              y: 5,
            },
            {
              x: 25,
              y: 5,
            },
          ],
        },
        {
          label: 'Always show both',
          showPoints: true,
          showLine: true,
          data: [
            {
              x: 0,
              y: 0,
            },
            {
              x: 12.5,
              y: 0,
            },
            {
              x: 25,
              y: 0,
            },
          ],
        },
      ],
    },
  },
};

export const ManageAnnotationExternally = () => {
  const [annotationsEnabled, setAnnotationsEnabled] = useState({
    0: false,
    1: false,
    2: false,
    3: true,
    4: false,
  });

  const toggleAnnotation = (index) => {
    setAnnotationsEnabled((prevState) => ({
      ...prevState,
      [index]: !prevState[index],
    }));
  };

  const annotationsConfig = [
    { annotationAxis: 'y', label: 'Annotation 1', value: 50 },
    { annotationAxis: 'x', label: 'Annotation 2', value: 10 },
    { annotationAxis: 'y', label: 'Annotation 3', value: 20, endValue: 170 },
    {
      type: 'point',
      enableDrag: true,
      xValue: 5,
      yValue: 120,
      radius: DEFAULT_POINT_RADIUS,
      color: 'rgba(200, 20, 20, 1)',
      label: 'Point Label 1',
      onDragStart: (cord) => {
        console.log('start-----', cord);
      },
      onDrag: (cord) => {
        // console.log('move-----', cord);
      },
      onDragEnd: (cord) => {
        console.log('end-----', cord);
      },
    },
    {
      type: 'point',
      enableDrag: true,
      xValue: 7,
      yValue: 120,
      radius: DEFAULT_POINT_RADIUS,
      color: 'rgba(200, 20, 20, 1)',
      label: 'Point Label 2',
      onDragStart: (cord) => {
        console.log('start-----', cord);
      },
      onDrag: (cord) => {
        // console.log('move-----', cord);
      },
      onDragEnd: (cord) => {
        console.log('end-----', cord);
      },
    },
  ];

  const filteredAnnotations = annotationsConfig.filter(
    (_, index) => annotationsEnabled[index],
  );

  return (
    <>
      {annotationsConfig.map((annotation, index) => (
        <FormRow key={index}>
          <CheckBox
            label={`Annotation ${index + 1}`}
            onChange={() => toggleAnnotation(index)}
            checked={annotationsEnabled[index]}
          />
        </FormRow>
      ))}

      <LineChart
        chart={{
          ...basicChart,
          options: {
            ...basicChart.options,
            legend: {
              display: false,
            },
            annotations: {
              showAnnotations: true,
              enableDragAnnotation: true,
              labelAnnotation: {
                showLabel: true,
                text: 'Annotations',
              },
              annotationsData: filteredAnnotations,
            },
          },
        }}
      />
    </>
  );
};

export const FilterAnnotationWhenLegendClick = () => {
  const annotationsConfig = [
    { annotationAxis: 'y', label: 'Annotation 1', value: 50, display: true },
    { annotationAxis: 'x', label: 'Annotation 2', value: 10, display: false },
    {
      annotationAxis: 'y',
      label: 'Annotation 3',
      value: 20,
      endValue: 170,
      display: true,
    },
  ];

  const [annotations, setAnnotations] = useState(annotationsConfig);

  const handleLegendClick = (visible, currentItem) => {
    const updatedAnnotations = annotations.map((annotation) => {
      if (annotation.label === 'Annotation 1') {
        return {
          ...annotation,
          display: !visible,
        };
      }
      if (annotation.label === currentItem) {
        return {
          ...annotation,
          display: !visible,
        };
      }

      return annotation;
    });

    setAnnotations(updatedAnnotations);
  };

  return (
    <LineChart
      chart={{
        ...basicChart,
        options: {
          ...basicChart.options,
          interactions: {
            onLegendClick: (text, visible) => {
              if (text !== 'Annotation 1') {
                handleLegendClick(!visible, text);
              }
            },
          },
          annotations: {
            controlAnnotation: true,
            showAnnotations: true,
            annotationsData: annotations,
          },
        },
      }}
    />
  );
};

export const GroupedDS = () => {
  const sampleDatasets = [
    { ...dataset1, displayGroup: 'Group A' },
    { ...dataset2, displayGroup: 'Group A' },
    { ...dataset3, displayGroup: 'Group B' },
  ];

  return (
    <LineChart
      chart={{
        data: {
          datasets: sampleDatasets,
        },
        options: {
          title: 'Chart title',
          chartStyling: {
            height: 400,
          },
        },
      }}
    />
  );
};

export const SimilarValuesWithAnnotation = () => {
  const datasets = [
    {
      label: 'Default',
      data: [
        {
          x: 3.552713678800501e-15,
          y: 100,
        },
        {
          x: 2.842170943040401e-14,
          y: 200,
        },
        {
          x: 0,
          y: 300,
        },
        {
          x: 5.84217033040401e-14,
          y: 400,
        },
      ],
    },
  ];
  const annotationsData = [
    {
      annotationAxis: 'y',
      label: 'Casing Shoe - 500 m',
      value: 500,
      color: '#8b0707',
    },
  ];
  return (
    <LineChart
      chart={{
        data: {
          datasets,
        },
        options: {
          title: 'Similar values with annotation',
          chartStyling: {
            height: 400,
          },
          annotations: {
            showAnnotations: true,
            controlAnnotation: true,
            annotationsData,
          },
        },
      }}
    />
  );
};

export const HideSpecificDatasetAndAnnotation = () => {
  const sampleDatasets = [
    dataset1,
    { ...dataset2, hideLegend: true },
    dataset3,
  ];

  const annotations = [
    { annotationAxis: 'y', label: 'Annotation 1', value: 50 },
    { annotationAxis: 'x', label: 'Annotation 2', value: 10, hideLegend: true },
    { annotationAxis: 'y', label: 'Annotation 3', value: 20 },
  ];

  return (
    <LineChart
      chart={{
        data: {
          datasets: sampleDatasets,
        },
        options: {
          title: 'Hide Specific Datasets And Annotation',
          chartStyling: {
            height: 400,
          },
          annotations: {
            controlAnnotation: true,
            showAnnotations: true,
            annotationsData: annotations,
          },
        },
      }}
    />
  );
};

export const DragAnnotations = Template.bind({});
DragAnnotations.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      chartOptions: {
        enableDragAnnotation: true,
      },
      legend: {
        display: false,
      },
      annotations: {
        showAnnotations: true,
        enableDragAnnotation: true,
        annotationsData: [
          {
            annotationAxis: 'y',
            label: 'Annotation on y-axis',
            value: 20,
          },
          {
            enableDrag: true,
            type: 'box',
            xMin: 15,
            xMax: 17,
            yMin: 150,
            yMax: 180,
            color: 'rgba(20, 200, 20, 0.5)',
            onDragStart: (cord, ann) => {
              console.group('Start');
              console.log('Coordinates:', cord);
              console.log('Annotation: ', ann);
              console.groupEnd();
            },
            onDrag: (cord, ann) => {
              // console.group('Drag')
              // console.log('Coordinates:', cord);
              // console.log('Annotation: ', ann);
              // console.groupEnd()
            },
            onDragEnd: (cord, ann) => {
              console.group('End');
              console.log('Coordinates:', cord);
              console.log('Annotation: ', ann);
              console.groupEnd();
            },
          },
          {
            enableDrag: false,
            type: 'box',
            xMin: 15,
            xMax: 17,
            yMin: 200,
            yMax: 230,
            color: 'rgba(200, 20, 20, 0.5)',
          },
          {
            type: 'point',
            enableDrag: true,
            xValue: 8,
            yValue: 190,
            radius: DEFAULT_POINT_RADIUS,
            color: 'rgba(200, 20, 20, 1)',
            label: 'Point Label',
            labelConfig: {
              font: `bold 8px ${DEFAULT_FONT_FAMILY}`,
              color: 'rgba(96, 32, 196, 0.8)',
            },
            onDragStart: (cord) => {
              console.log('start-----', cord);
            },
            onDrag: (cord) => {
              // console.log('move-----', cord);
            },
            onDragEnd: (cord) => {
              console.log('end-----', cord);
            },
          },
          {
            enableDrag: true,
            dragAxis: 'y',
            type: 'box',
            xMin: 15,
            xMax: 17,
            yMin: 30,
            yMax: 60,
            color: 'rgba(200, 20, 20, 0.5)',
            label: 'Drag Box by "Y" axis',
          },
          {
            type: 'point',
            enableDrag: true,
            dragAxis: 'x',
            xValue: 8,
            yValue: 160,
            radius: DEFAULT_POINT_RADIUS,
            color: 'rgba(200, 20, 20, 1)',
            label: 'Drag Point by "X" axis',
            dragRange: {
              x: [5, 10],
              y: [0, 250],
            },
          },
          {
            type: 'point',
            enableDrag: true,
            pointStyle: 'triangle',
            dragAxis: 'x',
            xValue: 3,
            yValue: 240,
            radius: DEFAULT_POINT_RADIUS,
            color: 'rgba(200, 20, 20, 1)',
            label: 'Custom shape - Triangle',
          },
          {
            type: 'point',
            enableDrag: true,
            pointStyle: 'rect', // PointStyle.Square
            dragAxis: 'x',
            xValue: 3,
            yValue: 210,
            radius: DEFAULT_POINT_RADIUS,
            color: 'rgba(200, 20, 20, 1)',
            label: 'Custom shape - Square',
          },
          {
            type: 'point',
            enableDrag: true,
            pointStyle: 'rectRot', // PointStyle.Diamond
            dragAxis: 'x',
            xValue: 3,
            yValue: 180,
            radius: DEFAULT_POINT_RADIUS,
            color: 'rgba(200, 20, 20, 1)',
            label: 'Custom shape - Diamond',
          },
          {
            // default point style - Circle
            type: 'point',
            enableDrag: true,
            dragAxis: 'x',
            xValue: 3,
            yValue: 150,
            radius: DEFAULT_POINT_RADIUS,
            color: 'rgba(200, 20, 20, 1)',
            label: 'Custom shape - Circle',
          },
          {
            enableDrag: true,
            resizable: false,
            type: 'box',
            xMin: 10,
            xMax: 10.1,
            yMin: 0,
            yMax: 250,
            color: 'rgba(200, 20, 20, 0.5)',
            label: 'Drag Box restricted by chart area',
          },
          {
            enableDrag: true,
            dragAxis: 'x',
            dragRange: {
              x: [20, 24],
            },
            type: 'box',
            xMin: 21,
            xMax: 21.1,
            yMin: 0,
            yMax: 250,
            color: 'rgba(50, 200, 20, 0.5)',
            label: 'Drag Box restricted by "X" axes and dragRange',
            displayDragCoordinates: false,
          },
        ],
      },
    },
  },
};

export const MultiAxesDragAnnotations = Template.bind({});
MultiAxesDragAnnotations.args = {
  chart: {
    data: {
      datasets: [
        {
          label: 'Rainfall',
          data: [
            {
              x: 1,
              y: 49.9,
            },
            {
              x: 2,
              y: 71.5,
            },
            {
              x: 3,
              y: 106.4,
            },
            {
              x: 4,
              y: 129.2,
            },
            {
              x: 5,
              y: 144.0,
            },
            {
              x: 6,
              y: 176.0,
            },
            {
              x: 7,
              y: 135.6,
            },
            {
              x: 8,
              y: 148.5,
            },
            {
              x: 9,
              y: 216.4,
            },
            {
              x: 10,
              y: 194.1,
            },
            {
              x: 11,
              y: 95.6,
            },
            {
              x: 12,
              y: 54.4,
            },
          ],
        },
        {
          label: 'Temperature',
          data: [
            {
              x: 1,
              y: 7.0,
            },
            {
              x: 2,
              y: 6.9,
            },
            {
              x: 3,
              y: 9.5,
            },
            {
              x: 4,
              y: 14.5,
            },
            {
              x: 5,
              y: 18.2,
            },
            {
              x: 6,
              y: 21.5,
            },
            {
              x: 7,
              y: 25.2,
            },
            {
              x: 8,
              y: 26.5,
            },
            {
              x: 9,
              y: 23.3,
            },
            {
              x: 10,
              y: 18.3,
            },
            {
              x: 11,
              y: 13.9,
            },
            {
              x: 12,
              y: 9.6,
            },
          ],
          yAxisID: 'y2',
        },
      ],
    },
    options: {
      title: 'Multiple y-axes',
      axes: {
        x: [
          {
            label: 'Month',
          },
        ],
        y: [
          {
            label: 'Rainfall [mm]',
            position: 'left',
          },
          {
            label: 'Temperature [C]',
            position: 'right',
          },
        ],
      },
      chartOptions: {
        enableDragAnnotation: true,
      },
      legend: {
        display: false,
      },
      additionalAxesOptions: {
        range: {
          y2: {
            min: 0,
            max: 80,
          },
        },
      },
      annotations: {
        showAnnotations: true,
        enableDragAnnotation: true,
        annotationsData: [
          {
            enableDrag: true,
            type: 'box',
            xMin: 15,
            xMax: 17,
            yMin: 200,
            yMax: 230,
            color: 'rgba(200, 20, 20, 0.5)',
          },
          {
            enableDrag: true,
            // xScaleID: 'x',
            yScaleID: 'y2',
            resizable: false,
            type: 'box',
            xMin: 15,
            xMax: 17,
            yMin: 30,
            yMax: 40,
            color: 'rgba(20, 200, 20, 0.5)',
            // onDragStart: (cord, ann) => {
            //   console.group('Start');
            //   console.log('Coordinates:', cord);
            //   console.log('Annotation: ', ann);
            //   console.groupEnd();
            // },
            // onDrag: (cord, ann) => {
            //   // console.group('Drag')
            //   // console.log('Coordinates:', cord);
            //   // console.log('Annotation: ', ann);
            //   // console.groupEnd()
            // },
            // onDragEnd: (cord, ann) => {
            //   console.group('End');
            //   console.log('Coordinates:', cord);
            //   console.log('Annotation: ', ann);
            //   console.groupEnd();
            // },
          },
        ],
      },
    },
  },
};

export const ChartFillerPlugin = Template.bind({});
ChartFillerPlugin.args = {
  chart: {
    ...basicChart,
    options: {
      ...basicChart.options,
      additionalAxesOptions: {
        range: {
          x: {
            min: 0,
            max: 10,
          },
          y: {
            min: -10,
            max: 100,
          },
        },
      },
    },
    data: {
      datasets: [
        {
          label: 'Show how filler plugin works',
          showPoints: false,
          backgroundColor: 'rgba(0, 0, 255, 0.1)',
          fill: 'shape',
          // fill: 'origin',      // 0: fill to 'origin'
          // fill: '+2',          // 1: fill to dataset 3
          // fill: 1,             // 2: fill to dataset 1
          // fill: false,         // 3: no fill
          // fill: '-2',          // 4: fill to dataset 2
          // fill: {value: 25}    // 5: fill to axis value 25
          data: [
            {
              x: 5,
              y: 0,
            },
            {
              x: 4,
              y: 40,
            },
            {
              x: 5,
              y: 80,
            },
            {
              x: 6,
              y: 40,
            },
            {
              x: 5,
              y: 0,
            },
          ],
        },
      ],
    },
  },
};
