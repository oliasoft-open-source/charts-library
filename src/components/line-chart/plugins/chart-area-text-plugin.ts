import { Chart, ChartConfiguration, LegendOptions, Plugin } from 'chart.js';
import { TextLabelPosition } from './plugin-constants';
import { AlignOptions, Position } from '../../common/helpers/enums';
import { IDrawText } from './plugins.interface';

const WORD_SEPARATOR = ' ';
const SEMI_TRANSPARENT = 'rgba(0, 0, 0, 0.5)';

interface CustomChartConfiguration extends ChartConfiguration {
  showLabel?: boolean;
  text: string;
  fontSize: number;
  xOffset: number;
  yOffset: number;
  lineHeight: number;
  maxWidth: number;
  position: Position;
}

/**
 * Splits the input text into words based on the predefined WORD_SEPARATOR.
 * If the input is an array, it first joins the array and then splits it.
 */
const getWords = (text: string | Array<string>) => {
  return (Array.isArray(text) ? text.join(WORD_SEPARATOR) : text).split(
    WORD_SEPARATOR,
  );
};

/**
 * Renders the lines of text on the canvas context.
 * It iterates over the array of lines and renders each line with the provided styling.
 */
const drawText = ({ ctx, lines, lineHeight, x, y, position }: IDrawText) => {
  lines.forEach((line, index) => {
    const lineY = position.includes('top')
      ? y + lineHeight * (index + 1) - 5
      : y - (lines.length - 1 - index) * lineHeight;
    ctx.fillText(line, x, lineY);
  });
};

/**
 * Calculates the maximum width for the text based on the initial maximum width
 * and the chart area width.
 */
const calculateMaxWidth = (initialMaxWidth: number, chartAreaWidth: number) => {
  const factorMiddle = 0.5;
  const factorSmall = 0.7;
  const maxWidthFactor =
    chartAreaWidth < 500
      ? factorMiddle
      : chartAreaWidth < 700
      ? factorSmall
      : 1;
  return initialMaxWidth * maxWidthFactor;
};

/**
 * Determines the legend dimensions (width and height) based on its position.
 */
const getLegendDimensions = (chart: Chart) => {
  const { legend } = chart;

  const legendWidth =
    legend &&
    (legend as unknown as LegendOptions<'line'>).display &&
    legend.options.position === 'left'
      ? legend.width + legend.options.labels.padding * 2
      : 0;

  const legendHeight =
    legend &&
    (legend as unknown as LegendOptions<'line'>).display &&
    (legend.options.position === 'top' || legend.options.position === 'bottom')
      ? legend.height + legend.options.labels.padding * 2
      : 0;

  return { legendWidth, legendHeight };
};

/**
 * Determines the X and Y coordinates for the provided position.
 */
const getPositionCoordinates = (
  position: string,
  chart: Chart,
  xOffset: number,
  yOffset: number,
) => {
  const { chartArea, width } = chart;
  const temporaryChartAreaRight = width - 8;

  const { legendWidth, legendHeight } = getLegendDimensions(chart);

  switch (position) {
    case TextLabelPosition.TOP_LEFT:
      return [
        chartArea.left + xOffset + legendWidth,
        chartArea.top + yOffset + legendHeight,
      ];
    case TextLabelPosition.TOP_CENTER:
      return [
        chartArea.left + chartArea.width / 2,
        chartArea.top + yOffset + legendHeight,
      ];
    case TextLabelPosition.TOP_RIGHT:
      return [
        temporaryChartAreaRight - xOffset, //replace within chartArea.right when resize bug will be fixed
        chartArea.top + yOffset + legendHeight,
      ];
    case TextLabelPosition.MIDDLE_LEFT:
      return [
        chartArea.left + xOffset + legendWidth,
        chartArea.top + chartArea.height / 2,
      ];
    case TextLabelPosition.MIDDLE_CENTER:
      return [
        chartArea.left + chartArea.width / 2,
        chartArea.top + chartArea.height / 2,
      ];
    case TextLabelPosition.MIDDLE_RIGHT:
      return [
        temporaryChartAreaRight - xOffset - legendWidth, //replace within chartArea.right when resize bug will be fixed
        chartArea.top + chartArea.height / 2,
      ];
    case TextLabelPosition.BOTTOM_LEFT:
      return [
        chartArea.left + xOffset + legendWidth,
        chartArea.bottom - yOffset - legendHeight,
      ];
    case TextLabelPosition.BOTTOM_CENTER:
      return [
        chartArea.left + chartArea.width / 2,
        chartArea.bottom - yOffset - legendHeight,
      ];
    case TextLabelPosition.BOTTOM_RIGHT:
    default:
      return [
        temporaryChartAreaRight - xOffset - legendWidth, //replace within chartArea.right when resize bug will be fixed
        chartArea.bottom - yOffset - legendHeight,
      ];
  }
};

/**
 * Determines the appropriate text alignment based on the position provided.
 */
const getTextAlign = (position: string) => {
  if (position.includes(AlignOptions.Center)) {
    return AlignOptions.Center;
  } else if (position.includes(AlignOptions.Left)) {
    return AlignOptions.Left;
  } else {
    return AlignOptions.Right;
  }
};

/**
 * Wraps the text into lines based on the maxWidth provided.
 */
const wrapText = (
  words: Array<string>,
  maxWidth: number,
  ctx: CanvasRenderingContext2D,
) => {
  let line = '';
  let lines = [];
  for (let i = 0; i < words.length; i++) {
    const testLine = `${line}${words[i]}${WORD_SEPARATOR}`;
    const { width: testWidth } = ctx.measureText(testLine);
    if (testWidth > maxWidth) {
      lines.push(line.trim());
      line = `${words[i]}${WORD_SEPARATOR}`;
    } else {
      line = testLine;
    }
  }
  lines.push(line.trim()); // Add the last line
  return lines;
};

/**
 * Renders the wrapped text on the canvas context.
 */
const renderWrappedText = (
  ctx: CanvasRenderingContext2D,
  options: ChartConfiguration | any, // As of Chart.js v3.x, chart.config is of type Record<string, unknown>,
) => {
  const { text, maxWidth, fontSize, lineHeight, x, y, position } = options;

  const words = getWords(text);
  const wrappedLines = wrapText(words, maxWidth, ctx);

  ctx.save();
  ctx.font = `${fontSize}px Arial`;
  ctx.fillStyle = SEMI_TRANSPARENT;
  ctx.textAlign = getTextAlign(position);

  drawText({ ctx, lines: wrappedLines, lineHeight, x, y, position });

  ctx.restore();
};

export const chartAreaTextPlugin = {
  id: 'chartAreaText',

  beforeDraw: (
    chart: Chart,
    _args: Plugin,
    options: CustomChartConfiguration,
  ) => {
    const {
      showLabel,
      text,
      fontSize,
      xOffset,
      yOffset,
      lineHeight,
      maxWidth: initialMaxWidth,
      position,
    } = options;

    if (!showLabel || !text) return;

    const { ctx, chartArea } = chart;

    // Determine the maxWidth based on chartArea width
    const maxWidth = calculateMaxWidth(initialMaxWidth, chartArea.width);

    // Get the position coordinates
    const [x, y] = getPositionCoordinates(position, chart, xOffset, yOffset);

    // Render the wrapped text
    renderWrappedText(ctx, {
      text,
      maxWidth,
      fontSize,
      lineHeight,
      x,
      y,
      position,
    });
  },
};
