import { Position } from '../../common/helpers/enums';

export interface IDrawText {
  ctx: CanvasRenderingContext2D;
  lines: Array<string>;
  lineHeight: number;
  x: number;
  y: number;
  position: Position;
}
