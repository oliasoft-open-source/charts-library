import { Chart, ChartConfiguration, Scale } from 'chart.js';
import { UnusedParameter } from '../../common/common.interface';

const MINOR_TICKS_PER_MAJOR = 10;

// Return largest gap between ticks
// (Tick distance may not be consistent if ranges are custom)
export const getLargestMajorTickWidth = (
  majorTickPositions: number[],
): number => {
  return majorTickPositions.reduce(
    (acc: number, curr: number, index: number): number => {
      if (index === 0) return acc;
      const gap = Math.abs(curr - majorTickPositions[index - 1]);
      return Math.max(gap, acc);
    },
    0,
  );
};

// Check if position is in visible part of axis scale
// Check position does not duplicate a major tick
export const isValidPosition = (
  minorTickPosition: number,
  majorTickPositions: number[],
  scale: Scale,
): boolean => {
  const { axis, left, top, right, bottom } = scale;
  const isHorizontal = axis === 'x';
  const start = isHorizontal ? left : top;
  const end = isHorizontal ? right : bottom;
  const isAfterStart = minorTickPosition > start;
  const isBeforeEnd = minorTickPosition < end;
  const isDuplicate = majorTickPositions.indexOf(minorTickPosition) !== -1;
  return isAfterStart && isBeforeEnd && !isDuplicate;
};

// Generate minor tick positions for a given axis/scale
// (Uses second tick as starting point in case first tick is an irregular custom value)
export const getMinorTickPositions = (
  majorTickPositions: number[],
  scale: Scale,
): number[] => {
  const sortedMajorTickPositions = majorTickPositions.sort((a, b) => {
    return a - b;
  });
  const minorTickWidth = getLargestMajorTickWidth(majorTickPositions) / 10;
  const startPosition = majorTickPositions[0];
  const numMinorTicks = (majorTickPositions.length + 1) * MINOR_TICKS_PER_MAJOR;
  const positions = [...Array(numMinorTicks)]
    .map((_, index) => {
      const minorTickPosition =
        startPosition + (index - MINOR_TICKS_PER_MAJOR + 1) * minorTickWidth;
      return parseFloat(minorTickPosition.toFixed(1));
    })
    .filter((position) =>
      isValidPosition(position, sortedMajorTickPositions, scale),
    );
  return positions;
};

const drawMinorTicksForScale = (scale: Scale) => {
  const { chart, ctx } = scale;
  const config = chart.config as ChartConfiguration | any; // As of Chart.js v3.x, chart.config is of type Record<string, unknown>

  if (!config?.options?.scales?.[scale.id]?.grid?.drawOnChartArea) return;
  if (config.options.indexAxis === scale.axis) return;

  const isHorizontal = scale.axis === 'x';
  const majorTickPositions = scale.ticks
    .map((_: UnusedParameter, index: number) => scale.getPixelForTick(index))
    .sort((a: number, b: number) => a - b);

  const minorTickPositions = getMinorTickPositions(majorTickPositions, scale);

  ctx.save();
  ctx.strokeStyle =
    config?._config?.options?.scales?.[scale.id]?.grid?.color ||
    'rgba(0,0,0,0.06)';
  ctx.lineWidth = 0.5;
  minorTickPositions.forEach((minorTickPosition) => {
    ctx.beginPath();
    if (isHorizontal) {
      const { top, bottom } = chart.chartArea;
      ctx.moveTo(minorTickPosition, top);
      ctx.lineTo(minorTickPosition, bottom);
    } else {
      const { left, right } = chart.chartArea;
      ctx.moveTo(left, minorTickPosition);
      ctx.lineTo(right, minorTickPosition);
    }
    ctx.stroke();
  });
  ctx.restore();
};

export const chartMinorGridlinesPlugin = {
  id: 'minorGridlines',
  beforeDatasetsDraw: (chart: Chart) => {
    const { scales } = chart;
    Object.keys(scales).forEach((id) => drawMinorTicksForScale(scales[id]));
  },
};
