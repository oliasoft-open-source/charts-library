import {
  getLargestMajorTickWidth,
  getMinorTickPositions,
  isValidPosition,
} from './line-chart.minor-gridlines-plugin';

describe('chartMinorGridlinesPlugin', () => {
  test('getLargestMajorTickWidth', () => {
    const even = [0, 1, 2, 3];
    const uneven = [0.85, 1, 2, 2.25];
    expect(getLargestMajorTickWidth(even)).toBe(1);
    expect(getLargestMajorTickWidth(uneven)).toBe(1);
  });
  test('isValidPosition', () => {
    const majorTicks = [1, 2];
    const scale = { axis: 'x', left: 0, right: 3 };
    const inBounds = 0.1;
    const outOfBounds = 3.1;
    expect(isValidPosition(inBounds, majorTicks, scale)).toBe(true);
    expect(isValidPosition(outOfBounds, majorTicks, scale)).toBe(false);
  });
  test('getMinorTickPositions', () => {
    const majorTicks = [1, 2];
    const evenScale = { axis: 'x', left: 0, right: 3 };
    expect(getMinorTickPositions(majorTicks, evenScale)).toStrictEqual([
      0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6,
      1.7, 1.8, 1.9, 2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9,
    ]);
    const unevenScale = { axis: 'x', left: 0.85, right: 2.25 };
    expect(getMinorTickPositions(majorTicks, unevenScale)).toStrictEqual([
      0.9, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.1, 2.2,
    ]);
  });
});
