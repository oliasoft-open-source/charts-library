import { RefObject } from 'react';
import { Chart } from 'chart.js';
import { ICommonAnnotationsData } from '../../common/common.interface.ts';
import { getAnnotationsData } from './get-annotations-data';

export type TAxisType = 'x' | 'y' | 'x1' | 'y1';

interface IMinMax {
  min: number;
  max: number;
}

interface IParsedData {
  [key: string]: number | null;
}

export const getAxisRangeByType = (
  chartRef: RefObject<Chart>,
  axesType: TAxisType,
  annotationsData: ICommonAnnotationsData[],
): IMinMax | null => {
  if (!chartRef || !chartRef.current) return null;

  const metasets = chartRef.current.getSortedVisibleDatasetMetas();
  const annotDataByType =
    (getAnnotationsData(annotationsData) as Record<TAxisType, any>)[axesType] ??
    [];
  let allData: number[] = [];

  metasets.forEach((metaset) => {
    const data = metaset._parsed
      .map((parsedData) => (parsedData as IParsedData)[axesType])
      .filter((value) => value != null && !isNaN(value)) as number[];
    allData = allData.concat(data);
  });

  // Include annotation data in the data array
  allData = allData.concat(annotDataByType);

  // If there's no data, return null
  if (allData.length === 0) return null;

  const min = Math.min(...allData);
  const max = Math.max(...allData);

  return { min, max };
};
