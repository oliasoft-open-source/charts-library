import { displayNumber, roundByMagnitude } from '@oliasoft-open-source/units';
import { TooltipItem } from 'chart.js';
import {
  ChartHoverMode,
  Position,
  TooltipLabel,
} from '../../common/helpers/enums';
import {
  afterLabelCallback,
  getTooltipLabel,
} from '../../common/helpers/chart-utils';
import {
  LEGEND_LABEL_BOX_SIZE,
  TOOLTIP_BOX_PADDING,
  TOOLTIP_PADDING,
} from '../../common/helpers/chart-consts';
import { ILineChartDataset, ILineChartOptions } from '../line-chart.interface';
import { LOWER_BOUND, UPPER_BOUND } from '../constants/line-chart-consts';

export const getUnitsFromLabel = (label: string) => {
  const matches = label?.match(/\[(.*)\]/g);
  const units = matches && matches?.length > 0 ? matches?.[0] : '';
  return units;
};

const customFormatNumber = (
  labelNumber: number,
  scientificNotation: boolean,
) => {
  let roundOptions = {};

  if (!scientificNotation) {
    roundOptions = { scientific: false };
  } else if (
    Math.abs(labelNumber) < LOWER_BOUND ||
    Math.abs(labelNumber) > UPPER_BOUND
  ) {
    roundOptions = { roundScientificCoefficient: 3 };
  }

  return displayNumber(roundByMagnitude(labelNumber), roundOptions);
};

const getLineChartToolTips = (options: ILineChartOptions) => {
  const { scientificNotation } = options.tooltip;
  const getTooltipLabels = ({
    xAxisID = '',
    yAxisID = '',
  }: ILineChartDataset) => {
    const xIndex = xAxisID?.length > 1 ? Number(xAxisID[1]) - 1 : 0;
    const yIndex = yAxisID?.length > 1 ? Number(yAxisID[1]) - 1 : 0;
    const xAxis = options.axes.x[xIndex];
    const yAxis = options.axes.y[yIndex];

    if (options?.axes?.x?.[0]?.position === Position.Top) {
      return {
        titleAxisLabel: yAxis?.label || '',
        valueAxisLabel: xAxis?.label || '',
        titleLabel: TooltipLabel.Y,
        valueLabel: TooltipLabel.X,
      };
    } else {
      return {
        titleAxisLabel: xAxis?.label || '',
        valueAxisLabel: yAxis?.label || '',
        titleLabel: TooltipLabel.X,
        valueLabel: TooltipLabel.Y,
      };
    }
  };

  const titleCallback = (tooltipItem: TooltipItem<any>[]) => {
    const labels = getTooltipLabels(tooltipItem[0].dataset);
    const { titleLabel, titleAxisLabel } = labels ?? {};

    const formattedValue =
      titleLabel === TooltipLabel.Y
        ? tooltipItem[0].parsed.y
        : tooltipItem[0].parsed.x;

    const roundedValue = customFormatNumber(formattedValue, scientificNotation);

    return `${roundedValue} ${titleAxisLabel}`;
  };

  const labelCallback = (tooltipItem: TooltipItem<any>) => {
    const { showLabelsInTooltips } = options.tooltip;
    let label = tooltipItem.dataset.label || '';
    const labels = getTooltipLabels(tooltipItem.dataset);
    const { valueLabel = '', valueAxisLabel = '' } = labels ?? {};

    const getTooltipItemValue = () => {
      const labelNumber =
        valueLabel === TooltipLabel.X
          ? tooltipItem.parsed.x
          : tooltipItem.parsed.y;

      return customFormatNumber(labelNumber, scientificNotation);
    };

    const tooltipItemValue = getTooltipItemValue();
    const units = getUnitsFromLabel(valueAxisLabel);
    const tooltipLabel = getTooltipLabel(tooltipItem, showLabelsInTooltips);

    return `${label}: ${tooltipItemValue} ${units} ${tooltipLabel}`;
  };

  return {
    enabled: options.tooltip.tooltips,
    mode: ChartHoverMode.Nearest,
    intersect: true,
    padding: TOOLTIP_PADDING,
    usePointStyle: true,
    boxWidth: LEGEND_LABEL_BOX_SIZE,
    boxHeight: LEGEND_LABEL_BOX_SIZE,
    boxPadding: TOOLTIP_BOX_PADDING,
    callbacks: {
      title: titleCallback,
      label: labelCallback,
      afterLabel: afterLabelCallback,
    },
  };
};

export default getLineChartToolTips;
