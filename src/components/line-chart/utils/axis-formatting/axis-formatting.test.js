import { formatAxisLabelNumbers } from './axis-formatting';

describe('formatAxisLabelNumbers', () => {
  test('large and very large numbers display in scientific notation', () => {
    expect(
      formatAxisLabelNumbers(1.22345e16, [
        { value: 1.12345e16 },
        { value: 1.32345e16 },
      ]),
    ).toBe('1.2·10¹⁶');
    expect(
      formatAxisLabelNumbers(1.757693134862315e308, [
        { value: 1.747693134862315e308 },
        { value: 1.767693134862315e308 },
      ]),
    ).toBe('1.8·10³⁰⁸');
  });
  test('small numbers display in scientific notation', () => {
    expect(
      formatAxisLabelNumbers(1.1235e-5, [
        { value: 1.1234e-5 },
        { value: 1.1236e-5 },
      ]),
    ).toBe('1.1235·10⁻⁵');
    expect(
      formatAxisLabelNumbers(0.000000045, [
        { value: 0.000000000000000043 },
        { value: 0.000000046 },
      ]),
    ).toBe('5·10⁻⁸');
  });
  test('rounding depends on magnitude of data range', () => {
    expect(formatAxisLabelNumbers(21, [{ value: 0 }, { value: 50 }])).toBe(
      '21',
    );
    expect(formatAxisLabelNumbers(21.45, [{ value: 0 }, { value: 50 }])).toBe(
      '21.5',
    );
    expect(formatAxisLabelNumbers(0.2, [{ value: 0 }, { value: 50 }])).toBe(
      '0.2',
    );
    expect(formatAxisLabelNumbers(0.02, [{ value: 0 }, { value: 50 }])).toBe(
      '0.02',
    );
    expect(formatAxisLabelNumbers(0.002, [{ value: 0 }, { value: 50 }])).toBe(
      '0.002',
    );
    expect(formatAxisLabelNumbers(0.0002, [{ value: 0 }, { value: 50 }])).toBe(
      '0.0002',
    );
    expect(formatAxisLabelNumbers(1.0002, [{ value: 0 }, { value: 50 }])).toBe(
      '1.0002',
    );
    expect(formatAxisLabelNumbers(2e-2, [{ value: 0 }, { value: 50 }])).toBe(
      '0.02',
    );
    expect(formatAxisLabelNumbers(2e-6, [{ value: 0 }, { value: 50 }])).toBe(
      '0.000002',
    );
    expect(formatAxisLabelNumbers(2e-15, [{ value: 0 }, { value: 50 }])).toBe(
      '0.000000000000002',
    );
    expect(formatAxisLabelNumbers(null, [{ value: 0 }, { value: 50 }])).toBe(
      '0', //todo: why not rounded?
    );
    expect(
      formatAxisLabelNumbers(890.000000045, [
        { value: 890.0000000000043 },
        { value: 890.000000046 },
      ]),
    ).toBe('890.00000005');

    expect(
      formatAxisLabelNumbers(0.00134567891011, [
        { value: 0.00134567891011 },
        { value: 0.00384567891011 },
      ]),
    ).toBe('0.001');
    expect(
      formatAxisLabelNumbers(2.1334567891011, [
        { value: 1.1234567891011 },
        { value: 3.1484567891011 },
      ]),
    ).toBe('2.1');
    expect(
      formatAxisLabelNumbers(-0.00004, [{ value: -0.00005 }, { value: 0.0 }]),
    ).toBe('-4·10⁻⁵');
  });
  test('zero never has decimals', () => {
    expect(formatAxisLabelNumbers(0.0, [{ value: 0 }, { value: 50 }])).toBe(
      '0',
    );
  });
  test('thousand separators in labels', () => {
    expect(
      formatAxisLabelNumbers(200000000.345, [{ value: 0 }, { value: 50 }]),
    ).toBe('200 000 000.3');
    expect(formatAxisLabelNumbers(3e5, [{ value: 0 }, { value: 50 }])).toBe(
      '300 000',
    );
  });
});
