import { displayNumber, round } from '@oliasoft-open-source/units';
import { Tick } from 'chart.js';
import { LOWER_BOUND, UPPER_BOUND } from '../../constants/line-chart-consts';

/*
  formatAxisLabelNumbers is cloned from the internal logic of chart.js
  We needed to override how it formats thousands separators, while retaining
  its other formatting decisions to avoid any breaking changes. We can change
  this more or deviate further from the chart.js built-in implementation in the
  future, depending on our requirements.

  The function comes from
    - https://github.com/chartjs/Chart.js/blob/master/src/helpers/helpers.intl.ts
    - https://github.com/chartjs/Chart.js/blob/master/src/core/core.ticks.js#L28
 */

const calculateDelta = (tickValue: string | number, ticks: Array<any>) => {
  // Figure out how many digits to show
  // The space between the first two ticks might be smaller than normal spacing
  let delta =
    ticks.length > 3
      ? ticks[2].value - ticks[1].value
      : ticks[1].value - ticks[0].value;

  // If we have a number like 2.5 as the delta, figure out how many decimal places we need
  if (Math.abs(delta) >= 1 && tickValue !== Math.floor(Number(tickValue))) {
    // not an integer
    delta = Number(tickValue) - Math.floor(Number(tickValue));
  }
  return delta;
};

export const formatAxisLabelNumbers = (
  tickValue: string | number,
  ticks: Tick[],
) => {
  if (tickValue === 0 || tickValue === null) {
    return '0'; // never show decimal places for 0
  }

  let notation;
  let delta = tickValue; // This is used when there are less than 2 ticks as the tick interval.

  if (ticks.length > 1) {
    // all ticks are small or there huge numbers; use scientific notation
    const maxTick = Math.max(
      Math.abs(ticks[0].value),
      Math.abs(ticks[ticks.length - 1].value),
    );
    if (maxTick < LOWER_BOUND || maxTick > UPPER_BOUND) {
      notation = 'scientific';
    }

    delta = calculateDelta(tickValue, ticks);
  }

  const logDelta = Math.log10(Math.abs(Number(delta)));

  // When datasets have values approaching Number.MAX_VALUE, the tick calculations might result in
  // infinity and eventually NaN. Passing NaN for minimumFractionDigits or maximumFractionDigits
  // will make the number formatter throw. So instead we check for isNaN and use a fallback value.
  //
  // toFixed has a max of 20 decimal places
  const numDecimal = isNaN(logDelta)
    ? 1
    : Math.max(Math.min(-1 * Math.floor(logDelta), 20), 0);
  // upwards adjust rounding of scientific coefficient to minimum of 1 (stops all axis ticks having same value)
  const roundScientificCoefficient = numDecimal === 0 ? 1 : numDecimal;

  return displayNumber(round(tickValue, numDecimal), {
    scientific: notation === 'scientific',
    roundScientificCoefficient,
  });
};
