import { getCondition } from './get-alignment-condition';
import { getAlignmentData } from './get-alignment-data';
import { AlignOptions } from '../../../common/helpers/enums';
import { IAlignmentContext } from './datalabels-alignment.interface.ts';

/**
 * Returning position depends on condition
 */
export const getPosition = () => {
  return (context: IAlignmentContext) => {
    const { x, y, left, right, bottom } = getAlignmentData(context);
    const {
      overLeftSide = false,
      overRightSide = false,
      overBottomSide = false,
    } = getCondition(x, y, left, right, bottom);

    return (
      (overLeftSide && AlignOptions.Right) ||
      (overRightSide && AlignOptions.Left) ||
      (overBottomSide && AlignOptions.End) ||
      AlignOptions.Start
    );
  };
};
