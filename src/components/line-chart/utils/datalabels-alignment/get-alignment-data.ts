import { IAlignmentContext } from './datalabels-alignment.interface';

/**
 * Returning destructured data from context
 */
export const getAlignmentData = (context: IAlignmentContext) => {
  const { chart, dataIndex = 0, datasetIndex = 0 } = context;
  const { chartArea } = chart;
  const { left = null, right = null, bottom = null } = chartArea;

  const meta = chart.getDatasetMeta(datasetIndex);
  const { x = null, y = null } = meta?.data?.[dataIndex] || {};

  return {
    x,
    y,
    left,
    right,
    bottom,
  };
};
