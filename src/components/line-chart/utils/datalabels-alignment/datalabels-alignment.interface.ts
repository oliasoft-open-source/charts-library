interface IChartArea {
  left: number | null;
  right: number | null;
  bottom: number | null;
}

interface IChartMeta {
  data: Array<{ x: number | null; y: number | null }>;
}

export interface IAlignmentContext {
  chart: {
    chartArea: IChartArea;
    getDatasetMeta: (datasetIndex: number) => IChartMeta;
  };
  dataIndex: number;
  datasetIndex: number;
}
