/**
 * Returning boolean condition depends on label position and chart area
 */
export const getCondition = (
  x: number | null,
  y: number | null,
  left: number | null,
  right: number | null,
  bottom: number | null,
) => {
  const threshold = 100;
  const safeX = x ?? 0;
  const safeY = y ?? 0;
  const safeLeft = left ?? 0;
  const safeRight = right ?? 0;
  const safeBottom = bottom ?? 0;

  const overLeftSide = safeX - threshold <= safeLeft;
  const overRightSide = safeX + threshold >= safeRight;
  const overBottomSide =
    safeX + threshold >= safeLeft &&
    safeX + threshold < safeRight &&
    safeY + threshold >= safeBottom;

  return { overLeftSide, overRightSide, overBottomSide };
};
