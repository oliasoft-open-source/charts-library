import { getCondition } from '../get-alignment-condition';

describe('Datalabels alignment condition', () => {
  test('should return overLeftSide = true', () => {
    const result = getCondition(36, 325, 36, 1204, 325);
    expect(result).toStrictEqual({
      overLeftSide: true,
      overRightSide: false,
      overBottomSide: true,
    });
  });
  test('should return overRightSide = true', () => {
    const result = getCondition(1200, 325, 36, 1204, 325);
    expect(result).toStrictEqual({
      overLeftSide: false,
      overRightSide: true,
      overBottomSide: false,
    });
  });
  test('should return overBottomSide = true', () => {
    const result = getCondition(457, 250, 36, 1204, 325);
    expect(result).toStrictEqual({
      overLeftSide: false,
      overRightSide: false,
      overBottomSide: true,
    });
  });
  test('should return all false', () => {
    const result = getCondition(789, 100, 36, 1204, 325);
    expect(result).toStrictEqual({
      overLeftSide: false,
      overRightSide: false,
      overBottomSide: false,
    });
  });
});
