import { vi } from 'vitest';
import { getPosition } from '../get-datalabels-position';

const mockedContext = {
  chart: {
    chartArea: {
      left: 36,
      right: 1204,
      bottom: 325,
    },
    getDatasetMeta: vi.fn(),
  },
  dataIndex: 0,
  datasetIndex: 0,
};

describe('Datalabels alignment position', () => {
  test('should return position', () => {
    const result = getPosition()(mockedContext);
    expect(result).toBe('right');
  });
});
