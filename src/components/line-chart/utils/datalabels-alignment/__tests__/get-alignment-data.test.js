import { vi } from 'vitest';
import { getAlignmentData } from '../get-alignment-data';

const mockedContext = {
  chart: {
    chartArea: {
      left: 36,
      right: 1204,
      bottom: 325,
    },
    getDatasetMeta: vi.fn(),
  },
  dataIndex: 0,
  datasetIndex: 0,
};

describe('Datalabels alignment data', () => {
  test('should return destructured data from context', () => {
    const result = getAlignmentData(mockedContext);
    expect(result).toEqual(
      expect.objectContaining({
        x: null,
        y: null,
        left: 36,
        right: 1204,
        bottom: 325,
      }),
    );
  });
});
