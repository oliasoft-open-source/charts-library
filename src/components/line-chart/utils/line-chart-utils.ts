import { toNum as toNumber } from '@oliasoft-open-source/units';
import { isNilOrEmpty } from '../../common/helpers/chart-utils';
import { ILineRange } from '../line-chart.interface';
import { TPrimitive } from '../../common/common.interface';

export const toNum = (value: string | number) => {
  const asNumber = toNumber(value);
  return value === '' || isNaN(asNumber) ? '' : asNumber;
};

export const isLessThanMax = (value: string | number, max: string | number) => {
  return (
    value === undefined || max === undefined || Number(value) < Number(max)
  );
};

export const isGreaterThanMin = (
  value: string | number,
  min: string | number,
) => {
  return (
    value === undefined || min === undefined || Number(value) > Number(min)
  );
};

/**
 *
 * @param {'x'|'y'} axisType
 * @param {number} [index] - axis index; optional, 0 by default
 * @param {boolean} [hasMultiAxes] - optional, false by default
 * @return {string} - e.g. x if chart has singular axes; x, x2 - in case of multiple axes
 */
export const generateAxisId = (
  axisType: string,
  index = 0,
  hasMultiAxes = false,
) => {
  const i = hasMultiAxes && index !== 0 ? index + 1 : '';
  return `${axisType}${i}`;
};

/**
 Get axis type from a key string.
 @param {string} string - The key string to extract from.
 @returns {string} e.g. x1 => x
 */
export const getAxisTypeFromKey = (string: string) => {
  return string?.match(/[^0-9/]+/gi)?.[0] ?? '';
};

/**
 * Generates a key based on an array of values. The key changes
 * if any of the values in the array change.
 */
export const generateKey = (values: Array<any>) => {
  const key = values.join('');
  return key;
};

/**
 * Checks if a value is a primitive value (which includes strings, numbers, booleans, and null).
 */
export const isPrimitiveValue = (value: TPrimitive) =>
  typeof value === 'string' ||
  typeof value === 'number' ||
  typeof value === 'boolean' ||
  value === null;

/**
 * Validates a range object to ensure none of its properties are null, undefined, or empty strings.
 */
export const isRangeValid = (r?: ILineRange) => {
  if (!r) return false;
  return Object.values(r).some((value) => !isNilOrEmpty(value));
};
