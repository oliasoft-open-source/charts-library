/**
 Function that returns an object containing all the values for a specific key in an array of objects,
 grouped by the unique values of another key in the same objects.
 @param {Array} data - An array of objects to search for the keys.
 @return {Object} - Returns an object with keys representing the unique values for the annotationAxis key in the data array,
 and values representing an array of all values for the value key in the data array.
 */
export const getAnnotationsData = (data) => {
  return data?.reduce((acc, obj) => {
    return {
      ...acc,
      [obj.annotationAxis]: [...(acc[obj.annotationAxis] || []), +obj.value],
    };
  }, {});
};
