import { defaultTranslations } from '../../constants/default-translations';
import { getTranslations } from './get-translations';

describe('getTranslations', () => {
  it('should return default getTranslations when no custom getTranslations are provided', () => {
    const result = getTranslations({});
    expect(result).toEqual(defaultTranslations);
  });

  it('should merge custom getTranslations with default getTranslations', () => {
    const customTranslations = {
      dragToZoom: 'Custom drag to zoom',
      doubleClickToReset: 'Custom double-click to reset',
    };

    const expectedResult = {
      ...defaultTranslations,
      ...customTranslations,
    };

    const result = getTranslations(customTranslations);
    expect(result).toEqual(expectedResult);
  });

  it('should use custom getTranslations when provided, overriding default getTranslations', () => {
    const customTranslations = {
      dragToZoom: 'Custom drag to zoom',
    };

    const expectedResult = {
      ...defaultTranslations,
      dragToZoom: customTranslations.dragToZoom,
    };

    const result = getTranslations(customTranslations);
    expect(result).toEqual(expectedResult);
  });
});
