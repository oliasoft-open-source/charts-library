import { defaultTranslations } from '../../constants/default-translations';

/**
 * Merges custom getTranslations with the default getTranslations.
 * If a custom translation is provided for a key, it will override the default one.
 * @param {object} translations - Custom getTranslations.
 * @returns {object} - The resulting getTranslations object, containing both default and custom getTranslations.
 */
export const getTranslations = (
  translations: Partial<Record<keyof typeof defaultTranslations, string>> = {},
) => {
  return (
    Object.keys(defaultTranslations) as Array<keyof typeof defaultTranslations>
  ).reduce(
    (acc, key) => ({
      ...acc,
      [key]: translations[key] || defaultTranslations[key],
    }),
    {},
  );
};
