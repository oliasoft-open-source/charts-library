import { Chart, LegendItem } from 'chart.js';

export const getGeneratedLabels = (chart: Chart): LegendItem[] =>
  chart?.options?.plugins?.legend?.labels?.generateLabels?.(chart) ?? [];
