import { Tick } from 'chart.js';
import { IAxisState, IState } from '../state/state.interfaces';
import {
  generateRandomColor,
  getAxisPosition,
} from '../../common/helpers/chart-utils';
import {
  COLORS,
  DEFAULT_FONT_SIZE,
  LOGARITHMIC_STEPS,
} from '../../common/helpers/chart-consts';
import { generateAxisId } from './line-chart-utils';
import { AxisType, ScaleType } from '../../common/helpers/enums';
import { formatAxisLabelNumbers } from './axis-formatting/axis-formatting';
import { ILineChartAxis, ILineChartOptions } from '../line-chart.interface';
import { UnusedParameter } from '../../common/common.interface';

const getLineChartAxis = (
  options: ILineChartOptions,
  axisType: AxisType,
  state: IState,
  currentScales?: ILineChartAxis,
  i = 0,
) => {
  const axisData = currentScales || options.axes[axisType][0] || {};
  const stateAxis = state?.axes.filter(
    (axis: IAxisState) => axis?.id?.startsWith(axisType),
  )[i];
  const ticksConfigFromProps =
    options?.scales?.[`${i === 0 ? axisType : axisType + i}`]?.ticks;
  const { additionalAxesOptions } = options;

  const getTicks = () => {
    const isLogarithmic =
      additionalAxesOptions?.chartScaleType === ScaleType.Logarithmic;
    const ticksFormattingCallback = (
      tick: number,
      _: UnusedParameter,
      ticks: Tick[],
    ) => {
      return isLogarithmic
        ? LOGARITHMIC_STEPS.includes(tick)
          ? formatAxisLabelNumbers(tick, ticks)
          : ''
        : formatAxisLabelNumbers(tick, ticks);
    };
    const stepSize = !isLogarithmic
      ? {
          stepSize:
            axisData.stepSize ??
            (axisType === AxisType.Y ? additionalAxesOptions.stepSize : null),
        }
      : {};
    const ticks = {
      ...stepSize,
      callback: ticksFormattingCallback,
      includeBounds: false, //OW-10088 disable irregular axis ticks
      ...ticksConfigFromProps,
      font: {
        size: DEFAULT_FONT_SIZE,
      },
    };
    return ticks;
  };

  return {
    display: true,
    type: additionalAxesOptions.chartScaleType,
    position: axisData.position,
    beginAtZero: additionalAxesOptions.beginAtZero,
    reverse: axisType === AxisType.Y ? additionalAxesOptions.reverse : false,
    suggestedMax: additionalAxesOptions.suggestedMax,
    suggestedMin: additionalAxesOptions.suggestedMin,
    min: stateAxis?.min ?? additionalAxesOptions?.range?.[axisType]?.min,
    max: stateAxis?.max ?? additionalAxesOptions?.range?.[axisType]?.max,
    title: {
      display: axisData.label?.length,
      text: axisData.label,
      padding: 0,
    },
    ticks: getTicks(),
    grid: {
      ...axisData.gridLines,
    },
  };
};

const getLineChartAxes = (
  options: ILineChartOptions,
  axisType: AxisType,
  state: IState,
) => {
  const axesData = options.axes[axisType] as ILineChartAxis[];
  const axes = axesData.reduce((acc, curr: ILineChartAxis, i: number) => {
    const axisData = curr;
    axisData.color = curr.color || COLORS[i] || generateRandomColor(COLORS);
    axisData.position = curr.position || getAxisPosition(axisType, i);

    const axis = getLineChartAxis(options, axisType, state, axisData, i);
    const axisId = generateAxisId(axisType, i, true);
    return { ...acc, [axisId]: axis };
  }, {});
  return axes;
};

const getLineChartScales = (options: ILineChartOptions, state: IState) => {
  const hasMultipleXAxes = options.axes.x?.length > 1;
  const hasMultipleYAxes = options.axes.y?.length > 1;

  const xAxes = hasMultipleXAxes
    ? getLineChartAxes(options, AxisType.X, state)
    : { x: getLineChartAxis(options, AxisType.X, state) };
  const yAxes = hasMultipleYAxes
    ? getLineChartAxes(options, AxisType.Y, state)
    : { y: getLineChartAxis(options, AxisType.Y, state) };

  return {
    ...xAxes,
    ...yAxes,
  };
};

export default getLineChartScales;
