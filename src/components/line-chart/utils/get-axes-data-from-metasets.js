import { getAnnotationsData } from './get-annotations-data';
import { getAxisTypeFromKey } from './line-chart-utils';

/**
 * This function takes a reference to a ChartJS instance, an array of axes and annotation data,
 * and returns a data structure that correlates each axis id with an array of unique data points
 * that are present in either the metasets of the chart or the annotations.
 *
 * @param {Object} chartRef - A reference to the ChartJS instance.
 * @param {Array} scalesKeys - An array of scales keys
 * @param {Object} annotationsData - An object with annotations data. Each key is an axis id and its value is an array of data points.
 *
 * @returns {Object} An object where each key is an axis id and its value is an array of unique data points
 * that are present in either the metasets of the chart or the annotations.
 */
export const getAxesDataFromMetasets = (
  chartRef,
  scalesKeys,
  annotationsData,
) => {
  if (!chartRef || !scalesKeys) return {};
  const metasets = chartRef?.current?.getSortedVisibleDatasetMetas() ?? [];
  const annotData = getAnnotationsData(annotationsData);

  return (
    metasets &&
    scalesKeys?.reduce((acc, key) => {
      const data = metasets
        .filter((dataset) => Object.values(dataset).includes(key))
        .flatMap((dataset) => dataset._parsed)
        .map((parsedData) => parsedData[getAxisTypeFromKey(key)])
        .concat(annotData?.[key] ?? []);

      return {
        ...acc,
        [key]: [...new Set(data)],
      };
    }, {})
  );
};
