import { Chart } from 'chart.js';
import { RefObject } from 'react';
import { ILineChartAxes, ILineChartAxis } from '../line-chart.interface';

export const getAxesRangesFromChart = (
  chartRef: RefObject<Chart>,
  axes: ILineChartAxes,
) => {
  const { scales = {} } = chartRef.current || {};
  return Object.entries(scales).map(([key, { min, max }], i) => {
    const axesArray: ILineChartAxis[] = [...axes.x, ...axes.y];
    const unit = axesArray?.[i]?.unit;
    return {
      id: key,
      min,
      max,
      ...(unit ? { unit } : {}),
    };
  });
};
