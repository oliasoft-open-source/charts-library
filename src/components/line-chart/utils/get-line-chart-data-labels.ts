import { TooltipItem } from 'chart.js';
import { AUTO } from '../../common/helpers/chart-consts';
import { getPosition } from './datalabels-alignment/get-datalabels-position';
import { ILineChartOptions } from '../line-chart.interface';
import { UnusedParameter } from '../../common/common.interface';

/**
 * adjusts the position of the label depends on chart area
 *
 * @param {import('../line-chart.interface').ILineChartOptions} options - line chart options object
 * @return {object} - returning position, if label exist in datasets item
 */
const getLineChartDataLabels = (options: ILineChartOptions) => {
  return options.graph.showDataLabels
    ? {
        display: AUTO,
        align: getPosition(),
        formatter: (_: UnusedParameter, context: TooltipItem<any>) =>
          context.dataset.data[context.dataIndex].label || '',
      }
    : { display: false };
};

export default getLineChartDataLabels;
