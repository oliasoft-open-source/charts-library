import { getAxesDataFromMetasets } from '../get-axes-data-from-metasets';

describe('getAxesDataFromMetasets', () => {
  const chartRef = {
    current: {
      getSortedVisibleDatasetMetas: () => [
        {
          _parsed: [
            { x: 5, y: 2 },
            { x: 25, y: 12 },
            { x: 45, y: 14 },
          ],
          xAxisID: 'x',
          yAxisID: 'y',
        },
        {
          _parsed: [
            { x: 10, y: 4 },
            { x: 75, y: 16 },
            { x: 105, y: 22 },
          ],
          xAxisID: 'x',
          yAxisID: 'y',
        },
      ],
    },
  };

  const scalesKeys = ['x', 'y'];

  const annotationsData = [
    { annotationAxis: 'x', value: '1' },
    { annotationAxis: 'y', value: '2' },
    { annotationAxis: 'x', value: '3' },
    { annotationAxis: 'y', value: '4' },
  ];

  test('returns the correct axes data', () => {
    const result = getAxesDataFromMetasets(
      chartRef,
      scalesKeys,
      annotationsData,
    );
    expect(result).toEqual({
      x: [5, 25, 45, 10, 75, 105, 1, 3],
      y: [2, 12, 14, 4, 16, 22],
    });
  });

  test('returns an empty object when no chartRef is given', () => {
    const result = getAxesDataFromMetasets(null, scalesKeys, annotationsData);
    expect(result).toEqual({});
  });

  test('returns an empty object when no scalesKeys are given', () => {
    const result = getAxesDataFromMetasets(chartRef, null, annotationsData);
    expect(result).toEqual({});
  });

  test('returns an object with empty arrays when no annotationsData is given', () => {
    const result = getAxesDataFromMetasets(chartRef, scalesKeys, null);
    expect(result).toEqual({
      x: [5, 25, 45, 10, 75, 105],
      y: [2, 12, 14, 4, 16, 22],
    });
  });
  test('correctly handles multiple x axes', () => {
    const multiAxesChartRef = {
      current: {
        getSortedVisibleDatasetMetas: () => [
          {
            _parsed: [
              { x: 5, x2: 2 },
              { x: 25, x2: 12 },
              { x: 45, x2: 14 },
            ],
            xAxisID: 'x',
          },
          {
            _parsed: [
              { x: 10, x2: 4 },
              { x: 75, x2: 16 },
              { x: 105, x2: 22 },
            ],
            xAxisID: 'x2',
          },
        ],
      },
    };

    const multiAxesScalesKeys = ['x', 'x2'];

    const result = getAxesDataFromMetasets(
      multiAxesChartRef,
      multiAxesScalesKeys,
      null,
    );
    expect(result).toEqual({
      x: [5, 25, 45],
      x2: [10, 75, 105],
    });
  });
});
