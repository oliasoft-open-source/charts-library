import { getAxisTypeFromKey } from '../line-chart-utils';

describe('getAxisTypeFromKey', () => {
  it('should return the non-numeric part of a string', () => {
    expect(getAxisTypeFromKey('x')).toBe('x');
    expect(getAxisTypeFromKey('x1')).toBe('x');
    expect(getAxisTypeFromKey('y')).toBe('y');
    expect(getAxisTypeFromKey('y123')).toBe('y');
  });

  it('should return an empty string when the input is all numeric', () => {
    expect(getAxisTypeFromKey('12345')).toBe('');
  });

  it('should return the whole string when there are no numbers', () => {
    expect(getAxisTypeFromKey('xyz')).toBe('xyz');
  });

  it('should handle empty strings', () => {
    expect(getAxisTypeFromKey('')).toBe('');
  });
});
