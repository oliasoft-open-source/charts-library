import { getAnnotationsData } from '../get-annotations-data';

describe('getAnnotationsData function', () => {
  it('should return an empty object when the input data is an empty array', () => {
    expect(getAnnotationsData([])).toEqual({});
  });

  it('should correctly group data by the annotationAxis property', () => {
    const data = [
      { annotationAxis: 'x', value: '1' },
      { annotationAxis: 'y', value: '2' },
      { annotationAxis: 'x', value: '3' },
      { annotationAxis: 'y', value: '4' },
    ];

    const expected = {
      x: [1, 3],
      y: [2, 4],
    };

    expect(getAnnotationsData(data)).toEqual(expected);
  });

  it('should correctly handle data with string values', () => {
    const data = [
      { annotationAxis: 'x', value: '10' },
      { annotationAxis: 'y', value: '20' },
      { annotationAxis: 'x', value: '30' },
      { annotationAxis: 'y', value: '40' },
    ];

    const expected = {
      x: [10, 30],
      y: [20, 40],
    };

    expect(getAnnotationsData(data)).toEqual(expected);
  });
});
