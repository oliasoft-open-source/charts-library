import { getUnitsFromLabel } from '../get-line-chart-tooltips';

describe('getUnitsFromLabel', () => {
  test('return units string if included in label', () => {
    const label = 'Depth [m]';
    expect(getUnitsFromLabel(label)).toBe('[m]');
  });

  test('return empty string if no units in label', () => {
    const label = 'Depth';
    expect(getUnitsFromLabel(label)).toBe('');
  });
});
