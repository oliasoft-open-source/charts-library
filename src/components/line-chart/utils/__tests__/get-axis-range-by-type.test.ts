import { RefObject } from 'react';
import { Chart } from 'chart.js';
import { ICommonAnnotationsData } from '../../../common/common.interface.ts';
import { getAxisRangeByType, TAxisType } from '../get-axis-range-by-type.ts';

describe('getAxisRangeByType', () => {
  const chartRef: Partial<RefObject<Chart>> = {
    current: {
      getSortedVisibleDatasetMetas: () => [
        {
          _parsed: [
            { x: 5, y: 2 },
            { x: 25, y: 12 },
            { x: 45, y: 14 },
          ],
          xAxisID: 'x',
          yAxisID: 'y',
        } as any,
        {
          _parsed: [
            { x: 10, y: 4 },
            { x: 75, y: 16 },
            { x: 105, y: 22 },
          ],
          xAxisID: 'x',
          yAxisID: 'y',
        } as any,
      ],
    } as any,
  };

  const annotationsData: ICommonAnnotationsData[] = [
    { annotationAxis: 'x', value: 1 },
    { annotationAxis: 'y', value: 2 },
    { annotationAxis: 'x', value: 3 },
    { annotationAxis: 'y', value: 4 },
  ] as any;

  test('returns the correct min and max values for x axis', () => {
    const axesType: TAxisType = 'x';
    const result = getAxisRangeByType(
      chartRef as any,
      axesType,
      annotationsData,
    );
    expect(result).toEqual({ min: 1, max: 105 });
  });

  test('returns the correct min and max values for y axis', () => {
    const axesType: TAxisType = 'y';
    const result = getAxisRangeByType(
      chartRef as any,
      axesType,
      annotationsData,
    );
    expect(result).toEqual({ min: 2, max: 22 });
  });

  test('returns null when no chartRef is given', () => {
    const axesType: TAxisType = 'x';
    const result = getAxisRangeByType(null as any, axesType, annotationsData);
    expect(result).toBeNull();
  });
});
