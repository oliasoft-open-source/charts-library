import { checkCustomOption } from './check-custom-option';
import { CUSTOM_OPTION } from '../enums';
import { TGeneratedLineChartDatasets } from '../../line-chart.interface';

describe('checkCustomOption function', () => {
  it('should return true if any dataset has a showLine key', () => {
    const datasets = [
      { label: 'Dataset 1', showLine: false, data: [] },
      { label: 'Dataset 2', data: [] },
    ];
    expect(checkCustomOption(datasets, CUSTOM_OPTION)).toBe(true);
  });

  it('should return true if any dataset has a showPoints key', () => {
    const datasets = [
      { label: 'Dataset 1', data: [] },
      { label: 'Dataset 2', showPoints: false, data: [] },
    ];
    expect(checkCustomOption(datasets, CUSTOM_OPTION)).toBe(true);
  });

  it('should return false if no dataset has showLine or showPoints key', () => {
    const datasets = [
      { label: 'Dataset 1', data: [] },
      { label: 'Dataset 2', data: [] },
    ];
    expect(checkCustomOption(datasets, CUSTOM_OPTION)).toBe(false);
  });

  it('should return false if datasets array is empty', () => {
    const datasets: TGeneratedLineChartDatasets = [];
    expect(checkCustomOption(datasets, CUSTOM_OPTION)).toBe(false);
  });

  it('should return true if a single dataset has showLine key', () => {
    const dataset = { label: 'Dataset 1', showLine: false, data: [] };
    expect(checkCustomOption(dataset, CUSTOM_OPTION)).toBe(true);
  });

  it('should return false if a single dataset does not have showLine or showPoints key', () => {
    const dataset = { label: 'Dataset 1', data: [] };
    expect(checkCustomOption(dataset, CUSTOM_OPTION)).toBe(false);
  });
});
