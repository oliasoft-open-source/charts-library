import { has, isArray, isEmpty, some } from 'lodash';
import {
  IGeneratedLineChartDataset,
  TGeneratedLineChartDatasets,
} from '../../line-chart.interface';

type TCustomOptionType = { [key: string]: string } | string | string[];
type TDataType =
  | IGeneratedLineChartDataset
  | Partial<IGeneratedLineChartDataset>
  | TGeneratedLineChartDatasets
  | Partial<TGeneratedLineChartDatasets>
  | any[];

/**
 * Checks if a single dataset or an array of datasets has a custom option.
 * Iterates through each custom option defined in the customOptions parameter.
 * Returns true if any dataset has any custom option, otherwise returns false.
 */
export const checkCustomOption = (
  data: TDataType,
  customOptions: TCustomOptionType,
): boolean => {
  if (isEmpty(data) || isEmpty(customOptions)) return false;

  const checkOption = (dataset: Partial<IGeneratedLineChartDataset>) => {
    if (typeof customOptions === 'string') {
      return dataset && has(dataset, customOptions);
    } else if (isArray(customOptions)) {
      return (
        dataset &&
        some(customOptions, (option) => dataset && has(dataset, option))
      );
    } else {
      return (
        dataset &&
        some(
          Object.values(customOptions),
          (option) => dataset && has(dataset, option),
        )
      );
    }
  };

  if (isArray(data)) {
    return some(data, checkOption);
  } else {
    return checkOption(data);
  }
};
