import { autoScale } from './axis-scales';

const options = {
  axes: {
    x: [
      {
        min: 5,
        max: 105,
      },
    ],
    y: [
      {
        min: 2,
        max: 22,
      },
    ],
  },
  additionalAxesOptions: {
    beginAtZero: false,
  },
};

const state = {
  axes: [
    {
      id: 'x',
    },
    {
      id: 'y',
    },
  ],
};

const chartRef = {
  current: {
    getSortedVisibleDatasetMetas: () => [
      {
        _parsed: [
          {
            x: 5,
            y: 2,
          },
          {
            x: 25,
            y: 12,
          },
          {
            x: 45,
            y: 14,
          },
        ],
        xAxisID: 'x',
        yAxisID: 'y',
      },
      {
        _parsed: [
          {
            x: 10,
            y: 4,
          },
          {
            x: 75,
            y: 16,
          },
          {
            x: 105,
            y: 22,
          },
        ],
        xAxisID: 'x',
        yAxisID: 'y',
      },
    ],
  },
};

const data = [
  {
    data: [
      {
        x: 5,
        y: 2,
      },
      {
        x: 25,
        y: 12,
      },
      {
        x: 45,
        y: 14,
      },
    ],
  },
  {
    data: [
      {
        x: 10,
        y: 4,
      },
      {
        x: 75,
        y: 16,
      },
      {
        x: 105,
        y: 22,
      },
    ],
  },
];

describe('Line Chart auto scale', () => {
  test('by default axis min and max or not set', () => {
    const result = autoScale(options, state, data, chartRef);
    expect(result.x.min).toBeUndefined();
    expect(result.x.max).toBeUndefined();
    expect(result.y.min).toBeUndefined();
    expect(result.y.max).toBeUndefined();
  });
  test('setting autoAxisPadding adds padding to axis range', () => {
    const withAutoAxisScale = {
      ...options,
      additionalAxesOptions: {
        ...options.additionalAxesOptions,
        autoAxisPadding: true,
      },
    };
    const result = autoScale(withAutoAxisScale, state, data, chartRef);
    expect(result.x.min).toBe(0);
    expect(result.x.max).toBe(110);
    expect(result.y.min).toBe(1);
    expect(result.y.max).toBe(23);
  });
  test('supports multiple x axes', () => {
    const optionsWithMultipleXAxes = {
      ...options,
      additionalAxesOptions: {
        ...options.additionalAxesOptions,
        autoAxisPadding: true,
      },
      axes: {
        ...options.axes,
        x: [
          ...options.axes.x,
          {
            min: 110,
            max: 130,
          },
        ],
      },
    };

    const stateWithMultipleXAxes = {
      ...state,
      axes: [...state.axes, { id: 'x2' }],
    };

    const chartRefWithMultipleXAxes = {
      current: {
        getSortedVisibleDatasetMetas: () => [
          ...chartRef.current.getSortedVisibleDatasetMetas(),
          {
            _parsed: [
              { x: 110, y: 5 },
              { x: 120, y: 10 },
              { x: 130, y: 15 },
            ],
            xAxisID: 'x2',
            yAxisID: 'y',
          },
        ],
      },
    };

    const result = autoScale(
      optionsWithMultipleXAxes,
      stateWithMultipleXAxes,
      data,
      chartRefWithMultipleXAxes,
    );
    expect(result.x2.min).toBeDefined();
    expect(result.x2.max).toBeDefined();
  });
});
