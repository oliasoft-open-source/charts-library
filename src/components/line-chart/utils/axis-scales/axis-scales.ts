import { RefObject } from 'react';
import { Chart } from 'chart.js';
import { isNil } from 'lodash';
import { getAxisRangeByType, TAxisType } from '../get-axis-range-by-type';
import { ICommonScales } from '../../../common/common.interface';
import getLineChartScales from '../get-line-chart-scales';
import { estimateDataSeriesHaveCloseValues } from '../../../common/helpers/range/estimate-data-series-have-close-values';
import { getSuggestedAxisRange } from '../../../common/helpers/range/range';
import { getAxesDataFromMetasets } from '../get-axes-data-from-metasets';
import {
  ILineChartOptions,
  TGeneratedLineChartDatasets,
} from '../../line-chart.interface';
import { IState } from '../../state/state.interfaces';

interface IScales {
  [key: string]: ICommonScales;
}

const shouldCalculate = (min?: any, max?: any) => !isNil(min) || !isNil(max);

/**
 * Auto scales axis ranges (mix, max) if no explicit range is set
 * - overrides some edge cases not handled well by default chart.js
 * - supports optional padding when `autoAxisPadding` is set
 * - otherwise does not set min/max (falls back to chart.js default)
 */
export const autoScale = (
  options: ILineChartOptions,
  state: IState,
  generatedDatasets: TGeneratedLineChartDatasets,
  chartRef: RefObject<Chart>,
) => {
  const {
    additionalAxesOptions,
    annotations: { annotationsData = [], controlAnnotation = false } = {},
  } = options || {};

  const scales: IScales = getLineChartScales(options, state) || {};
  const datasets = controlAnnotation
    ? generatedDatasets.filter(({ isAnnotation }) => !isAnnotation)
    : generatedDatasets;

  if (
    !additionalAxesOptions?.autoAxisPadding &&
    !estimateDataSeriesHaveCloseValues(datasets)
  ) {
    return scales;
  }

  const scalesKeys = (Object.keys(scales) ?? []) as TAxisType[];
  const data = getAxesDataFromMetasets(
    chartRef,
    scalesKeys,
    annotationsData,
  ) as Record<string, any>;

  const adjustedScales =
    data &&
    scalesKeys?.reduce((acc, key) => {
      const scale = scales[key];
      const { min: propMin = undefined, max: propMax = undefined } = scale;
      const { min: calcMin, max: calcMax } =
        getAxisRangeByType(chartRef, key, annotationsData) ?? {};
      const { min: suggestedMin, max: suggestedMax } = getSuggestedAxisRange({
        data: data[key],
        beginAtZero: additionalAxesOptions?.beginAtZero,
        autoAxisPadding: additionalAxesOptions?.autoAxisPadding,
      });

      const res = {
        [key]: {
          ...scale,
          min:
            propMin ??
            (shouldCalculate(propMin, propMax) ? calcMin : suggestedMin),
          max:
            propMax ??
            (shouldCalculate(propMin, propMax) ? calcMax : suggestedMax),
        },
      };

      return { ...acc, ...res };
    }, {});

  return adjustedScales ?? scales;
};
