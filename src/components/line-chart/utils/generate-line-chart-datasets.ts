import {
  ANNOTATION_DASH,
  BORDER_WIDTH,
  COLORS,
} from '../../common/helpers/chart-consts';
import {
  BORDER_JOIN_STYLE,
  DEFAULT_BACKGROUND_COLOR,
  DEFAULT_BORDER_WIDTH,
  DEFAULT_HOVER_RADIUS,
  DEFAULT_POINT_RADIUS,
} from '../constants/line-chart-consts';
import { generateRandomColor } from '../../common/helpers/chart-utils';
import { IState } from '../state/state.interfaces';
import {
  ILineChartOptions,
  TGeneratedLineChartDatasets,
} from '../line-chart.interface';
import { checkCustomOption } from './check-custom-option/check-custom-option';
import { CUSTOM_OPTION } from './enums';

/**
 * Generates line chart datasets based on the provided datasets, state, and options.
 */
export const generateLineChartDatasets = (
  datasets: Array<any>,
  state: IState,
  options: ILineChartOptions,
  { label }: { label: string },
): TGeneratedLineChartDatasets => {
  const copyDataset = [...(datasets ?? [])];
  const { annotations, graph } = options ?? {};
  const {
    controlAnnotation,
    showAnnotations,
    annotationsData = [],
  } = annotations ?? {};
  // Add annotations to dataset to have them appear in legend.
  if (controlAnnotation && showAnnotations) {
    const annotationDatasets = annotationsData.map((ann, index: number) => {
      const { type, ...annotation } = ann;
      const color = annotation.color ?? COLORS[index];
      const borderDash = ANNOTATION_DASH;
      const borderWidth = BORDER_WIDTH.INITIAL;
      // Create an annotation dataset
      return {
        ...annotation,
        isAnnotation: true,
        annotationType: type,
        label: annotation.label,
        annotationIndex: index,
        backgroundColor: color,
        pointBackgroundColor: color,
        borderColor: color,
        borderDash,
        borderWidth,
        data: [{}],
      };
    });

    // Add annotation datasets to the copyDataset array
    copyDataset.push(...annotationDatasets);
  }

  // Map over the copyDataset array and generate line chart datasets with applied settings and configurations
  return copyDataset?.map((line, i) => {
    // Destructure properties from the line, state, and graph objects
    const {
      formation,
      data = [],
      pointRadius: pointRadiusProp,
      pointHoverRadius,
      borderWidth,
      borderColor,
      backgroundColor,
      pointBackgroundColor,
      borderDash,
      showPoints: customShowPoints,
      showLine: customShowLine,
    } = line ?? {};
    const { lineEnabled, pointsEnabled, axes = [] } = state ?? {};
    const { lineTension, spanGaps } = graph ?? {};

    // Adjust the start and end points of the line if it has a formation flag
    if (formation) {
      const axesMin = axes[0]?.min;
      const axesMax = axes[0]?.max;
      const [startPoint, endPoint] = data;

      if (axesMin && startPoint?.x) {
        data[0].x = Math.max(axesMin, startPoint.x);
      }

      if (axesMax && endPoint?.x) {
        data[2].x = Math.min(axesMax, endPoint.x);
      }
    }

    // Remove invalid falsy data points (OW-9855)
    const hasCustomOpt = checkCustomOption(line, CUSTOM_OPTION);
    const filteredData = data.filter(Boolean) || [];
    const isSinglePoint = filteredData?.length === 1;
    let pointState, lineState;

    // 1. Use custom point option if defined,
    // 2. Otherwise, always show points if lines are disabled
    // 3. Otherwise, use global setting
    if (customShowPoints !== undefined) {
      pointState = customShowPoints;
    } else if (customShowLine === false) {
      pointState = true;
    } else {
      pointState = pointsEnabled || isSinglePoint;
    }

    // 1. Use custom line option if defined,
    // 2. Otherwise, always show line if points are disabled
    // 3. Otherwise, use global setting
    if (customShowLine !== undefined) {
      lineState = customShowLine;
    } else if (customShowPoints === false) {
      lineState = true;
    } else {
      lineState = lineEnabled;
    }

    const linePointRadius = parseFloat(pointRadiusProp) || DEFAULT_POINT_RADIUS;

    const hoverRadius = parseFloat(pointHoverRadius) || DEFAULT_HOVER_RADIUS;
    const indexedColor = COLORS[i];

    // Return the dataset with applied settings and configurations
    return {
      ...line,
      label: line?.label ?? `${label} ${i + 1}`,
      data: filteredData,
      showLine: lineState,
      lineTension: line?.lineTension ?? lineTension,
      spanGaps,
      borderWidth: parseFloat(borderWidth) || DEFAULT_BORDER_WIDTH,
      borderDash: borderDash || [],
      borderJoinStyle: BORDER_JOIN_STYLE,
      borderColor: borderColor ?? indexedColor ?? generateRandomColor(COLORS),
      backgroundColor: backgroundColor ?? DEFAULT_BACKGROUND_COLOR,
      pointBackgroundColor:
        pointBackgroundColor ?? indexedColor ?? generateRandomColor(COLORS),
      pointRadius: pointState ? linePointRadius : 0,
      pointHoverRadius: hoverRadius,
      pointHitRadius: line?.pointHitRadius ?? hoverRadius,
      ...(hasCustomOpt ? { hasCustomOpt } : {}),
    };
  });
};
