import { MutableRefObject, useMemo, useReducer, useRef } from 'react';
import {
  CategoryScale,
  Chart as ChartJS,
  Filler,
  Legend,
  LinearScale,
  LineElement,
  LogarithmicScale,
  PointElement,
  Title,
  Tooltip,
} from 'chart.js';
import { Line } from 'react-chartjs-2';
import zoomPlugin from 'chartjs-plugin-zoom';
import dataLabelsPlugin from 'chartjs-plugin-datalabels';
import annotationPlugin from 'chartjs-plugin-annotation';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import styles from './line-chart.module.less';
import { reducer } from './state/line-chart-reducer';
import initialState from './state/initial-state';
import Controls from './controls/controls';
import { getDefaultProps } from './line-chart-get-default-props';
import { getClassName, setDefaultTheme } from '../common/helpers/chart-utils';
import { AUTO } from '../common/helpers/chart-consts';
import { generateLineChartDatasets } from './utils/generate-line-chart-datasets';
import { useChartFunctions } from './hooks/use-chart-functions';
import { useChartOptions } from './hooks/use-chart-options';
import { useChartPlugins } from './hooks/use-chart-plugins';
import { generateKey } from './utils/line-chart-utils';
import { useChartState } from './hooks/use-chart-state';
import { chartAreaTextPlugin } from './plugins/chart-area-text-plugin';
import { annotationDraggerPlugin } from '../common/plugins/annotation-dragger-plugin/annotation-dragger-plugin';
import { getConfig } from './initialize/config';
import { ILineChartOptions, ILineChartProps } from './line-chart.interface';
import CustomLegend from '../common/legend-component/legend';
import { LegendProvider } from '../common/legend-component/state/legend-context';
import { ChartType } from '../common/enums';

ChartJS.register(
  LinearScale,
  PointElement,
  LineElement,
  CategoryScale,
  LogarithmicScale,
  Legend,
  Tooltip,
  Title,
  Filler,
  zoomPlugin,
  dataLabelsPlugin,
  annotationPlugin,
  chartAreaTextPlugin,
  annotationDraggerPlugin,
);

const LineChart = (props: ILineChartProps) => {
  setDefaultTheme();
  const chartRef: MutableRefObject<null> = useRef(null);
  const { table } = props ?? {};
  const { translations, languageKey } = getConfig();
  const chart = getDefaultProps(props);
  const {
    data: { datasets } = { datasets: [] },
    options,
    testId,
    persistenceId,
    controlsPortalId,
  } = chart;
  const { annotations, axes, chartStyling, graph } = options ?? {};
  const showCustomLegend =
    !options?.legend?.customLegend?.customLegendContainerID;

  const [state, dispatch] = useReducer(
    reducer,
    {
      options,
      persistenceId,
    },
    initialState,
  );

  const generatedDatasets = useMemo(() => {
    return generateLineChartDatasets(datasets, state, options, translations);
  }, [state.lineEnabled, state.pointsEnabled, axes, annotations, graph]);

  // Call the custom hooks.
  useChartState({
    chartRef,
    options,
    state,
    dispatch,
    persistenceId,
    generatedDatasets,
  });

  const { resetZoom, handleKeyDown, handleKeyUp } = useChartFunctions({
    chartRef,
    state,
    options,
    dispatch,
    generatedDatasets,
  });

  const useOptions = useChartOptions({
    chartRef,
    state,
    options,
    dispatch,
    generatedDatasets,
  });

  const usePlugins = useChartPlugins({ options, resetZoom });

  return (
    <div
      key={generateKey([
        state?.enableDragPoints,
        state?.enableDragAnnotation,
        state?.zoomEnabled,
        state?.panEnabled,
        languageKey,
      ])}
      className={getClassName(chartStyling, styles)}
      style={{
        width: chartStyling.width || AUTO,
        height: chartStyling.height || AUTO,
      }}
      tabIndex={0} //eslint-disable-line jsx-a11y/no-noninteractive-tabindex
      onKeyDown={handleKeyDown}
      onKeyUp={handleKeyUp}
      data-testid={testId}
    >
      <Controls
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...props}
        chartRef={chartRef}
        state={state}
        options={options as ILineChartOptions}
        dispatch={dispatch}
        generatedDatasets={generatedDatasets}
        translations={translations}
        controlsPortalId={controlsPortalId}
      />
      {table && state.showTable ? (
        <div className={styles.table}>{table}</div>
      ) : (
        <DndProvider backend={HTML5Backend} context={window}>
          <div className={styles.canvas} id="canvas">
            <Line
              ref={chartRef}
              data={{
                datasets: generatedDatasets,
              }}
              options={useOptions}
              plugins={usePlugins}
            />
            {showCustomLegend && !!generatedDatasets.length && (
              <CustomLegend
                chartRef={chartRef}
                legendConfig={{
                  options,
                  generatedDatasets,
                  chartType: ChartType.LINE,
                }}
              />
            )}
          </div>
        </DndProvider>
      )}
    </div>
  );
};

const LineChartWithLegend = (props: ILineChartProps) => {
  const { options } = getDefaultProps(props);
  return (
    <LegendProvider options={options}>
      <LineChart {...props} />
    </LegendProvider>
  );
};

export { LineChartWithLegend as LineChart };
