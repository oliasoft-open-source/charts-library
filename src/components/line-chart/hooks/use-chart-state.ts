import { Dispatch, RefObject, useEffect, useMemo, useRef } from 'react';
import isEqual from 'fast-deep-equal';
import { Chart, CoreScaleOptions, Scale } from 'chart.js';
import { storeChartStateInStorage } from '../state/manage-state-in-local-storage';
import {
  SAVE_INITIAL_AXES_RANGES,
  UPDATE_AXES_RANGES,
} from '../state/action-types';
import { IAxisState, IState } from '../state/state.interfaces';
import {
  ILineChartDataset,
  ILineChartOptions,
  ILineRange,
} from '../line-chart.interface';
import { isRangeValid } from '../utils/line-chart-utils';
import { getAxisRangeByType, TAxisType } from '../utils/get-axis-range-by-type';
import { ICommonAnnotationsData } from '../../common/common.interface';

const RANGE_GAP = 1;

/**
 * Hook for toggling the visibility of the custom legend.
 *
 * @param memoState - The memoized state object.
 * @param memoOptions - The memoized options object.
 */
const useToggleCustomLegendVisibility = (
  memoState: IState,
  memoOptions: ILineChartOptions,
) => {
  useEffect(() => {
    if (memoOptions?.legend?.customLegend?.customLegendPlugin) {
      // Get the parent element of the custom legend container and toggle its visibility based on the state.
      const parent: HTMLElement | null = document.getElementById(
        memoOptions.legend.customLegend.customLegendContainerID,
      );
      if (parent !== null) {
        parent.style.visibility = memoState.legendEnabled
          ? 'visible'
          : 'hidden';
      }
    }
  }, [
    memoOptions?.legend?.customLegend?.customLegendPlugin,
    memoState.legendEnabled,
  ]);
};

/**
 * Hook for storing the chart state in local storage.
 *
 * @param memoState - The memoized state object.
 * @param persistenceId - The chart persistenceId.
 */
const useStoreChartStateInStorage = (
  memoState: IState,
  persistenceId?: string,
) => {
  useEffect(() => {
    // Store the chart state in local storage.
    storeChartStateInStorage(memoState, persistenceId);
  }, [
    memoState.panEnabled,
    memoState.lineEnabled,
    memoState.pointsEnabled,
    memoState.legendEnabled,
    memoState.enableDragPoints,
    memoState.zoomEnabled,
  ]);
};

/**
 * Hook for updating the axes ranges in the chart state.
 *
 * @param range - The range to the chart.
 * @param dispatch - The dispatch function for state management.
 */
const useUpdateAxesRanges = ({
  chartRef,
  range,
  state,
  dispatch,
  annotationsData,
}: {
  chartRef: RefObject<Chart>;
  range?: ILineRange;
  state: IState;
  dispatch: Dispatch<any>;
  annotationsData: ICommonAnnotationsData[];
}) => {
  const prevAxes = useRef<IAxisState[] | null>(state.axes);

  useEffect(() => {
    if (range && isRangeValid(range)) {
      const newAxes = Object.entries(range).map(([key, { min, max }]) => {
        const { min: minFromData = 0, max: maxFromData = 0 } =
          getAxisRangeByType(chartRef, key as TAxisType, annotationsData) ?? {};

        let finalMin = min ?? minFromData;
        let finalMax = max ?? maxFromData;

        if (finalMin === finalMax) {
          finalMax += RANGE_GAP;
        }

        return {
          id: key,
          min: finalMin,
          max: finalMax,
        };
      });

      const mergedAxes = [...(prevAxes.current ?? [])].map((axis) => {
        const newAxis = newAxes.find((a) => a.id === axis.id);
        return newAxis ? { ...axis, ...newAxis } : axis;
      });

      if (!isEqual(mergedAxes, prevAxes.current)) {
        dispatch({
          type: UPDATE_AXES_RANGES,
          payload: { axes: mergedAxes },
        });
        prevAxes.current = mergedAxes;
      }
    }
  }, [range]);
};

interface IExtendedScale extends Scale<CoreScaleOptions> {
  _range?: { min: number; max: number };
}

/**
 * A custom hook to save the initial axes ranges of a chart when the generated datasets change.
 */
const useSaveInitialAxesRanges = (
  chartRef: RefObject<Chart>,
  dispatch: Dispatch<any>,
  generatedDatasets: ILineChartDataset[],
) => {
  const prevDatasets = useRef<ILineChartDataset[] | null>(null);

  const getDatasetsSignature = (datasets: ILineChartDataset[]) => {
    if (!datasets.length) return '';

    const datasetSignatures = datasets.map((dataset) => {
      const data = dataset.data ?? [];
      if (!data.length) return 'empty';

      const firstDataPoint = data.at(0);
      const lastDataPoint = data.at(-1);
      const lengthChecksum = data.length;

      return firstDataPoint && lastDataPoint
        ? `${firstDataPoint.x},${firstDataPoint.y}-${lastDataPoint.x},${lastDataPoint.y}-${lengthChecksum}`
        : 'invalid';
    });

    return `${datasets.length}-${datasetSignatures.join('|')}`;
  };

  useEffect(() => {
    if (chartRef?.current?.scales) {
      const prevHash = prevDatasets.current
        ? getDatasetsSignature(prevDatasets.current)
        : '';
      const currentHash = getDatasetsSignature(generatedDatasets);

      if (prevHash !== currentHash) {
        const scales = chartRef.current.scales as Record<
          string,
          IExtendedScale
        >;
        const initialAxesRanges = Object.entries(scales).map(
          ([id, { min, max, _range: range }]) => ({
            id,
            min: range?.min ?? min,
            max: range?.max ?? max,
          }),
        );

        dispatch({
          type: SAVE_INITIAL_AXES_RANGES,
          payload: { initialAxesRanges },
        });

        prevDatasets.current = generatedDatasets;
      }
    }
  }, [chartRef?.current?.scales, generatedDatasets]);
};

interface IUseChartState {
  chartRef: RefObject<Chart>;
  options: ILineChartOptions;
  state: IState;
  dispatch: Dispatch<any>;
  persistenceId?: string;
  generatedDatasets: ILineChartDataset[];
}

/**
 * Hook for managing the state of the chart.
 */
export const useChartState = ({
  chartRef,
  options,
  state,
  dispatch,
  persistenceId,
  generatedDatasets,
}: IUseChartState) => {
  useEffect(() => {
    return () => {
      if (chartRef?.current) {
        const chart = chartRef?.current;
        chart?.destroy();
      }
    };
  }, []);

  const memoState = useMemo(() => state, [state]);
  const memoOptions = useMemo(() => options, [options]);
  const {
    additionalAxesOptions: { range = undefined },
    annotations: { annotationsData = [] } = {},
  } = memoOptions;

  useStoreChartStateInStorage(memoState, persistenceId);
  useToggleCustomLegendVisibility(memoState, memoOptions);
  useUpdateAxesRanges({
    range,
    state: memoState,
    dispatch,
    chartRef,
    annotationsData,
  });
  useSaveInitialAxesRanges(chartRef, dispatch, generatedDatasets);
};
