import {
  Dispatch,
  KeyboardEvent,
  RefObject,
  SetStateAction,
  useCallback,
} from 'react';
import { Chart, ChartEvent } from 'chart.js';
import {
  ICommonDataValue,
  ICommonHoveredItems,
} from '../../common/common.interface';
import {
  ILineChartAxes,
  ILineChartDataset,
  ILineChartOptions,
  TGeneratedLineChartDatasets,
} from '../line-chart.interface';
import { IAxisState, IState } from '../state/state.interfaces';
import { AxisType, Key, PanZoomMode } from '../../common/helpers/enums';
import { generateAxisId } from '../utils/line-chart-utils';
import { RESET_AXES_RANGES, UPDATE_AXES_RANGES } from '../state/action-types';
import { downloadPgn } from '../../common/helpers/download-pgn';

interface IUseChartFunctions {
  chartRef: RefObject<Chart>;
  state: IState;
  options: ILineChartOptions;
  dispatch: Dispatch<any>;
  generatedDatasets: TGeneratedLineChartDatasets;
}
/**
 * Custom hook that encapsulates the chart-related functions and their dependencies
 */
export const useChartFunctions = ({
  chartRef,
  state,
  options,
  dispatch,
  generatedDatasets,
}: IUseChartFunctions) => {
  const {
    interactions: { onHover: onPointHover, onUnhover: onPointUnhover },
    axes,
  } = options;

  /**
   * Resets the chart zoom and updates the axes ranges
   * @returns {Function} Memoized resetZoom function
   */
  const resetZoom = useCallback(() => {
    const chartInstance = chartRef?.current;
    chartInstance?.resetZoom();
    dispatch({ type: 'RESET_AXES_RANGES' });
  }, [chartRef]);

  /**
   * Handles the hover event on chart points
   */
  const onHover = useCallback(
    (
      hoveredPoint: ICommonDataValue | null,
      setHoveredPoint: Dispatch<SetStateAction<ICommonDataValue | null>>,
    ) => {
      return (evt: ChartEvent, hoveredItems: ICommonHoveredItems[]) => {
        if (!hoveredItems?.length && onPointUnhover && hoveredPoint) {
          setHoveredPoint(null);
          onPointUnhover(evt);
        }

        if (hoveredItems?.length && onPointHover) {
          const { index, datasetIndex } = hoveredItems[0];
          const dataset: ILineChartDataset = generatedDatasets[datasetIndex];
          const point = dataset?.data[index];

          if (point && hoveredPoint !== point) {
            setHoveredPoint(point);
            onPointHover(evt, datasetIndex, index, generatedDatasets);
          }
        }
      };
    },
    [],
  );

  /**
   * Handles the download of the chart as an image
   * @returns {Function} Memoized handleDownload function
   */
  const handleDownload = useCallback(
    () => downloadPgn(chartRef, state),
    [chartRef, state.axes],
  );

  /**
   * Handles the key down event for the chart
   * @returns {Function} Memoized handleKeyDown function
   */
  const handleKeyDown = useCallback(
    (event: KeyboardEvent<HTMLDivElement>) => {
      if (event.key === Key.Shift) {
        const chart = chartRef.current;
        const zoom = chart?.config?.options?.plugins?.zoom;
        if (zoom?.zoom?.mode && zoom?.pan?.mode) {
          zoom.zoom.mode = PanZoomMode.Y;
          zoom.pan.mode = PanZoomMode.Y;
          chart?.update();
        }
      }
    },
    [chartRef],
  );

  /**
   * Handles the key up event for the chart
   * @returns {Function} Memoized handleKeyUp function
   */
  const handleKeyUp = useCallback(
    (event: KeyboardEvent<HTMLDivElement>) => {
      if (event.key === Key.Shift) {
        const chart = chartRef.current;
        const zoom = chart?.config?.options?.plugins?.zoom;
        if (zoom?.zoom?.mode && zoom?.pan?.mode) {
          zoom.zoom.mode = PanZoomMode.XY;
          zoom.pan.mode = PanZoomMode.XY;
          chart?.update();
        }
      }
    },
    [chartRef],
  );

  /**
   * Get the controls axes labels based on the propsAxes.
   * @param {Object} propsAxes - Props axes object.
   * @returns {Array} Array of controls axes labels.
   */
  const getControlsAxesLabels = useCallback(
    (propsAxes: ILineChartAxes) => {
      if (!Object.keys(propsAxes)?.length) {
        return [];
      }

      const getAxesLabels = (axes: ILineChartAxes, axisType: string) => {
        if (!axes[axisType] || !axes[axisType]?.length) {
          return [];
        } else {
          return axes[axisType].map((axisObj: IAxisState, index: number) => {
            return {
              id: generateAxisId(axisType, index, axes[axisType].length > 1),
              label: axisObj?.label || '',
            };
          });
        }
      };

      const axesLabels = [
        ...getAxesLabels(propsAxes, AxisType.X),
        ...getAxesLabels(propsAxes, AxisType.Y),
      ];
      return axesLabels;
    },
    [axes],
  );

  /**
   * Update axes ranges from the chart.
   * @param {Object} chart - Chart instance.
   */
  const updateAxesRangesFromChart = useCallback(() => {
    const { scales = {} } = chartRef?.current || {};
    const axes = Object.entries(scales)?.map(([key, { min, max }]) => {
      return {
        id: key,
        min: min ?? 0,
        max: max ?? 0,
      };
    });
    dispatch({
      type: UPDATE_AXES_RANGES,
      payload: { axes },
    });
  }, [axes]);

  /**
   * Handler for resetting axes ranges.
   * @type {Function}
   */
  const onResetAxes = useCallback(() => {
    // Dispatch the RESET_AXES_RANGES action
    dispatch({ type: RESET_AXES_RANGES });
  }, []);

  /**
   * Handler for updating axes ranges.
   */
  const onUpdateAxes = useCallback(({ axes }: { axes: IAxisState }) => {
    // Dispatch the UPDATE_AXES_RANGES action with payload
    dispatch({ type: UPDATE_AXES_RANGES, payload: { axes } });
  }, []);

  return {
    resetZoom,
    onHover,
    handleDownload,
    handleKeyDown,
    handleKeyUp,
    controlsAxesLabels: getControlsAxesLabels(axes),
    updateAxesRangesFromChart,
    onResetAxes,
    onUpdateAxes,
  };
};
