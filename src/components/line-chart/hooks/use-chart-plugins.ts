import { useCallback, useMemo } from 'react';
import { debounce } from 'lodash';
import { ILineChartOptions } from '../line-chart.interface';
import { getPlugins } from '../../common/helpers/chart-utils';
import { DOUBLE_CLICK } from '../constants/line-chart-consts';
import { UnusedParameter } from '../../common/common.interface';

interface IUseChartPlugins {
  options: ILineChartOptions;
  resetZoom: () => void;
}

interface IEvent {
  type: string;
}

interface IArgs {
  event: IEvent;
}

export const useChartPlugins = ({ options, resetZoom }: IUseChartPlugins) => {
  const { graph, legend } = options;

  const debouncedResetZoom = useCallback(
    debounce(() => {
      resetZoom();
    }, 100), // 100ms debounce delay
    [resetZoom],
  );

  const handleDoubleClick = useCallback(
    (_: UnusedParameter, args: IArgs) => {
      const { event } = args;
      if (event.type === DOUBLE_CLICK) {
        debouncedResetZoom();
      }
    },
    [debouncedResetZoom],
  );

  return useMemo(() => {
    return [
      ...getPlugins(graph, legend),
      {
        id: 'customEventCatcher',
        beforeEvent: handleDoubleClick,
      },
    ];
  }, [handleDoubleClick]);
};
