import { Dispatch, useCallback } from 'react';
import {
  DISABLE_DRAG_OPTIONS,
  TOGGLE_DRAG_ANNOTATION,
  TOGGLE_DRAG_POINTS,
  TOGGLE_LEGEND,
  TOGGLE_LINE,
  TOGGLE_PAN,
  TOGGLE_POINTS,
  TOGGLE_TABLE,
  TOGGLE_ZOOM,
} from '../state/action-types';

export const useToggleHandlers = (dispatch: Dispatch<any>) => {
  const createToggleHandler = useCallback(
    (type: string) => () => {
      dispatch({ type });
    },
    [dispatch],
  );

  const toggleHandlers = {
    onToggleLegend: createToggleHandler(TOGGLE_LEGEND),
    onToggleLine: createToggleHandler(TOGGLE_LINE),
    onTogglePan: createToggleHandler(TOGGLE_PAN),
    onTogglePoints: createToggleHandler(TOGGLE_POINTS),
    onToggleTable: createToggleHandler(TOGGLE_TABLE),
    onToggleZoom: createToggleHandler(TOGGLE_ZOOM),
    onToggleDragPoints: createToggleHandler(TOGGLE_DRAG_POINTS),
    onToggleDragAnnotation: createToggleHandler(TOGGLE_DRAG_ANNOTATION),
    onDisableDragOptions: createToggleHandler(DISABLE_DRAG_OPTIONS),
  };

  return toggleHandlers;
};
