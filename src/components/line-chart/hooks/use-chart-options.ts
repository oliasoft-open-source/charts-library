import { Dispatch, RefObject, useMemo, useState } from 'react';
import { Chart } from 'chart.js';
import { ICommonDataValue } from '../../common/common.interface';
import { IState } from '../state/state.interfaces';
import {
  ILineChartOptions,
  TGeneratedLineChartDatasets,
} from '../line-chart.interface';
import {
  ANIMATION_DURATION,
  BORDER_COLOR,
} from '../../common/helpers/chart-consts';
import {
  ChartHoverMode,
  Events,
  PanZoomMode,
  PointStyle,
} from '../../common/helpers/enums';
import { autoScale } from '../utils/axis-scales/axis-scales';
import getLineChartDataLabels from '../utils/get-line-chart-data-labels';
import { ZOOM_BOX_BACKGROUND_COLOR } from '../constants/line-chart-consts';
import getLineChartToolTips from '../utils/get-line-chart-tooltips';
import getDraggableData from '../../common/helpers/get-draggableData';
import { useChartFunctions } from './use-chart-functions';
import { useLegendState } from '../../common/hooks/use-legend-state';
import { useLegend } from '../../common/hooks/use-legend';
import { toAnnotationObject } from '../../common/helpers/to-annotation';

interface IUseChartOptions {
  chartRef: RefObject<Chart>;
  state: IState;
  options: ILineChartOptions;
  dispatch: Dispatch<any>;
  generatedDatasets: TGeneratedLineChartDatasets;
}

/**
 * Custom hook to generate chart options.
 */
export const useChartOptions = ({
  chartRef,
  state,
  options,
  dispatch,
  generatedDatasets,
}: IUseChartOptions) => {
  const {
    interactions: { onAnimationComplete },
    annotations: {
      labelAnnotation: {
        showLabel,
        text,
        position,
        fontSize,
        xOffset,
        yOffset,
        maxWidth,
        lineHeight,
      } = {},
    },
    chartStyling: { layoutPadding },
  } = options;

  const { enableDragPoints, zoomEnabled, panEnabled, lineEnabled } = state;

  const [hoveredPoint, setHoveredPoint] = useState<ICommonDataValue | null>(
    null,
  );
  const { updateAxesRangesFromChart, onHover } = useChartFunctions({
    chartRef,
    state,
    options,
    dispatch,
    generatedDatasets,
  });
  const { legend, customLegendPlugin } =
    useLegendState({ chartRef, options }) ?? {};
  const { state: legendState } = useLegend() ?? {};
  const { annotation } = legendState ?? {};

  const datalabels = getLineChartDataLabels(options);
  const tooltip = getLineChartToolTips(options);

  const scales = useMemo(
    () => autoScale(options, state, generatedDatasets, chartRef),
    [options, state, generatedDatasets, chartRef],
  );

  const dragData = useMemo(
    () => enableDragPoints && getDraggableData(options),
    [enableDragPoints, options],
  );

  const panOptions = useMemo(
    () => ({
      enabled: panEnabled,
      mode: PanZoomMode.XY,
      onPanComplete() {
        updateAxesRangesFromChart();
      },
    }),
    [panEnabled],
  );

  const zoomOptions = useMemo(
    () => ({
      mode: PanZoomMode.XY,
      drag: {
        enabled: zoomEnabled,
        threshold: 3,
        backgroundColor: ZOOM_BOX_BACKGROUND_COLOR,
        borderColor: BORDER_COLOR,
        borderWidth: 1,
      },
      onZoomComplete() {
        updateAxesRangesFromChart();
      },
    }),
    [zoomEnabled],
  );

  const plugins = {
    datalabels,
    annotationDraggerPlugin: {
      enabled: state?.enableDragAnnotation,
    },
    annotation: toAnnotationObject(annotation),
    zoom: { pan: panOptions, zoom: zoomOptions },
    tooltip,
    legend: { ...legend, display: false, events: [] }, // hide default legend for line chart only
    customLegendPlugin,
    chartAreaBorder: {
      borderColor: BORDER_COLOR,
    },
    chartAreaText: {
      showLabel,
      text,
      position,
      fontSize,
      xOffset,
      yOffset,
      maxWidth,
      lineHeight,
    },
    ...dragData,
  };

  return useMemo(
    () => ({
      layout: {
        padding: layoutPadding,
      },
      onHover: onHover(hoveredPoint, setHoveredPoint),
      maintainAspectRatio: options?.chartStyling?.maintainAspectRatio,
      aspectRatio: options?.chartStyling?.squareAspectRatio ? 1 : 0,
      animation: options?.chartStyling?.performanceMode
        ? false
        : {
            duration: ANIMATION_DURATION.FAST,
            onComplete: onAnimationComplete,
          },
      hover: {
        mode: ChartHoverMode.Nearest,
        intersect: true,
      },
      elements: {
        line: {
          pointStyle: PointStyle.Circle,
          showLine: lineEnabled,
          tension: 0,
        },
      },
      scales,
      plugins,
      events: Object.values(Events),
    }),
    [state, options, chartRef],
  ) as any; //TODO: fix conflict between internal chart.js and out custom interfaces(temporary decision)
};
