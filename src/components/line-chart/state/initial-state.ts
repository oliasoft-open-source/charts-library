import { IState } from './state.interfaces';
import {
  ILineChartAxis,
  ILineChartOptions,
  ILineChartRange,
} from '../line-chart.interface';
import { AxisType } from '../../common/helpers/enums';
import { generateAxisId } from '../utils/line-chart-utils';
import { getChartStateFromStorage } from './manage-state-in-local-storage';

interface inputInitState {
  options: ILineChartOptions;
  persistenceId?: string;
}

type CustomAxesRange = {
  [key: string]: ILineChartRange;
};

/**
 Initial chart state for the line chart.

 @param {Object} options - The chart options.
 @return {Object} The initial chart state.
 */
const initialState = ({ options, persistenceId }: inputInitState): IState => {
  const {
    additionalAxesOptions: { range: customAxesRange = {} },
    axes,
    chartOptions: {
      enableZoom,
      enablePan,
      showPoints,
      showLine,
      enableDragAnnotation,
    },
    legend: { display },
    dragData,
  } = options;

  /**
   * getStateAxesByType
   * @param {'x'|'y'} axisType
   * @param {Object} customAxesRange
   * @return {{id: string}[] | []} returns array of objects describing all chart axis or empty array
   */
  const getStateAxesByType = (
    axisType: AxisType,
    customAxesRange: CustomAxesRange,
  ) => {
    if (!axes[axisType]) {
      return [];
    }
    if (axes[axisType]?.length > 1) {
      return axes[axisType].map((axisObj: ILineChartAxis, index: number) => {
        const id = generateAxisId(axisType, index, axes[axisType].length > 1);
        const customMin = customAxesRange?.[id]?.min;
        const customMax = customAxesRange?.[id]?.max;
        const { unit } = axisObj;
        return {
          id,
          //only add custom axis ranges if defined:
          ...(customMin ? { min: customMin } : {}),
          ...(customMax ? { max: customMax } : {}),
          ...(unit ? { unit } : {}),
        };
      });
    } else {
      const id = generateAxisId(axisType);
      const customMin = customAxesRange?.[id]?.min;
      const customMax = customAxesRange?.[id]?.max;
      const unit = axes?.[id]?.[0]?.unit;
      return [
        {
          id,
          //only add custom axis ranges if defined:
          ...(customMin ? { min: customMin } : {}),
          ...(customMax ? { max: customMax } : {}),
          ...(unit ? { unit } : {}),
        },
      ];
    }
  };

  const xStateAxes = getStateAxesByType(AxisType.X, customAxesRange);
  const yStateAxes = getStateAxesByType(AxisType.Y, customAxesRange);
  const stateAxes = [...xStateAxes, ...yStateAxes];

  const {
    zoomEnabled,
    panEnabled,
    pointsEnabled,
    lineEnabled,
    legendEnabled,
    enableDragPoints,
    enableDragAnnotation: enableDragAnnotationStorage,
  } = getChartStateFromStorage(persistenceId) || {};

  return {
    zoomEnabled:
      enableDragAnnotation ?? enableDragAnnotationStorage
        ? false
        : zoomEnabled ?? enableZoom,
    panEnabled: panEnabled ?? enablePan,
    pointsEnabled: pointsEnabled ?? showPoints,
    lineEnabled: lineEnabled ?? showLine,
    legendEnabled: legendEnabled ?? display,
    axes: stateAxes,
    showTable: false,
    enableDragPoints: dragData?.enableDragData && enableDragPoints,
    enableDragAnnotation: enableDragAnnotation ?? enableDragAnnotationStorage,
    initialAxesRanges: [],
  };
};

export default initialState;
