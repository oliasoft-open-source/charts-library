import { IUnitOptions } from '../line-chart.interface';

export interface IAxisState {
  id?: string;
  label?: string;
  min?: number;
  max?: number;
  unit?: IUnitOptions | string;
  axisType?: string;
}

export interface InitialState {
  zoomEnabled: boolean;
  panEnabled: boolean;
  pointsEnabled: boolean;
  lineEnabled: boolean;
  legendEnabled: boolean;
  axes: IAxisState[];
  showAnnotationLineIndex?: number[];
  showTable: boolean;
  enableDragPoints: boolean;
  enableDragAnnotation: boolean;
}

export interface IInitAxisRange {
  id: string;
  min: number;
  max: number;
  unit?: string;
}

export interface IState extends InitialState {
  initialAxesRanges: IInitAxisRange[];
}

export interface IAction<T = string, P = any> {
  type: T;
  payload?: P;
}
