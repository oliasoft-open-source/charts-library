import { IState } from './state.interfaces';

/**
 * Retrieves the chart state from local storage.
 *
 * @param {string|''} persistenceId - The chart persistenceId.
 * @returns {object|null} The chart state object or null if not found.
 */
export const getChartStateFromStorage = (persistenceId?: string) => {
  if (!persistenceId) return null;

  // Retrieve the chart state object from local storage
  const chartStateKey = `line-chart-state-${persistenceId}`;
  const chartStateObjectJSON = localStorage.getItem(chartStateKey);

  if (chartStateObjectJSON) {
    // If the chart state object was found, parse it
    const chartStateObject = JSON.parse(chartStateObjectJSON);

    // Return the state property of the parsed chart state object
    return chartStateObject.state;
  }

  // If the chart state object was not found, return null
  return null;
};

/**
 * Remove expired chart states from local storage.
 *
 * @param {number} maxAgeInHours - The maximum age of chart states to keep in local storage (in hours). Default is 72 hours.
 */
const removeExpiredChartStates = (maxAgeInHours = 72) => {
  const currentTime = new Date().getTime();
  const maxAgeInMilliseconds = maxAgeInHours * 60 * 60 * 1000;

  // Iterate through all keys in local storage
  for (let i = 0; i < localStorage.length; i++) {
    const key = localStorage.key(i);

    // Check if the key is related to a line-chart-state
    if (key?.includes('line-chart-state-')) {
      const chartStateObjectJSON = localStorage.getItem(key);

      // If a valid chart state object JSON is found
      if (chartStateObjectJSON) {
        const chartStateObject = JSON.parse(chartStateObjectJSON);
        const storedTime = chartStateObject.timestamp;

        // If a valid timestamp is found
        if (storedTime) {
          const ageInMilliseconds = currentTime - storedTime;

          // If the age of the chart state is older than the specified maxAgeInHours
          if (ageInMilliseconds > maxAgeInMilliseconds) {
            // Remove the expired chart state from local storage
            localStorage.removeItem(key);
          }
        }
      }
    }
  }
};

/**
 * Stores the chart state in local storage.
 *
 * @param {object} state - The chart state object to store.
 * @param {string|''} persistenceId - The chart persistenceId.
 */
export const storeChartStateInStorage = (
  state: IState,
  persistenceId?: string,
) => {
  if (!persistenceId) return;

  const currentTime = new Date().getTime();
  const chartStateKey = `line-chart-state-${persistenceId}`;

  // Create an object containing the chart state and the timestamp
  const chartStateObject = {
    state,
    timestamp: currentTime,
  };

  // Serialize the chart state object as a JSON string and store it in local storage
  localStorage.setItem(chartStateKey, JSON.stringify(chartStateObject));

  // Remove expired chart states from local storage
  removeExpiredChartStates();
};
