import { produce } from 'immer';
import { IAction, IState } from './state.interfaces';
import {
  DISABLE_DRAG_OPTIONS,
  RESET_AXES_RANGES,
  SAVE_INITIAL_AXES_RANGES,
  TOGGLE_DRAG_ANNOTATION,
  TOGGLE_DRAG_POINTS,
  TOGGLE_LEGEND,
  TOGGLE_LINE,
  TOGGLE_PAN,
  TOGGLE_POINTS,
  TOGGLE_TABLE,
  TOGGLE_ZOOM,
  UPDATE_AXES_RANGES,
} from './action-types';

export const reducer = (state: IState, action: IAction) => {
  return produce(state, (draft: IState) => {
    switch (action.type) {
      case TOGGLE_ZOOM: {
        draft.zoomEnabled = !draft.zoomEnabled;
        if (draft.panEnabled) {
          draft.panEnabled = false;
        }
        if (draft.enableDragPoints) {
          draft.enableDragPoints = false;
        }
        if (draft.enableDragAnnotation) {
          draft.enableDragAnnotation = false;
        }
        break;
      }
      case TOGGLE_PAN: {
        draft.panEnabled = !draft.panEnabled;
        if (draft.zoomEnabled) {
          draft.zoomEnabled = false;
        }
        if (draft.enableDragPoints) {
          draft.enableDragPoints = false;
        }
        if (draft.enableDragAnnotation) {
          draft.enableDragAnnotation = false;
        }
        break;
      }
      case TOGGLE_POINTS: {
        draft.pointsEnabled = !draft.pointsEnabled;
        break;
      }
      case TOGGLE_LINE: {
        draft.lineEnabled = !draft.lineEnabled;
        break;
      }
      case TOGGLE_LEGEND: {
        draft.legendEnabled = !draft.legendEnabled;
        break;
      }
      case TOGGLE_TABLE: {
        draft.showTable = !draft.showTable;
        break;
      }
      case SAVE_INITIAL_AXES_RANGES: {
        const { initialAxesRanges } = action.payload;
        draft.initialAxesRanges = initialAxesRanges;
        break;
      }
      case UPDATE_AXES_RANGES: {
        const { axes } = action.payload;
        draft.axes = axes;
        break;
      }
      case RESET_AXES_RANGES: {
        const { initialAxesRanges } = draft;
        draft.axes = [...initialAxesRanges];
        break;
      }
      case TOGGLE_DRAG_POINTS: {
        draft.enableDragPoints = !draft.enableDragPoints;
        if (draft.panEnabled) {
          draft.panEnabled = false;
        }
        if (draft.zoomEnabled) {
          draft.zoomEnabled = false;
        }
        if (draft.enableDragAnnotation) {
          draft.enableDragAnnotation = false;
        }
        break;
      }
      case TOGGLE_DRAG_ANNOTATION: {
        draft.enableDragAnnotation = !draft.enableDragAnnotation;
        if (draft.panEnabled) {
          draft.panEnabled = false;
        }
        if (draft.zoomEnabled) {
          draft.zoomEnabled = false;
        }
        break;
      }
      case DISABLE_DRAG_OPTIONS: {
        if (
          draft.enableDragAnnotation ||
          draft.enableDragPoints ||
          draft.panEnabled ||
          draft.zoomEnabled
        ) {
          draft.enableDragAnnotation = false;
          draft.enableDragPoints = false;
          draft.panEnabled = false;
          draft.zoomEnabled = false;
        }
        break;
      }
    }
  });
};
