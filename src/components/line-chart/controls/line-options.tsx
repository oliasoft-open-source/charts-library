import { useState } from 'react';
import {
  Button,
  Icon,
  IMenuSection,
  Menu,
  Tooltip,
} from '@oliasoft-open-source/react-ui-library';
import LineOnlyIcon from 'src/assets/icons/line-only.svg';
import PointOnlyIcon from 'src/assets/icons/point-only.svg';
import LineAndPointIcon from 'src/assets/icons/line-and-point.svg';

interface ILineOptions {
  lineEnabled: boolean;
  onToggleLine: () => void;
  onTogglePoints: () => void;
  pointsEnabled: boolean;
  translations: Record<string, string>;
}

export function getLineOptions({
  lineEnabled,
  onToggleLine,
  onTogglePoints,
  pointsEnabled,
  translations,
}: ILineOptions) {
  return [
    {
      type: 'Option',
      label: translations.pointsLines,
      icon: <Icon icon={<LineAndPointIcon />} />,
      selected: pointsEnabled && lineEnabled,
      onClick: () => {
        if (!pointsEnabled) onTogglePoints();
        if (!lineEnabled) onToggleLine();
      },
    },
    {
      type: 'Option',
      label: translations.linesOnly,
      icon: <Icon icon={<LineOnlyIcon />} />,
      selected: !pointsEnabled && lineEnabled,
      onClick: () => {
        if (pointsEnabled) onTogglePoints();
        if (!lineEnabled) onToggleLine();
      },
    },
    {
      type: 'Option',
      label: translations.pointsOnly,
      icon: <Icon icon={<PointOnlyIcon />} />,
      selected: pointsEnabled && !lineEnabled,
      onClick: () => {
        if (!pointsEnabled) onTogglePoints();
        if (lineEnabled) onToggleLine();
      },
    },
  ];
}

export const LineOptions = ({ options }: { options: IMenuSection[] }) => {
  const [menuOpen, setMenuOpen] = useState(false);
  const selectedOption = options.find((option) => option.selected);

  return (
    <Menu
      open={menuOpen}
      setOpen={setMenuOpen}
      menu={{
        sections: options,
        trigger: 'Component',
        component: (
          <Tooltip
            display="inline-flex"
            text={selectedOption?.label}
            placement="bottom-start"
            enabled={!menuOpen}
          >
            <Button
              colored="muted"
              basic
              small
              round
              icon={selectedOption?.icon}
            />
          </Tooltip>
        ),
      }}
    />
  );
};
