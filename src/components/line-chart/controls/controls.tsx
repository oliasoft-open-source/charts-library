import React, { useMemo } from 'react';
import {
  Button,
  Menu,
  Text,
  Tooltip,
} from '@oliasoft-open-source/react-ui-library';
import ControlsPortal from 'src/components/common/controls-portal';
import { useMeasure } from 'react-use';
import { getLineOptions, LineOptions } from './line-options';
import { DragOptions, getDragOptions } from './drag-options';
import styles from './controls.module.less';
import { useToggleHandlers } from '../hooks/use-toggle-handler';
import { useChartFunctions } from '../hooks/use-chart-functions';
import { AxesOptions } from './axes-options/axes-options';
import { IControls } from './controls-interfaces';

/**
 * @param {import('./controls-interfaces').IControls} obj
 */

const Controls = ({
  headerComponent,
  subheaderComponent,
  table,
  chartRef,
  state,
  options,
  dispatch,
  generatedDatasets,
  translations,
  controlsPortalId,
}: IControls) => {
  const {
    enableDragPoints,
    initialAxesRanges,
    lineEnabled,
    panEnabled,
    pointsEnabled,
    showTable,
    zoomEnabled,
    enableDragAnnotation,
  } = state;
  const { dragData, depthType, annotations } = options;
  const {
    onToggleLine,
    onTogglePan,
    onTogglePoints,
    onToggleTable,
    onToggleZoom,
    onToggleDragPoints,
    onToggleDragAnnotation,
    onDisableDragOptions,
  } = useToggleHandlers(dispatch);
  const { handleDownload, controlsAxesLabels, onResetAxes, onUpdateAxes } =
    useChartFunctions({
      chartRef,
      state,
      options,
      dispatch,
      generatedDatasets,
    });

  const [measureRef, { width }] = useMeasure();
  const isNarrow = width < 200;

  const lineOptions = useMemo(
    () =>
      getLineOptions({
        lineEnabled,
        onToggleLine,
        onTogglePoints,
        pointsEnabled,
        translations,
      }),
    [lineEnabled, pointsEnabled],
  );

  const dragOptions = useMemo(
    () =>
      getDragOptions({
        onTogglePan,
        onToggleZoom,
        panEnabled,
        zoomEnabled,
        enableDragPoints,
        enableDragAnnotation,
        isDragDataAllowed: dragData?.enableDragData,
        isDragAnnotationAllowed: annotations?.enableDragAnnotation,
        onToggleDragAnnotation,
        onToggleDragPoints,
        onDisableDragOptions,
        translations,
      }),
    [
      zoomEnabled,
      panEnabled,
      enableDragPoints,
      dragData?.enableDragData,
      annotations?.enableDragAnnotation,
    ],
  );

  return (
    <>
      <div className={styles.controls}>
        {!!options.title && <Text bold>{options.title}</Text>}
        {headerComponent}
        <ControlsPortal controlsPortalId={controlsPortalId ?? ''}>
          <div
            className={styles.buttons}
            ref={measureRef as React.LegacyRef<HTMLDivElement>}
          >
            {!showTable && (
              <>
                <AxesOptions
                  initialAxesRanges={initialAxesRanges}
                  axes={state.axes}
                  controlsAxesLabels={controlsAxesLabels}
                  onUpdateAxes={onUpdateAxes}
                  onResetAxes={onResetAxes}
                  depthType={depthType}
                  translations={translations}
                />
                {isNarrow ? (
                  <Menu
                    menu={{
                      placement: 'bottom-end',
                      sections: [
                        ...lineOptions,
                        {
                          type: 'Divider',
                        },
                        ...dragOptions,
                        {
                          type: 'Divider',
                        },
                        {
                          icon: 'download',
                          label: translations.downloadAsPNG,
                          type: 'Option',
                          onClick: handleDownload,
                        },
                      ],
                      trigger: 'Component',
                      component: (
                        <Button colored="muted" basic small round icon="menu" />
                      ),
                    }}
                  />
                ) : (
                  <>
                    <LineOptions options={lineOptions} />
                    <Tooltip
                      display="inline-flex"
                      text={translations.downloadAsPNG}
                      placement="bottom-end"
                    >
                      <Button
                        small
                        basic
                        colored="muted"
                        round
                        icon="download"
                        onClick={handleDownload}
                      />
                    </Tooltip>
                    <DragOptions options={dragOptions} />
                  </>
                )}
              </>
            )}

            {table ? (
              <Tooltip
                display="inline-flex"
                text={
                  showTable ? translations.showChart : translations.showTable
                }
                placement="bottom-end"
              >
                <Button
                  small
                  basic
                  colored="muted"
                  round
                  icon={showTable ? 'chart' : 'table'}
                  onClick={onToggleTable}
                />
              </Tooltip>
            ) : null}
          </div>
        </ControlsPortal>
      </div>
      {subheaderComponent}
    </>
  );
};

export default Controls;
