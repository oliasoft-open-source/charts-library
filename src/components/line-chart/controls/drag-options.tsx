import {
  Button,
  IMenuSection,
  Menu,
} from '@oliasoft-open-source/react-ui-library';
import { TbArrowsMove, TbBan, TbHandStop, TbZoomIn } from 'react-icons/tb';
import { IDragOptions } from './drag-options-interfaces';

export function getDragOptions({
  onTogglePan,
  onToggleZoom,
  panEnabled,
  zoomEnabled,
  enableDragPoints,
  enableDragAnnotation,
  isDragDataAllowed,
  isDragAnnotationAllowed,
  onToggleDragAnnotation,
  onToggleDragPoints,
  onDisableDragOptions,
  translations: {
    dragToZoom,
    doubleClickToReset,
    dragToPan,
    dragToMovePoints,
    dragDisabled,
    dragToMoveAnnotation,
  },
}: IDragOptions) {
  return [
    {
      label: dragToZoom,
      description: doubleClickToReset,
      icon: <TbZoomIn />,
      selected: zoomEnabled,
      type: 'Option',
      onClick: () => {
        if (!zoomEnabled) onToggleZoom();
      },
    },
    {
      label: dragToPan,
      description: doubleClickToReset,
      icon: <TbArrowsMove />,
      selected: panEnabled,
      type: 'Option',
      onClick: () => {
        if (!panEnabled) onTogglePan();
      },
    },
    ...(isDragDataAllowed
      ? [
          {
            label: dragToMovePoints,
            icon: <TbHandStop />,
            selected: enableDragPoints,
            type: 'Option',
            onClick: () => {
              if (!enableDragPoints) onToggleDragPoints();
            },
          },
        ]
      : []),
    ...(isDragAnnotationAllowed
      ? [
          {
            label: dragToMoveAnnotation,
            icon: <TbHandStop />,
            selected: enableDragAnnotation,
            type: 'Option',
            onClick: () => {
              if (!enableDragAnnotation) onToggleDragAnnotation();
            },
          },
        ]
      : []),
    {
      label: dragDisabled,
      icon: <TbBan />,
      selected:
        !zoomEnabled &&
        !panEnabled &&
        !enableDragPoints &&
        !enableDragAnnotation,
      type: 'Option',
      onClick: onDisableDragOptions,
    },
  ];
}

/**
 * @param {import('./drag-options-interfaces').IDragOptions} obj
 */
export const DragOptions = ({ options }: { options: IMenuSection[] }) => {
  const selectedOption = options.find((option) => option.selected);

  return (
    <Menu
      menu={{
        sections: options,
        trigger: 'Component',
        component: (
          <Button
            colored="muted"
            basic
            small
            label={selectedOption?.label}
            icon={selectedOption?.icon}
          />
        ),
      }}
    />
  );
};
