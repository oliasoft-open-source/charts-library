export interface IDragOptions {
  onTogglePan: () => void;
  onToggleZoom: () => void;
  panEnabled: boolean;
  zoomEnabled: boolean;
  enableDragPoints?: boolean;
  enableDragAnnotation?: boolean;
  isDragDataAllowed?: boolean;
  isDragAnnotationAllowed?: boolean;
  onToggleDragAnnotation: () => void;
  onToggleDragPoints: () => void;
  onDisableDragOptions: () => void;
  translations: Record<string, string>;
}
