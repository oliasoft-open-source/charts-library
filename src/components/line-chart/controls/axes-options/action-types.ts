export enum actionTypes {
  Reset = 'reset',
  ValueUpdated = 'valueUpdated',
  UnitUpdated = 'unitUpdated',
}
