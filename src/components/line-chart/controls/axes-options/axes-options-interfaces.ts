import { IUnitOptions } from '../../line-chart.interface.ts';
import { IAxisState, IInitAxisRange } from '../../state/state.interfaces.ts';

export interface IDepthType {
  selectedDepthType?: string;
  setSelectedDepthType?: (type: string) => void;
  options?: Array<string>;
}

export interface IAxisControlLabel {
  id: string;
  label: string;
}

export interface IAxesOptions {
  initialAxesRanges: IInitAxisRange[];
  axes: IAxisState[];
  controlsAxesLabels: IAxisControlLabel[];
  onUpdateAxes: ({ axes }: { axes: IAxisState }) => void;
  onResetAxes: () => void;
  depthType?: IDepthType | object;
  close?: () => void;
}

export interface IOnEdit {
  name?: string;
  value?: string;
  id?: string;
}

export interface IAxisFormState {
  id: string;
  min: number | string;
  max: number | string;
  unit?: string | IUnitOptions;
}

export type IInitializeAxisFormState = IAxisFormState[];

export interface IError {
  id: string;
  min: string[];
  max: string[];
}

export interface IFormState extends IInitializeAxisFormState {
  errors?: IError[];
  valid?: boolean;
}

type ActionType = 'reset' | 'valueUpdated' | 'unitUpdated';
export type AxisKey = 'min' | 'max' | 'unit';

interface ActionPayload extends IOnEdit {
  initialAxesRanges?: IInitAxisRange[];
  axes?: IAxisState[];
}

export interface IAction {
  type: ActionType;
  payload: ActionPayload;
}
