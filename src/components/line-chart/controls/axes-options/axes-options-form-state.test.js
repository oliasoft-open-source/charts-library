import { validateAxes } from './axes-options-form-state';

describe('Axes options', () => {
  test('validateAxes passes for valid inputs', () => {
    const scalesMinMax = [
      {
        id: 'x1',
        min: '0',
        max: '25',
      },
      {
        id: 'x2',
        min: '0',
        max: '15',
      },
      {
        id: 'y',
        min: '0',
        max: '240',
      },
    ];
    const result = validateAxes(scalesMinMax);
    expect(result.valid).toBe(true);
    expect(result.errors).toStrictEqual([
      {
        id: 'x1',
        min: [],
        max: [],
      },
      {
        id: 'x2',
        min: [],
        max: [],
      },
      {
        id: 'y',
        min: [],
        max: [],
      },
    ]);
  });
  test('validateAxes lists errors for invalid inputs', () => {
    const scalesMinMax = [
      {
        id: 'x',
        min: '0x',
        max: '25',
      },
      {
        id: 'y',
        min: '0',
        max: '240',
      },
    ];
    const result = validateAxes(scalesMinMax);
    expect(result.valid).toBe(false);
    expect(result.errors).toStrictEqual([
      {
        id: 'x',
        min: ['mustBeLessThanMax'],
        max: ['mustBeGreaterThanMin'],
      },
      {
        id: 'y',
        min: [],
        max: [],
      },
    ]);
  });
});
