import { produce } from 'immer';
import { IInitAxisRange, IState } from '../../state/state.interfaces';
import {
  AxisKey,
  IAction,
  IAxisFormState,
  IFormState,
  IInitializeAxisFormState,
} from './axes-options-interfaces.ts';
import { isGreaterThanMin, isLessThanMax } from '../../utils/line-chart-utils';
import { actionTypes } from './action-types';

/**
 * Initialize local component form state for a custom loads density table
 *
 * @param {Object} args
 * @param {Object} args.initialAxesRanges
 * @param {Array} [args.axes]
 * @returns {Object} formState
 */
export const initializeFormState = ({
  initialAxesRanges,
  axes = [],
}: Partial<IState>): IInitializeAxisFormState => {
  return (
    initialAxesRanges?.map((initialAxisRange: IInitAxisRange) => {
      const currentAxisRange = axes.find((a) => a.id === initialAxisRange.id);
      return {
        id: initialAxisRange.id,
        min: currentAxisRange?.min ?? initialAxisRange?.min,
        max: currentAxisRange?.max ?? initialAxisRange?.max,
        unit: currentAxisRange?.unit,
      };
    }) || []
  );
};

/**
 * @typedef {Object} Action
 * @property {String} type Action type
 * @property {Object} [payload] Action payload (optional)
 */

const isEmptyString = (value: string) => value === '';

const createErrorMessages = (
  value: string,
  compareTo: string | number,
  type: string,
) => {
  const errors = [];
  if (isEmptyString(value)) errors.push('mustHaveAValue');

  if (type === 'min' && !isLessThanMax(value, compareTo)) {
    errors.push('mustBeLessThanMax');
  } else if (type === 'max' && !isGreaterThanMin(value, compareTo)) {
    errors.push('mustBeGreaterThanMin');
  }

  return errors;
};

export const validateAxes = (formState: IFormState) => {
  return formState.reduce(
    (acc, { id, min, max }: IAxisFormState) => {
      return produce(acc, (draft: IFormState) => {
        const errors = {
          min: createErrorMessages(String(min), String(max), 'min'),
          max: createErrorMessages(String(max), String(min), 'max'),
        };
        draft?.errors?.push({
          id,
          ...errors,
        });
        draft.valid = !(
          !acc.valid ||
          !!errors.min.length ||
          !!errors.max.length
        );
      });
    },
    { errors: [], valid: true },
  );
};

/**
 * Local component form state reducer for a axes options form
 *
 * @param {Object} state Local component form state
 * @param {Action} action Action with type and payload
 * @returns {Object} FormState
 */
export const reducer = (state: IFormState, action: IAction) => {
  switch (action.type) {
    case actionTypes.Reset: {
      const { initialAxesRanges, axes } = action.payload;
      return initializeFormState({ initialAxesRanges, axes });
    }
    case actionTypes.ValueUpdated:
      return produce(state, (draft: IFormState) => {
        const { name, value, id } = action.payload;
        const axis = draft.find((a) => a.id === id);
        if (axis && value) {
          axis[name as AxisKey] = value;
        }
      });
    case actionTypes.UnitUpdated:
      return produce(state, (draft: IFormState) => {
        const { name, value, id } = action.payload;
        // Using 'any' here because the type of 'unit' is not known in advance.
        const unit: any = draft.find((a) => a.id === id)?.unit;
        if (unit && name) {
          unit[name as string] = value;
        }
      });
    default:
      return state;
  }
};
