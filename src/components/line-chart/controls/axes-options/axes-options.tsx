import {
  ChangeEvent,
  FocusEvent as FocusEvent_2,
  FormEvent,
  useReducer,
  useState,
} from 'react';
import {
  Button,
  ButtonGroup,
  Field,
  Flex,
  InputGroup,
  InputGroupAddon,
  ISelectProps,
  NumberInput,
  Popover,
  Select,
  Spacer,
  Text,
  Tooltip,
} from '@oliasoft-open-source/react-ui-library';
import { TbRuler } from 'react-icons/tb';
import { IAxisState } from '../../state/state.interfaces';
import { AxisControls } from '../controls-interfaces';
import {
  ILineChartAxes,
  ILineChartAxis,
  IUnitOptions,
} from '../../line-chart.interface';
import {
  initializeFormState,
  reducer,
  validateAxes,
} from './axes-options-form-state';
import { actionTypes } from './action-types';
import {
  IAxesOptions,
  IAxisFormState,
  IError,
  IOnEdit,
} from './axes-options-interfaces';

interface IAxesOptionsWithTranslations extends IAxesOptions {
  translations: Record<string, string>;
}

const AxesOptionsPopover = (optionsPopover: Record<string, any>) => {
  const {
    initialAxesRanges,
    axes,
    controlsAxesLabels,
    onUpdateAxes,
    onResetAxes,
    depthType,
    translations,
    close,
  } = optionsPopover;
  const [depthTypeState, setDepthTypeState] = useState(
    depthType?.selectedDepthType,
  );
  const [formState, dispatch] = useReducer(
    reducer,
    {
      axes,
      initialAxesRanges,
    },
    initializeFormState,
  );

  const { errors, valid } = validateAxes(formState);

  const onEditValue = ({ name, value, id }: IOnEdit) => {
    dispatch({
      type: actionTypes.ValueUpdated,
      payload: { name, value, id },
    });
  };

  const onEditUnit = ({ name, value, id }: IOnEdit) => {
    dispatch({
      type: actionTypes.UnitUpdated,
      payload: { name, value, id },
    });
  };

  const onDone = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (valid) {
      const sanitizedFormState = formState.map((axis: IAxisFormState) => ({
        ...axis,
        min: typeof axis.min === 'string' ? Number(axis.min) : axis.min,
        max: typeof axis.max === 'string' ? Number(axis.max) : axis.max,
      }));
      onUpdateAxes({
        axes: sanitizedFormState,
      });

      //update units
      sanitizedFormState.map((el: IAxisState, i: number) => {
        const selectedUnit = (el.unit as IUnitOptions)?.selectedUnit;
        if (selectedUnit) {
          axes?.[i]?.unit?.setSelectedUnit(selectedUnit);
        }
      });
      //update depthType
      if (depthType && depthType?.setSelectedDepthType) {
        depthType?.setSelectedDepthType(depthTypeState);
      }
    }

    close?.();
  };

  const onReset = () => {
    //reset local form
    dispatch({
      type: actionTypes.Reset,
      payload: { axes, initialAxesRanges },
    });
    //update units
    initialAxesRanges.map((el: IAxisState, i: number) => {
      const selectedUnit = (el.unit as IUnitOptions)?.selectedUnit;
      if (selectedUnit) {
        axes?.[i]?.unit?.setSelectedUnit(selectedUnit);
      }
    });
    //reset parent state
    onResetAxes();
    close?.();
  };

  const isCustomValue =
    axes.filter((axis: ILineChartAxes) => axis.max || axis.min).length > 0;
  const handleInputFocus = (e: FocusEvent_2<HTMLInputElement>) =>
    e.target.select();
  return (
    <form onSubmit={onDone}>
      {axes.map((axis: ILineChartAxis, i: number) => {
        const axisControl: AxisControls | undefined = controlsAxesLabels.find(
          (el: AxisControls) => el.id === axis.id,
        );
        const axisLabel: string | undefined = axisControl?.label;
        const axisState = formState.find(
          (a: IAxisFormState) => a.id === axis.id,
        );
        const axisErrors = errors?.find((a: IError) => a.id === axis.id) as
          | IError
          | undefined;

        const { min, max, unit } = axisState || {};
        const minErrorMsg = axisErrors?.min?.[0]
          ? translations[axisErrors.min[0]]
          : null;
        const maxErrorMsg = axisErrors?.max?.[0]
          ? translations[axisErrors.max[0]]
          : null;

        return (
          <Field key={i} label={axisLabel || axis.id || ''}>
            <InputGroup small width="300px">
              <NumberInput
                name="min"
                value={min}
                error={minErrorMsg}
                width="100%"
                onChange={(evt: ChangeEvent<HTMLInputElement>) =>
                  onEditValue({
                    name: evt.target.name,
                    value: evt.target.value,
                    id: axis.id,
                  })
                }
                onFocus={handleInputFocus}
              />
              <InputGroupAddon>to</InputGroupAddon>
              <NumberInput
                name="max"
                value={max}
                error={maxErrorMsg}
                width="100%"
                onChange={(evt: ChangeEvent<HTMLInputElement>) =>
                  onEditValue({
                    name: evt.target.name,
                    value: evt.target.value,
                    id: axis.id,
                  })
                }
                onFocus={handleInputFocus}
              />
              {axis.unit ? (
                <Select
                  name="selectedUnit"
                  options={axis?.unit?.options as ISelectProps['options']}
                  value={
                    typeof unit !== 'string' ? unit?.selectedUnit : undefined
                  }
                  onChange={(e: ChangeEvent<HTMLInputElement>) => {
                    onEditUnit({
                      name: e.target.name,
                      value: e.target.value,
                      id: axis.id,
                    });
                  }}
                  autoLayerWidth
                  width="15%"
                />
              ) : null}
            </InputGroup>
          </Field>
        );
      })}

      {depthType?.options?.length > 0 ? (
        <>
          <ButtonGroup
            items={depthType.options.map((depth: string, i: number) => ({
              key: i,
              label: depth,
            }))}
            onSelected={(key: number) => {
              setDepthTypeState(depthType.options[key]);
            }}
            small
            value={depthType.options.indexOf(depthTypeState)}
          />
          <Spacer />
        </>
      ) : null}

      <Flex gap="8px" alignItems="center">
        <Button
          type="submit"
          small
          colored
          label={translations.done}
          disabled={!valid}
        />
        <Button
          small
          name="resetAxes"
          label={translations.resetAxes}
          onClick={onReset}
          disabled={!isCustomValue}
        />
        <Text small muted>
          {translations.orDoubleClickToCanvas}
        </Text>
      </Flex>
    </form>
  );
};

export const AxesOptions = (optionsPopover: IAxesOptionsWithTranslations) => {
  const {
    initialAxesRanges,
    axes,
    controlsAxesLabels,
    onUpdateAxes,
    onResetAxes,
    depthType,
    translations,
  } = optionsPopover;
  return (
    <Popover
      placement="bottom-start"
      overflowContainer
      content={
        <AxesOptionsPopover
          initialAxesRanges={initialAxesRanges}
          axes={axes}
          controlsAxesLabels={controlsAxesLabels}
          onUpdateAxes={onUpdateAxes}
          onResetAxes={onResetAxes}
          depthType={depthType}
          translations={translations}
        />
      }
    >
      <Tooltip
        text={translations.axesOptions}
        placement="bottom-start"
        display="inline-flex"
      >
        <Button small basic colored="muted" round icon={<TbRuler />} />
      </Tooltip>
    </Popover>
  );
};
