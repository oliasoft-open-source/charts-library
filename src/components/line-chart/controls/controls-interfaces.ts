import { Dispatch, ReactNode, RefObject } from 'react';
import { Chart } from 'chart.js';
import {
  ILineChartOptions,
  TGeneratedLineChartDatasets,
} from '../line-chart.interface';
import { IState } from '../state/state.interfaces';

export interface IControls {
  headerComponent?: ReactNode;
  subheaderComponent?: Element;
  table?: ReactNode;
  chartRef: RefObject<Chart>;
  state: IState;
  options: ILineChartOptions;
  dispatch: Dispatch<any>;
  generatedDatasets: TGeneratedLineChartDatasets;
  translations: Record<string, string>;
  controlsPortalId?: string;
}

export interface AxisControls {
  id?: string;
  label?: string;
}
