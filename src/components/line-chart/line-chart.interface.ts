import { ReactNode } from 'react';
import {
  ICommonAnnotations,
  ICommonChartOptions,
  ICommonCustomLegend,
  ICommonData,
  ICommonDataset,
  ICommonDataValue,
  ICommonDragData,
  ICommonInteractions,
  ICommonLegend,
  ICommonOptions,
  ICommonScales,
  ICommonStyling,
  ICommonTooltip,
} from '../common/common.interface';

export interface ILChartOptions extends ICommonChartOptions {
  showPoints: boolean;
  closeOnOutsideClick: boolean;
  showLine: boolean;
  enableDragAnnotation?: boolean;
}

export interface ILineChartGraph {
  lineTension: number;
  spanGaps: boolean;
  showDataLabels: boolean;
  showMinorGridlines: boolean;
}

export interface ILineChartTooltip extends ICommonTooltip {
  hideSimulationName: boolean;
  scientificNotation: boolean;
}

export interface ILineChartRange {
  min: number;
  max: number;
}

export interface ILineRange {
  [key: string]: ILineChartRange;
}

export interface ILineChartAdditionalAxesOptions {
  chartScaleType: 'linear' | 'logarithmic';
  reverse: boolean;
  beginAtZero: boolean;
  stepSize?: number;
  suggestedMin?: number;
  suggestedMax?: number;
  range?: ILineRange;
  autoAxisPadding: boolean;
}

export interface IUnitOptions {
  options: string[];
  selectedUnit: string;
  setSelectedUnit: () => void;
}

export interface ILineChartAxis<PositionType = any> {
  id?: string;
  label: string;
  position: PositionType;
  color: string | string[];
  unit?: IUnitOptions;
  stepSize?: number;
  gridLines?: ILineChartGraph;
}

export interface ILineChartAxes {
  x: ILineChartAxis<'top' | 'bottom'>[];
  y: ILineChartAxis<'left' | 'right'>[];
  [key: string]: ILineChartAxis[];
}

export interface IDepthType {
  options: string[];
  selectedUnit: string;
  setSelectedUnit: () => void;
}

export interface ILineChartStyling extends ICommonStyling {
  squareAspectRatio?: number | boolean;
  layoutPadding?:
    | number
    | { top: number; bottom: number; left: number; right: number };
}

export interface ILineInteractions extends ICommonInteractions {
  onAnimationComplete?: () => void;
}

export interface ILineLegend extends ICommonLegend {
  customLegend?: ICommonCustomLegend<any>;
  usePointStyle?: boolean;
}

export interface ILineChartOptions extends ICommonOptions {
  title?: string | string[];
  axes: ILineChartAxes;
  scales: ICommonScales;
  additionalAxesOptions: ILineChartAdditionalAxesOptions;
  chartStyling: ILineChartStyling;
  tooltip: ILineChartTooltip;
  graph: ILineChartGraph;
  annotations: ICommonAnnotations;
  legend: ILineLegend;
  chartOptions: ILChartOptions;
  interactions: ILineInteractions;
  dragData?: ICommonDragData;
  depthType?: IDepthType | object;
}

export interface ILineChartDataset extends ICommonDataset {
  lineTension?: number;
  pointBackgroundColor?: string;
  pointRadius?: number;
  pointHoverRadius?: number;
  pointHitRadius?: number;
  fill?: boolean;
  xAxisID?: string;
  yAxisID?: string;
  formation?: boolean;
  displayGroup?: string | number;
  pointStyle?: string;
}

export interface ILineChartData extends ICommonData {
  data?: {
    datasets: ILineChartDataset[];
  };
  options: ILineChartOptions;
  persistenceId?: string;
  controlsPortalId?: string;
}

export interface ILineChartProps {
  chart: ILineChartData;
  table?: ReactNode;
}

export interface IGeneratedLineChartDataset {
  label?: string;
  data: ICommonDataValue[];
  showLine?: boolean;
  lineTension: number;
  spanGaps: boolean;
  borderWidth: number;
  borderDash: number[];
  borderJoinStyle: 'round' | 'bevel' | 'miter';
  borderColor: string;
  backgroundColor: string | string[];
  pointBackgroundColor: string;
  pointRadius: number;
  pointHoverRadius: number;
  pointHitRadius: number;
  isAnnotation?: boolean;
  annotationType?: string;
  annotationIndex?: number;
  annotationLabel?: string;
  annotationBackgroundColor?: string;
  annotationPointBackgroundColor?: string;
  annotationBorderColor?: string;
  annotationBorderDash?: number[];
  annotationBorderWidth?: number;
  displayGroup?: string | number;
  hideLegend?: boolean;
  pointStyle?: string;
  xAxisID?: string;
  yAxisID?: string;
}

export type TGeneratedLineChartDatasets = IGeneratedLineChartDataset[];
