import {
  ILChartOptions,
  ILineChartAdditionalAxesOptions,
  ILineChartAxes,
  ILineChartAxis,
  ILineChartData,
  ILineChartGraph,
  ILineChartProps,
  ILineChartStyling,
  ILineChartTooltip,
  ILineInteractions,
  ILineLegend,
} from './line-chart.interface';
import { TextLabelPosition } from './plugins/plugin-constants';
import {
  ICommonAnnotations,
  ICommonAnnotationsData,
  ICommonDragData,
  TAxisPosition,
} from '../common/common.interface';
import { AlignOptions, Position } from '../common/helpers/enums';

const defaultAxis = <T extends TAxisPosition>(
  position: T,
): ILineChartAxis<T> => ({
  label: '',
  position,
  color: '',
  unit: {
    options: [],
    selectedUnit: '',
    setSelectedUnit: () => {},
  },
});

const defaultAxes = (axes?: ILineChartAxes) => ({
  x: axes?.x || [defaultAxis('bottom')],
  y: axes?.y || [defaultAxis('left')],
});

const defaultAdditionalAxesOptions = (
  options?: ILineChartAdditionalAxesOptions,
) => ({
  chartScaleType: options?.chartScaleType || 'linear',
  reverse: options?.reverse || false,
  beginAtZero: options?.beginAtZero ?? false,
  stepSize: options?.stepSize,
  suggestedMin: options?.suggestedMin,
  suggestedMax: options?.suggestedMax,
  range: options?.range,
  autoAxisPadding: options?.autoAxisPadding ?? false,
});

const defaultChartStyling = (options?: ILineChartStyling) => ({
  width: options?.width,
  height: options?.height,
  maintainAspectRatio: options?.maintainAspectRatio ?? false,
  staticChartHeight: options?.staticChartHeight ?? false,
  performanceMode: options?.performanceMode ?? true,
  squareAspectRatio: options?.squareAspectRatio ?? false,
  layoutPadding: options?.layoutPadding || {
    top: 0,
    bottom: 20,
    left: 0,
    right: 0,
  },
});

const defaultTooltip = (tooltip?: ILineChartTooltip) => ({
  tooltips: tooltip?.tooltips ?? true,
  showLabelsInTooltips: tooltip?.showLabelsInTooltips ?? false,
  hideSimulationName: tooltip?.hideSimulationName ?? false,
  scientificNotation: tooltip?.scientificNotation ?? true,
});

const defaultGraph = (graph?: ILineChartGraph) => ({
  lineTension: graph?.lineTension ?? 0.01,
  spanGaps: graph?.spanGaps ?? false,
  showDataLabels: graph?.showDataLabels ?? false,
  showMinorGridlines: graph?.showMinorGridlines ?? false,
});

const defaultAnnotationsData = (annotationsData?: ICommonAnnotationsData[]) => {
  return annotationsData
    ? annotationsData.map((ann) => ({ ...ann, display: ann?.display ?? true }))
    : [];
};

const defaultAnnotations = (annotations?: ICommonAnnotations) => ({
  labelAnnotation: {
    showLabel: annotations?.labelAnnotation?.showLabel ?? false,
    text: annotations?.labelAnnotation?.text ?? '',
    position:
      annotations?.labelAnnotation?.position ?? TextLabelPosition.BOTTOM_RIGHT,
    fontSize: annotations?.labelAnnotation?.fontSize ?? 12,
    xOffset: annotations?.labelAnnotation?.xOffset ?? 5,
    yOffset: annotations?.labelAnnotation?.yOffset ?? 5,
    maxWidth: annotations?.labelAnnotation?.maxWidth ?? 300,
    lineHeight: annotations?.labelAnnotation?.lineHeight ?? 12,
  },
  showAnnotations: annotations?.showAnnotations ?? false,
  controlAnnotation: annotations?.controlAnnotation ?? false,
  enableDragAnnotation: annotations?.enableDragAnnotation ?? false,
  annotationsData: defaultAnnotationsData(annotations?.annotationsData),
});

const defaultLegend = (legend?: ILineLegend) => ({
  display: legend?.display ?? true,
  position: legend?.position ?? Position.BottomLeft,
  align: legend?.align ?? AlignOptions.Center,
  customLegend: legend?.customLegend ?? {
    customLegendPlugin: null,
    customLegendContainerID: '',
  },
  usePointStyle: legend?.usePointStyle ?? true,
});

const defaultChartOptions = (options?: ILChartOptions) => ({
  showPoints: options?.showPoints ?? true,
  showLine: options?.showLine ?? true,
  enableZoom: options?.enableZoom ?? true,
  enablePan: options?.enablePan ?? false,
  enableDragAnnotation: options?.enableDragAnnotation ?? false,
  closeOnOutsideClick: options?.closeOnOutsideClick ?? false,
});

const defaultInteractions = (interactions?: ILineInteractions) => ({
  onLegendClick: interactions?.onLegendClick,
  onHover: interactions?.onHover,
  onUnhover: interactions?.onUnhover,
  onAnimationComplete: interactions?.onAnimationComplete,
});

const defaultDragData = (dragData?: ICommonDragData) => ({
  enableDragData: dragData?.enableDragData ?? false,
  showTooltip: dragData?.showTooltip ?? true,
  roundPoints: dragData?.roundPoints ?? true,
  dragX: dragData?.dragX ?? true,
  dragY: dragData?.dragY ?? true,
  onDragStart: dragData?.onDragStart,
  onDrag: dragData?.onDrag,
  onDragEnd: dragData?.onDragEnd,
});

export const getDefaultProps = (props: ILineChartProps): ILineChartData => {
  const chart = props?.chart || {};
  const options = chart?.options || {};

  // Set defaults for missing properties
  return {
    persistenceId: chart?.persistenceId ?? '',
    controlsPortalId: chart?.controlsPortalId ?? '',
    testId: chart?.testId ?? undefined,
    data: chart?.data,
    options: {
      title: options?.title ?? '',
      scales: options?.scales ?? {},
      axes: defaultAxes(options?.axes),
      additionalAxesOptions: defaultAdditionalAxesOptions(
        options?.additionalAxesOptions,
      ),
      chartStyling: defaultChartStyling(options?.chartStyling),
      tooltip: defaultTooltip(options?.tooltip),
      graph: defaultGraph(options?.graph),
      annotations: defaultAnnotations(options?.annotations),
      legend: defaultLegend(options?.legend),
      chartOptions: defaultChartOptions(options?.chartOptions),
      interactions: defaultInteractions(options?.interactions),
      dragData: defaultDragData(options?.dragData),
      depthType: options?.depthType ?? {},
    },
  };
};
