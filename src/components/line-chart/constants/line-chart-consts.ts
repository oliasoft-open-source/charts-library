export const DEFAULT_POINT_RADIUS = 2;
export const DEFAULT_HOVER_RADIUS = 5;
export const DEFAULT_BORDER_WIDTH = 1;
export const BORDER_JOIN_STYLE = 'round';
export const DEFAULT_BACKGROUND_COLOR = 'transparent';
export const ZOOM_BOX_BACKGROUND_COLOR = 'rgba(0, 0, 0, 0.1)';
export const DOUBLE_CLICK = 'dblclick';
export const INIT_KEYS = {
  TRANSLATIONS: 'translations',
  LANGUAGE_KEY: 'languageKey',
};

export const LOWER_BOUND = 1e-4;
export const UPPER_BOUND = 1e7;
