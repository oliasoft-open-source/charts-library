import { INIT_KEYS } from '../constants/line-chart-consts';
import { getConfig } from './config';
import { defaultTranslations } from '../constants/default-translations';
import { initializeLineChart } from './initialize-line-chart';

describe('initializeLineChart', () => {
  // Test that the default translations are set even if initializeLineChart is not called
  test('should have default translations when not initialized', () => {
    expect(getConfig(INIT_KEYS.TRANSLATIONS)).toEqual(defaultTranslations);
  });

  // Test that the custom translations are merged correctly with the default translations
  test('should merge custom translations with default translations', () => {
    const customTranslations = {
      label: 'MyLabel',
    };

    initializeLineChart({
      [INIT_KEYS.TRANSLATIONS]: customTranslations,
    });

    const expectedTranslations = {
      ...defaultTranslations,
      ...customTranslations,
    };

    expect(getConfig(INIT_KEYS.TRANSLATIONS)).toEqual(expectedTranslations);
  });
});
