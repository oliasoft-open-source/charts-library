import { INIT_KEYS } from '../constants/line-chart-consts';
import { setConfig } from './config';
import { getTranslations } from '../utils/translations/get-translations';
import { isPrimitiveValue } from '../utils/line-chart-utils';

/**
 * Initialize the charts library with the provided configurations.
 * This function will store the configuration options in a config object.
 * @param {object} options - An object containing the configuration options for the library.
 * @param {object} options.translations - The translations to be used in the library.
 * @param {string} options.languageKey - The language key to be stored in the config object, used for translations.
 * @param {...object} options - Any additional options to be stored in the config object.
 */
export const initializeLineChart = ({ languageKey = 'en', ...options }) => {
  setConfig(INIT_KEYS.LANGUAGE_KEY, languageKey);

  Object.entries(options).forEach(([key, value]) => {
    if (key === INIT_KEYS.TRANSLATIONS) {
      setConfig(key, getTranslations(value));
    } else {
      const newValue = isPrimitiveValue(value) ? value : { ...value };
      setConfig(key, newValue);
    }
  });
};
