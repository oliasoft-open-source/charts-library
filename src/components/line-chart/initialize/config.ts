import { INIT_KEYS } from '../constants/line-chart-consts';
import { getTranslations } from '../utils/translations/get-translations';

const config: Record<string, any> = {
  [INIT_KEYS.TRANSLATIONS]: getTranslations(),
  [INIT_KEYS.LANGUAGE_KEY]: 'en',
};

/**
 * Retrieve the value for the given key from the config object.
 * @param {string} key - The key to access the corresponding value in the config object.
 * @returns {*} - The value associated with the given key in the config object or config.
 */
export const getConfig = (key = '') => (key ? config?.[key] : config);

/**
 * Set a new value for the given key in the config object.
 * @param {string} key - The key to access the corresponding value in the config object.
 * @param {*} newValue - The new value to be set for the given key.
 */
export const setConfig = (
  key: string,
  newValue: string | Record<string, string>,
) => {
  config[key] = newValue;
};
