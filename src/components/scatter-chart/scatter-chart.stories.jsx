import React from 'react';
import { ScatterChart } from './scatter-chart';

const labels1 = ['label 1', 'label 2', 'label 3'];
const dataset1 = {
  label: 'Dataset 1',
  data: [
    { x: 1, y: 5 },
    { x: 3, y: 7 },
    { x: 6, y: 2 },
    { x: 8, y: 6 },
    { x: 9, y: 4 },
  ],
};

const dataset2 = {
  label: 'Dataset 2',
  data: [
    { x: 1, y: 3 },
    { x: 4, y: 5 },
    { x: 7, y: 9 },
    { x: 10, y: 2 },
    { x: 12, y: 7 },
  ],
};

const dataset3 = {
  label: 'Dataset 3',
  data: [
    { x: 2, y: 8 },
    { x: 5, y: 6 },
    { x: 8, y: 10 },
    { x: 11, y: 5 },
    { x: 14, y: 9 },
  ],
};
const dataset4 = {
  label: 'Dataset 4',
  data: [
    { x: 2, y: 8 },
    { x: 14, y: 9 },
  ],
};

const dataset5 = {
  label: 'Dataset 5',
  data: [
    { x: 14, y: 7, label: 'Echo' },
    { x: 3, y: 18, label: 'Lima' },
    { x: 19, y: 2, label: 'Romeo' },
    { x: 8, y: 11, label: 'Charlie' },
    { x: 12, y: 9, label: 'Oscar' },
  ],
};

const dataset6 = {
  label: 'Dataset 6',
  data: [
    { x: 15, y: 3, label: 'Hotel' },
    { x: 9, y: 14, label: 'Tango' },
    { x: 4, y: 17, label: 'Whiskey' },
    { x: 11, y: 7, label: 'Juliet' },
    { x: 13, y: 1, label: 'Zulu' },
  ],
};

const datasetDecimals = {
  label: 'Dataset Decimals',
  data: [
    { x: 4.30021321123002, y: 2.30021321123005 },
    { x: 5.12321321000003, y: 3.23213210000037 },
  ],
};

const singleScatterChart = {
  data: {
    labels: labels1,
    datasets: [dataset1],
  },
};

const styledScatterChart = {
  data: {
    labels: labels1,
    datasets: [dataset1],
  },
  options: {
    chartStyling: {
      height: 300,
      width: 700,
    },
  },
};

const multipleScatterData = {
  data: {
    labels: labels1,
    datasets: [dataset1, dataset2, dataset3],
  },
};

const scatterWithDataLabels = {
  data: {
    labels: labels1,
    datasets: [dataset1],
  },
  options: {
    title: 'With Data Labels',
    graph: {
      showDataLabels: true,
    },
  },
};

const scatterWithTooltips = {
  data: {
    labels: labels1,
    datasets: [dataset1, dataset2, datasetDecimals],
  },
  options: {
    title: 'With Tooltips',
    tooltip: {
      title: 'Tooltip',
      showLabelsInTooltips: true,
    },
  },
};

const scatterWithCustomColors = {
  data: {
    labels: labels1,
    datasets: [{ ...dataset1, backgroundColor: 'red', borderColor: 'red' }],
  },
};

const scatterWithAnnotation = {
  data: {
    labels: labels1,
    datasets: [dataset1],
  },
  options: {
    title: 'With Annotation',
    annotations: {
      showAnnotations: true,
      annotationsData: [
        {
          annotationAxis: 'y',
          label: 'Horizontal Annotation',
          value: 2.5,
          color: 'rgba(96, 32, 196, 0.5)',
        },
      ],
    },
  },
};

const scatterWithAxesLabels = {
  data: {
    labels: labels1,
    datasets: [dataset1, dataset2, dataset4],
  },
  options: {
    title: 'With Axes Labels',
    axes: {
      x: [
        {
          label: 'x axis',
        },
      ],
      y: [
        {
          label: 'y axis',
        },
      ],
    },
  },
};

const scatterWithGradient = {
  ...singleScatterChart,
  options: {
    title: 'With Gradient',
    chartStyling: {
      gradient: {
        display: true,
        // gradientColors: GRADIENT_COLORS, // Default colors applied, but you can define your own as an array of objects
        // direction: GradientDirection.BottomLeftToTopRight, // Direction by default
      },
    },
  },
};

const scatterWithDatapointsHavingLabel = {
  data: {
    // labels: labels1,
    datasets: [dataset5, dataset6, dataset4],
  },
  options: {
    title: 'With datapoint label showing when hovered',
    tooltip: {
      showLabelsInTooltips: true,
    },
  },
};

export default {
  title: 'ScatterChart',
  component: ScatterChart,
  args: {
    chart: singleScatterChart,
  },
};

const Template = (args) => {
  return (
    <ScatterChart
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
    />
  );
};

export const Default = Template.bind({});
Default.args = { chart: singleScatterChart };

export const StyledChart = Template.bind({});
StyledChart.args = { chart: styledScatterChart };

export const MultipleDatasets = Template.bind({});
MultipleDatasets.args = { chart: multipleScatterData };

export const WithDataLabels = Template.bind({});
WithDataLabels.args = { chart: scatterWithDataLabels };

export const WithTooltips = Template.bind({});
WithTooltips.args = { chart: scatterWithTooltips };

export const WithAnnotation = Template.bind({});
WithAnnotation.args = { chart: scatterWithAnnotation };

export const WithAxesLabels = Template.bind({});
WithAxesLabels.args = { chart: scatterWithAxesLabels };

export const WithGradient = Template.bind({});
WithGradient.args = { chart: scatterWithGradient };

export const WithDatapointsHavingLabel = Template.bind({});
WithDatapointsHavingLabel.args = { chart: scatterWithDatapointsHavingLabel };

export const CustomPointColor = Template.bind({});
CustomPointColor.args = { chart: scatterWithCustomColors };
