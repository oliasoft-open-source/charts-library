import { ChartDirection } from 'components/common/enums.ts';
import {
  ICommonAdditionalAxesOptions,
  ICommonAxis,
  ICommonData,
  ICommonDataset,
  ICommonDataValue,
  ICommonLegend,
  ICommonOptions,
} from '../common/common.interface';

export interface IScatterLegend extends ICommonLegend {
  useDataset?: boolean;
}

export interface IScatterGraph {
  showMinorGridlines?: boolean;
  showDataLabels?: boolean;
}

export interface IScatterTooltip {
  enabled?: boolean;
  tooltips?: boolean;
  showLabelsInTooltips: boolean;
  backgroundColor?: string;
  displayColors?: boolean;
  scientificNotation?: boolean;
}

export interface IScatterAxes {
  x: ICommonAxis<'top' | 'bottom'>[];
  y: ICommonAxis<'left' | 'right'>[];
  [key: string]: ICommonAxis[];
}

export interface IScatterOptions extends ICommonOptions {
  graph?: IScatterGraph;
  legend?: IScatterLegend;
  axes?: IScatterAxes;
  additionalAxesOptions?: ICommonAdditionalAxesOptions;
  direction: ChartDirection;
}

export interface IScatterDataValue extends ICommonDataValue {
  label?: string;
}

export interface IScatterChartDataset extends ICommonDataset {
  data: IScatterDataValue[];
  hideLegend?: boolean;
}

export interface IScatterData {
  labels?: string[];
  datasets?: IScatterChartDataset[];
  xUnit?: string;
  yUnit?: string;
}

export interface IScatterChartData extends ICommonData {
  data: IScatterData;
  options: IScatterOptions;
}

export interface IScatterChartProps {
  chart: IScatterChartData;
  testId?: string | null;
}

export interface IScatterGeneratedLabel {
  text: string;
  fillStyle: string;
  strokeStyle: string;
  index: number;
}

export interface IScatterLegendItemFilter {
  index: number;
  datasetIndex: number;
  hidden: boolean;
}

export interface IScatterDataLabelsOptions {
  display?: 'auto' | boolean;
  align?: 'center';
  anchor?: 'center';
  formatter?: (_value: any, context: any) => any;
}

export interface IGeneratedScatterChartDataset {
  label: string;
  backgroundColor: string[];
  borderColor: string;
  borderWidth: number;
  borderDash: number[];
  annotationType?: string;
  data: IScatterDataValue[];
  hideLegend?: boolean;
  displayGroup?: string | number;
  isAnnotation?: boolean;
  annotationIndex?: number;
}

export type TGeneratedScatterChartDatasets = IGeneratedScatterChartDataset[];
