import { AxisType, ScaleType } from 'helpers/enums';
import { isVertical } from 'helpers/chart-utils';
import { DEFAULT_FONT_SIZE } from 'helpers/chart-consts';
import { IDefaultScatterOptions } from 'components/scatter-chart/scatter-chart-default-props.interface';
import { ICommonAxis } from '../../common/common.interface';

interface IGetScatterChartAxisParams {
  options: IDefaultScatterOptions;
  axisType?: AxisType;
  currentScale?: ICommonAxis;
}

const getScatterChartAxis = ({
  options,
  axisType = AxisType.X,
  currentScale,
}: IGetScatterChartAxisParams) => {
  const isDirectionVertical = isVertical(options.direction);

  const axisData =
    currentScale || (options?.axes?.[axisType]?.[0] as ICommonAxis);
  const isDirectionCompatibleWithAxisType =
    (isDirectionVertical && axisType === AxisType.Y) ||
    (!isDirectionVertical && axisType === AxisType.X);

  const grid = axisData?.gridLines || {};

  const getReverse = () => {
    const axisWithReverse = isDirectionVertical ? AxisType.Y : AxisType.X;
    return axisType === axisWithReverse
      ? options?.additionalAxesOptions?.reverse
      : false;
  };

  const getTicks = () => {
    const additionalAxesOptions = options?.additionalAxesOptions;
    const stepSize = {
      stepSize:
        axisData?.stepSize ??
        (axisType === AxisType.Y ? additionalAxesOptions?.stepSize : undefined),
    };
    return {
      ...stepSize,
      includeBounds: false, //OW-10088 disable irregular axis ticks
      font: {
        size: DEFAULT_FONT_SIZE,
      },
    };
  };

  return {
    type: ScaleType.Linear as 'linear',
    position: axisData?.position,
    beginAtZero: options?.additionalAxesOptions?.beginAtZero,
    reverse: getReverse(),
    suggestedMax: options?.additionalAxesOptions?.suggestedMax,
    suggestedMin: options?.additionalAxesOptions?.suggestedMin,
    min: isDirectionCompatibleWithAxisType
      ? options?.additionalAxesOptions?.min
      : undefined,
    max: isDirectionCompatibleWithAxisType
      ? options?.additionalAxesOptions?.max
      : undefined,
    title: {
      display: !!axisData?.label?.length || !!axisData?.unit?.length,
      text: axisData?.label || axisData?.unit,
      padding: 0,
    },
    ticks: getTicks(),
    grid: {
      ...grid,
    },
  };
};

export const getScatterChartScales = (options: IDefaultScatterOptions) => {
  const xAxis = getScatterChartAxis({ options, axisType: AxisType.X });
  const yAxis = getScatterChartAxis({ options, axisType: AxisType.Y });

  return {
    x: xAxis,
    y: yAxis,
  };
};
