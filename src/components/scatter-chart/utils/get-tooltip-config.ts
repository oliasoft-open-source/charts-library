import { TooltipItem } from 'chart.js';
import { IDefaultScatterOptions } from 'components/scatter-chart/scatter-chart-default-props.interface';
import { customFormatNumber } from 'helpers/custom-format-number.ts';

const titleCallback = (
  tooltipItems: TooltipItem<'scatter'>[],
  options?: IDefaultScatterOptions,
) => {
  if (options?.tooltip?.showLabelsInTooltips) {
    const item = tooltipItems[0];
    const { label } = item?.dataset ?? {};
    return label;
  }
};

interface IRawData {
  x: number;
  y: number;
  label?: string;
}

export interface IScatterTooltipItem extends TooltipItem<'scatter'> {
  raw: IRawData;
}

const labelCallback = (
  tooltipItem: IScatterTooltipItem,
  options: IDefaultScatterOptions,
) => {
  const { raw, dataset } = tooltipItem ?? {};
  const datapointLabel = raw?.label;
  const scientificNotation = options?.tooltip?.scientificNotation;

  return `${datapointLabel ?? dataset?.label} ( x: ${customFormatNumber(
    raw?.x,
    scientificNotation,
  )} , y: ${customFormatNumber(raw?.y, scientificNotation)} )`;
};

export const getTooltipsConfig = (options: IDefaultScatterOptions) => {
  return {
    enabled: options?.tooltip?.enabled,
    callbacks: {
      title: (tooltipItems: TooltipItem<'scatter'>[]) =>
        titleCallback(tooltipItems, options),
      label: (tooltipItems: IScatterTooltipItem) =>
        labelCallback(tooltipItems, options),
    },
    backgroundColor: options?.tooltip?.backgroundColor,
    displayColors: options?.tooltip?.displayColors,
    padding: 7,
  };
};
