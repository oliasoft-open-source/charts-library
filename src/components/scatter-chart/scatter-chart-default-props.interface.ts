import { ChartEvent } from 'chart.js';
import {
  ICommonAdditionalAxesOptions,
  ICommonAnnotations,
  ICommonCustomLegend,
  ICommonLegend,
} from 'components/common/common.interface';
import { ChartDirection } from 'components/common/enums';
import { IScatterAxes, IScatterChartDataset } from './scatter-chart.interface';

export interface IDefaultChartStyling {
  width?: number | string;
  height?: number | string;
  maintainAspectRatio: boolean;
  staticChartHeight: boolean;
  performanceMode: boolean;
}

interface IDefaultTooltip {
  enabled: boolean;
  tooltips: boolean;
  showLabelsInTooltips: boolean;
  backgroundColor: string;
  displayColors: boolean;
  scientificNotation: boolean;
}

interface IDefaultScatterGraph {
  showMinorGridlines: boolean;
  showDataLabels: boolean;
}

export interface IDefaultScatterLegend extends ICommonLegend {
  customLegend?: ICommonCustomLegend<any>;
}

interface IDefaultChartOptions {
  enableZoom: boolean;
  enablePan: boolean;
}

export interface IDefaultInteractions {
  onLegendClick?: (text: string, hidden: boolean) => void;
  onHover?: (
    event?: ChartEvent,
    datasetIndex?: number,
    index?: number,
    generatedDataset?: IScatterChartDataset[],
  ) => void;
  onUnhover?: (
    event?: ChartEvent,
    datasetIndex?: number,
    index?: number,
    generatedDataset?: any[],
  ) => void;
  onAnimationComplete?: () => void;
}

interface IDefaultChartData {
  labels: string[];
  datasets: IScatterChartDataset[];
}

export interface IDefaultScatterOptions {
  title: string | string[];
  chartStyling: IDefaultChartStyling;
  tooltip: IDefaultTooltip;
  graph: IDefaultScatterGraph;
  axes: IScatterAxes;
  additionalAxesOptions: ICommonAdditionalAxesOptions;
  legend: IDefaultScatterLegend;
  chartOptions: IDefaultChartOptions;
  interactions: IDefaultInteractions;
  annotations?: ICommonAnnotations;
  direction: ChartDirection;
}

export interface IScatterChartDefaultProps {
  testId: string | null;
  data: IDefaultChartData;
  options: {
    title: string | string[];
    chartStyling: IDefaultChartStyling;
    tooltip: IDefaultTooltip;
    graph: IDefaultScatterGraph;
    axes: IScatterAxes;
    additionalAxesOptions: ICommonAdditionalAxesOptions;
    legend: IDefaultScatterLegend;
    chartOptions: IDefaultChartOptions;
    interactions: IDefaultInteractions;
    annotations?: ICommonAnnotations;
    direction: ChartDirection;
  };
}
