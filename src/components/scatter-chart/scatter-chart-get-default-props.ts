import {
  IScatterChartDefaultProps,
  IDefaultInteractions,
} from 'components/scatter-chart/scatter-chart-default-props.interface';
import { AlignOptions, Position } from 'helpers/enums';
import { AUTO, GRADIENT_COLORS } from 'helpers/chart-consts';
import { ChartDirection } from 'components/common/enums';
import { GradientDirection } from 'components/common/plugins/gradient-background-plugin/enums';
import {
  IScatterAxes,
  IScatterChartProps,
  IScatterData,
  IScatterGraph,
  IScatterLegend,
  IScatterTooltip,
} from './scatter-chart.interface';
import {
  ICommonAdditionalAxesOptions,
  ICommonAnnotations,
  ICommonAnnotationsData,
  ICommonAxis,
  ICommonChartOptions,
  ICommonGradient,
  ICommonStyling,
} from '../common/common.interface';

const defaultChartGradient = (gradient?: ICommonGradient) => ({
  display: gradient?.display ?? false,
  gradientColors: gradient?.gradientColors ?? GRADIENT_COLORS,
  direction: gradient?.direction ?? GradientDirection.BottomLeftToTopRight,
});

const defaultChartStyling = (styling?: ICommonStyling) => ({
  width: styling?.width ?? AUTO,
  height: styling?.height ?? AUTO,
  maintainAspectRatio: styling?.maintainAspectRatio || false,
  staticChartHeight: styling?.staticChartHeight || false,
  performanceMode: styling?.performanceMode ?? true,
  gradient: defaultChartGradient(styling?.gradient),
});

const defaultTooltip = (tooltip?: IScatterTooltip) => ({
  enabled: tooltip?.enabled ?? true,
  tooltips: tooltip?.tooltips ?? true,
  showLabelsInTooltips: tooltip?.showLabelsInTooltips || false,
  backgroundColor: tooltip?.backgroundColor || '#333',
  displayColors: tooltip?.displayColors || false,
  scientificNotation: tooltip?.scientificNotation ?? true,
});

const defaultGraph = (graph?: IScatterGraph) => ({
  showMinorGridlines: graph?.showMinorGridlines ?? false,
  showDataLabels: graph?.showDataLabels ?? false,
});

const defaultLegend = (legend?: IScatterLegend) => ({
  display: legend?.display ?? true,
  useDataset: legend?.useDataset || false,
  position: legend?.position || Position.BottomLeft,
  align: legend?.align || AlignOptions.Center,
});

const defaultChartOptions = (options?: ICommonChartOptions) => ({
  enableZoom: options?.enableZoom || false,
  enablePan: options?.enablePan || false,
});

const defaultInteractions = (interactions?: IDefaultInteractions) => ({
  onLegendClick: interactions?.onLegendClick,
  onHover: interactions?.onHover,
  onUnhover: interactions?.onUnhover,
  onAnimationComplete: interactions?.onAnimationComplete,
});

const defaultChartData = (data?: IScatterData) => {
  return {
    labels: data?.labels || [],
    datasets: data?.datasets || [],
  };
};

const defaultAnnotationsData = (annotationsData?: ICommonAnnotationsData[]) => {
  return annotationsData
    ? annotationsData.map((ann) => ({ ...ann, display: ann?.display ?? true }))
    : [];
};

const defaultAnnotations = (annotations?: ICommonAnnotations) => ({
  showAnnotations: annotations?.showAnnotations ?? true,
  controlAnnotation: annotations?.controlAnnotation || false,
  annotationsData: defaultAnnotationsData(annotations?.annotationsData),
});

const defaultAxes = (axes?: IScatterAxes) => ({
  x: axes?.x || ([{}] as ICommonAxis[]),
  y: axes?.y || ([{}] as ICommonAxis[]),
});

const defaultAdditionalAxesOptions = (
  options?: ICommonAdditionalAxesOptions,
) => ({
  reverse: options?.reverse || false,
  stacked: options?.stacked || false,
  beginAtZero: options?.beginAtZero ?? true,
  stepSize: options?.stepSize,
  suggestedMin: options?.suggestedMin,
  suggestedMax: options?.suggestedMax,
  min: options?.min,
  max: options?.max,
});

export const getDefaultProps = (
  props: IScatterChartProps,
): IScatterChartDefaultProps => {
  const chart = props?.chart || {};
  const options = chart?.options || {};

  return {
    testId: chart?.testId ?? null,
    data: defaultChartData(chart?.data),
    options: {
      title: options?.title || '',
      axes: defaultAxes(options?.axes),
      additionalAxesOptions: defaultAdditionalAxesOptions(
        options?.additionalAxesOptions,
      ),
      direction: options?.direction || ChartDirection.VERTICAL,
      chartStyling: defaultChartStyling(options?.chartStyling),
      tooltip: defaultTooltip(options?.tooltip),
      graph: defaultGraph(options?.graph),
      legend: defaultLegend(options?.legend),
      annotations: defaultAnnotations(options?.annotations),
      chartOptions: defaultChartOptions(options?.chartOptions),
      interactions: defaultInteractions(options?.interactions),
    },
  };
};
