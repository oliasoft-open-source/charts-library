import { MutableRefObject, useRef } from 'react';
import {
  Chart as ChartJS,
  LinearScale,
  PointElement,
  Legend,
  Tooltip,
  Title,
  CategoryScale,
} from 'chart.js';
import { Scatter } from 'react-chartjs-2';
import zoomPlugin from 'chartjs-plugin-zoom';
import dataLabelsPlugin from 'chartjs-plugin-datalabels';
import annotationPlugin from 'chartjs-plugin-annotation';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { getClassName, getPlugins, setDefaultTheme } from 'helpers/chart-utils';
import { useScatterChartConfig } from 'components/scatter-chart/hooks/use-scatter-chart-config';
import { getDefaultProps } from 'components/scatter-chart/scatter-chart-get-default-props';
import { IScatterChartProps } from 'components/scatter-chart/scatter-chart.interface';
import { gradientBackgroundPlugin } from 'components/common/plugins/gradient-background-plugin/gradient-background-plugin';
import CustomLegend from '../common/legend-component/legend';
import { LegendProvider } from '../common/legend-component/state/legend-context';
import { ChartType } from '../common/enums';
import styles from './scatter-chart.module.less';

ChartJS.register(
  LinearScale,
  PointElement,
  CategoryScale,
  Legend,
  Tooltip,
  Title,
  zoomPlugin,
  dataLabelsPlugin,
  annotationPlugin,
  gradientBackgroundPlugin,
);

const ScatterChart = (props: IScatterChartProps) => {
  setDefaultTheme();
  const chartRef: MutableRefObject<null> = useRef(null);
  const chart = getDefaultProps(props);
  const { options, testId } = chart;

  const { scatterOptions, generatedDatasets } = useScatterChartConfig(
    chart,
    chartRef,
  );

  return (
    <div
      className={getClassName(options.chartStyling, styles)}
      style={{
        width: options.chartStyling.width,
        height: options.chartStyling.height,
      }}
      data-testid={testId}
    >
      <DndProvider backend={HTML5Backend} context={window}>
        <div className={styles.canvas} id="canvas">
          <Scatter
            ref={chartRef}
            data={{
              datasets: generatedDatasets,
            }}
            options={scatterOptions}
            plugins={getPlugins<'scatter'>(options.graph, options.legend)}
          />
          {!!generatedDatasets.length && (
            <CustomLegend
              chartRef={chartRef}
              legendConfig={{
                options,
                generatedDatasets,
                chartType: ChartType.SCATTER,
              }}
            />
          )}
        </div>
      </DndProvider>
    </div>
  );
};

const ScatterChartWithLegend = (props: IScatterChartProps) => {
  const { options } = getDefaultProps(props);

  return (
    <LegendProvider options={options}>
      <ScatterChart {...props} />
    </LegendProvider>
  );
};

export { ScatterChartWithLegend as ScatterChart };
