import { ActiveElement, AnimationSpec, Chart, ChartEvent } from 'chart.js';
import { ANIMATION_DURATION, BORDER_COLOR, COLORS } from 'helpers/chart-consts';
import { getTitle } from 'helpers/chart-utils';
import { IScatterChartDefaultProps } from 'components/scatter-chart/scatter-chart-default-props.interface';
import { MutableRefObject, useState } from 'react';
import { useLegendState } from 'components/common/hooks/use-legend-state';
import { ICommonHoveredItems } from 'components/common/common.interface';
import { toAnnotationObject } from 'helpers/to-annotation';
import { useLegend } from 'components/common/hooks/use-legend';
import { DEFAULT_BORDER_WIDTH } from 'components/line-chart/constants/line-chart-consts';
import { getTooltipsConfig } from 'components/scatter-chart/utils/get-tooltip-config';
import { getScatterChartScales } from 'components/scatter-chart/utils/get-scales-config';
import {
  IScatterChartDataset,
  TGeneratedScatterChartDatasets,
} from '../scatter-chart.interface';

export const useScatterChartConfig = (
  chart: IScatterChartDefaultProps,
  chartRef: MutableRefObject<Chart | null>,
) => {
  const { data, options: defaultOptions } = chart;
  const { interactions, chartStyling } = defaultOptions;

  const [pointHover, setPointHover] = useState(false);

  const { legend, customLegendPlugin, legendClick } = useLegendState({
    chartRef,
    options: defaultOptions,
  });
  const { state: legendState } = useLegend();
  const { annotation } = legendState;

  const generateDatasets = (datasets: IScatterChartDataset[]) => {
    return datasets.map(
      (scatterDataset: IScatterChartDataset, index: number) => {
        const { borderWidth: inputBorderWidth = DEFAULT_BORDER_WIDTH } =
          scatterDataset ?? {};

        const borderWidth = parseFloat(String(inputBorderWidth)) || 1;
        const color = COLORS[index];
        const borderColor = scatterDataset.borderColor ?? color;
        const backgroundColor = scatterDataset.backgroundColor ?? color;

        return {
          ...scatterDataset,
          borderWidth,
          borderColor,
          backgroundColor,
        };
      },
    );
  };

  const generatedDatasets = generateDatasets(
    data.datasets,
  ) as TGeneratedScatterChartDatasets;

  const onClick = (
    _evt: ChartEvent,
    _elements: ActiveElement[],
    chartInstance: Chart,
  ) => {
    chartInstance.resetZoom();
  };

  const onHover = (evt: ChartEvent, hoveredItems: ICommonHoveredItems[]) => {
    if (pointHover && !hoveredItems?.length) {
      setPointHover(false);
      if (interactions.onUnhover) {
        interactions.onUnhover(evt);
      }
    }
    if (!pointHover && hoveredItems?.length) {
      setPointHover(true);
      if (interactions.onHover) {
        const { index, datasetIndex } = hoveredItems[0];
        interactions.onHover(evt, datasetIndex, index, generatedDatasets);
      }
    }
  };

  const scatterOptions = {
    onClick,
    onHover,
    ...chartStyling,
    interactions: {
      onLegendClick: legendClick,
      onHover,
    },
    scales: getScatterChartScales(chart?.options),
    animation: chartStyling.performanceMode
      ? {
          duration: ANIMATION_DURATION.NO,
        }
      : ({
          duration: ANIMATION_DURATION.FAST,
          onComplete: interactions?.onAnimationComplete,
        } as Partial<AnimationSpec<'scatter'>>),
    plugins: {
      legend: { ...legend, display: false, events: [] }, // hide default legend
      customLegendPlugin,
      title: getTitle(defaultOptions),
      annotation: toAnnotationObject(annotation),
      chartAreaBorder: {
        borderColor: BORDER_COLOR,
      },
      datalabels: {
        display: defaultOptions.graph.showDataLabels,
      },
      tooltip: getTooltipsConfig(defaultOptions),
    },
  };

  return {
    generatedDatasets,
    scatterOptions,
  };
};
