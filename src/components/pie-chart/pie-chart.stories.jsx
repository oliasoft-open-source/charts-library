import React from 'react';
import { PieChart } from './pie-chart';

const labels1 = ['label 1', 'label 2', 'label 3'];
const dataset1 = { label: 'data1', data: [3, 8, 7] };
const dataset2 = { label: 'data2', data: [1, 2, 3] };
const dataset3 = { label: 'data3', data: [5, 0, 4] };

const offsetDataset = {
  label: 'spacing data',
  offset: 40,
  data: [3, 8, 7],
};

const hoverOffsetDataset = {
  label: 'hover data',
  data: [3, 8, 7],
  hoverOffset: 40,
};

const datasetDatalabels = {
  label: 'data',
  data: [
    {
      value: 210,
      label: 'label',
    },
    {
      value: 333,
      label: 'labal',
    },
    {
      value: 248,
      label: 'lebel',
    },
  ],
};
const datasetNoDataLabels = {
  label: 'data',
  data: [210, 333, 248],
};

const singleChart = {
  data: {
    labels: labels1,
    datasets: [dataset1],
  },
};

const multiplePieData = {
  data: {
    labels: labels1,
    datasets: [dataset1, dataset2, dataset3],
  },
};
const stackedMultiplePieData = {
  data: {
    labels: labels1,
    datasets: [dataset1, dataset2, dataset3],
  },
  options: {
    title: 'Stacked',
    graph: { stacked: true },
  },
};

const dataLabelsChart = {
  data: {
    labels: labels1,
    datasets: [datasetDatalabels],
  },
  options: {
    title: 'With custom datalabels',
    graph: {
      showDataLabels: true,
    },
  },
};
const dataLabelsNumberChart = {
  data: {
    labels: labels1,
    datasets: [datasetNoDataLabels],
  },
  options: {
    title: 'With value as datalabels',
    graph: {
      showDataLabels: true,
    },
  },
};

const dataLabelssTooltips = {
  data: {
    labels: labels1,
    datasets: [datasetDatalabels],
  },
  options: {
    title: 'With datalabels in tooltips',
    tooltip: {
      showLabelsInTooltips: true,
    },
  },
};

const hoverOffsetChart = {
  data: {
    labels: labels1,
    datasets: [hoverOffsetDataset],
  },
  options: { title: 'Hover offset' },
};

const offsetChart = {
  data: {
    labels: labels1,
    datasets: [offsetDataset],
  },
  options: { title: 'Segment offset' },
};

const animatedChart = {
  data: {
    labels: labels1,
    datasets: [dataset1],
  },
  options: {
    chartStyling: { performanceMode: false },
  },
};

const doughnutPercentage = {
  data: {
    labels: labels1,
    datasets: [dataset1],
  },
  options: {
    title: 'Cutout 50%',
    graph: { cutout: '50%' },
  },
};

const doughnutNumber = {
  data: {
    labels: labels1,
    datasets: [dataset1],
  },
  options: {
    title: 'Cutout 20 pixels',
    graph: { cutout: 20 },
  },
};

export default {
  title: 'PieChart',
  component: PieChart,
  args: {
    chart: singleChart,
  },
};

const Template = (args) => {
  return (
    <PieChart
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
    />
  );
};
export const Default = Template.bind({});

export const MultipleSets = Template.bind({});
MultipleSets.args = { chart: multiplePieData };

export const MultipleSetsStacked = Template.bind({});
MultipleSetsStacked.args = { chart: stackedMultiplePieData };

export const Datalabels = Template.bind({});
Datalabels.args = { chart: dataLabelsChart };

export const DatalabelsValues = Template.bind({});
DatalabelsValues.args = { chart: dataLabelsNumberChart };

export const DatalabelsInTooltips = Template.bind({});
DatalabelsInTooltips.args = { chart: dataLabelssTooltips };

export const Animated = Template.bind({});
Animated.args = { chart: animatedChart };

export const Doughnut = Template.bind({});
Doughnut.args = { chart: doughnutPercentage };

export const DoughnutFixedCutout = Template.bind({});
DoughnutFixedCutout.args = { chart: doughnutNumber };

export const HoverOffset = Template.bind({});
HoverOffset.args = { chart: hoverOffsetChart };

export const SegmentOffset = Template.bind({});
HoverOffset.args = { chart: offsetChart };
