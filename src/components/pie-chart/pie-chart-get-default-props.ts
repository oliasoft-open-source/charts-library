import {
  IPieChartProps,
  IPieData,
  IPieGraph,
  IPieLegend,
} from './pie-chart.interface';
import {
  ICommonChartOptions,
  ICommonInteractions,
  ICommonStyling,
  ICommonTooltip,
} from '../common/common.interface';
import { AlignOptions, Position } from '../common/helpers/enums';
import { IPieChartDefaultProps } from './pie-chart-defalut-props.interface';

const defaultChartStyling = (styling?: ICommonStyling) => ({
  width: styling?.width,
  height: styling?.height,
  maintainAspectRatio: styling?.maintainAspectRatio || false,
  staticChartHeight: styling?.staticChartHeight || false,
  performanceMode: styling?.performanceMode ?? true,
});

const defaultTooltip = (tooltip?: ICommonTooltip) => ({
  tooltips: tooltip?.tooltips ?? true,
  showLabelsInTooltips: tooltip?.showLabelsInTooltips || false,
  scientificNotation: tooltip?.scientificNotation ?? true,
});

const defaultGraph = (graph?: IPieGraph) => ({
  showDataLabels: graph?.showDataLabels || false,
  stacked: graph?.stacked || false,
  cutout: graph?.cutout || 0,
});

const defaultLegend = (legend?: IPieLegend) => ({
  display: legend?.display ?? true,
  useDataset: legend?.useDataset || false,
  position: legend?.position || Position.Bottom,
  align: legend?.align || AlignOptions.Center,
});

const defaultChartOptions = (options?: ICommonChartOptions) => ({
  enableZoom: options?.enableZoom || false,
  enablePan: options?.enablePan || false,
});

const defaultInteractions = (interactions?: ICommonInteractions) => ({
  onLegendClick: interactions?.onLegendClick,
  onHover: interactions?.onHover,
  onUnhover: interactions?.onUnhover,
});

const defaultChartData = (data?: IPieData) => {
  return {
    labels: data?.labels || [],
    datasets: data?.datasets || [],
  };
};

export const getDefaultProps = (
  props: IPieChartProps,
): IPieChartDefaultProps => {
  const chart = props?.chart || {};
  const options = chart?.options || {};

  return {
    testId: chart?.testId ?? null,
    data: defaultChartData(chart?.data),
    options: {
      title: options?.title || '',
      chartStyling: defaultChartStyling(options?.chartStyling),
      tooltip: defaultTooltip(options?.tooltip),
      graph: defaultGraph(options?.graph),
      legend: defaultLegend(options?.legend),
      chartOptions: defaultChartOptions(options?.chartOptions),
      interactions: defaultInteractions(options?.interactions),
    },
  };
};
