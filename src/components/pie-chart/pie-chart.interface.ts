import {
  ICommonData,
  ICommonDataset,
  ICommonDataValue,
  ICommonLegend,
  ICommonOptions,
} from '../common/common.interface';

export interface IPieLegend extends ICommonLegend {
  useDataset?: boolean;
}

export interface IPieGraph {
  showDataLabels?: boolean;
  stacked?: boolean;
  cutout?: number | string;
}

export interface IPieOptions extends ICommonOptions {
  graph?: IPieGraph;
  legend?: IPieLegend;
}

export interface IPieDataValue extends ICommonDataValue {
  label: string;
  borderWidth: string | number;
  value: number;
  backgroundColor?: string | string[];
  borderColor?: string;
  labels: string[];
}

export interface IPieChartDataset extends ICommonDataset {
  data: IPieDataValue[];
  hideLegend?: boolean;
}

export interface IPieData {
  labels?: string[];
  datasets?: IPieChartDataset[];
  xUnit?: string;
  yUnit?: string;
}

export interface IPieChartData extends ICommonData {
  data: IPieData;
  options: IPieOptions;
}

export interface IPieChartProps {
  chart: IPieChartData;
  testId?: string | null;
}

export interface IPieGeneratedLabel {
  text: string;
  fillStyle: string;
  strokeStyle: string;
  index: number;
}

export interface IPieLegendItemFilter {
  index: number;
  datasetIndex: number;
  hidden: boolean;
}

export interface IPiesDataLabelsOptions {
  display?: 'auto' | boolean;
  align?: 'center';
  anchor?: 'center';
  formatter?: (_value: any, context: any) => any;
}

export interface IGeneratedPieChartDataset {
  label: string;
  backgroundColor: string[];
  borderColor: string;
  borderWidth: number;
  borderDash: number[];
  annotationType?: string;
  data: IPieDataValue[];
  hideLegend?: boolean;
  displayGroup?: string | number;
  isAnnotation?: boolean;
  annotationIndex?: number;
}

export type TGeneratedPieChartDatasets = IGeneratedPieChartDataset[];
