import { ChartEvent } from 'chart.js';
import { ICommonTooltip } from 'components/common/common.interface';
import { IPieChartDataset, IPieOptions } from './pie-chart.interface';
import { AlignOptions, Position } from '../common/helpers/enums';

export interface IDefaultChartStyling {
  width?: number | string;
  height?: number | string;
  maintainAspectRatio: boolean;
  staticChartHeight: boolean;
  performanceMode: boolean;
}

interface IDefaultPieGraph {
  showDataLabels: boolean;
  stacked: boolean;
  cutout: number | string;
}

interface IDefaultPieLegend {
  display: boolean;
  useDataset: boolean;
  position: Position;
  align: AlignOptions;
}

interface IDefaultChartOptions {
  enableZoom: boolean;
  enablePan: boolean;
}

interface IDefaultInteractions {
  onLegendClick?: (text: string, hidden: boolean) => void;
  onHover?: (
    event?: ChartEvent,
    datasetIndex?: number,
    index?: number,
    generatedDataset?: IPieChartDataset[],
  ) => void;
  onUnhover?: (
    event?: ChartEvent,
    datasetIndex?: number,
    index?: number,
    generatedDataset?: IPieChartDataset[],
  ) => void;
}

interface IDefaultChartData {
  labels: string[];
  datasets: IPieChartDataset[];
}

interface IDefaultPieOptions extends IPieOptions {
  title: string | string[];
  chartStyling: IDefaultChartStyling;
  tooltip: ICommonTooltip;
  graph: IDefaultPieGraph;
  legend: IDefaultPieLegend;
  chartOptions: IDefaultChartOptions;
  interactions: IDefaultInteractions;
}

export interface IPieChartDefaultProps {
  testId: string | null;
  data: IDefaultChartData;
  options: IDefaultPieOptions;
}
