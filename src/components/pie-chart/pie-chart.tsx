import { useRef } from 'react';
import cx from 'classnames';
import {
  ArcElement,
  CategoryScale,
  Chart as ChartJS,
  Filler,
  Legend,
  LinearScale,
  LogarithmicScale,
  Title,
  Tooltip,
} from 'chart.js';
import zoomPlugin from 'chartjs-plugin-zoom';
import dataLabelsPlugin from 'chartjs-plugin-datalabels';
import { Pie } from 'react-chartjs-2';
import { ANIMATION_DURATION } from '../common/helpers/chart-consts';
import styles from './pie-chart.module.less';
import { getDefaultProps } from './pie-chart-get-default-props';
import { setDefaultTheme } from '../common/helpers/chart-utils';
import { IPieChartProps } from './pie-chart.interface';
import { usePieChartConfig } from './use-pie-chart-config';
import { LegendProvider } from '../common/legend-component/state/legend-context';

ChartJS.register(
  LinearScale,
  CategoryScale,
  LogarithmicScale,
  ArcElement,
  Legend,
  Tooltip,
  Title,
  Filler,
  zoomPlugin,
  dataLabelsPlugin,
);

/**
 * this is a pie chart component
 * @param {props: IPieChartProps} IPieChartProps
 * ./pie-chart.interface
 */
const PieChart = (props: IPieChartProps) => {
  setDefaultTheme();
  const chart = getDefaultProps(props);
  const chartRef = useRef(null);

  const { data, options, testId } = chart;

  const {
    generatedDatasets,
    getTitle,
    onClick,
    onHover,
    getDatalabels,
    getLegend,
    getToolTips,
  } = usePieChartConfig(chart);

  return (
    <div
      className={cx(
        styles.chart,
        !options.chartStyling.width || !options.chartStyling.height
          ? options.chartStyling.staticChartHeight
            ? styles.fixedHeight
            : styles.stretchHeight
          : '',
      )}
      style={{
        width: options.chartStyling.width || 'auto',
        height: options.chartStyling.height || 'auto',
      }}
      data-testid={testId}
    >
      <Pie
        ref={chartRef}
        data={{
          datasets: generatedDatasets,
          labels: options.graph.stacked ? undefined : data.labels,
        }}
        options={{
          cutout: options.graph.cutout,
          onClick,
          responsive: true, // Defaults to true, should be removed
          maintainAspectRatio: options.chartStyling.maintainAspectRatio,
          animation: options.chartStyling.performanceMode
            ? false
            : {
                duration: ANIMATION_DURATION.FAST,
              },
          hover: {
            mode: 'nearest',
            intersect: true,
          },
          onHover,
          plugins: {
            annotation: false as any, // Disable globally-registered annotation plugin
            title: getTitle(),
            datalabels: getDatalabels(),
            tooltip: getToolTips(),
            legend: getLegend(),
          },
        }}
      />
    </div>
  );
};

const PieChartWithLegend = (props: IPieChartProps) => {
  const { options } = getDefaultProps(props);
  return (
    <LegendProvider options={options}>
      <PieChart {...props} />
    </LegendProvider>
  );
};

export { PieChartWithLegend as PieChart };
