import {
  ActiveElement,
  Chart,
  Chart as ChartJS,
  ChartEvent,
  LegendItem,
  Plugin,
  TooltipItem,
} from 'chart.js';
import { useState } from 'react';
import { displayNumber, roundByMagnitude } from '@oliasoft-open-source/units';
import {
  DISPLAY_SCIENTIFIC_LOWER_BOUND,
  DISPLAY_SCIENTIFIC_UPPER_BOUND,
} from 'components/bar-chart/bar-chart-const';
import { generateRandomColor } from '../common/helpers/chart-utils';
import {
  COLORS,
  LEGEND_LABEL_BOX_SIZE,
  TOOLTIP_BOX_PADDING,
  TOOLTIP_PADDING,
} from '../common/helpers/chart-consts';
import {
  IGeneratedPieChartDataset,
  IPieChartDataset,
  IPieData,
  IPieDataValue,
  IPieLegendItemFilter,
  IPiesDataLabelsOptions,
} from './pie-chart.interface';
import { IPieChartDefaultProps } from './pie-chart-defalut-props.interface';
import { UnusedParameter } from '../common/common.interface';

interface ILegendPlugin extends Plugin {
  chart?: Chart;
}

export const usePieChartConfig = (chart: IPieChartDefaultProps) => {
  const [pointHover, setPointHover] = useState(false);
  const { data, options } = chart;

  const generateDatasets = (datasets: IPieChartDataset[]) => {
    const copyDataset = [...datasets];

    if (options.graph.stacked) {
      const generatedDataset: IGeneratedPieChartDataset = {
        label: copyDataset?.[0]?.label ?? '',
        backgroundColor: [],
        borderColor: '',
        borderWidth: Number(copyDataset[0].borderWidth) || 1,
        data: [] as IPieDataValue[],
        borderDash: [],
      };
      data.labels.map((_label: string, labelIndex: number) => {
        copyDataset.map((arc: IPieChartDataset, arcIndex: number) => {
          generatedDataset.data.push(arc.data[labelIndex]);
          const { backgroundColor } = arc;
          const { borderColor } = arc;
          generatedDataset.backgroundColor.push(
            (Array.isArray(backgroundColor)
              ? backgroundColor[labelIndex % (borderColor?.length || 1)]
              : backgroundColor) ||
              COLORS[labelIndex] + `${99 - 10 * arcIndex}`,
          );
          generatedDataset.borderColor = borderColor ?? '';
        });
      });
      return [generatedDataset];
    }

    const generatedDatasets = copyDataset.map(
      (pieDataset: IPieChartDataset, index: number) => {
        const { borderWidth: inputBorderWidth = '1' } = pieDataset ?? {};
        const borderWidth = parseFloat(String(inputBorderWidth)) || 1;
        const color = COLORS[index];
        const borderColor = pieDataset.borderColor || color;
        const backgroundColor =
          pieDataset.backgroundColor || generateRandomColor(COLORS);

        return {
          ...pieDataset,
          borderWidth,
          borderColor,
          backgroundColor,
        };
      },
    );
    return generatedDatasets;
  };

  const generatedDatasets = generateDatasets(data.datasets);

  const getTitle = () => {
    return options.title !== ''
      ? {
          display: true,
          text: chart.options.title,
        }
      : {};
  };

  const legendClick = (
    _: UnusedParameter,
    legendItem?: LegendItem,
    legend?: ILegendPlugin,
  ) => {
    const { chart } = legend ?? {};
    if (legendItem && chart) {
      const { index = 0 } = legendItem;
      chart.toggleDataVisibility(index);
      chart.update();

      // For custom interaction callback, invoke it
      if (options.interactions?.onLegendClick) {
        const hidden = chart.isDatasetVisible(index);
        options.interactions.onLegendClick(legendItem.text, !hidden);
      }
    }
  };

  const onClick = (
    _evt: ChartEvent,
    _elements: ActiveElement[],
    chartInstance: ChartJS,
  ) => {
    chartInstance.resetZoom();
    // TODO: Restore redux-logic for zoom
  };

  const onHover = (evt: ChartEvent, hoveredItems: ActiveElement[]) => {
    if (pointHover && !hoveredItems?.length) {
      setPointHover(false);
      if (options.interactions.onUnhover) {
        options.interactions.onUnhover(evt);
      }
    }
    if (!pointHover && hoveredItems?.length) {
      setPointHover(true);
      if (options.interactions.onHover) {
        const { index, datasetIndex } = hoveredItems[0];
        const generatedDataset = generatedDatasets;
        options.interactions.onHover(
          evt,
          datasetIndex,
          index,
          generatedDataset,
        );
      }
    }
  };

  const getDatalabels = (): IPiesDataLabelsOptions => {
    return options.graph.showDataLabels
      ? {
          display: 'auto',
          align: 'center',
          anchor: 'center',
          formatter: (_: UnusedParameter, context: any) => {
            if (
              context.chart.getDatasetMeta(
                options.graph.stacked ? 0 : context.datasetIndex,
              ).data[context.dataIndex].hidden
            )
              return '';
            const dataElement = context.dataset.data[context.dataIndex];
            const label =
              dataElement?.label ||
              (typeof dataElement === 'number'
                ? dataElement
                : Array.isArray(dataElement)
                ? dataElement.reduce((acc, curr, i) => {
                    if (i === 0) return `${acc} ${curr}`;
                    return `${acc}, ${curr}`;
                  }, '')
                : '');
            const dataLabel =
              typeof dataElement === 'number'
                ? Number.isInteger(label)
                  ? label
                  : label.toFixed(2)
                : label;
            return dataLabel;
          },
        }
      : {
          display: false,
        };
  };

  const getLegend = (): object => {
    return {
      position: options.legend.position,
      display: options.legend.display,
      align: options.legend.align,
      labels: {
        boxHeight: 6,
        boxWidth: 6,
        usePointStyle: true,
        filter: (item: IPieLegendItemFilter) => {
          if (!options.legend.useDataset) return true;
          return !data.datasets[item.index]?.hideLegend;
        },
      },
      onClick: legendClick,
    };
  };

  const getToolTips = () => {
    const customFormatNumber = (labelNumber: number) => {
      let roundOptions = {};

      if (!options?.tooltip?.scientificNotation) {
        roundOptions = { scientific: false };
      } else if (
        Math.abs(labelNumber) < DISPLAY_SCIENTIFIC_LOWER_BOUND ||
        Math.abs(labelNumber) > DISPLAY_SCIENTIFIC_UPPER_BOUND
      ) {
        roundOptions = { roundScientificCoefficient: 3 };
      }

      return displayNumber(roundByMagnitude(labelNumber), roundOptions);
    };

    return {
      callbacks: {
        title: (tooltipItem: TooltipItem<any>[]) => {
          const { dataIndex } = tooltipItem[0];
          const index = options.graph.stacked
            ? parseInt(String(dataIndex / data.labels.length))
            : dataIndex;
          const label = data.labels[index];
          return `${label}`;
        },
        label: (tooltipItem: TooltipItem<any>) => {
          const { dataIndex } = tooltipItem;
          const datasetIndex = options.graph.stacked
            ? dataIndex % data.datasets.length
            : tooltipItem.datasetIndex;
          const dataLabel =
            data.datasets.length > 1
              ? `${data.datasets[datasetIndex]?.label}: `
              : '';
          const value = tooltipItem.dataset.data[dataIndex];
          const labelValue =
            typeof value === 'object'
              ? `${value.value || ''} ${
                  options.tooltip.showLabelsInTooltips && value.label
                    ? '(' + value.label + ')'
                    : ''
                }`
              : value;

          const unit = (data as IPieData)?.yUnit
            ? `[${(data as IPieData)?.yUnit}]`
            : '';

          return `${dataLabel} ${customFormatNumber(labelValue)} ${unit}`;
        },
      },
      padding: TOOLTIP_PADDING,
      boxWidth: LEGEND_LABEL_BOX_SIZE,
      boxHeight: LEGEND_LABEL_BOX_SIZE,
      boxPadding: TOOLTIP_BOX_PADDING,
      usePointStyle: true,
    };
  };
  return {
    generatedDatasets,
    getTitle,
    legendClick,
    onClick,
    onHover,
    getDatalabels,
    getLegend,
    getToolTips,
  };
};
