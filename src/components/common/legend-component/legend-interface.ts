import { MouseEventHandler, RefObject } from 'react';
import { Chart, ChartArea } from 'chart.js';
import {
  IGeneratedScatterChartDataset,
  TGeneratedScatterChartDatasets,
  IScatterOptions,
} from 'components/scatter-chart/scatter-chart.interface';
import { Position } from '../helpers/enums';
import {
  IGeneratedLineChartDataset,
  ILineChartOptions,
  TGeneratedLineChartDatasets,
} from '../../line-chart/line-chart.interface';
import {
  IBarOptions,
  IGenerateBarChartDataset,
  TGenerateBarChartDatasets,
} from '../../bar-chart/bar-chart.interface';
import {
  IGeneratedPieChartDataset,
  IPieOptions,
  TGeneratedPieChartDatasets,
} from '../../pie-chart/pie-chart.interface';
import { ChartType } from '../enums';

export interface ILegendDropZone {
  position: Position;
  onDrop: () => void;
  placeholderSize?: {
    width?: number;
    height?: number;
  };
}

export interface ILegendDropZones {
  chartArea?: ChartArea;
  setLegendPosition: (position: Position) => void;
  isDragging: boolean;
  placeholderSize?: {
    width?: number;
    height?: number;
  };
}

export type TLegendDataset =
  | IGeneratedLineChartDataset
  | IGenerateBarChartDataset
  | IGeneratedPieChartDataset
  | IGeneratedScatterChartDataset;
export type TLegendDatasets =
  | TGeneratedLineChartDatasets
  | TGenerateBarChartDatasets
  | TGeneratedPieChartDatasets
  | TGeneratedScatterChartDatasets;

export type TChartsOption =
  | ILineChartOptions
  | IBarOptions
  | IPieOptions
  | IScatterOptions;

export interface ILegendItem {
  hidden: boolean;
  dataset: TLegendDataset;
  handleClick: MouseEventHandler<HTMLDivElement>;
  chartType: ChartType;
  index: number;
}

export interface ILegendConfig {
  options: TChartsOption;
  generatedDatasets: TLegendDatasets;
  chartType: ChartType;
}

export interface ILegend {
  chartRef: RefObject<Chart>;
  legendConfig: ILegendConfig;
}

export interface ILegendPanel extends ILegend {
  legendPosition: Position;
  isDragging: boolean;
}
