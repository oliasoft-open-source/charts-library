import { forwardRef, useState } from 'react';
import cx from 'classnames';
import { Button } from '@oliasoft-open-source/react-ui-library';
import { TbList, TbX } from 'react-icons/tb';
import { UnusedParameter } from '../common.interface';
import styles from './legend.module.less';
import { ILegendPanel, TLegendDatasets } from './legend-interface';
import LegendItem from './legend-item/legend-item';
import { ChartType } from '../enums';
import { useLegendState } from '../hooks/use-legend-state';
import { useGeneratedLabels } from '../hooks/use-generated-labels';
import { createLegendStyle } from './utils/create-style-object';

interface ILegendItemProps {
  label: string;
  text: string;
  hidden: boolean;
  datasetIndex: number;
}

interface ILegendItemsProps {
  items: ILegendItemProps[];
  datasets: TLegendDatasets;
  legendClick: (_: UnusedParameter, item: ILegendItemProps) => void;
  chartType: ChartType;
}

const LegendItems = ({
  items,
  datasets,
  legendClick,
  chartType,
}: ILegendItemsProps) => (
  <div className={styles.legendItems}>
    {items?.map((item, index) => {
      if (datasets?.[item?.datasetIndex]?.hideLegend) {
        return null;
      }

      return (
        <LegendItem
          key={`${item?.datasetIndex}-hidden-${item?.hidden}`}
          index={index}
          hidden={item?.hidden}
          dataset={datasets?.[item?.datasetIndex]}
          handleClick={(_: UnusedParameter) => legendClick(_, item)}
          chartType={chartType}
        />
      );
    })}
  </div>
);

const LegendPanel = forwardRef<HTMLDivElement, ILegendPanel>(
  ({ legendPosition, chartRef, isDragging, legendConfig }, ref) => {
    const chart = chartRef?.current;
    const { options, generatedDatasets, chartType } = legendConfig ?? {};
    const datasets = chart?.data?.datasets;

    const { legend, legendClick } = useLegendState({
      chartRef,
      options,
    });
    const [legendEnabled, setLegendEnabled] = useState(legend?.display ?? true);
    const items = useGeneratedLabels(chartRef, generatedDatasets);
    const style = createLegendStyle(legendPosition, chart);

    return (
      <div
        ref={ref}
        className={cx(
          styles.legend,
          !legendEnabled && styles.isHidden,
          isDragging && styles.isDragging,
        )}
        style={style}
      >
        <div className={styles.legendToggle}>
          <Button
            onClick={() => setLegendEnabled(!legendEnabled)}
            small
            round
            icon={legendEnabled ? <TbX /> : <TbList />}
          />
        </div>
        <LegendItems
          items={items}
          datasets={datasets as TLegendDatasets}
          legendClick={legendClick}
          chartType={chartType}
        />
      </div>
    );
  },
);

export default LegendPanel;
