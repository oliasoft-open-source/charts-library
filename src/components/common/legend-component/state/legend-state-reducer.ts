import { cloneDeep, defaultTo, findIndex, set } from 'lodash';
import { Chart } from 'chart.js';
import { TLegendDataset, TLegendDatasets } from '../legend-interface';
import { RESET_STATE, TOGGLE_DATA_VISIBILITY } from './legend-action-types';
import { ICommonAnnotationElement } from '../../common.interface';

export interface ILegendState {
  annotation: ICommonAnnotationElement[];
}

export type TLegendAction =
  | {
      type: typeof TOGGLE_DATA_VISIBILITY;
      payload: {
        chartInstance: Chart;
        dsConfig: {
          datasetIndex: number;
          dataset: TLegendDataset;
          datasets: TLegendDatasets;
        };
        annotationConfig: {
          annotationIndex: number;
        };
      };
    }
  | { type: typeof RESET_STATE; payload: ILegendState };

export const legendInitialState: ILegendState = {
  annotation: [],
};

export const legendReducer = (state: ILegendState, action: TLegendAction) => {
  switch (action.type) {
    case RESET_STATE: {
      return action.payload;
    }
    case TOGGLE_DATA_VISIBILITY: {
      const { chartInstance, dsConfig, annotationConfig } = action.payload;

      const { datasetIndex, datasets, dataset } = dsConfig;
      const { annotationIndex } = annotationConfig;
      const updatedState = cloneDeep(state);

      const isDatasetVisible = defaultTo(
        chartInstance?.isDatasetVisible(datasetIndex),
        true,
      );

      if (datasetIndex >= 0) {
        // Toggle dataset visibility
        chartInstance?.setDatasetVisibility(datasetIndex, !isDatasetVisible);

        // Update all datasets in the same display group
        if (dataset?.displayGroup) {
          const group = dataset.displayGroup;
          datasets.forEach((ds, ix) => {
            if (ds.displayGroup === group) {
              chartInstance?.setDatasetVisibility(
                Number(ix),
                !isDatasetVisible,
              );
            }
          });
        }
      }

      if (annotationIndex >= 0) {
        const idx = findIndex(updatedState.annotation, { annotationIndex });
        if (idx >= 0) {
          const currentDisplay = defaultTo(
            updatedState.annotation[idx]?.display,
            false,
          );
          set(updatedState, ['annotation', idx, 'display'], !currentDisplay);
        }
      }

      return updatedState;
    }
    default:
      return state;
  }
};
