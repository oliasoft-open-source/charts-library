import React, {
  createContext,
  ReactNode,
  Reducer,
  useMemo,
  useReducer,
} from 'react';
import {
  ILegendState,
  legendInitialState,
  legendReducer,
  TLegendAction,
} from './legend-state-reducer';
import { TChartsOption } from '../legend-interface';
import getAnnotation from '../../helpers/get-chart-annotation';
import { ICommonAnnotationElement } from '../../common.interface';
import { useReset } from '../../hooks/use-reset';

export const LegendContext = createContext<
  { state: ILegendState; dispatch: React.Dispatch<any> } | undefined
>(undefined);

export interface ILegendProviderProps {
  children: ReactNode;
  options: TChartsOption;
}

export const LegendProvider = ({ children, options }: ILegendProviderProps) => {
  const { showAnnotations = true, annotationsData = [] } =
    options?.annotations ?? {};

  const annotation = getAnnotation({
    showAnnotations,
    annotationsData,
  }) as ICommonAnnotationElement[];

  const initialState: ILegendState = {
    ...legendInitialState,
    annotation,
  };

  const [state, dispatch] = useReducer<Reducer<ILegendState, TLegendAction>>(
    legendReducer,
    initialState,
  );
  const contextValue = useMemo(
    () => ({ state, dispatch }),
    [state, options, dispatch],
  );

  useReset(initialState, dispatch, [options]);

  return (
    <LegendContext.Provider value={contextValue}>
      {children}
    </LegendContext.Provider>
  );
};
