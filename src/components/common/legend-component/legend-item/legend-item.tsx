import cx from 'classnames';
import { Icon } from '@oliasoft-open-source/react-ui-library';
import { TbCircleFilled, TbSquareFilled } from 'react-icons/tb';
import CircleSvg from 'src/assets/circle.svg';
import { ChartType } from '../../enums';
import { ILegendItem, TLegendDataset } from '../legend-interface';
import { LegendItemLine } from './LegendItemLine';
import styles from '../legend.module.less';

const LEGEND_SYMBOL_SIZE = 16;

const renderLegendItemSymbol = (
  dataset: TLegendDataset,
  chartType: ChartType,
  index: number,
) => {
  switch (chartType) {
    case ChartType.LINE:
      return <LegendItemLine dataset={dataset} />;
    case ChartType.BAR: {
      const { backgroundColor } = dataset ?? {};
      const color =
        backgroundColor instanceof Array
          ? backgroundColor?.[index]
          : backgroundColor;
      return (
        <span className={styles.legendItemBox}>
          <TbSquareFilled color={color} />
        </span>
      );
    }
    case ChartType.PIE:
      return (
        <span className={styles.legendItemPoint}>
          <Icon icon={<CircleSvg />} />
        </span>
      );
    case ChartType.SCATTER: {
      const { backgroundColor, borderColor } = dataset ?? {};
      const color =
        backgroundColor instanceof Array
          ? backgroundColor?.[index]
          : backgroundColor;
      return (
        <span className={styles.legendItemPoint}>
          <TbCircleFilled color={color ?? borderColor} />
        </span>
      );
    }
    default:
      return null;
  }
};

const LegendItem = ({
  hidden,
  dataset,
  handleClick,
  chartType,
  index,
}: ILegendItem) => {
  return (
    <div
      className={cx(styles?.legendItem, hidden && styles?.isHidden)}
      onClick={handleClick}
    >
      <span
        className={styles.legendItemSymbol}
        style={{ width: LEGEND_SYMBOL_SIZE }}
      >
        {renderLegendItemSymbol(dataset, chartType, index)}
      </span>
      <span className={styles.legendItemText}>{dataset?.label}</span>
    </div>
  );
};

export default LegendItem;
