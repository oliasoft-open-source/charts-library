import { ReactNode } from 'react';
import { Icon } from '@oliasoft-open-source/react-ui-library';
import CircleSvg from 'src/assets/circle.svg';
import RectSvg from 'src/assets/rect.svg';
import RectRotSvg from 'src/assets/rectRot.svg';
import TriangleSvg from 'src/assets/triangle.svg';
import { AnnotationType } from 'helpers/enums';
import styles from '../legend.module.less';
import { IGeneratedLineChartDataset } from '../../../line-chart/line-chart.interface';

const LEGEND_SYMBOL_SIZE = 16;

export interface ILineLegendItemProp {
  dataset: IGeneratedLineChartDataset;
}

const LineItem = ({ dataset }: ILineLegendItemProp) => {
  const { borderColor, borderDash, borderWidth } = dataset ?? {};
  // Center line dash on point
  const [dash0, dash1] = borderDash ?? [];
  const offset =
    dash0 && dash1
      ? (((LEGEND_SYMBOL_SIZE - dash0) / 2) % (dash0 + dash1)) * -1
      : 0;

  const borderDashString = dataset?.borderDash?.join(' ') || '';

  return (
    <span className={styles.legendItemLine}>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox={`0 0 ${LEGEND_SYMBOL_SIZE} ${LEGEND_SYMBOL_SIZE}`}
        width="100%"
        height="100%"
      >
        <line
          x1="0"
          y1="50%"
          x2="100%"
          y2="50%"
          stroke={borderColor}
          strokeWidth={borderWidth}
          strokeDasharray={borderDashString}
          strokeDashoffset={offset}
        />
      </svg>
    </span>
  );
};

const PointItem = ({ dataset }: ILineLegendItemProp) => {
  const { pointBackgroundColor, pointRadius, pointStyle = '' } = dataset ?? {};
  if (!pointRadius) return null;
  const size = pointRadius * 2;
  const icons: Record<string, ReactNode> = {
    circle: <CircleSvg />,
    triangle: <TriangleSvg />,
    rectRot: <RectRotSvg />,
    rect: <RectSvg />,
  };
  return (
    <span className={styles.legendItemPoint}>
      <Icon
        icon={icons?.[pointStyle] ?? <CircleSvg />}
        size={size}
        color={pointBackgroundColor}
      />
    </span>
  );
};

const BoxItem = ({ dataset }: Record<string, any>) => {
  const { backgroundColor } = dataset ?? {};
  const style = { backgroundColor };
  return <span className={styles.legendItemBox} style={style} />;
};

export const LegendItemLine = ({ dataset }: Record<string, any>) => {
  const { annotationType, showLine } = dataset ?? {};
  switch (annotationType) {
    case AnnotationType.Box:
    case AnnotationType.Ellipse:
      return <BoxItem dataset={dataset} />;
    case AnnotationType.Line:
      return <LineItem dataset={dataset} />;
    default:
      return (
        <>
          {showLine && <LineItem dataset={dataset} />}
          <PointItem dataset={dataset} />
        </>
      );
  }
};
