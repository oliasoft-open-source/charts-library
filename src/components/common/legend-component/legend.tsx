import { MutableRefObject, useEffect, useRef, useState } from 'react';
import { useMeasure } from 'react-use';
import { ChartArea } from 'chart.js';
import { DragSourceMonitor, useDrag } from 'react-dnd';
import { noop } from 'lodash';
import LegendPanel from './legend-panel';
import LegendDropZones from './legend-dropzone';
import styles from './legend.module.less';
import { Position } from '../helpers/enums';
import { ILegend } from './legend-interface';

const Legend = ({ chartRef, legendConfig }: ILegend) => {
  const chart = chartRef?.current;
  const [chartArea, setChartArea] = useState<ChartArea | undefined>(
    chart?.chartArea,
  );
  const {
    options: { legend },
  } = legendConfig ?? {};
  const [measureRef, measureRect] = useMeasure(); // Trigger update when chart is resized
  const resizeRef = measureRef as unknown as MutableRefObject<HTMLDivElement>;
  const panelRef = useRef<HTMLDivElement | null>(null);
  const [legendPosition, setLegendPosition] = useState(
    legend?.position || Position.BottomLeft,
  );
  const panelRect = panelRef?.current
    ? panelRef.current.getBoundingClientRect()
    : null;
  const panelSize = { width: panelRect?.width, height: panelRect?.height };

  const [{ isDragging = false }, dragRef = noop] = useDrag(() => ({
    type: 'legend',
    collect: (monitor: DragSourceMonitor) => ({
      isDragging: monitor.isDragging(),
    }),
  }));

  useEffect(() => {
    const timeout = setTimeout(() => {
      setChartArea(chart?.chartArea);
    }, 20);
    return () => clearTimeout(timeout);
  }, [measureRect]);

  return (
    <>
      <div ref={resizeRef} className={styles.resizeContainer} />
      <LegendPanel
        chartRef={chartRef}
        legendPosition={legendPosition as Position}
        isDragging={isDragging}
        ref={(r) => {
          dragRef(r);
          panelRef.current = r;
        }}
        legendConfig={legendConfig}
      />
      <LegendDropZones
        key={measureRect.width}
        chartArea={chartArea}
        setLegendPosition={(position: Position) => setLegendPosition(position)}
        isDragging={isDragging}
        placeholderSize={panelSize}
      />
    </>
  );
};

export default Legend;
