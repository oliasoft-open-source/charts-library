import cx from 'classnames';
import { DropTargetMonitor, useDrop } from 'react-dnd';
import { noop } from 'lodash';
import styles from './legend.module.less';
import { Position } from '../helpers/enums';
import { ILegendDropZone, ILegendDropZones } from './legend-interface';
import { LEGEND_MARGIN } from './utils/create-style-object';

const LegendDropZone = (legendDropZoneProps: ILegendDropZone) => {
  const {
    position = '',
    onDrop = noop,
    placeholderSize = {},
  } = legendDropZoneProps ?? {};
  const [{ isOver, canDrop }, dropRef] = useDrop(() => ({
    accept: 'legend',
    drop: onDrop,
    collect: (monitor: DropTargetMonitor) => ({
      isOver: monitor?.isOver(),
      canDrop: monitor?.canDrop(),
    }),
  }));
  const isActive = isOver && canDrop;
  return (
    <div
      ref={dropRef}
      className={cx(
        styles.dropzone,
        isActive && styles.isActive,
        position?.includes('left') && styles.left,
        position?.includes('right') && styles.right,
        position?.includes('top') && styles.top,
        position?.includes('bottom') && styles.bottom,
      )}
    >
      <div
        className={styles.dropzonePlaceholder}
        style={{ ...placeholderSize, margin: LEGEND_MARGIN }}
      />
    </div>
  );
};

const LegendDropZones = (legendDropZonesProps: ILegendDropZones) => {
  const { chartArea, setLegendPosition, isDragging, placeholderSize } =
    legendDropZonesProps ?? {};
  const positions = [
    Position.TopLeft,
    Position.TopRight,
    Position.BottomLeft,
    Position.BottomRight,
  ];
  const { top, left, width, height } = chartArea ?? {};
  return (
    <div
      className={styles.dropzoneContainer}
      style={{
        top,
        left,
        width,
        height,
        zIndex: isDragging ? 0 : -1, // Position dropzones behind chart when not needed
      }}
    >
      {positions?.map((position) => (
        <LegendDropZone
          key={position}
          position={position}
          onDrop={() => setLegendPosition(position)}
          placeholderSize={placeholderSize}
        />
      ))}
    </div>
  );
};

export default LegendDropZones;
