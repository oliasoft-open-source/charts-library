import { Chart, LegendItem } from 'chart.js';
import { cloneDeep, find, get, map } from 'lodash';

interface Annotation {
  label: { content: string };
  display: boolean;
}

const generateAnnotationLabels = (chart: Chart): Annotation[] => {
  const annotations = get(
    chart,
    'options.plugins.annotation.annotations',
    null,
  );

  if (!annotations || !Object.keys(annotations).length) {
    return [];
  }

  return Object.values(annotations) as Annotation[];
};

const generatedLabels = (chart: Chart): LegendItem[] => {
  return (
    cloneDeep(
      chart?.options?.plugins?.legend?.labels?.generateLabels?.(chart),
    ) ?? []
  );
};

export const getGeneratedLabels = (chart: Chart): LegendItem[] => {
  const datasetLabels = generatedLabels(chart);
  const annotationLabels = generateAnnotationLabels(chart);
  return map(datasetLabels, (el) => {
    const annotation = find(
      annotationLabels,
      (item) => el.text === item?.label?.content,
    );

    return annotation
      ? {
          ...el,
          hidden: !annotation?.display,
        }
      : el;
  });
};
