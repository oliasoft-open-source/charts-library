import { Position } from '../../helpers/enums';

export const LEGEND_MARGIN = 4;

interface IChartArea {
  top: number;
  left: number;
  bottom: number;
  right: number;
}

interface IChart {
  height?: number;
  width?: number;
  chartArea: IChartArea;
}

interface ILegendStyle {
  left: number | undefined;
  right: number | undefined;
  top: number | undefined;
  bottom: number | undefined;
  maxHeight: number;
  maxWidth: number;
  margin: number;
}

/**
 * Creates a style object for positioning and sizing the legend panel based on the chart dimensions and legend position.
 */
export const createLegendStyle = (
  legendPosition: Position,
  chart: IChart | null,
): ILegendStyle => {
  const { height = 0, width = 0, chartArea } = chart ?? {};
  const { top = 0, left = 0, bottom = 0, right = 0 } = chartArea ?? {};
  return {
    left: legendPosition?.includes('left') ? left : undefined,
    right: legendPosition?.includes('right') ? width - right : undefined,
    top: legendPosition?.includes('top') ? top : undefined,
    bottom: legendPosition?.includes('bottom') ? height - bottom : undefined,
    maxHeight: 0.5 * (bottom - top - LEGEND_MARGIN * 2),
    maxWidth: 0.9 * (right - left - LEGEND_MARGIN * 2),
    margin: LEGEND_MARGIN,
  };
};
