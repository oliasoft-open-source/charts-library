import { Chart, ChartConfiguration, Plugin } from 'chart.js';
import { getGeneratedLabels } from '../../line-chart/utils/get-generated-labels';

const getOrCreateLegendList = (_chart: Chart, id: string) => {
  const legendContainer = document.getElementById(id);
  let listContainer = legendContainer
    ? legendContainer.querySelector('ul')
    : null;

  if (!listContainer) {
    listContainer = document.createElement('ul');
    listContainer.style.display = 'flex';
    listContainer.style.flexDirection = 'row';
    listContainer.style.margin = '0';
    listContainer.style.padding = '0';

    // eslint-disable-next-line no-unused-expressions
    legendContainer ? legendContainer.appendChild(listContainer) : null;
  }

  return listContainer;
};

/**
 * Gets an example custom legend plugin for use in storybook.
 * @param {string} customLegendContainerID - the id of the div container to put the generated legend in
 */
export const getCustomLegendPlugin = (customLegendContainerID: string) => ({
  afterUpdate(chart: Chart, _args: Plugin, _options: ChartConfiguration) {
    const ul = getOrCreateLegendList(chart, customLegendContainerID);

    // Remove old legend items
    while (ul.firstChild) {
      ul.firstChild.remove();
    }

    // Reuse the built-in legendItems generator
    const items = getGeneratedLabels(chart);

    items.forEach((item) => {
      const li = document.createElement('li');
      li.style.alignItems = 'center';
      li.style.cursor = 'pointer';
      li.style.display = 'flex';
      li.style.flexDirection = 'row';
      li.style.marginLeft = '10px';

      li.onclick = () => {
        const { type } = chart?.config as ChartConfiguration;
        if (type === 'pie' || type === 'doughnut') {
          // Pie and doughnut charts only have a single dataset and visibility is per item
          if (typeof item.index === 'number') {
            chart.toggleDataVisibility(item.index);
          }
        } else {
          if (typeof item.datasetIndex === 'number') {
            chart.setDatasetVisibility(
              item.datasetIndex,
              !chart.isDatasetVisible(item.datasetIndex),
            );
          }
        }
        chart.update();
      };

      // Color box
      const boxSpan = document.createElement('span');
      boxSpan.style.background = item.fillStyle as string;
      boxSpan.style.borderColor = item.strokeStyle as string;
      boxSpan.style.borderWidth = item.lineWidth + 'px';
      boxSpan.style.display = 'inline-block';
      boxSpan.style.height = '20px';
      boxSpan.style.marginRight = '10px';
      boxSpan.style.width = '20px';

      // Text
      const textContainer = document.createElement('p');
      textContainer.style.margin = '0';
      textContainer.style.padding = '0';
      textContainer.style.textDecoration = item.hidden ? 'line-through' : '';

      const text = document.createTextNode(item.text);
      textContainer.appendChild(text);

      li.appendChild(boxSpan);
      li.appendChild(textContainer);
      ul.appendChild(li);
    });
  },
});
