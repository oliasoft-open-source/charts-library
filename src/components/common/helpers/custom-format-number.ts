import {
  DISPLAY_SCIENTIFIC_LOWER_BOUND,
  DISPLAY_SCIENTIFIC_UPPER_BOUND,
} from 'components/bar-chart/bar-chart-const.ts';
import { displayNumber, roundByMagnitude } from '@oliasoft-open-source/units';

export const customFormatNumber = (
  labelNumber: number,
  scientificNotation: boolean,
) => {
  let roundOptions = {};

  if (!scientificNotation) {
    roundOptions = { scientific: false };
  } else if (
    Math.abs(labelNumber) < DISPLAY_SCIENTIFIC_LOWER_BOUND ||
    Math.abs(labelNumber) > DISPLAY_SCIENTIFIC_UPPER_BOUND
  ) {
    roundOptions = { roundScientificCoefficient: 3 };
  }

  return displayNumber(roundByMagnitude(labelNumber), roundOptions);
};
