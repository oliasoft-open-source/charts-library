import '../../../style/reset/reset.less';
import '../../../style/global.less';
import { CSSProperties, ReactNode } from 'react';

const outerContainerStyle = {
  margin: '50px',
};

interface ContainerProps {
  style?: CSSProperties;
  children: ReactNode;
  margin?: boolean;
  deprecatedMessage?: string;
  warningMessage?: string;
}

export const Container = ({
  style,
  children,
  margin,
  deprecatedMessage,
  warningMessage,
}: ContainerProps) => {
  return (
    <div style={margin ? outerContainerStyle : {}}>
      {deprecatedMessage ? (
        <div
          style={{
            border: '1px solid red',
            color: 'red',
            padding: '10px',
            marginBottom: '20px',
          }}
        >
          {deprecatedMessage}
        </div>
      ) : null}
      {warningMessage ? (
        <div
          style={{
            border: '1px solid orange',
            color: 'orange',
            padding: '10px',
            marginBottom: '20px',
          }}
        >
          {warningMessage}
        </div>
      ) : null}
      <div style={style}>{children}</div>
    </div>
  );
};

Container.defaultProps = {
  style: {},
  margin: true,
  deprecatedMessage: null,
  warningMessage: null,
};
