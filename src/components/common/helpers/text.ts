export const getTextWidth = (text: string, font: string) => {
  const element = document.createElement('canvas');
  const context = element.getContext('2d');

  if (context) {
    context.font = font;
    return context.measureText(text).width;
  }
};
