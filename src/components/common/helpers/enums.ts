export enum AxisType {
  X = 'x',
  Y = 'y',
}

export enum Position {
  Bottom = 'bottom',
  Top = 'top',
  Left = 'left',
  Right = 'right',
  TopRight = 'top-right',
  TopLeft = 'top-left',
  BottomLeft = 'bottom-left',
  BottomRight = 'bottom-right',
}

export enum ChartType {
  Line = 'line',
  Bar = 'bar',
}

export enum CursorStyle {
  Pointer = 'pointer',
  Initial = 'initial',
  Grab = 'grab',
  Grabbing = 'grabbing',
  Crosshair = 'crosshair',
  Move = 'move',
}

export enum ScaleType {
  Category = 'category',
  Linear = 'linear',
  Logarithmic = 'logarithmic',
}

export enum ChartDirection {
  Vertical = 'vertical',
}

export enum TooltipLabel {
  Y = 'yLabel',
  X = 'xLabel',
}

export enum AlignOptions {
  End = 'end',
  Start = 'start',
  Center = 'center',
  Right = 'right',
  Left = 'left',
}

export enum PointType {
  Casing = 'casing',
}

export enum AnnotationType {
  Box = 'box',
  Ellipse = 'ellipse',
  Line = 'line',
  Text = 'text',
}

export enum PointStyle {
  Circle = 'circle',
  Square = 'rect',
  Diamond = 'rectRot',
  Triangle = 'triangle',
}

export enum ChartHoverMode {
  Nearest = 'nearest',
}

export enum PanZoomMode {
  X = 'x',
  Y = 'y',
  XY = 'xy',
  Z = 'z',
}

export enum Key {
  Shift = 'Shift',
}

export enum Events {
  Mousemove = 'mousemove',
  Mouseout = 'mouseout',
  Click = 'click',
  Touchstart = 'touchstart',
  Touchmove = 'touchmove',
  Dblclick = 'dblclick',
}
