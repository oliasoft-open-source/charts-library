import { Chart } from 'chart.js';
import { AnnotationType } from 'components/common/plugins/annotation-dragger-plugin/enums';
import { AxisType, CursorStyle, PointStyle, Position } from './enums';
import {
  ANNOTATION_DASH,
  BORDER_WIDTH,
  COLORS,
  DEFAULT_COLOR,
  DEFAULT_FONT_FAMILY,
  TRANSPARENT,
} from './chart-consts';
import {
  ICommonAnnotationElement,
  ICommonAnnotationsData,
  ICoordinates,
} from '../common.interface';

export interface IGetAnnotationsProps {
  showAnnotations: boolean;
  annotationsData: ICommonAnnotationsData[];
}

interface TChart extends Chart {
  hoveredAnnotationId?: string | null;
}

const handleLineEnter = (
  element: ICommonAnnotationElement,
  chart: TChart,
  annotation: ICommonAnnotationsData,
) => {
  chart.canvas.style.cursor = CursorStyle.Pointer;

  if (element.options.scaleID?.includes(AxisType.X)) {
    if (element.options.label) {
      element.options.label.xAdjust = chart.chartArea.left;
    }
  }
  element.options.borderWidth = BORDER_WIDTH.HOVERED;

  if (element.options.label) {
    element.label.options.display = true;
    element.options.label.enabled = true;
  }

  if (annotation?.enableDrag) {
    chart.hoveredAnnotationId = element.options.id || null;
    chart.update();
  }
  chart.draw();
};

const handleLineLeave = (
  element: ICommonAnnotationElement,
  chart: TChart,
  annotation: ICommonAnnotationsData,
) => {
  element.options.borderWidth = BORDER_WIDTH.INITIAL;
  chart.canvas.style.cursor = CursorStyle.Initial;
  chart.hoveredAnnotationId = null;
  if (element.options.label) {
    element.label.options.display = false;
    element.options.label.enabled = false;
  }

  if (annotation?.enableDrag) {
    chart.hoveredAnnotationId = null;
    chart.update();
  }
  chart.draw();
};

const handleBoxEnter = (
  element: ICommonAnnotationElement,
  chart: TChart,
  annotation: ICommonAnnotationsData,
) => {
  if (annotation?.enableDrag) {
    chart.hoveredAnnotationId = element.options.id || null;
    chart.update();

    element.options.borderWidth = BORDER_WIDTH.HOVERED;
    chart.canvas.style.cursor = CursorStyle.Pointer;
  }

  chart.draw();
};

const handleBoxLeave = (
  element: ICommonAnnotationElement,
  chart: TChart,
  annotation: ICommonAnnotationsData,
) => {
  if (annotation?.enableDrag) {
    chart.hoveredAnnotationId = null;
    element.options.borderWidth = BORDER_WIDTH.INITIAL;
    chart.canvas.style.cursor = CursorStyle.Initial;
    chart.update();
  }

  chart.draw();
};

const handlePointEnter = (
  element: ICommonAnnotationElement,
  chart: TChart,
  annotation: ICommonAnnotationsData,
) => {
  if (annotation?.enableDrag) {
    chart.hoveredAnnotationId = element.options.id || null;
    chart.update();

    element.options.borderWidth = BORDER_WIDTH.POINT_HOVERED;
    chart.canvas.style.cursor = CursorStyle.Pointer;
  }
  chart.draw();
};

const handlePointLeave = (
  element: ICommonAnnotationElement,
  chart: TChart,
  annotation: ICommonAnnotationsData,
) => {
  if (annotation?.enableDrag) {
    chart.hoveredAnnotationId = null;
    element.options.borderWidth = BORDER_WIDTH.INITIAL;
    chart.canvas.style.cursor = CursorStyle.Initial;

    chart.update();
  }
  chart.draw();
};

const generateAnnotations = (annotationsData: ICommonAnnotationsData[]) => {
  return annotationsData?.map((ann, idx) => {
    const scaleID = ann?.annotationAxis ?? undefined;
    const color = ann?.color ?? COLORS[idx];
    const type = ann?.type ?? AnnotationType.LINE;
    const adjustScaleRange = ann?.adjustScaleRange;
    const borderColor =
      {
        [AnnotationType.LINE]: color,
        [AnnotationType.POINT]: color,
        [AnnotationType.BOX]: color,
        [AnnotationType.ELLIPSE]: TRANSPARENT,
      }[type] || TRANSPARENT;
    const borderWidth = type === AnnotationType.LINE ? BORDER_WIDTH.INITIAL : 0;
    const borderDash =
      type === AnnotationType.LINE ? ANNOTATION_DASH : undefined;

    const defLabel = {
      content: ann?.label,
      display: true,
      font: { weight: 'normal' },
    };

    const label =
      {
        [AnnotationType.LINE]: {
          backgroundColor: color,
          content: ann?.label,
          display: false,
          position: Position.Top,
        },
        [AnnotationType.POINT]: {
          backgroundColor: ann?.labelConfig?.backgroundColor ?? TRANSPARENT,
          content: ann?.label,
          display: ann?.labelConfig?.display ?? !!ann?.label,
          position: ann?.labelConfig?.position ?? Position.Bottom,
          color: ann?.labelConfig?.color ?? DEFAULT_COLOR,
          font: ann?.labelConfig?.font ?? `12px ${DEFAULT_FONT_FAMILY}`,
          borderWidth: BORDER_WIDTH.INITIAL,
          padding: 5,
          borderRadius: 3,
          borderColor: ann?.labelConfig?.borderColor ?? TRANSPARENT,
        },
        [AnnotationType.BOX]: defLabel,
        [AnnotationType.ELLIPSE]: defLabel,
      }[type] || defLabel;

    const enter = (
      {
        element,
      }: {
        element: ICommonAnnotationElement;
      },
      {
        chart,
      }: {
        chart: TChart;
      },
    ) => {
      switch (type) {
        case AnnotationType.LINE:
          handleLineEnter(element, chart, ann);
          break;
        case AnnotationType.BOX:
          handleBoxEnter(element, chart, ann);
          break;
        case AnnotationType.POINT:
          handlePointEnter(element, chart, ann);
          break;
        default: {
          break;
        }
      }
    };

    const leave = (
      {
        element,
      }: {
        element: ICommonAnnotationElement;
      },
      {
        chart,
      }: {
        chart: TChart;
      },
    ) => {
      switch (type) {
        case AnnotationType.LINE:
          handleLineLeave(element, chart, ann);
          break;
        case AnnotationType.BOX:
          handleBoxLeave(element, chart, ann);
          break;
        case AnnotationType.POINT:
          handlePointLeave(element, chart, ann);
          break;
        default: {
          break;
        }
      }
    };

    const onDragStart =
      () => (cord: ICoordinates, annotation: ICommonAnnotationsData) =>
        ann?.onDragStart ? ann?.onDragStart(cord, annotation) : undefined;
    const onDrag =
      () => (cord: ICoordinates, annotation: ICommonAnnotationsData) =>
        ann?.onDrag ? ann?.onDrag(cord, annotation) : undefined;
    const onDragEnd =
      () => (cord: ICoordinates, annotation: ICommonAnnotationsData) =>
        ann?.onDragEnd ? ann?.onDragEnd(cord, annotation) : undefined;

    return {
      ...ann,
      display: ann?.display,
      annotationIndex: idx,
      id: `${ann?.label}-${ann?.value}-${idx}`,
      scaleID,
      label,
      backgroundColor: color,
      borderColor,
      borderWidth,
      borderDash,
      type,
      adjustScaleRange,
      enter,
      leave,
      onDragStart,
      onDrag,
      onDragEnd,
      pointStyle: ann?.pointStyle ?? PointStyle.Circle,
      resizable: ann?.resizable ?? true,
      displayDragCoordinates: ann?.displayDragCoordinates ?? true,
    };
  });
};

const getAnnotation = ({
  showAnnotations,
  annotationsData,
}: IGetAnnotationsProps) => {
  const shouldGenerateAnnotations =
    showAnnotations && annotationsData && annotationsData?.length;

  return shouldGenerateAnnotations
    ? generateAnnotations(annotationsData) ?? []
    : ([] as ICommonAnnotationElement[]);
};

export default getAnnotation;
