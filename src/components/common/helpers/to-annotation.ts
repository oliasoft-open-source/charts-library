import { ICommonAnnotationElement } from '../common.interface';

export const toAnnotationObject = (
  annotationArr: ICommonAnnotationElement[] = [],
) => {
  let annotations = {};
  if (annotationArr.length) {
    annotations = annotationArr.reduce(
      (acc, annotation, idx) => {
        acc[`annotation${idx + 1}`] = annotation;
        return acc;
      },
      {} as Record<string, ICommonAnnotationElement>,
    );
  }

  return { annotations };
};
