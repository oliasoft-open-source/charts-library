import { ICommonOptions } from '../common.interface';

export interface IDraggableDataOptions {
  dragX: boolean;
  dragY: boolean;
  round: boolean;
  showTooltip: boolean;
  onDragStart: () => void;
  onDrag: () => void;
  onDragEnd: () => void;
}

export interface IDraggableData {
  dragData: IDraggableDataOptions;
}

/**
 * @param {ICommonChartOptions} options - line chart options object
 */
const getDraggableData = (options: ICommonOptions): IDraggableData | object => {
  const { dragData = {} } = options;
  const {
    enableDragData,
    showTooltip,
    roundPoints,
    dragX,
    dragY,
    onDragStart = () => {},
    onDrag = () => {},
    onDragEnd = () => {},
  } = dragData;

  return enableDragData
    ? {
        dragData: {
          dragX,
          dragY,
          round: roundPoints,
          showTooltip,
          onDragStart,
          onDrag,
          onDragEnd,
        },
      }
    : {};
};

export default getDraggableData;
