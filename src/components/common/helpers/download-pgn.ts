import { RefObject } from 'react';
import { toPng } from 'html-to-image';
import { triggerBase64Download } from 'react-base64-downloader';
import { Chart } from 'chart.js';
import { getChartFileName } from './chart-utils';
import { IState } from '../../line-chart/state/state.interfaces';

export const downloadPgn = (chartRef: RefObject<Chart>, state?: IState) => {
  const chart = chartRef.current;
  // Get the parent element that includes the legend
  const canvasElement = chart?.canvas?.parentElement;
  if (!canvasElement) return;

  // Add temporary canvas background
  const { ctx } = chart;
  ctx.save();
  ctx.globalCompositeOperation = 'destination-over';
  const isDark = document.body.dataset.theme === 'dark';
  ctx.fillStyle = isDark ? 'black' : 'white';
  ctx.fillRect(0, 0, chart.width, chart.height);
  ctx.restore();
  const fileName = getChartFileName(state?.axes);
  toPng(canvasElement).then((dataUrl: string) => {
    triggerBase64Download(dataUrl, fileName);
  });
};
