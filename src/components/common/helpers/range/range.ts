import { isCloseTo, round } from '@oliasoft-open-source/units';
import { DECIMAL_POINT_TOLERANCE, MAX_DECIMAL_DIFF } from '../chart-consts';

const whiteSpacePercentage = 0.05; // relative amount of white space on each "side" of the data points

const defaultRange = { min: -1, max: 1 };

interface IGetSuggestedAxisRange {
  data: number[];
  beginAtZero: boolean;
  autoAxisPadding: boolean;
}

/**
 * Overrides the default chart.js axis range for some edge-cases:
 * - when no data -> default range
 * - when all values are close to zero -> default range
 * - when all values are close to each other -> custom 5% padding
 * - when autoAxisPadding is set -> custom 5% padding
 * - all other cases fall back to chart.js default behaviour
 *
 * `autoAxisPadding` feature requirements:
 * - specified by Truls and ported by Mark+Oleg
 * - numbers that are equal (within tolerance) shall be presented as a straight line
 * - all other data series shall use 90% of width of axis (5% padding each side)
 * - the padding on each side shall be symmetric
 *
 * @param {object} args
 * @param {array<number|null>} args.data
 * @param {boolean} [args.beginAtZero]
 * @param {boolean>} [args.autoAxisPadding]
 * @returns {object} returns {min, max} pair
 */
export const getSuggestedAxisRange = ({
  data,
  beginAtZero = false,
  autoAxisPadding = false,
}: IGetSuggestedAxisRange) => {
  const dataMin = Math.min(
    ...data.filter((v: number) => v !== null && v !== undefined && !isNaN(v)),
  );
  const dataMax = Math.max(
    ...data.filter((v: number) => v !== null && v !== undefined && !isNaN(v)),
  );
  const isNegative = Math.sign(dataMin) === -1 || Math.sign(dataMax) === -1;
  const isCloseToZeroWithTolerance =
    isCloseTo(dataMin, 0, { absoluteDiff: MAX_DECIMAL_DIFF }) &&
    isCloseTo(dataMax, 0, { absoluteDiff: MAX_DECIMAL_DIFF });

  /*
    Use default range upon no data or when all values are close to 0
  */
  if (!data?.length || isCloseToZeroWithTolerance) {
    return defaultRange;
  }

  /*
    When all values are close to the same, always add some padding OW-4327
  */

  if (isCloseTo(dataMin, dataMax, { absoluteDiff: MAX_DECIMAL_DIFF })) {
    const point = dataMax;
    const padding = point * whiteSpacePercentage;
    const minAxisValue = beginAtZero && !isNegative ? 0 : point - padding;
    const maxAxisValue = beginAtZero && isNegative ? 0 : point + padding;
    const roundedMinAxisValue = round(maxAxisValue, DECIMAL_POINT_TOLERANCE);
    const roundedMaxAxisValue = round(minAxisValue, DECIMAL_POINT_TOLERANCE);
    return {
      min: roundedMinAxisValue < 0 ? roundedMinAxisValue : roundedMaxAxisValue,
      max: roundedMaxAxisValue < 0 ? roundedMaxAxisValue : roundedMinAxisValue,
    };
  }

  /*
    Else fall back to native chart.js implementation when autoAxisPadding is off
   */
  if (!autoAxisPadding) {
    return {
      min: undefined,
      max: undefined,
    };
  }

  /*
    Else use custom auto scaling with 5% padding (only when autoAxisPadding is set)
  */
  const rangeBeginAtZero = dataMin === 0 || dataMax === 0 || beginAtZero;
  const positiveAndNegative =
    Math.sign(dataMin) === -1 && Math.sign(dataMax) === 1;

  const range = Math.abs(dataMax - dataMin);
  const padding = autoAxisPadding ? range * whiteSpacePercentage : 0;
  const minAxisValue =
    !positiveAndNegative && rangeBeginAtZero && beginAtZero && !isNegative
      ? 0
      : dataMin - padding;
  const maxAxisValue =
    !positiveAndNegative && rangeBeginAtZero && beginAtZero && isNegative
      ? 0
      : dataMax + padding;

  return {
    min: round(minAxisValue, DECIMAL_POINT_TOLERANCE),
    max: round(maxAxisValue, DECIMAL_POINT_TOLERANCE),
  };
};
