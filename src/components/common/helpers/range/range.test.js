import { getSuggestedAxisRange } from './range';

describe('getSuggestedAxisRange (axis range auto-scale with padding)', () => {
  test('returns nulls when autoAxisPadding is false (falls back to chart.js default)', () => {
    expect(
      getSuggestedAxisRange({
        data: [-2, 8, 13, 11, 18],
        autoAxisPadding: false,
      }),
    ).toStrictEqual({ min: undefined, max: undefined });
  });
  test('always returns default range upon no values', () => {
    expect(
      getSuggestedAxisRange({
        data: [],
        autoAxisPadding: false,
      }),
    ).toStrictEqual({ min: -1, max: 1 });
  });
  test('always returns default range when all values are (close to) 0', () => {
    expect(
      getSuggestedAxisRange({
        data: [0, 0, 0],
        autoAxisPadding: false,
      }),
    ).toStrictEqual({ min: -1, max: 1 });
    expect(
      getSuggestedAxisRange({
        data: [-0.0000000006, 0.0000000004, 0.0000000009],
        autoAxisPadding: false,
      }),
    ).toStrictEqual({ min: -1, max: 1 });
  });
  test('returns a suggested axis range with padding when autoAxisPadding is true', () => {
    //positive axis
    expect(
      getSuggestedAxisRange({ data: [5, 10, 25], autoAxisPadding: true }),
    ).toStrictEqual({
      min: 4,
      max: 26,
    });
    //negative axis
    expect(
      getSuggestedAxisRange({ data: [-25, -10, -5], autoAxisPadding: true }),
    ).toStrictEqual({
      min: -26,
      max: -4,
    });
    //positive and negative axis
    expect(
      getSuggestedAxisRange({
        data: [-2, 8, 13, 11, 18],
        autoAxisPadding: true,
      }),
    ).toStrictEqual({
      min: -3,
      max: 19,
    });
  });
  test('handles decimal precision when autoAxisPadding is true', () => {
    expect(
      getSuggestedAxisRange({
        data: [-0.0002, -0.00004, 0, 0.0007, 0.007],
        autoAxisPadding: true,
      }),
    ).toStrictEqual({
      min: -0.00056,
      max: 0.00736,
    });
  });
  test('respects 0 origin beginAtZero is true', () => {
    expect(
      getSuggestedAxisRange({
        data: [40, 440],
        autoAxisPadding: true,
        beginAtZero: false,
      }),
    ).toStrictEqual({
      min: 20,
      max: 460,
    });
    expect(
      getSuggestedAxisRange({
        data: [40, 440],
        autoAxisPadding: true,
        beginAtZero: true,
      }),
    ).toStrictEqual({
      min: 0,
      max: 460,
    });
    expect(
      getSuggestedAxisRange({
        data: [-440, -40],
        autoAxisPadding: true,
        beginAtZero: true,
      }),
    ).toStrictEqual({
      min: -460,
      max: 0,
    });
    expect(
      getSuggestedAxisRange({
        data: [-100, 100],
        autoAxisPadding: true,
        beginAtZero: true,
      }),
    ).toStrictEqual({
      min: -110,
      max: 110,
    });
  });
  test('handles unordered inputs', () => {
    expect(
      getSuggestedAxisRange({
        data: [18, 13, -2, 8, 11],
        autoAxisPadding: true,
      }),
    ).toStrictEqual({
      min: -3,
      max: 19,
    });
  });
  test('handles null/empty data points', () => {
    expect(
      getSuggestedAxisRange({ data: [4, null, 5], autoAxisPadding: true }),
    ).toStrictEqual({
      min: 3.95,
      max: 5.05,
    });
  });
  test('handles when all data values are (close to) the same', () => {
    expect(
      getSuggestedAxisRange({ data: [12, 12, 12], autoAxisPadding: true }),
    ).toStrictEqual({
      min: 11.4,
      max: 12.6,
    });
    expect(
      getSuggestedAxisRange({
        data: [12.00000000004, 12.00000000006, 12.00000000009],
        autoAxisPadding: false,
      }),
    ).toStrictEqual({ min: 11.4, max: 12.6 });
  });
});
