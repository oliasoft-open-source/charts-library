import { isCloseTo } from '@oliasoft-open-source/units';
import { MAX_DECIMAL_DIFF } from '../chart-consts';
import { TGeneratedLineChartDatasets } from '../../../line-chart/line-chart.interface';

interface IAccType {
  [key: string]: {
    xFirst?: number;
    xLast?: number;
    yFirst?: number;
    yLast?: number;
  };
}

/**
 * Estimates whether any of the data series has values that are all close together
 * - checks only the first and last values in each series (i.e. assumes they are ordered)
 * - uses an equality check with tolerance for decimal precision noise
 * - this is just an inexpensive "guesstimate" (full min/max detection can be used afterwards)
 */
export const estimateDataSeriesHaveCloseValues = (
  generatedDatasets: TGeneratedLineChartDatasets,
) => {
  if (!Array.isArray(generatedDatasets) || !generatedDatasets?.length) {
    return false;
  }
  const axesFirstLast = generatedDatasets?.reduce((acc, dataset) => {
    const xAxisId = dataset?.xAxisID ?? 'defaultX';
    const yAxisId = dataset?.yAxisID ?? 'defaultY';
    const data = dataset?.data ?? [];
    if (data && data?.length) {
      const { x: xFirstCurrent, y: yFirstCurrent } = data?.[0] ?? {};
      const { x: xLastCurrent = 0, y: yLastCurrent = 0 } = data?.at(-1) ?? {};
      const xFirstAcc = acc?.[xAxisId]?.xFirst ?? xFirstCurrent;
      const xLastAcc = acc?.[xAxisId]?.xLast ?? xLastCurrent;
      const yFirstAcc = acc?.[yAxisId]?.yFirst ?? yFirstCurrent;
      const yLastAcc = acc?.[yAxisId]?.yLast ?? yLastCurrent;
      const xFirst = Math.min(xFirstCurrent, xFirstAcc);
      const xLast = Math.max(xLastCurrent, xLastAcc);
      const yFirst = Math.min(yFirstCurrent, yFirstAcc);
      const yLast = Math.max(yLastCurrent, yLastAcc);
      acc = {
        ...acc,
        [xAxisId]: {
          ...acc[xAxisId],
          xFirst,
          xLast,
        },
        [yAxisId]: {
          yFirst,
          yLast,
        },
      };
    }
    return acc;
  }, {} as IAccType);
  return (
    Object.values(axesFirstLast) as {
      xFirst: number;
      xLast: number;
      yFirst: number;
      yLast: number;
    }[]
  )?.some(({ xFirst, xLast, yFirst, yLast }) => {
    return (
      isCloseTo(xFirst, xLast, { absoluteDiff: MAX_DECIMAL_DIFF }) ||
      isCloseTo(yFirst, yLast, { absoluteDiff: MAX_DECIMAL_DIFF })
    );
  });
};
