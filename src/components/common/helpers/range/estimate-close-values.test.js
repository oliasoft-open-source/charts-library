import { estimateDataSeriesHaveCloseValues } from './estimate-data-series-have-close-values';

describe('estimateDataSeriesHaveCloseValues', () => {
  test('returns true when any data series has close values (grouped by axis)', () => {
    const datasets = [
      {
        data: [
          { x: 10, y: 15.599680785028 },
          { x: 20, y: 15.599680785026 },
          { x: 30, y: 15.599680785024 },
        ],
      },
      {
        data: [
          { x: 11, y: 15.599680785027 },
          { x: 21, y: 15.599680785022 },
          { x: 31, y: 15.599680785024 },
        ],
      },
      {
        data: [
          { x: 10, y: 1000 },
          { x: 20, y: 2000 },
          { x: 30, y: 3000 },
        ],
        yAxisID: 'y2',
      },
    ];
    expect(estimateDataSeriesHaveCloseValues(datasets)).toBe(true);
  });
  test('returns false when no data series have close values (grouped by axis)', () => {
    const datasets = [
      {
        data: [
          { x: 10, y: 15.599680785028 },
          { x: 20, y: 15.599680785026 },
          { x: 30, y: 15.599680785024 },
        ],
      },
      {
        data: [
          { x: 11, y: 9.599680785028 },
          { x: 21, y: 10 },
          { x: 31, y: 12 },
        ],
      },
      {
        data: [
          { x: 10, y: 1000 },
          { x: 20, y: 2000 },
          { x: 30, y: 3000 },
        ],
        yAxisID: 'y2',
      },
    ];
    expect(estimateDataSeriesHaveCloseValues(datasets)).toBe(false);
  });
  test('handles when values are close to zero', () => {
    const datasets = [
      {
        data: [
          { x: 10, y: -0.0000000001 },
          { x: 20, y: 0 },
          { x: 30, y: 1.123e-10 },
        ],
      },
      {
        data: [
          { x: 10, y: 1000 },
          { x: 20, y: 2000 },
          { x: 30, y: 3000 },
        ],
        yAxisID: 'y2',
      },
    ];
    expect(estimateDataSeriesHaveCloseValues(datasets)).toBe(true);
  });
  test('returns false for empty, bad or missing inputs', () => {
    expect(estimateDataSeriesHaveCloseValues()).toBe(false);
    expect(estimateDataSeriesHaveCloseValues([])).toBe(false);
    expect(estimateDataSeriesHaveCloseValues(1)).toBe(false);
    expect(estimateDataSeriesHaveCloseValues('string')).toBe(false);
    expect(
      estimateDataSeriesHaveCloseValues([
        {
          data: [
            { x: null, y: null },
            { x: null, y: null },
          ],
        },
        {
          data: [{}, { x: 1, y: 123 }],
        },
        {
          data: [],
        },
      ]),
    ).toBe(false);
  });
});
