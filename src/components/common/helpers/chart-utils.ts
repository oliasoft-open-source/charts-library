import { round } from '@oliasoft-open-source/units';
import { ChartTypeRegistry, defaults, LegendItem, Plugin } from 'chart.js';
import cx from 'classnames';
import { ICommonCustomLegend, UnusedParameter } from '../common.interface';
import { chartMinorGridlinesPlugin } from '../../line-chart/plugins/line-chart.minor-gridlines-plugin';
import {
  BORDER_COLOR,
  CUSTOM_LEGEND_PLUGIN_NAME,
  DEFAULT_CHART_NAME,
  DEFAULT_COLOR,
  DEFAULT_FONT_FAMILY,
  DEFAULT_FONT_SIZE,
  LEGEND_LABEL_BOX_SIZE,
  WHITE_COLOR_AS_DECIMAL,
} from './chart-consts';
import { AxisType, ChartDirection, Position } from './enums';
import { chartAreaBorderPlugin } from './chart-border-plugin';

export interface IChartGraph {
  showMinorGridlines: boolean;
}

interface IOptions {
  title?: string | string[];
  legend?: {
    position: Position;
    display?: boolean;
    align?: string;
    usePointStyle?: boolean;
  };
}

interface IStyling {
  width?: string | number;
  height?: string | number;
  squareAspectRatio?: number | boolean;
  maintainAspectRatio?: boolean;
  staticChartHeight?: boolean;
  performanceMode?: boolean;
}

interface IAxis {
  label?: string;
}

interface ILegend {
  customLegend?: ICommonCustomLegend<any>;
}

/**
 * @param { graph } graph - graph data from chart options
 * @param { legend } legend
 * @return {[]}
 */
export const getPlugins = <T extends keyof ChartTypeRegistry>(
  graph: IChartGraph,
  legend: ILegend,
): Plugin<T>[] | any => {
  let plugins = [];
  if (graph.showMinorGridlines) {
    plugins.push(chartMinorGridlinesPlugin);
  }
  const customLegend = legend?.customLegend;
  if (
    customLegend?.customLegendPlugin &&
    customLegend?.customLegendContainerID
  ) {
    plugins.push({
      id: CUSTOM_LEGEND_PLUGIN_NAME,
      ...legend?.customLegend?.customLegendPlugin,
    });
  }
  plugins.push(chartAreaBorderPlugin);
  return plugins;
};

/**
 * @param {string[]} colors - color schema
 * @return {string} - random color
 */
export const generateRandomColor = (colors: string[]): string[] => {
  const color = `#${Math.floor(Math.random() * WHITE_COLOR_AS_DECIMAL).toString(
    16,
  )}`;
  if (colors.includes(color)) {
    return generateRandomColor(colors);
  } else {
    colors.push(color);
    return colors;
  }
};

/**
 * @param {ICommonChartOptions} options - chart options object
 * @return {{color: (string|undefined), display: boolean, text}|{}}
 */
export const getTitle = (options: IOptions) => {
  return options.title !== ''
    ? {
        display: true,
        text: options.title,
      }
    : {};
};

/**
 * @function isVertical
 */
export const isVertical = (direction: 'vertical' | 'horizontal') => {
  return direction === ChartDirection.Vertical;
};

/**
 * @param {'x'|'y'} axisType
 * @param {number} i - index
 * @return {'top'|'bottom'|'left'|'right'|*}
 */
export const getAxisPosition = (axisType: string, i: number) => {
  const [positionA, positionB] =
    axisType === AxisType.Y
      ? [Position.Left, Position.Right]
      : [Position.Top, Position.Bottom];
  return i % 2 === 0 ? positionA : positionB;
};

/**
 * @param {chartStyling} chartStyling
 * @param {Object} styles - styles imported form .less file
 * @return {string} - class name
 */
export const getClassName = (
  chartStyling: IStyling,
  styles: Record<string, string>,
) => {
  const {
    width,
    height,
    staticChartHeight = false,
    squareAspectRatio = 0,
  } = chartStyling;
  const squareAspectRatioStyle = squareAspectRatio
    ? styles.squareAspectRatio
    : '';
  let heightStyles = '';
  if (width || height) {
    heightStyles = '';
  } else {
    heightStyles = staticChartHeight
      ? styles?.fixedHeight
      : styles?.stretchHeight;
  }
  return cx(styles.chart, heightStyles, squareAspectRatioStyle);
};

export const getLegend = (
  options: IOptions,
  clickHandler: (_: UnusedParameter, legendItem: LegendItem) => void,
) => {
  const { legend } = options || {};
  return {
    // Using 'as any' here to bypass a TypeScript type-checking issue.
    // There is a conflict between our custom interface and the Chart.js interface
    // for the 'plugins.legend.position' property. In our current setup, this property
    // is typed as a string, which conflicts with the more specific string literals
    // expected by Chart.js ('top', 'center', 'bottom', 'left', 'right', 'chartArea').
    // As a temporary workaround, 'as any' is used to avoid this type conflict.
    position: legend?.position || (Position.Top as any),
    display: legend?.display,
    align: legend?.align as 'center' | 'start' | 'end',
    labels: {
      boxHeight: LEGEND_LABEL_BOX_SIZE,
      boxWidth: legend?.usePointStyle ? LEGEND_LABEL_BOX_SIZE : undefined,
      usePointStyle: legend?.usePointStyle,
    },
    onClick: clickHandler,
  };
};

export const afterLabelCallback = (tooltipItem: Record<string, any>) => {
  const { error } = tooltipItem.dataset.data[tooltipItem?.dataIndex];
  return error ? `Error: ${round(error, 4)}` : '';
};

export const getTooltipLabel = (
  tooltipItem: Record<string, any>,
  showLabelsInTooltips: boolean,
) => {
  const datasetDataLabel =
    tooltipItem?.dataset?.data?.[tooltipItem?.dataIndex]?.label;
  const dataLabel = Array.isArray(datasetDataLabel)
    ? datasetDataLabel.join(' , ')
    : datasetDataLabel;

  return showLabelsInTooltips && dataLabel?.length ? ` (${dataLabel})` : '';
};

//TODO: consider returning chart name instead of axis names
export const getChartFileName = (axes?: IAxis[]) => {
  if (!axes) {
    return DEFAULT_CHART_NAME;
  }
  const axesLabels = axes.reduce((acc: string[], cur: IAxis, index: number) => {
    const labelWithNoSpace = cur.label?.replace(/\s/g, '_') || String(index);
    return [...acc, labelWithNoSpace];
  }, []);
  return axesLabels.join('_');
};

export const setDefaultTheme = () => {
  defaults.font.size = DEFAULT_FONT_SIZE;
  defaults.font.family = DEFAULT_FONT_FAMILY;
  defaults.color = DEFAULT_COLOR;
  defaults.borderColor = BORDER_COLOR;
};

export const isNilOrEmpty = <T>(value: T | null | undefined): boolean => {
  if (value === null || value === undefined || value === '') {
    return true;
  }

  if (Array.isArray(value) && value.length === 0) {
    return true;
  }

  if (typeof value === 'object' && Object.keys(value).length === 0) {
    return true;
  }

  return false;
};
