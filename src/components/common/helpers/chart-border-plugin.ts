import { Chart } from 'chart.js';
import { UnusedParameter } from '../common.interface';

interface IChartAreaBorderPluginOptions {
  borderColor: string;
  borderWidth: number;
  borderDash: number[];
  borderDashOffset?: number;
}

/**
 * Chart border plugin config
 */
export const chartAreaBorderPlugin = {
  id: 'chartAreaBorder',
  beforeDraw(
    chart: Chart,
    _: UnusedParameter,
    options: IChartAreaBorderPluginOptions,
  ) {
    const {
      ctx,
      chartArea: { left, top, width, height },
    } = chart;

    ctx.save();
    ctx.strokeStyle = options.borderColor;
    ctx.lineWidth = options.borderWidth;
    ctx.setLineDash(options.borderDash || []);
    ctx.lineDashOffset = options.borderDashOffset ?? 0;
    ctx.strokeRect(left, top, width, height);
    ctx.restore();
  },
};
