import { ICommonGradientColor } from 'components/common/common.interface';

export const BORDER_WIDTH = {
  ZERO: 0,
  INITIAL: 2,
  HOVERED: 6,
  POINT_HOVERED: 8,
};
export const BORDER_COLOR = 'rgba(0,0,0,0.1)';

export const ANNOTATION_DASH = [10, 2];
export const DEFAULT_FONT_SIZE = 12;
export const DEFAULT_FONT_FAMILY = '"Roobert", "Noto Sans", sans-serif';
export const DEFAULT_COLOR = 'hsl(60, 10.34482759%, 12.5%)';
export const LEGEND_LABEL_BOX_SIZE = 12;
export const TOOLTIP_PADDING = 8;
export const TOOLTIP_BOX_PADDING = 4;
export const TRANSPARENT = 'transparent';

export const LOGARITHMIC_STEPS = [1, 10, 100, 1000, 10000];

export const COLORS = [
  '#3366cc',
  '#dc3912',
  '#ff9900',
  '#109618',
  '#990099',
  '#0099c6',
  '#dd4477',
  '#66aa00',
  '#b82e2e',
  '#316395',
  '#994499',
  '#22aa99',
  '#aaaa11',
  '#6633cc',
  '#e67300',
  '#8b0707',
  '#651067',
  '#329262',
  '#5574a6',
  '#3b3eac',
];

/**
 * @type {string}
 * @desc equivalent of 0.6 rgba alpha chanel
 */
export const ALPHA_CHANEL = '99';

/**
 * @type {number}
 * @desc #FFFFFF as decimal number
 */
export const WHITE_COLOR_AS_DECIMAL = 16777215;

export const AUTO = 'auto';

export const ANIMATION_DURATION = {
  NO: 0,
  SLOW: 400,
  FAST: 1000,
};

export const DEFAULT_CHART_NAME = 'new_chart';

export const CUSTOM_LEGEND_PLUGIN_NAME = 'htmlLegend';

export const DECIMAL_POINT_TOLERANCE = 9;

export const MAX_DECIMAL_DIFF = 1 / 10 ** DECIMAL_POINT_TOLERANCE;

export const GRADIENT_COLORS: ICommonGradientColor[] = [
  { offset: 0, color: 'rgba(144,238,144,0.8)' }, // Light green
  { offset: 0.6, color: 'rgba(255,255,224,0.8)' }, // Light yellow
  { offset: 0.8, color: 'rgba(255,255,224,0.8)' }, // Light yellow
  { offset: 0.92, color: 'rgba(255,182,193,0.5)' }, // Light red
  { offset: 0.99, color: 'rgba(255,182,193,0.8)' }, // Light red
];
