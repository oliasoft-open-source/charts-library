export enum ChartType {
  LINE = 'line',
  BAR = 'bar',
  PIE = 'pie',
  SCATTER = 'scatter',
}

export enum ChartDirection {
  VERTICAL = 'vertical',
  HORIZONTAL = 'horizontal',
}
