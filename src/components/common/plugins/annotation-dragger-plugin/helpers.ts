import { Chart } from 'chart.js';
import { ICommonAnnotationsData } from 'components/common/common.interface';
import { IScales } from 'components/common/plugins/annotation-dragger-plugin/annotation-dragger-plugin';
import { isNil } from 'lodash';
import { AnnotationType, DragAxis } from './enums';

export const getMousePosition = (
  event: MouseEvent,
  canvas: HTMLCanvasElement,
): { x: number; y: number } => {
  const rect = canvas.getBoundingClientRect();
  return {
    x: event.clientX - rect.left,
    y: event.clientY - rect.top,
  };
};

export const getMousePositionInCoordinates = (
  event: MouseEvent,
  canvas: HTMLCanvasElement,
  scales: IScales,
  xScaleID: string,
  yScaleID: string,
): { x: number; y: number } => {
  const rect = canvas.getBoundingClientRect();
  const x = event.clientX - rect.left;
  const y = event.clientY - rect.top;

  const xScale = scales[xScaleID];
  const yScale = scales[yScaleID];

  const xValue = xScale?.getValueForPixel(x) ?? -1;
  const yValue = yScale?.getValueForPixel(y) ?? -1;

  return { x: xValue, y: yValue };
};

export const calculateAnnotationMetricsInValues = (
  annotation: ICommonAnnotationsData,
) => {
  const { xMin, xMax, yMin, yMax, xValue, yValue, type } = annotation ?? {};
  let centerX;
  let centerY;
  let width;
  let height;

  if (
    !isNil(xMin) &&
    !isNil(xMax) &&
    !isNil(yMin) &&
    !isNil(yMax) &&
    type === AnnotationType.BOX
  ) {
    centerX = (xMin + xMax) / 2;
    centerY = (yMin + yMax) / 2;
    width = xMax - xMin;
    height = yMax - yMin;
  } else if (
    !isNil(xValue) &&
    !isNil(yValue) &&
    type === AnnotationType.POINT
  ) {
    centerX = xValue;
    centerY = yValue;
    width = 0;
    height = 0;
  }

  return {
    centerX,
    centerY,
    width,
    height,
  };
};

export const calculateAnnotationMetricsInValuesInPixels = (
  annotation: ICommonAnnotationsData,
  scales: IScales,
  xScaleID: string,
  yScaleID: string,
) => {
  const { xMin, xMax, yMin, yMax, xValue, yValue, type } = annotation ?? {};

  let centerX;
  let centerY;
  let width;
  let height;

  const xScale = scales[xScaleID];
  const yScale = scales[yScaleID];

  if (
    !isNil(xMin) &&
    !isNil(xMax) &&
    !isNil(yMin) &&
    !isNil(yMax) &&
    type === AnnotationType.BOX
  ) {
    const xCenterValue = (xMin + xMax) / 2;
    const yCenterValue = (yMin + yMax) / 2;
    const xMinValue = xScale?.getPixelForValue(xMin) ?? 0;
    const xMaxValue = xScale?.getPixelForValue(xMax) ?? 0;
    const yMinValue = yScale?.getPixelForValue(yMin) ?? 0;
    const yMaxValue = yScale?.getPixelForValue(yMax) ?? 0;

    width = xMinValue - xMaxValue;
    height = yMinValue - yMaxValue;

    centerX = xScale?.getPixelForValue(xCenterValue) ?? -1;
    centerY = yScale?.getPixelForValue(yCenterValue) ?? -1;
  } else if (
    !isNil(xValue) &&
    !isNil(yValue) &&
    type === AnnotationType.POINT
  ) {
    width = 0;
    height = 0;

    centerX = xScale?.getPixelForValue(xValue) ?? -1;
    centerY = yScale?.getPixelForValue(yValue) ?? -1;
  }

  return {
    centerX,
    centerY,
    width,
    height,
  };
};

export interface IIsWithinChartAreaProps {
  x?: number;
  y?: number;
  chartArea: { left: number; right: number; top: number; bottom: number };
  scales: IScales;
  metrics: { width?: number; height?: number };
  resizable?: boolean;
  dragRange?: {
    x?: [number, number];
    y?: [number, number];
  } | null;
  annotationType: AnnotationType;
  xScaleID: string;
  yScaleID: string;
}

export const isWithinChartArea = ({
  x,
  y,
  chartArea,
  scales,
  metrics,
  resizable = false,
  dragRange,
  annotationType,
  xScaleID,
  yScaleID,
}: IIsWithinChartAreaProps): boolean => {
  if (resizable && !dragRange) {
    return true;
  }

  if (dragRange) {
    const withinRangeX =
      x !== undefined && dragRange.x
        ? x >= dragRange.x[0] && x <= dragRange.x[1]
        : true;
    const withinRangeY =
      y !== undefined && dragRange.y
        ? y >= dragRange.y[0] && y <= dragRange.y[1]
        : true;

    return withinRangeX && withinRangeY;
  }
  const xScale = scales[xScaleID];
  const yScale = scales[yScaleID];
  const minX =
    xScale?.getValueForPixel(chartArea.left) ?? Number.NEGATIVE_INFINITY;
  const maxX =
    xScale?.getValueForPixel(chartArea.right) ?? Number.POSITIVE_INFINITY;
  const minY =
    yScale?.getValueForPixel(chartArea.bottom) ?? Number.NEGATIVE_INFINITY;
  const maxY =
    yScale?.getValueForPixel(chartArea.top) ?? Number.POSITIVE_INFINITY;

  if (annotationType === AnnotationType.POINT) {
    const withinX = x !== undefined ? x >= minX && x <= maxX : true;
    const withinY = y !== undefined ? y >= minY && y <= maxY : true;

    return withinX && withinY;
  }

  if (
    annotationType === AnnotationType.BOX &&
    metrics.width !== undefined &&
    metrics.height !== undefined
  ) {
    const boxXMin = x !== undefined ? x - metrics.width / 2 : undefined;
    const boxXMax = x !== undefined ? x + metrics.width / 2 : undefined;
    const boxYMin = y !== undefined ? y - metrics.height / 2 : undefined;
    const boxYMax = y !== undefined ? y + metrics.height / 2 : undefined;

    const withinBoxX =
      boxXMin !== undefined && boxXMax !== undefined
        ? boxXMin >= minX && boxXMax <= maxX
        : true;
    const withinBoxY =
      boxYMin !== undefined && boxYMax !== undefined
        ? boxYMin >= minY && boxYMax <= maxY
        : true;

    return withinBoxX && withinBoxY;
  }

  return true;
};

export const updateAnnotationPositionWithinBounds = (
  axis: DragAxis,
  newPosition: number,
  activeAnnotation: ICommonAnnotationsData,
  chart: Chart,
  scales: IScales,
  xScaleID: string,
  yScaleID: string,
  metrics?: { width?: number; height?: number },
) => {
  const { resizable, dragRange = null } = activeAnnotation ?? {};

  const checkArea = isWithinChartArea({
    [axis]: newPosition,
    chartArea: chart.chartArea,
    scales,
    resizable,
    dragRange,
    metrics: metrics || {},
    annotationType: activeAnnotation.type,
    xScaleID,
    yScaleID,
  });

  if (!checkArea) return;

  if (activeAnnotation.type === AnnotationType.POINT) {
    if (axis === 'x') {
      activeAnnotation.xValue = newPosition;
    } else if (axis === 'y') {
      activeAnnotation.yValue = newPosition;
    }
  } else if (activeAnnotation.type === AnnotationType.BOX && metrics) {
    if (axis === 'x' && metrics.width) {
      activeAnnotation.xMin = newPosition - metrics.width / 2;
      activeAnnotation.xMax = newPosition + metrics.width / 2;
    }
    if (axis === 'y' && metrics.height) {
      activeAnnotation.yMin = newPosition - metrics.height / 2;
      activeAnnotation.yMax = newPosition + metrics.height / 2;
    }
  }
};

export const setAnnotationBorderWidth = (
  chart: Chart,
  annotationId?: string,
  borderWidth?: number,
) => {
  if (!annotationId) return;
  const annotations = chart?.options?.plugins?.annotation
    ?.annotations as Record<string, any>;

  if (annotations && annotationId && annotations[annotationId]) {
    annotations[annotationId].borderWidth = borderWidth;
  }
};
