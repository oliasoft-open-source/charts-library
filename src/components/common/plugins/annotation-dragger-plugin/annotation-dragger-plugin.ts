import { Chart, ChartType, Plugin, Scale } from 'chart.js';
import { ICommonAnnotationsData } from 'components/common/common.interface';
import {
  handleAnnotationMouseDown,
  handleAnnotationMouseMove,
  handleAnnotationMouseUp,
} from 'components/common/plugins/annotation-dragger-plugin/event-helpers';
import { annotationLabel } from 'components/common/plugins/annotation-dragger-plugin/point-annotation-label';
import { ChartHoverMode } from 'helpers/enums';
import { debounce } from 'lodash';
import { MouseEvents } from './enums';

export interface IScales {
  x: Scale;
  y: Scale;
  [key: string]: Scale;
}

export interface AnnotationDraggerPluginOptions {
  enabled: boolean;
}

declare module 'chart.js' {
  interface PluginOptionsByType<TType extends ChartType> {
    annotationDraggerPlugin?: AnnotationDraggerPluginOptions;
  }
}

export const annotationDraggerPlugin: Plugin = {
  id: 'annotationDraggerPlugin',
  beforeDraw(chart: Chart) {
    let annotations = (chart.options.plugins?.annotation?.annotations ??
      {}) as Record<string, ICommonAnnotationsData>;
    if (!annotations) return;
    annotationLabel(
      chart.ctx,
      annotations as any,
      chart.scales as unknown as IScales,
    );

    const pluginOptions = (chart?.options?.plugins?.annotationDraggerPlugin || {
      enabled: false,
    }) as AnnotationDraggerPluginOptions;

    if (pluginOptions.enabled) {
      if (chart?.options?.plugins?.tooltip) {
        chart.options.plugins.tooltip.enabled = false;
      }
      chart.options.hover = {
        mode: undefined,
        intersect: false,
      };
    } else {
      if (chart?.options?.plugins?.tooltip) {
        chart.options.plugins.tooltip.enabled = true;
      }
      chart.options.hover = {
        mode: ChartHoverMode.Nearest,
        intersect: true,
      };
    }
  },
  afterUpdate(chart: Chart & { hoveredAnnotationId: string | null }) {
    const { canvas, scales, hoveredAnnotationId } = chart ?? {};
    const pluginOptions = (chart?.options?.plugins?.annotationDraggerPlugin || {
      enabled: false,
    }) as AnnotationDraggerPluginOptions;
    const typedScales: IScales = { ...scales, x: scales.x, y: scales.y };
    let annotations = (chart.options.plugins?.annotation?.annotations ??
      {}) as Record<string, ICommonAnnotationsData>;
    if (!chart && !annotations) return;

    if (pluginOptions.enabled) {
      if (chart?.options?.plugins?.tooltip) {
        chart.options.plugins.tooltip.enabled = false;
      }
      chart.options.hover = {
        mode: undefined,
        intersect: false,
      };
    } else {
      if (chart?.options?.plugins?.tooltip) {
        chart.options.plugins.tooltip.enabled = true;
      }
      chart.options.hover = {
        mode: ChartHoverMode.Nearest,
        intersect: true,
      };
    }

    let isDragging = false;
    let dragStartX: number, dragStartY: number;
    let activeAnnotation: ICommonAnnotationsData | null = null;

    const setDraggingState = (
      dragging: boolean,
      annotation: ICommonAnnotationsData | null,
      startX?: number,
      startY?: number,
    ) => {
      isDragging = dragging;
      activeAnnotation = annotation;
      if (startX !== undefined && startY !== undefined) {
        dragStartX = startX;
        dragStartY = startY;
      }
    };

    if (
      !canvas.dataset.annotationDraggerInitialized &&
      pluginOptions.enabled &&
      hoveredAnnotationId
    ) {
      canvas.addEventListener(MouseEvents.MOUSEDOWN, (event: MouseEvent) => {
        handleAnnotationMouseDown(
          event,
          canvas,
          typedScales,
          chart?.options?.plugins?.annotation?.annotations as Record<
            string,
            ICommonAnnotationsData
          >,
          setDraggingState,
          chart.hoveredAnnotationId,
        );
      });

      canvas.addEventListener(MouseEvents.MOUSEMOVE, (event: MouseEvent) => {
        const handleAnnotationMouseMoveDebounce = debounce(
          () =>
            handleAnnotationMouseMove(
              event,
              canvas,
              typedScales,
              isDragging,
              activeAnnotation,
              dragStartX,
              dragStartY,
              chart,
            ),
          5,
        );
        handleAnnotationMouseMoveDebounce();
      });

      canvas.addEventListener(MouseEvents.MOUSEUP, (event) => {
        const canvasEvent = event.currentTarget as HTMLCanvasElement;
        handleAnnotationMouseUp(
          isDragging,
          activeAnnotation,
          chart,
          canvasEvent,
          setDraggingState,
        );
      });

      canvas.dataset.annotationDraggerInitialized = 'true';
    }

    if (!pluginOptions.enabled) {
      canvas.removeEventListener(MouseEvents.MOUSEDOWN, () => {});
      canvas.removeEventListener(MouseEvents.MOUSEMOVE, () => {});
      canvas.removeEventListener(MouseEvents.MOUSEUP, () => {});
      canvas.dataset.annotationDraggerInitialized = '';
    }
  },
};
