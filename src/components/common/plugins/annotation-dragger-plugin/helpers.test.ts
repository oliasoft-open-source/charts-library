import {
  calculateAnnotationMetricsInValues,
  calculateAnnotationMetricsInValuesInPixels,
  getMousePosition,
  getMousePositionInCoordinates,
} from './helpers';
import { IScales } from './annotation-dragger-plugin';
import { AnnotationType } from './enums';
import { ICommonAnnotationsData } from '../../common.interface';

describe('Annotation Utils', () => {
  it('should get mouse position correctly', () => {
    const event = {
      clientX: 50,
      clientY: 50,
    } as MouseEvent;
    const canvas = {
      getBoundingClientRect: () => ({
        left: 10,
        top: 10,
      }),
    } as HTMLCanvasElement;

    const position = getMousePosition(event, canvas);
    expect(position).toEqual({ x: 40, y: 40 });
  });

  it('should get mouse position in coordinates correctly', () => {
    const event = {
      clientX: 50,
      clientY: 50,
    } as MouseEvent;
    const canvas = {
      getBoundingClientRect: () => ({
        left: 10,
        top: 10,
      }),
    } as HTMLCanvasElement;
    const scales = {
      x: {
        getValueForPixel: (pixel: number) => pixel / 2,
      },
      y: {
        getValueForPixel: (pixel: number) => pixel / 2,
      },
    } as IScales;

    const position = getMousePositionInCoordinates(
      event,
      canvas,
      scales,
      'x',
      'y',
    );
    expect(position).toEqual({ x: 20, y: 20 });
  });

  it('should calculate annotation metrics in values correctly for BOX', () => {
    const annotation = {
      xMin: 0,
      xMax: 10,
      yMin: 0,
      yMax: 10,
      type: AnnotationType.BOX,
    } as ICommonAnnotationsData;

    const metrics = calculateAnnotationMetricsInValues(annotation);
    expect(metrics).toEqual({
      centerX: 5,
      centerY: 5,
      width: 10,
      height: 10,
    });
  });

  it('should calculate annotation metrics in values correctly for POINT', () => {
    const annotation = {
      xValue: 5,
      yValue: 5,
      type: AnnotationType.POINT,
    } as ICommonAnnotationsData;

    const metrics = calculateAnnotationMetricsInValues(annotation);
    expect(metrics).toEqual({
      centerX: 5,
      centerY: 5,
      width: 0,
      height: 0,
    });
  });

  it('should calculate annotation metrics in pixels correctly for BOX', () => {
    const annotation = {
      xMin: 0,
      xMax: 10,
      yMin: 0,
      yMax: 10,
      type: AnnotationType.BOX,
    } as ICommonAnnotationsData;
    const scales = {
      x: {
        getPixelForValue: (value: number) => value * 10,
      },
      y: {
        getPixelForValue: (value: number) => value * 10,
      },
    } as IScales;

    const metrics = calculateAnnotationMetricsInValuesInPixels(
      annotation,
      scales,
      'x',
      'y',
    );
    expect(metrics).toEqual({
      centerX: 50,
      centerY: 50,
      width: -100,
      height: -100,
    });
  });

  it('should calculate annotation metrics in pixels correctly for POINT', () => {
    const annotation = {
      xValue: 5,
      yValue: 5,
      type: AnnotationType.POINT,
    } as ICommonAnnotationsData;
    const scales = {
      x: {
        getPixelForValue: (value: number) => value * 10,
      },
      y: {
        getPixelForValue: (value: number) => value * 10,
      },
    } as IScales;

    const metrics = calculateAnnotationMetricsInValuesInPixels(
      annotation,
      scales,
      'x',
      'y',
    );
    expect(metrics).toEqual({
      centerX: 50,
      centerY: 50,
      width: 0,
      height: 0,
    });
  });
});
