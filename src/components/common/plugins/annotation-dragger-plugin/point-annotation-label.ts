import { isNil } from 'lodash';
import { calculateAnnotationMetricsInValuesInPixels } from 'components/common/plugins/annotation-dragger-plugin/helpers';
import { Position } from 'helpers/enums.ts';
import { IScales } from 'components/common/plugins/annotation-dragger-plugin/annotation-dragger-plugin';
import { AnnotationType } from './enums';
import { ICommonAnnotationsData } from '../../common.interface';

export const annotationLabel = (
  ctx: CanvasRenderingContext2D,
  annotations: Record<
    string,
    ICommonAnnotationsData & { label: Record<string, any> }
  >,
  scales: IScales,
) => {
  Object.values(annotations).forEach((annotation) => {
    if (
      annotation.type === AnnotationType.POINT &&
      !isNil(annotation?.label) &&
      annotation?.label?.display
    ) {
      const { content, font, color, position, padding } =
        annotation?.label ?? {};
      const { xScaleID = 'x', yScaleID = 'y' } = annotation;

      ctx.save();
      ctx.font = font;
      ctx.fillStyle = color;
      const { centerX = -1, centerY = -1 } =
        calculateAnnotationMetricsInValuesInPixels(
          annotation,
          scales,
          xScaleID,
          yScaleID,
        );

      const textWidth = ctx.measureText(content).width;
      let textX = centerX - textWidth / 2;
      let textY = centerY + 20;

      switch (position) {
        case Position.Top:
          textY = centerY - 20;
          break;
        case Position.Left:
          textX = centerX - textWidth - padding;
          textY = centerY;
          break;
        case Position.Right:
          textX = centerX + padding;
          textY = centerY;
          break;
      }

      ctx.fillText(content, textX, textY);
      ctx.restore();
    }
  });
};
