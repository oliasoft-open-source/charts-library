export enum FontStyle {
  DEFAULT = '14px Arial',
  CUSTOM = '16px Arial',
}

export enum MouseEvents {
  MOUSEDOWN = 'mousedown',
  MOUSEMOVE = 'mousemove',
  MOUSEUP = 'mouseup',
}

export enum AnnotationType {
  POINT = 'point',
  BOX = 'box',
  LINE = 'line',
  ELLIPSE = 'ellipse',
}

export enum DragAxis {
  X = 'x',
  Y = 'y',
  Both = 'both',
}
