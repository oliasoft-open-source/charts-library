import { Chart } from 'chart.js';
import { IScales } from 'components/common/plugins/annotation-dragger-plugin/annotation-dragger-plugin';
import { ICommonAnnotationsData } from 'components/common/common.interface';
import {
  calculateAnnotationMetricsInValues,
  calculateAnnotationMetricsInValuesInPixels,
  getMousePositionInCoordinates,
  setAnnotationBorderWidth,
  updateAnnotationPositionWithinBounds,
} from 'components/common/plugins/annotation-dragger-plugin/helpers';
import { BORDER_WIDTH, DEFAULT_FONT_FAMILY } from 'helpers/chart-consts';
import { round } from '@oliasoft-open-source/units';
import { isNil } from 'lodash';
import { CursorStyle } from 'helpers/enums';
import { DragAxis, FontStyle } from './enums';

export const handleAnnotationMouseDown = (
  event: MouseEvent,
  canvas: HTMLCanvasElement,
  scales: IScales,
  annotations: Record<string, ICommonAnnotationsData>,
  setDraggingState: (
    isDragging: boolean,
    annotation: ICommonAnnotationsData | null,
    dragStartX: number,
    dragStartY: number,
  ) => void,
  hoveredAnnotationId: string | null,
) => {
  const annotation = hoveredAnnotationId
    ? annotations?.[hoveredAnnotationId]
    : null;
  if (!annotation) return;
  const { xScaleID = 'x', yScaleID = 'y' } = annotation;
  const { x, y } = getMousePositionInCoordinates(
    event,
    canvas,
    scales,
    xScaleID,
    yScaleID,
  );

  const metrics = calculateAnnotationMetricsInValues(annotation);

  if (
    annotation &&
    annotation.enableDrag &&
    !isNil(metrics.centerY) &&
    !isNil(metrics.centerX)
  ) {
    if (!isNil(canvas) && !isNil(canvas.style)) {
      canvas.style.cursor = CursorStyle.Move;
    }

    const dragStartX = x - metrics.centerX;
    const dragStartY = y - metrics.centerY;
    setDraggingState(true, annotation, dragStartX, dragStartY);

    if (annotation?.onDragStart) {
      annotation.onDragStart({ x, y }, annotation);
    }
  }
};

export const handleAnnotationMouseMove = (
  event: MouseEvent,
  canvas: HTMLCanvasElement,
  scales: IScales,
  isDragging: boolean,
  activeAnnotation: ICommonAnnotationsData | null,
  dragStartX: number,
  dragStartY: number,
  chart: Chart,
) => {
  if (isDragging && activeAnnotation) {
    if (!isNil(canvas) && !isNil(canvas.style)) {
      canvas.style.cursor = CursorStyle.Move;
    }
    const annotationId = activeAnnotation?.id;
    setAnnotationBorderWidth(chart, annotationId, BORDER_WIDTH.HOVERED);

    const dragAxis = activeAnnotation?.dragAxis ?? DragAxis.Both;
    const { xScaleID = 'x', yScaleID = 'y' } = activeAnnotation;

    const { x, y } = getMousePositionInCoordinates(
      event,
      canvas,
      scales,
      xScaleID,
      yScaleID,
    );
    const metrics = calculateAnnotationMetricsInValues(activeAnnotation);

    let newCenterX = x - dragStartX;
    let newCenterY = y - dragStartY;

    if (dragAxis !== DragAxis.Y) {
      updateAnnotationPositionWithinBounds(
        DragAxis.X,
        newCenterX,
        activeAnnotation,
        chart,
        scales,
        xScaleID,
        yScaleID,
        metrics,
      );
    }

    if (dragAxis !== DragAxis.X) {
      updateAnnotationPositionWithinBounds(
        DragAxis.Y,
        newCenterY,
        activeAnnotation,
        chart,
        scales,
        xScaleID,
        yScaleID,
        metrics,
      );
    }

    chart.update();

    if (activeAnnotation?.onDrag) {
      activeAnnotation.onDrag({ x, y }, activeAnnotation);
    }

    const { ctx, width, height } = chart;
    const { centerX, centerY } =
      calculateAnnotationMetricsInValues(activeAnnotation) ?? {};
    const { centerX: xValue, centerY: yValue } =
      calculateAnnotationMetricsInValuesInPixels(
        activeAnnotation,
        scales,
        xScaleID,
        yScaleID,
      );

    if (
      !isNil(centerX) &&
      !isNil(centerY) &&
      !isNil(xValue) &&
      !isNil(yValue)
    ) {
      ctx.save();
      ctx.clearRect(0, 0, width, height);
      chart.draw();
      ctx.font = DEFAULT_FONT_FAMILY ?? FontStyle.DEFAULT;
      ctx.fillStyle = 'black';

      if (activeAnnotation?.displayDragCoordinates) {
        let labelX = xValue - 45;
        let labelY = yValue - 20;
        const labelText = `X: ${round(centerX, 2)}, Y: ${round(centerY, 2)}`;

        const labelHeight = 14;

        if (labelY - labelHeight < 0) {
          labelY = yValue + labelHeight + 15;
        }
        if (labelY > height) {
          labelY = height - 7;
        }

        ctx.fillText(labelText, labelX, labelY);
        ctx.restore();
      }
    }
  }
};

export const handleAnnotationMouseUp = (
  isDragging: boolean,
  activeAnnotation: ICommonAnnotationsData | null,
  chart: Chart,
  canvas: HTMLCanvasElement,
  setDraggingState: (
    isDragging: boolean,
    annotation: ICommonAnnotationsData | null,
  ) => void,
) => {
  if (isDragging) {
    if (activeAnnotation) {
      const annotationId = activeAnnotation?.id;
      setAnnotationBorderWidth(chart, annotationId, BORDER_WIDTH.ZERO);

      const { centerX = -1, centerY = -1 } =
        calculateAnnotationMetricsInValues(activeAnnotation) ?? {};

      if (!isNil(activeAnnotation?.onDragEnd)) {
        activeAnnotation?.onDragEnd(
          { x: centerX, y: centerY },
          activeAnnotation,
        );
      }

      if (!isNil(canvas) && !isNil(canvas.style)) {
        canvas.style.cursor = CursorStyle.Pointer;
      }

      setDraggingState(false, null);
    }
  }
};
