import { Chart, ChartArea, ChartOptions, Plugin } from 'chart.js';
import { ICommonStyling } from 'components/common/common.interface';
import { GradientDirection } from 'components/common/plugins/gradient-background-plugin/enums';

type TExtendedChartOptions = ChartOptions & ICommonStyling;

const getGradientCoordinates = (
  direction: GradientDirection,
  chartArea: ChartArea,
) => {
  switch (direction) {
    case GradientDirection.TopToBottom:
      return [chartArea.left, chartArea.top, chartArea.left, chartArea.bottom];
    case GradientDirection.BottomToTop:
      return [chartArea.left, chartArea.bottom, chartArea.left, chartArea.top];
    case GradientDirection.LeftToRight:
      return [chartArea.left, chartArea.top, chartArea.right, chartArea.top];
    case GradientDirection.RightToLeft:
      return [chartArea.right, chartArea.top, chartArea.left, chartArea.top];
    case GradientDirection.TopLeftToBottomRight:
      return [chartArea.left, chartArea.top, chartArea.right, chartArea.bottom];
    case GradientDirection.BottomRightToTopLeft:
      return [chartArea.right, chartArea.bottom, chartArea.left, chartArea.top];
    case GradientDirection.BottomLeftToTopRight:
      return [chartArea.left, chartArea.bottom, chartArea.right, chartArea.top];
    case GradientDirection.TopRightToBottomLeft:
      return [chartArea.right, chartArea.top, chartArea.left, chartArea.bottom];
    default:
      return [chartArea.left, chartArea.bottom, chartArea.right, chartArea.top];
  }
};

export const gradientBackgroundPlugin: Plugin<'bubble'> = {
  id: 'gradientBackgroundPlugin',
  beforeDraw: (chart: Chart<'bubble'>) => {
    const { ctx, chartArea, options } = chart;
    const gradientOptions = (options as TExtendedChartOptions).gradient;
    const {
      display = false,
      gradientColors,
      direction = GradientDirection.BottomLeftToTopRight,
    } = gradientOptions ?? {};

    if (!display || !gradientColors || gradientColors.length === 0) return;

    const [x0, y0, x1, y1] = getGradientCoordinates(direction, chartArea);
    const gradient = ctx.createLinearGradient(x0, y0, x1, y1);
    gradientColors.forEach(({ offset, color }) => {
      gradient.addColorStop(offset, color);
    });

    ctx.save();
    ctx.globalCompositeOperation = 'destination-over';
    ctx.fillStyle = gradient;
    ctx.fillRect(
      chartArea.left,
      chartArea.top,
      chartArea.right - chartArea.left,
      chartArea.bottom - chartArea.top,
    );
    ctx.restore();
  },
};
