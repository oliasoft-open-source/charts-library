export enum GradientDirection {
  TopToBottom,
  BottomToTop,
  LeftToRight,
  RightToLeft,
  TopLeftToBottomRight,
  BottomRightToTopLeft,
  BottomLeftToTopRight,
  TopRightToBottomLeft,
}
