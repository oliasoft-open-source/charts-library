import { Portal } from '@oliasoft-open-source/react-ui-library';
import { ReactElement } from 'react';

interface ControlsPortalProps {
  children: ReactElement;
  controlsPortalId: string;
}

const ControlsPortal = ({
  children,
  controlsPortalId,
}: ControlsPortalProps) => {
  if (controlsPortalId) {
    return <Portal id={controlsPortalId}>{children}</Portal>;
  }

  return children;
};

export default ControlsPortal;
