import {
  ChartEvent,
  ChartTypeRegistry,
  LinearScaleOptions,
  Plugin,
} from 'chart.js';
import { LabelOptions } from 'chartjs-plugin-annotation';
import { GradientDirection } from 'components/common/plugins/gradient-background-plugin/enums';
import {
  AnnotationType,
  DragAxis,
} from 'components/common/plugins/annotation-dragger-plugin/enums';
import { AlignOptions, Position } from './helpers/enums';

export type TAxisPosition = 'top' | 'bottom' | 'left' | 'right';

export interface ICommonDataValue {
  x: number;
  y: number;
  label?: string;
}

export interface ICommonDataset {
  label?: string;
  borderColor?: string;
  backgroundColor?: string | string[];
  borderWidth?: number;
  data: ICommonDataValue[];
}

export interface ICommonScales
  extends Partial<Record<string, LinearScaleOptions>> {
  x: LinearScaleOptions;
  y: LinearScaleOptions;
}

export interface ICommonAnnotationElement {
  display: boolean;
  annotationIndex: number;
  label: {
    options: LabelOptions;
    content: string;
  };
  options: {
    id?: string;
    scaleID?: string;
    borderColor?: string;
    borderWidth?: number;
    radius?: number;
    label?: {
      content?: string;
      position?: string;
      enabled?: boolean;
      xAdjust?: number;
    };
  };
}

export interface ICoordinates {
  x: number;
  y: number;
}

export interface ICommonAnnotationsData {
  display?: boolean;
  hideLegend?: boolean;
  id?: string;
  adjustScaleRange: boolean;
  annotationAxis: 'x' | 'y';
  color: string;
  endValue: number;
  label: string;
  labelConfig?: Record<string, any>;
  type: AnnotationType;
  value: number;
  xMin: number;
  xMax: number;
  yMin: number;
  yMax: number;
  xValue?: number;
  yValue?: number;
  radius?: number;
  enableDrag?: boolean;
  onDragStart?: (
    coordinates: ICoordinates,
    annotation: ICommonAnnotationsData,
  ) => void;
  onDrag?: (
    coordinates: ICoordinates,
    annotation: ICommonAnnotationsData,
  ) => void;
  onDragEnd?: (
    coordinates: ICoordinates,
    annotation: ICommonAnnotationsData,
  ) => void;
  dragAxis?: DragAxis;
  pointStyle?: string;
  resizable?: boolean;
  dragRange?: {
    x?: [number, number]; // Min and max limits for x-axis dragging
    y?: [number, number]; // Min and max limits for y-axis dragging
  };
  displayDragCoordinates?: boolean;
  xScaleID?: string;
  yScaleID?: string;
}

export interface ICommonGradientColor {
  offset: number;
  color: string;
}

export interface ICommonGradient {
  display: boolean;
  gradientColors?: ICommonGradientColor[];
  direction?: GradientDirection;
}

export interface ICommonStyling {
  width?: number | string;
  height?: number | string;
  maintainAspectRatio?: boolean;
  staticChartHeight?: boolean;
  performanceMode?: boolean;
  gradient?: ICommonGradient;
}

export interface ICommonLegend {
  display: boolean;
  position: Position;
  align: AlignOptions;
}

export interface ICommonTooltip {
  tooltips: boolean;
  showLabelsInTooltips: boolean;
  scientificNotation: boolean;
}

export interface ICommonAdditionalAxesOptions {
  chartScaleType?: 'linear' | 'logarithmic';
  reverse: boolean;
  beginAtZero: boolean;
  stepSize?: number;
  stacked?: boolean;
  suggestedMin?: number;
  suggestedMax?: number;
  min?: number;
  max?: number;
}

export interface ICommonAnnotation {
  showLabel: boolean;
  text: string | string[];
  position: string;
  fontSize: number;
  xOffset: number;
  yOffset: number;
  maxWidth: number;
  lineHeight: number;
}

export interface ICommonAnnotations {
  showAnnotations?: boolean;
  controlAnnotation?: boolean;
  enableDragAnnotation?: boolean;
  annotationsData?: ICommonAnnotationsData[];
  labelAnnotation?: ICommonAnnotation;
}

export interface ICommonGraph {
  lineTension?: number;
  spanGaps?: boolean;
  showDataLabels?: boolean;
  showMinorGridlines?: boolean;
}

export interface ICommonInteractions {
  onLegendClick?: (text: string, hidden: boolean) => void;
  onHover?: (
    event?: ChartEvent,
    datasetIndex?: number,
    index?: number,
    generatedDataset?: any[],
  ) => void;
  onUnhover?: (
    event?: ChartEvent,
    datasetIndex?: number,
    index?: number,
    generatedDataset?: any[],
  ) => void;
}

export interface ICommonCustomLegend<T extends keyof ChartTypeRegistry> {
  customLegendPlugin: Plugin<T> | null;
  customLegendContainerID: string;
}

export interface ICommonChartOptions {
  enableZoom: boolean;
  enablePan: boolean;
}

export interface ICommonDragData {
  enableDragData?: boolean;
  showTooltip?: boolean;
  roundPoints?: boolean;
  dragX?: boolean;
  dragY?: boolean;
  onDragStart?: () => void;
  onDrag?: () => void;
  onDragEnd?: () => void;
}

export interface ICommonOptions {
  title?: string | string[];
  chartStyling?: ICommonStyling;
  tooltip?: ICommonTooltip;
  legend?: ICommonLegend;
  chartOptions?: ICommonChartOptions;
  interactions?: ICommonInteractions;
  dragData?: ICommonDragData;
  annotations?: ICommonAnnotations;
}

export interface ICommonData {
  testId?: string;
  data?: {
    labels?: string[];
    datasets?: ICommonDataset[];
  };
  options: ICommonOptions;
}

export interface ICommonHoveredItems {
  index: number;
  datasetIndex: number;
}

export interface ICommonChartPlugins {
  legend: {
    position: TAxisPosition;
  };
  title: {
    display: boolean;
    text: string;
  };
}

export interface ICommonAxis<PositionType = any> {
  label: string;
  position: PositionType;
  color: string | string[];
  unit?: string;
  gridLines?: boolean;
  stepSize?: number;
}

export type UnusedParameter = unknown | any;

export type TPrimitive = string | number | boolean | null;
