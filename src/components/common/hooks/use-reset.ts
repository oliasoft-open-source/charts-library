import { Dispatch, useEffect, useRef } from 'react';
import isEqual from 'fast-deep-equal';
import { ILegendState } from '../legend-component/state/legend-state-reducer';

export const useReset = (
  state: ILegendState,
  dispatch: Dispatch<any>,
  dependencies: any[],
) => {
  const prevState = useRef<ILegendState | null>(state);

  useEffect(() => {
    if (!isEqual(state, prevState?.current)) {
      dispatch({ type: 'RESET_STATE', payload: state });
      prevState.current = state;
    }
  }, [...dependencies]);
};
