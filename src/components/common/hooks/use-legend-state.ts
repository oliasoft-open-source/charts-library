import { RefObject, useCallback, useMemo } from 'react';
import { Chart, LegendItem } from 'chart.js';
import { UnusedParameter } from '../common.interface';
import { getLegend } from '../helpers/chart-utils';
import { CUSTOM_LEGEND_PLUGIN_NAME } from '../helpers/chart-consts';
import { TLegendDataset } from '../legend-component/legend-interface';
import { useLegend } from './use-legend';
import { TOGGLE_DATA_VISIBILITY } from '../legend-component/state/legend-action-types';

export interface IUseLegendStateProps {
  chartRef: RefObject<Chart>;
  options: any;
}

/**
 * Custom hook to manage legend state.
 */
export const useLegendState = ({ chartRef, options }: IUseLegendStateProps) => {
  const { state, dispatch } = useLegend() ?? {};
  const {
    annotations,
    interactions: { onLegendClick },
    legend: { customLegend },
  } = options ?? {};

  const legendClick = useCallback(
    (_: UnusedParameter, legendItem: LegendItem) => {
      const { datasetIndex = -1 } = legendItem ?? {};
      let annotationIndex = null;
      const chartInstance = chartRef?.current;
      const datasets = chartInstance?.data?.datasets ?? [];
      const dataset = (datasets?.[datasetIndex] as TLegendDataset) ?? {};

      if (annotations?.controlAnnotation && dataset?.isAnnotation) {
        annotationIndex = dataset?.annotationIndex ?? null;
      }

      if (onLegendClick) {
        const legendState = legendItem?.hidden ?? false;
        onLegendClick(legendItem?.text, legendState);
      }

      dispatch({
        type: TOGGLE_DATA_VISIBILITY,
        payload: {
          chartInstance,
          dsConfig: {
            datasetIndex,
            datasets,
            dataset,
          },
          annotationConfig: {
            annotationIndex,
          },
        },
      });

      chartInstance?.update();
    },
    [chartRef, annotations, dispatch],
  );

  const legend = useMemo(
    () => getLegend(options, legendClick),
    [options, legendClick],
  );

  const customLegendPlugin = useMemo(
    () => ({
      [CUSTOM_LEGEND_PLUGIN_NAME]: customLegend?.customLegendPlugin && {
        containerID: customLegend?.customLegendContainerID,
      },
    }),
    [customLegend],
  );

  return {
    legendClick,
    annotation: state?.annotation,
    legend,
    customLegendPlugin,
  };
};
