import { useContext } from 'react';
import { LegendContext } from '../legend-component/state/legend-context';

export const useLegend = () => {
  const context = useContext(LegendContext);
  if (context === undefined) {
    throw new Error('useLegend must be used within a LegendProvider');
  }
  return context;
};
