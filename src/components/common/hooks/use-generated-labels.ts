import { RefObject, useEffect, useState } from 'react';
import { Chart } from 'chart.js';
import { TLegendDatasets } from '../legend-component/legend-interface';
import { getGeneratedLabels } from '../legend-component/utils/get-generated-labels';

export const useGeneratedLabels = (
  chartRef: RefObject<Chart>,
  generatedDatasets: TLegendDatasets,
): any[] => {
  const [items, setItems] = useState<any[]>([]);
  useEffect(() => {
    const chart = chartRef?.current;
    if (!chart) return;
    const newItems = getGeneratedLabels(chart);
    setItems(newItems);
  }, [generatedDatasets, chartRef]);

  return items;
};
