import React from 'react';
import { getCustomLegendPlugin } from '../common/helpers/get-custom-legend-plugin-example';
import { BarChart } from './bar-chart';

const labels1 = ['United Kingdom', 'Norway', 'France'];
const dataset1_1a = { label: 'Cats', data: [3.12, 8, 7] };
const dataset1_2a = { label: 'Dogs', data: [1, 2, 3] };
const dataset1_3a = { label: 'Parrots', data: [5, 0, 4] };

const dataset1_1b = { label: 'Cats', data: [3, 8, 7], yAxisID: 'y' };
const dataset1_2b = { label: 'Dogs', data: [1, 2, 3], yAxisID: 'y2' };
const dataset1_3b = { label: 'Parrots', data: [5, 0, 4], yAxisID: 'y2' };

const dataset1_1c = { label: 'Cats', data: [3, 8, 7], stack: 1 };
const dataset1_2c = { label: 'Dogs', data: [1, 2, 3], stack: 1 };
const dataset1_3c = { label: 'Parrots', data: [5, 0, 4], stack: 2 };

const hugeNumbersDataset1 = {
  label: 'Cats',
  data: [1000, 2000, 3000],
  stack: 1,
};
const hugeNumbersDataset2 = {
  label: 'Dogs',
  data: [4000, 5000, 6000],
  stack: 1,
};
const hugeNumbersDataset4 = {
  label: 'Parrots',
  data: [11000, 22000, 33000],
  stack: 2,
};

const annotationDataset = { label: 'Cats', data: [42, 57, 64] };

const datasetNoDataLabels = {
  label: 'Cats',
  data: [210, 333, 248],
};

const waterfallDataset = {
  label: 'Income',
  data: [100, [100, 120], [120, 130], [130, 80], 80],
  backgroundColor: [
    'lightGray',
    'lightGreen',
    'lightGreen',
    'pink',
    'lightGray',
  ],
};

const singleChart = {
  data: {
    labels: labels1,
    datasets: [dataset1_1a],
  },
};

const customColorsChart = {
  data: {
    labels: labels1,
    datasets: [
      {
        ...dataset1_1a,
        label: 'Men',
        backgroundColor: 'lightblue',
      },
      {
        ...dataset1_2a,
        label: 'Women',
        backgroundColor: 'pink',
      },
    ],
  },
};

const customColorArrayChart = {
  data: {
    labels: labels1,
    datasets: [
      {
        ...dataset1_1a,
        backgroundColor: ['gray', 'red', 'gray'],
      },
    ],
  },
};

const multipleBarData = {
  data: {
    labels: labels1,
    datasets: [dataset1_1a, dataset1_2a, dataset1_3a],
  },
};

const stakedBarData = {
  data: {
    labels: labels1,
    datasets: [dataset1_1a, dataset1_2a, dataset1_3a],
  },
  options: {
    title: 'Stacked',
    additionalAxesOptions: {
      stacked: true,
    },
  },
};

const multipleAxisData = {
  data: {
    labels: labels1,
    datasets: [dataset1_1b, dataset1_2b, dataset1_3b],
  },
  options: {
    title: 'Multiple y axes',
    axes: {
      y: [
        {},
        {
          gridLines: {
            drawOnChartArea: false,
          },
        },
      ],
    },
  },
};

const groupedStacked = {
  data: {
    labels: labels1,
    datasets: [dataset1_1c, dataset1_2c, dataset1_3c],
  },
  options: {
    title: 'Grouped stacked',
    additionalAxesOptions: {
      stacked: true,
    },
  },
};

const reversed = {
  data: {
    labels: labels1,
    datasets: [dataset1_1a],
  },
  options: {
    title: 'reversed vertical',
    additionalAxesOptions: {
      reverse: true,
    },
  },
};
const horizontal = {
  data: {
    labels: labels1,
    datasets: [dataset1_1a],
  },
  options: {
    title: 'horizontal',
    direction: 'horizontal',
  },
};
const horizontalReversed = {
  data: {
    labels: labels1,
    datasets: [dataset1_1a],
  },
  options: {
    title: 'reversed horizontal',
    direction: 'horizontal',
    additionalAxesOptions: {
      reverse: true,
    },
  },
};

const axesWithLabels = {
  data: {
    labels: labels1,
    datasets: [dataset1_1a],
  },
  options: {
    title: 'With axes labels',
    axes: {
      x: [
        {
          label: 'Country',
        },
      ],
      y: [
        {
          label: 'Number of pets',
        },
      ],
    },
  },
};

const axisPosition = {
  data: {
    labels: labels1,
    datasets: [dataset1_1a],
  },
  options: {
    title: ['x axis on top', 'y axis on right'],
    axes: {
      x: [
        {
          position: 'top',
        },
      ],
      y: [
        {
          position: 'right',
        },
      ],
    },
  },
};

const notBeginAtZero = {
  data: {
    labels: labels1,
    datasets: [dataset1_1a],
  },
  options: {
    title: 'Not beginning at zero',
    additionalAxesOptions: {
      beginAtZero: false,
    },
  },
};

const stepSizeChart = {
  data: {
    labels: labels1,
    datasets: [dataset1_1a],
  },
  options: {
    title: 'With step size of 2',
    additionalAxesOptions: {
      stepSize: 2,
    },
  },
};

const suggestedRange = {
  data: {
    labels: labels1,
    datasets: [dataset1_1a],
  },
  options: {
    title: 'With suggested range',
    additionalAxesOptions: {
      beginAtZero: false,
      suggestedMin: 2,
      suggestedMax: 12,
    },
  },
};

const logartihmicChart = {
  data: {
    labels: labels1,
    datasets: [dataset1_1a],
  },
  options: {
    title: 'With logarithmic scale',
    additionalAxesOptions: {
      chartScaleType: 'logarithmic',
    },
  },
};

const dataLabelsNumberChart = {
  data: {
    labels: labels1,
    datasets: [datasetNoDataLabels],
  },
  options: {
    title: 'With value as datalabels',
    graph: {
      showDataLabels: true,
    },
  },
};

const borderedChart = {
  data: {
    labels: labels1,
    datasets: [
      {
        ...dataset1_1a,
        backgroundColor: 'rgba(255, 99, 132, 0.2)',
        borderColor: 'black',
        borderWidth: 1,
      },
    ],
  },
};

const horizontalAnnotationChart = {
  data: {
    labels: labels1,
    datasets: [annotationDataset],
  },
  options: {
    title: 'Annotation Y Axis',
    annotations: {
      showAnnotations: true,
      annotationsData: [
        {
          annotationAxis: 'y',
          label: 'Horizontal Annotation',
          value: 53,
          color: 'rgba(96, 32, 196, 0.5)',
        },
      ],
    },
  },
};
const verticalAnnotationChart = {
  data: {
    labels: labels1,
    datasets: [annotationDataset],
  },
  options: {
    title: 'Annotation X Axis',
    annotations: {
      showAnnotations: true,
      annotationsData: [
        {
          annotationAxis: 'x',
          label: 'Vertical Annotation',
          value: 0.5,
          color: 'green',
        },
      ],
    },
  },
};

const diagonalAnnotationChart = {
  data: {
    labels: labels1,
    datasets: [annotationDataset],
  },
  options: {
    title: 'Diagonal Annotation',
    annotations: {
      showAnnotations: true,
      annotationsData: [
        {
          annotationAxis: 'y',
          label: 'Diagonal Annotation',
          value: 40,
          endValue: 67,
          color: 'rgba(123, 16, 32, 0.8)',
        },
      ],
    },
  },
};

const controlAnnotationChart = {
  data: {
    labels: labels1,
    datasets: [annotationDataset],
  },
  options: {
    title: 'Control annotations in legend',
    annotations: {
      showAnnotations: true,
      controlAnnotation: true,
      annotationsData: [
        {
          annotationAxis: 'y',
          label: 'Diagonal Annotation',
          value: 40,
          endValue: 67,
          color: 'rgba(123, 16, 32, 0.8)',
        },
        {
          annotationAxis: 'y',
          label: 'Horizontal Annotation',
          value: 53,
          color: 'rgba(96, 32, 196, 0.5)',
        },
      ],
    },
  },
};

const legendRight = {
  data: {
    labels: labels1,
    datasets: [dataset1_1a],
  },
  options: {
    title: 'Legend to the right',
    legend: {
      position: 'right',
    },
  },
};

const animatedChart = {
  data: {
    labels: labels1,
    datasets: [dataset1_1a],
  },
  options: {
    chartStyling: { performanceMode: false },
  },
};

const dragDataChart = {
  data: {
    labels: labels1,
    datasets: [dataset1_1a],
  },
  options: {
    dragData: {
      enableDragData: true,
      showTooltip: true,
      roundPoints: true,
      onDragStart: (evt, element) => {
        // evt = event, element = datapoint that was dragged
        // you may use this callback to prohibit dragging certain datapoints
        // by returning false in this callback
        // if (element.datasetIndex === 0 && element.index === 0) {
        // this would prohibit dragging the first datapoint in the first
        // dataset entirely
        // return false
        // }
        // console.log('onDragStart =>', evt, element)
      },
      onDrag: (evt, datasetIndex, index, value) => {
        // you may control the range in which datapoints are allowed to be
        // dragged by returning `false` in this callback
        // if (value < 0) return false // this only allows positive values
        // if (datasetIndex === 0 && index === 0 && value > 20) return false
        // console.log('onDrag =>', evt, datasetIndex, index, value)
      },
      onDragEnd: (evt, datasetIndex, index, value) => {
        // you may use this callback to store the final datapoint value
        // (after dragging) in a database, or update other UI elements that
        // dependent on it
        // console.log('onDragEnd =>', evt, datasetIndex, index, value)
      },
    },
  },
};

const hugeNumbersData = {
  data: {
    labels: labels1,
    datasets: [hugeNumbersDataset1, hugeNumbersDataset1, hugeNumbersDataset2],
  },
};

const stackedData = {
  data: {
    labels: ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5', 'Week 6'],
    datasets: [
      {
        label: 'Budget Sales',
        data: [200, 1000, 800, 700, 900, 1600],
        backgroundColor: 'rgba(128, 128, 128, 0.5)',
        borderColor: 'rgba(128, 128, 128, 1)',
        barThickness: 40,
      },
      {
        label: 'Actual Sales',
        data: [300, 600, 400, 500, 700, 1200],
        backgroundColor: 'rgba(64, 64, 64, 0.5)',
        borderColor: 'rgba(64, 64, 64, 1)',
        barThickness: 60,
      },
      {
        label: 'Units',
        data: [20, 40, 30, 50, 70, 90],
        type: 'line',
        backgroundColor: 'rgba(255, 165, 0, 0.5)',
        borderColor: 'rgba(255, 165, 0, 1)',
        yAxisID: 'y-axis-units',
      },
    ],
  },
  options: {
    title: 'Specific axis stacked',
    additionalAxesOptions: {
      stackedX: true,
      stackedY: false,
    },
  },
};

const waterfallChart = {
  data: {
    labels: ['original', 'change 1', 'change 2', 'change 3', 'result'],
    datasets: [waterfallDataset],
  },
  options: {
    title: 'Waterfall chart',
    graph: {
      showDataLabels: true,
    },
  },
};

export default {
  title: 'BarChart',
  component: BarChart,
  args: {
    chart: singleChart,
  },
};

const Template = (args) => {
  return (
    <BarChart
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...args}
    />
  );
};

const customLegendContainerID = 'custom-legend-container';

const TemplateWithCustomLegendContainer = (args) => {
  return (
    <>
      <BarChart
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...args}
      />
      <div id={customLegendContainerID}></div>
    </>
  );
};

export const Default = Template.bind({});

export const CustomColors = Template.bind({});
CustomColors.args = { chart: customColorsChart };

export const CustomColorsArray = Template.bind({});
CustomColorsArray.args = { chart: customColorArrayChart };

export const MultipleSets = Template.bind({});
MultipleSets.args = { chart: multipleBarData };

export const Stacked = Template.bind({});
Stacked.args = { chart: stakedBarData };

export const StackedGrouped = Template.bind({});
StackedGrouped.args = { chart: groupedStacked };

export const MultipleYAxes = Template.bind({});
MultipleYAxes.args = { chart: multipleAxisData };

export const Horizontal = Template.bind({});
Horizontal.args = { chart: horizontal };

export const Reversed = Template.bind({});
Reversed.args = { chart: reversed };

export const HorizontalReversed = Template.bind({});
HorizontalReversed.args = { chart: horizontalReversed };

export const AxesLabels = Template.bind({});
AxesLabels.args = { chart: axesWithLabels };

export const AxesPositions = Template.bind({});
AxesPositions.args = { chart: axisPosition };

export const NonZeroStart = Template.bind({});
NonZeroStart.args = { chart: notBeginAtZero };

export const CustomRange = Template.bind({});
CustomRange.args = { chart: suggestedRange };

export const CustomStepSize = Template.bind({});
CustomStepSize.args = { chart: stepSizeChart };

export const LogarithmicScale = Template.bind({});
LogarithmicScale.args = { chart: logartihmicChart };

export const Datalabels = Template.bind({});
Datalabels.args = { chart: dataLabelsNumberChart };

export const LegendOnRight = Template.bind({});
LegendOnRight.args = { chart: legendRight };

export const Border = Template.bind({});
Border.args = { chart: borderedChart };

export const AnnotationYAxis = Template.bind({});
AnnotationYAxis.args = { chart: horizontalAnnotationChart };

export const AnnotationXAxis = Template.bind({});
AnnotationXAxis.args = { chart: verticalAnnotationChart };

export const AnnotationDiagonal = Template.bind({});
AnnotationDiagonal.args = { chart: diagonalAnnotationChart };

export const AnnotationsInLegend = Template.bind({});
AnnotationsInLegend.args = { chart: controlAnnotationChart };

export const Animated = Template.bind({});
Animated.args = { chart: animatedChart };

export const CustomLegend = TemplateWithCustomLegendContainer.bind({});
CustomLegend.args = {
  chart: {
    ...singleChart,
    options: {
      title: 'Custom HTML legend',
      legend: {
        customLegend: {
          customLegendPlugin: getCustomLegendPlugin(customLegendContainerID),
          customLegendContainerID,
        },
      },
    },
  },
};

export const DragDataChart = Template.bind({});
DragDataChart.args = { chart: dragDataChart };

export const PrettyLabelsTooltips = Template.bind({});
PrettyLabelsTooltips.args = { chart: hugeNumbersData };

export const StackedSelectedAxis = Template.bind({});
StackedSelectedAxis.args = { chart: stackedData };

export const WaterfallChart = Template.bind({});
WaterfallChart.args = { chart: waterfallChart };

export const ControlsPortal = () => {
  const controlsPortalId = 'controls-portal-container';
  return (
    <>
      <div style={{ padding: 20, border: '1px solid red' }}>
        This div is outside the BarChart component
        <div id={controlsPortalId} />
      </div>

      <BarChart
        chart={{
          ...singleChart,
          controlsPortalId,
        }}
      />
    </>
  );
};
