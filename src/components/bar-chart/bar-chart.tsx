import { MutableRefObject, useRef } from 'react';
import {
  BarElement,
  CategoryScale,
  Chart as ChartJS,
  Filler,
  Legend,
  LinearScale,
  LineElement,
  LogarithmicScale,
  PointElement,
  Title,
  Tooltip as ChartTooltip,
} from 'chart.js';
import { Bar } from 'react-chartjs-2';
import zoomPlugin from 'chartjs-plugin-zoom';
import dataLabelsPlugin from 'chartjs-plugin-datalabels';
import annotationPlugin from 'chartjs-plugin-annotation';
import dragDataPlugin from 'chartjs-plugin-dragdata';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { DndProvider } from 'react-dnd';
import { Button, Tooltip } from '@oliasoft-open-source/react-ui-library';
import ControlsPortal from 'components/common/controls-portal';
import { getConfig } from '../line-chart/initialize/config';
import { downloadPgn } from '../common/helpers/download-pgn';
import { useBarChartOptions } from './utils/use-bar-chart-options';
import styles from './bar-chart.module.less';
import { getDefaultProps } from './bar-chart-default-props';
import {
  getClassName,
  getPlugins,
  setDefaultTheme,
} from '../common/helpers/chart-utils';
import { AUTO } from '../common/helpers/chart-consts';
import { IBarChartProps } from './bar-chart.interface';
import { useBarChartConfig } from './utils/use-bar-chart-config';
import CustomLegend from '../common/legend-component/legend';
import { LegendProvider } from '../common/legend-component/state/legend-context';
import { ChartType } from '../common/enums';

ChartJS.register(
  LinearScale,
  PointElement,
  LineElement,
  CategoryScale,
  LogarithmicScale,
  BarElement,
  Legend,
  ChartTooltip,
  Title,
  Filler,
  zoomPlugin,
  dataLabelsPlugin,
  annotationPlugin,
  dragDataPlugin,
);

const BarChart = (props: IBarChartProps) => {
  setDefaultTheme();
  const chartRef: MutableRefObject<null> = useRef(null);
  const chart = getDefaultProps(props);
  const { translations, languageKey } = getConfig();
  const { options, testId, controlsPortalId } = chart;
  const { chartStyling, graph } = options;

  const { generatedDatasets } = useBarChartConfig(chartRef, chart);
  const barChartOptions = useBarChartOptions({ chart, chartRef });
  const showCustomLegend =
    !options?.legend?.customLegend?.customLegendContainerID;

  return (
    <div
      key={languageKey}
      className={getClassName(chartStyling, styles)}
      style={{
        width: chartStyling.width || AUTO,
        height: chartStyling.height || AUTO,
      }}
      data-testid={testId}
    >
      <ControlsPortal controlsPortalId={controlsPortalId}>
        <div className={styles.actions}>
          <Tooltip text={translations.downloadAsPNG} placement="bottom-end">
            <Button
              small
              basic
              colored="muted"
              round
              icon="download"
              onClick={() => downloadPgn(chartRef)}
            />
          </Tooltip>
        </div>
      </ControlsPortal>
      <DndProvider backend={HTML5Backend} context={window}>
        <div className={styles.canvas} id="canvas">
          <Bar
            ref={chartRef}
            data={{
              labels: chart?.data?.labels?.length ? chart.data.labels : [''],
              datasets: generatedDatasets,
            }}
            options={barChartOptions}
            plugins={getPlugins<'bar'>(graph, options.legend)}
          />
          {showCustomLegend && !!generatedDatasets.length && (
            <CustomLegend
              chartRef={chartRef}
              legendConfig={{
                options,
                generatedDatasets,
                chartType: ChartType.BAR,
              }}
            />
          )}
        </div>
      </DndProvider>
    </div>
  );
};

const BarChartWithLegend = (props: IBarChartProps) => {
  const { options } = getDefaultProps(props);
  return (
    <LegendProvider options={options}>
      <BarChart {...props} />
    </LegendProvider>
  );
};

export { BarChartWithLegend as BarChart };
