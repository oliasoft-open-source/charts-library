import { ChartDataset } from 'chart.js';
import {
  ICommonAdditionalAxesOptions,
  ICommonAnnotations,
  ICommonAnnotationsData,
  ICommonAxis,
  ICommonChartOptions,
  ICommonCustomLegend,
  ICommonData,
  ICommonDataset,
  ICommonDataValue,
  ICommonDragData,
  ICommonGraph,
  ICommonInteractions,
  ICommonLegend,
  ICommonOptions,
  ICommonScales,
  ICommonStyling,
  ICommonTooltip,
} from '../common/common.interface';
import { IChartGraph } from '../common/helpers/chart-utils';
import { ChartType } from '../common/helpers/enums';

export interface IBarStyling extends ICommonStyling {
  squareAspectRatio?: boolean;
  layoutPadding?:
    | string
    | number
    | { top: number; bottom: number; left: number; right: number };
}

export interface IBarLegend extends ICommonLegend {
  customLegend?: ICommonCustomLegend<any>;
}

export interface IBarAxes {
  x: ICommonAxis<'top' | 'bottom'>[];
  y: ICommonAxis<'left' | 'right'>[];
  [key: string]: ICommonAxis[];
}

export interface IBarAdditionalAxesOptions
  extends ICommonAdditionalAxesOptions {
  stackedX?: boolean;
  stackedY?: boolean;
}

export interface IBarOptions extends ICommonOptions {
  direction: 'vertical' | 'horizontal';
  axes: IBarAxes;
  scales?: ICommonScales;
  additionalAxesOptions?: IBarAdditionalAxesOptions;
  chartStyling: IBarStyling;
  graph?: ICommonGraph;
  annotations?: ICommonAnnotations;
  legend?: IBarLegend;
  dragData?: ICommonDragData;
}

export interface IBarChartDataset extends ICommonDataset {
  isAnnotation?: boolean;
  annotationIndex?: number;
  borderSkipped?: boolean;
  borderRadius?: number;
  xAxisID?: string;
  yAxisID?: string;
  hidden?: boolean;
  displayGroup?: string | number;
  type?: ChartType.Bar;
}

export interface IBarChartBaseData {
  labels: string[];
  datasets: IBarChartDataset[];
}

export interface IBarChartData extends ICommonData {
  testId?: string;
  controlsPortalId?: string;
  data: IBarChartBaseData;
  options: IBarOptions;
}

export interface IBarChartProps {
  chart: IBarChartData;
}

export interface IBarDefaultProps {
  testId: string;
  controlsPortalId: string;
  data: IBarChartBaseData;
  options: {
    title?: string | string[];
    direction: 'vertical' | 'horizontal';
    axes: IBarAxes;
    additionalAxesOptions: IBarAdditionalAxesOptions;
    chartStyling: IBarStyling;
    tooltip: ICommonTooltip;
    graph: IChartGraph;
    scales?: ICommonScales;
    annotations: {
      showAnnotations: boolean;
      controlAnnotation: boolean;
      annotationsData: ICommonAnnotationsData[];
    };
    legend: IBarLegend;
    chartOptions: ICommonChartOptions;
    interactions: ICommonInteractions;
    dragData: ICommonDragData;
  };
}

export interface IGenerateBarChartDataset
  extends ChartDataset<'bar', ICommonDataValue[]> {
  borderColor: string;
  backgroundColor: string | string[];
  borderDash?: number[];
  borderWidth?: number;
  annotationType?: string;
  hideLegend?: boolean;
  displayGroup?: string | number;
  isAnnotation?: boolean;
  annotationIndex?: number;
}

export type TGenerateBarChartDatasets = IGenerateBarChartDataset[];
