import {
  IBarAdditionalAxesOptions,
  IBarAxes,
  IBarChartProps,
  IBarDefaultProps,
  IBarLegend,
  IBarStyling,
} from './bar-chart.interface';
import {
  ICommonAnnotations,
  ICommonAnnotationsData,
  ICommonAxis,
  ICommonChartOptions,
  ICommonDragData,
  ICommonGraph,
  ICommonInteractions,
  ICommonTooltip,
} from '../common/common.interface';
import { AlignOptions, Position } from '../common/helpers/enums';

// Helper functions for setting default properties
const defaultAxes = (axes?: IBarAxes) => ({
  x: axes?.x || ([{}] as ICommonAxis[]),
  y: axes?.y || ([{}] as ICommonAxis[]),
});

const defaultAdditionalAxesOptions = (options?: IBarAdditionalAxesOptions) => ({
  chartScaleType: options?.chartScaleType || 'linear',
  reverse: options?.reverse || false,
  stacked: options?.stacked || false,
  beginAtZero: options?.beginAtZero ?? true,
  stepSize: options?.stepSize,
  suggestedMin: options?.suggestedMin,
  suggestedMax: options?.suggestedMax,
  min: options?.min,
  max: options?.max,
  stackedX: options?.stackedX,
  stackedY: options?.stackedY,
});

const defaultChartStyling = (styling?: IBarStyling) => ({
  width: styling?.width,
  height: styling?.height,
  maintainAspectRatio: styling?.maintainAspectRatio || false,
  staticChartHeight: styling?.staticChartHeight || false,
  performanceMode: styling?.performanceMode ?? true,
});

const defaultTooltip = (tooltip?: ICommonTooltip) => ({
  tooltips: tooltip?.tooltips ?? true,
  showLabelsInTooltips: tooltip?.showLabelsInTooltips || false,
  scientificNotation: tooltip?.scientificNotation ?? true,
});

const defaultGraph = (graph?: ICommonGraph) => ({
  showDataLabels: graph?.showDataLabels || false,
  showMinorGridlines: graph?.showMinorGridlines || false,
});

const defaultAnnotationsData = (annotationsData?: ICommonAnnotationsData[]) => {
  return annotationsData
    ? annotationsData.map((ann) => ({ ...ann, display: ann?.display ?? true }))
    : [];
};

const defaultAnnotations = (annotations?: ICommonAnnotations) => ({
  showAnnotations: annotations?.showAnnotations ?? true,
  controlAnnotation: annotations?.controlAnnotation || false,
  annotationsData: defaultAnnotationsData(annotations?.annotationsData),
});

const defaultLegend = (legend?: IBarLegend) => ({
  display: legend?.display ?? true,
  position: legend?.position || Position.TopLeft,
  align: legend?.align || AlignOptions.Center,
  customLegend: legend?.customLegend || {
    customLegendPlugin: null,
    customLegendContainerID: '',
  },
});

const defaultChartOptions = (options?: ICommonChartOptions) => ({
  enableZoom: options?.enableZoom || false,
  enablePan: options?.enablePan || false,
});

const defaultInteractions = (interactions?: ICommonInteractions) => ({
  onLegendClick: interactions?.onLegendClick,
  onHover: interactions?.onHover,
  onUnhover: interactions?.onUnhover,
});

const defaultDragData = (dragData?: ICommonDragData) => ({
  enableDragData: dragData?.enableDragData ?? false,
  showTooltip: dragData?.showTooltip ?? true,
  roundPoints: dragData?.roundPoints ?? true,
  dragX: dragData?.dragX ?? true,
  dragY: dragData?.dragY ?? true,
  onDragStart: dragData?.onDragStart,
  onDrag: dragData?.onDrag,
  onDragEnd: dragData?.onDragEnd,
});

export const getDefaultProps = (props: IBarChartProps): IBarDefaultProps => {
  const chart = props?.chart || {};
  const options = chart?.options || {};

  // Set defaults for missing properties
  return {
    testId: chart?.testId ?? '',
    controlsPortalId: chart?.controlsPortalId ?? '',
    data: chart?.data,
    options: {
      title: options?.title || '',
      direction: options?.direction || 'vertical',
      axes: defaultAxes(options?.axes),
      additionalAxesOptions: defaultAdditionalAxesOptions(
        options?.additionalAxesOptions,
      ),
      chartStyling: defaultChartStyling(options?.chartStyling),
      tooltip: defaultTooltip(options?.tooltip),
      graph: defaultGraph(options?.graph),
      annotations: defaultAnnotations(options?.annotations),
      legend: defaultLegend(options?.legend),
      chartOptions: defaultChartOptions(options?.chartOptions),
      interactions: defaultInteractions(options?.interactions),
      dragData: defaultDragData(options?.dragData),
    },
  };
};
