import { Tick } from 'chart.js';
import { AxisType, ScaleType } from '../../common/helpers/enums';
import {
  generateRandomColor,
  getAxisPosition,
  isVertical,
} from '../../common/helpers/chart-utils';
import {
  COLORS,
  DEFAULT_FONT_SIZE,
  LOGARITHMIC_STEPS,
} from '../../common/helpers/chart-consts';
import { formatAxisLabelNumbers } from '../../line-chart/utils/axis-formatting/axis-formatting';
import {
  IBarAdditionalAxesOptions,
  IBarDefaultProps,
} from '../bar-chart.interface';
import { ICommonAxis, UnusedParameter } from '../../common/common.interface';

const getStackedOptions = (
  axisType: AxisType,
  { stacked, stackedX, stackedY }: IBarAdditionalAxesOptions,
) => {
  if (axisType === AxisType.X) {
    return stackedX ?? stacked;
  } else if (axisType === AxisType.Y) {
    return stackedY ?? stacked;
  }

  return stacked;
};

interface IGetBarChartAxisParams {
  chart: IBarDefaultProps;
  axisType?: AxisType;
  currentScale?: ICommonAxis;
  i?: number;
}

const getBarChartAxis = ({
  chart,
  axisType = AxisType.X,
  currentScale,
  i,
}: IGetBarChartAxisParams) => {
  const { data, options } = chart ?? {};

  const isDirectionVertical = isVertical(options.direction);

  const axisData =
    currentScale || (options?.axes?.[axisType]?.[0] as ICommonAxis);
  const isDirectionCompatibleWithAxisType =
    (isDirectionVertical && axisType === AxisType.Y) ||
    (!isDirectionVertical && axisType === AxisType.X);

  const grid = axisData?.gridLines || {};

  const getScaleType = () => {
    const scaleType = data.labels ? ScaleType.Category : ScaleType.Linear;
    const axisWithScale = isDirectionVertical ? AxisType.X : AxisType.Y;
    return axisType === axisWithScale
      ? scaleType
      : options?.additionalAxesOptions?.chartScaleType;
  };

  const getReverse = () => {
    const axisWithReverse = isDirectionVertical ? AxisType.Y : AxisType.X;
    return axisType === axisWithReverse
      ? options?.additionalAxesOptions?.reverse
      : false;
  };

  const getTicks = () => {
    const additionalAxesOptions = options?.additionalAxesOptions;
    const ticksConfigFromProps =
      options?.scales?.[`${i === 0 ? axisType : axisType + i}`]?.ticks;
    const isLogarithmic =
      isDirectionCompatibleWithAxisType &&
      additionalAxesOptions?.chartScaleType === ScaleType.Logarithmic;
    const ticksFormattingCallback = (
      tick: string | number,
      _: UnusedParameter,
      ticks: Tick[],
    ) => {
      return isLogarithmic
        ? LOGARITHMIC_STEPS.includes(Number(tick))
          ? formatAxisLabelNumbers(tick, ticks)
          : ''
        : formatAxisLabelNumbers(tick, ticks);
    };
    const stepSize = !isLogarithmic
      ? {
          stepSize:
            axisData.stepSize ??
            (axisType === AxisType.Y ? additionalAxesOptions?.stepSize : null),
        }
      : {};
    const ticks = {
      ...stepSize,
      includeBounds: false, //OW-10088 disable irregular axis ticks
      ...ticksConfigFromProps,
      font: {
        size: DEFAULT_FONT_SIZE,
      },
    };
    if (additionalAxesOptions?.chartScaleType === ScaleType.Logarithmic) {
      ticks.callback = ticksFormattingCallback;
    }
    return ticks;
  };

  return {
    type: getScaleType(),
    position: axisData?.position,
    beginAtZero: options?.additionalAxesOptions?.beginAtZero,
    reverse: getReverse(),
    suggestedMax: options?.additionalAxesOptions?.suggestedMax,
    suggestedMin: options?.additionalAxesOptions?.suggestedMin,
    min: isDirectionCompatibleWithAxisType
      ? options?.additionalAxesOptions?.min
      : undefined,
    max: isDirectionCompatibleWithAxisType
      ? options?.additionalAxesOptions?.max
      : undefined,
    title: {
      display: axisData?.label?.length || axisData?.unit?.length,
      text: axisData?.label || axisData?.unit,
      padding: 0,
    },
    ticks: getTicks(),
    grid: {
      ...grid,
    },
    stacked: getStackedOptions(axisType, options?.additionalAxesOptions),
  };
};

/**
 * @param {IBarChartData} chart - chart data
 * @param {'x'|'y'} axisType
 */
const getBarChartAxes = (chart: IBarDefaultProps, axisType: AxisType) => {
  const axesData = (chart?.options?.axes?.[axisType] || []) as any[];
  const axes = axesData.reduce(
    (acc: ICommonAxis[], curr: ICommonAxis, i: number) => {
      const axisData = curr;
      const color = curr.color || COLORS[i] || generateRandomColor(COLORS);
      axisData.color = color;
      axisData.position = curr.position || getAxisPosition(axisType, i);

      const axis = getBarChartAxis({ chart, axisType, currentScale: axisData });
      const axisId = i === 0 ? axisType : `${axisType}${i + 1}`;
      return { ...acc, [axisId]: axis };
    },
    {},
  );
  return axes;
};

const getBarChartScales = (chart: IBarDefaultProps) => {
  const hasMultipleXAxes = chart.options.axes.x?.length > 1;
  const hasMultipleYAxes = chart.options.axes.y?.length > 1;

  const xAxesScales = hasMultipleXAxes
    ? getBarChartAxes(chart, AxisType.X)
    : { x: getBarChartAxis({ chart, axisType: AxisType.X }) };
  const yAxesScales = hasMultipleYAxes
    ? getBarChartAxes(chart, AxisType.Y)
    : { y: getBarChartAxis({ chart, axisType: AxisType.Y }) };

  return {
    ...xAxesScales,
    ...yAxesScales,
  };
};

export default getBarChartScales;
