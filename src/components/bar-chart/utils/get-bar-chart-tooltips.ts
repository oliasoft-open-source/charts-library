import { displayNumber, roundByMagnitude } from '@oliasoft-open-source/units';
import { ChartData, TooltipItem } from 'chart.js';
import {
  afterLabelCallback,
  getTooltipLabel,
  isVertical,
} from 'helpers/chart-utils.ts';
import { ChartHoverMode, TooltipLabel } from 'helpers/enums';
import {
  LEGEND_LABEL_BOX_SIZE,
  TOOLTIP_BOX_PADDING,
  TOOLTIP_PADDING,
} from 'helpers/chart-consts';
import { IBarChartDataset, IBarOptions } from '../bar-chart.interface';
import { ICommonAxis } from '../../common/common.interface';
import {
  DISPLAY_SCIENTIFIC_LOWER_BOUND,
  DISPLAY_SCIENTIFIC_UPPER_BOUND,
} from '../bar-chart-const';

/**
 * @param {IBarOptions} options - bar chart options object
 */
const getBarChartToolTips = (options: IBarOptions) => {
  const getTooltipLabels = (dataset: IBarChartDataset) => {
    const isDirectionVertical = isVertical(options.direction);
    const x: ICommonAxis[] = Array.isArray(options.axes?.x)
      ? options.axes.x
      : [];
    const y: ICommonAxis[] = Array.isArray(options.axes?.y)
      ? options.axes.y
      : [];

    const getAxisIndex = (axisID: string | undefined): number => {
      if (!axisID) return 0;

      // Extract the trailing number part using a regex
      const match = axisID.match(/\d+$/);

      // If there was a match, parse the number, subtract 1 to make it zero-indexed
      // If there was no match, default to 0
      return match ? parseInt(match[0], 10) - 1 : 0;
    };

    // Use the helper function to get index
    const xIndex = getAxisIndex(dataset?.xAxisID);
    const yIndex = getAxisIndex(dataset?.yAxisID);

    const xAxis = isDirectionVertical ? x[xIndex] : y[yIndex];
    const yAxis = isDirectionVertical ? y[yIndex] : x[xIndex];

    return {
      titleAxisLabel: xAxis?.label || '',
      valueAxisLabel: yAxis?.label || '',
      titleLabel: TooltipLabel.X,
      valueLabel: TooltipLabel.Y,
      titleUnit: xAxis?.unit || '',
      valueUnit: yAxis?.unit || '',
    };
  };

  const titleCallback = (
    tooltipItems: TooltipItem<any>[],
    _data: ChartData,
  ) => {
    const barLabel = tooltipItems?.[0]?.label || '';
    const labels = getTooltipLabels(tooltipItems?.[0]?.dataset);
    const { titleAxisLabel, titleUnit } = labels;
    const axisLabel =
      !titleAxisLabel && !titleUnit ? '' : `${titleAxisLabel || titleUnit}: `;
    return axisLabel + barLabel;
  };

  const customFormatNumber = (labelNumber: number) => {
    let roundOptions = {};

    if (!options?.tooltip?.scientificNotation) {
      roundOptions = { scientific: false };
    } else if (
      Math.abs(labelNumber) < DISPLAY_SCIENTIFIC_LOWER_BOUND ||
      Math.abs(labelNumber) > DISPLAY_SCIENTIFIC_UPPER_BOUND
    ) {
      roundOptions = { roundScientificCoefficient: 3 };
    }

    return displayNumber(roundByMagnitude(labelNumber), roundOptions);
  };

  const labelCallback = (tooltipItem: TooltipItem<any>) => {
    const { showLabelsInTooltips = false } = options?.tooltip || {};
    let label = tooltipItem.dataset?.label || '';
    const labels = getTooltipLabels(tooltipItem.dataset);
    const { valueUnit, valueAxisLabel } = labels;

    const getTooltipItemValue = () => {
      const tooltipValue = tooltipItem?.raw;
      const labelNumberFormatted =
        typeof tooltipValue === 'number'
          ? customFormatNumber(tooltipValue)
          : '';

      let tooltipItemValue = '';
      if (typeof tooltipItem.raw === 'number') {
        tooltipItemValue = labelNumberFormatted;
      }
      if (Array.isArray(tooltipItem.raw)) {
        tooltipItemValue = tooltipItem.raw.reduce(
          (acc: string, curr: string, i: number) => {
            return i === 0 ? `${acc} ${curr}` : `${acc}, ${curr}`;
          },
          '',
        );
      } else if (typeof tooltipItem.raw === 'object') {
        tooltipItemValue = labelNumberFormatted;
      }

      return tooltipItemValue;
    };

    const hideValueAxisLabel =
      label === valueAxisLabel || valueAxisLabel.includes(label);

    const tooltipItemValue = getTooltipItemValue();
    const unit = valueUnit ? `[${valueUnit}] ` : '';
    const valAxisLabel = hideValueAxisLabel ? '' : valueAxisLabel;
    const tooltipLabel = getTooltipLabel(tooltipItem, showLabelsInTooltips);

    return `${label}: ${tooltipItemValue} ${unit}${valAxisLabel}${tooltipLabel}`;
  };

  return {
    enabled: options?.tooltip?.tooltips,
    mode: ChartHoverMode.Nearest,
    intersect: true,
    padding: TOOLTIP_PADDING,
    boxWidth: LEGEND_LABEL_BOX_SIZE,
    boxHeight: LEGEND_LABEL_BOX_SIZE,
    boxPadding: TOOLTIP_BOX_PADDING,
    callbacks: {
      title: titleCallback,
      label: labelCallback,
      afterLabel: afterLabelCallback,
    },
  } as Record<string, any>;
};

export default getBarChartToolTips;
