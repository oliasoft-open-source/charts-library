import { Chart, ScriptableContext } from 'chart.js';
import { round } from '@oliasoft-open-source/units';
import { AlignOptions } from '../../common/helpers/enums.ts';
import { AUTO } from '../../common/helpers/chart-consts.ts';
import { IBarOptions } from '../bar-chart.interface.ts';
import { UnusedParameter } from '../../common/common.interface';

type ExtendedDisplayOptions = 'auto' | boolean;

export interface IExtendedDataLabelsOptions {
  display: ExtendedDisplayOptions;
}

interface IDataLabelContext extends ScriptableContext<'bar'> {
  chart: Chart;
  datasetIndex: number;
  dataIndex: number;
}

/**
 * Returns the data labels configuration for a bar chart.
 */
const getBarChartDataLabels = (options: IBarOptions) => {
  const { beginAtZero, reverse } = options?.additionalAxesOptions || {};

  const formatterCallback = (
    _: UnusedParameter,
    context: IDataLabelContext,
  ) => {
    const { datasetIndex, dataIndex } = context;
    const dataset = context.chart.data.datasets![datasetIndex!];
    const dataElement = dataset.data[dataIndex!];

    let dataLabel: string | number = '';
    if (typeof dataElement === 'number') {
      dataLabel = Number.isInteger(dataElement)
        ? dataElement
        : dataElement.toFixed(2);
    }
    if (Array.isArray(dataElement)) {
      if (dataElement.length === 2) {
        const sum = dataElement[1] - dataElement[0];
        dataLabel = sum < 0 ? round(sum, 2) : `+${round(sum, 2)}`;
      } else {
        dataLabel = dataElement.reduce((acc, curr, i) => {
          return i === 0 ? `${acc} ${curr}` : `${acc}, ${curr}`;
        }, '');
      }
    }

    return dataLabel;
  };

  const beginAtNotZeroAlign = reverse ? AlignOptions.End : AlignOptions.Start;
  const beginAtNotZeroAnchor = reverse ? AlignOptions.Start : AlignOptions.End;

  return options?.graph?.showDataLabels
    ? ({
        display: AUTO,
        align: beginAtZero ? AlignOptions.Center : beginAtNotZeroAlign,
        anchor: beginAtZero ? AlignOptions.Center : beginAtNotZeroAnchor,
        formatter: formatterCallback,
      } as IExtendedDataLabelsOptions)
    : { display: false };
};

export default getBarChartDataLabels;
