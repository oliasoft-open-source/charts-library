import { MutableRefObject } from 'react';
import { Chart } from 'chart.js';
import { useBarChartConfig } from './use-bar-chart-config';
import { getTitle, isVertical } from '../../common/helpers/chart-utils';
import { AxisType, ChartHoverMode } from '../../common/helpers/enums';
import {
  ANIMATION_DURATION,
  BORDER_COLOR,
} from '../../common/helpers/chart-consts';
import getBarChartScales from './get-bar-chart-scales';
import getBarChartDataLabels from './get-bar-chart-data-labels';
import getBarChartToolTips from './get-bar-chart-tooltips';
import getDraggableData from '../../common/helpers/get-draggableData';
import { IBarDefaultProps } from '../bar-chart.interface';
import { useLegend } from '../../common/hooks/use-legend';
import { useLegendState } from '../../common/hooks/use-legend-state';
import { toAnnotationObject } from '../../common/helpers/to-annotation';

export interface IUseBarChartOptionsProps {
  chart: IBarDefaultProps;
  chartRef: MutableRefObject<Chart | null>;
}

export const useBarChartOptions = ({
  chart,
  chartRef,
}: IUseBarChartOptionsProps) => {
  const { options } = chart;
  const { chartStyling, dragData } = options;
  const { state: legendState } = useLegend();
  const { annotation } = legendState;
  const { onClick, onHover } = useBarChartConfig(chartRef, chart);
  const { legend, customLegendPlugin } = useLegendState({ chartRef, options });

  return {
    onClick,
    onHover,
    indexAxis: isVertical(options.direction) ? AxisType.X : AxisType.Y,
    maintainAspectRatio: chartStyling.maintainAspectRatio,
    animation: chartStyling.performanceMode && {
      duration: ANIMATION_DURATION.FAST,
    },
    hover: {
      mode: ChartHoverMode.Nearest,
      intersect: true,
    },
    scales: getBarChartScales(chart),
    plugins: {
      title: getTitle(options),
      datalabels: getBarChartDataLabels(options),
      annotation: toAnnotationObject(annotation),
      tooltip: getBarChartToolTips(options),
      legend: { ...legend, display: false, events: [] }, // hide default legend
      customLegendPlugin,
      chartAreaBorder: {
        borderColor: BORDER_COLOR,
      },
      dragData: dragData?.enableDragData && getDraggableData(options),
    },
  };
};
