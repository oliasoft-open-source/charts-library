import { RefObject, useMemo, useState } from 'react';
import { ActiveElement, Chart, ChartEvent } from 'chart.js';
import {
  IBarDefaultProps,
  IBarOptions,
  TGenerateBarChartDatasets,
} from '../bar-chart.interface';
import {
  ICommonAnnotationsData,
  ICommonHoveredItems,
  UnusedParameter,
} from '../../common/common.interface';
import { ALPHA_CHANEL, COLORS } from '../../common/helpers/chart-consts';
import { generateRandomColor } from '../../common/helpers/chart-utils';
import { ChartType } from '../../common/helpers/enums';

export interface IGenerateBarChartDatasetsProps {
  datasets: TGenerateBarChartDatasets;
  options: IBarOptions;
}

export const useBarChartConfig = (
  chartRef: RefObject<Chart>,
  chart: IBarDefaultProps,
) => {
  const { options } = chart ?? {};
  const { interactions } = options ?? {};
  const [pointHover, setPointHover] = useState(false);

  const generateBarChartDatasets = ({
    datasets,
    options,
  }: IGenerateBarChartDatasetsProps): TGenerateBarChartDatasets => {
    const {
      annotations: { controlAnnotation, showAnnotations, annotationsData } = {},
    } = options;

    const annotationsDatasets =
      controlAnnotation && showAnnotations && annotationsData?.length
        ? annotationsData.map(
            (annotation: ICommonAnnotationsData, index: number) => ({
              isAnnotation: true,
              label: annotation?.label,
              annotationIndex: index,
              backgroundColor: annotation?.color || COLORS[index],
              borderColor: annotation?.color || COLORS[index],
              data: [],
              type: ChartType.Bar as const,
            }),
          )
        : [];

    const barDatasetsCopy = [...datasets, ...annotationsDatasets];

    const generatedDatasets = barDatasetsCopy?.map((barDataset, index) => {
      const colorSchema = COLORS;
      const colors = barDataset?.data?.map((_: UnusedParameter) => {
        const colorSelectionIndex = datasets?.length > 1 ? index : 0;
        return (
          colorSchema?.[colorSelectionIndex] || generateRandomColor(COLORS)
        );
      }) as string[];

      const backgroundColors =
        barDataset?.backgroundColor ||
        colors?.map((color: string) => color + ALPHA_CHANEL);

      const datasetWithBorderWidth =
        'borderWidth' in barDatasetsCopy
          ? {
              borderWidth:
                parseFloat(String(barDatasetsCopy?.borderWidth)) ?? 1,
            }
          : {};

      return {
        ...barDataset,
        ...datasetWithBorderWidth,
        borderColor: barDataset?.borderColor || colors?.[index],
        backgroundColor: backgroundColors,
      };
    });

    return generatedDatasets || [];
  };

  const generatedDatasets = generateBarChartDatasets({
    datasets: chart?.data?.datasets as TGenerateBarChartDatasets,
    options,
  });

  const onClick = (
    _evt: ChartEvent,
    _elements: ActiveElement[],
    chartInstance: Chart,
  ) => {
    chartInstance.resetZoom();
    // TODO: Restore redux-logic for zoom
  };

  const onHover = (evt: ChartEvent, hoveredItems: ICommonHoveredItems[]) => {
    if (pointHover && !hoveredItems?.length) {
      setPointHover(false);
      if (interactions?.onUnhover) {
        interactions?.onUnhover(evt);
      }
    }
    if (!pointHover && hoveredItems?.length) {
      setPointHover(true);
      if (interactions.onHover) {
        const { index, datasetIndex } = hoveredItems?.[0] ?? {};
        const generatedDataset = generatedDatasets;
        interactions?.onHover(evt, datasetIndex, index, generatedDataset);
      }
    }
  };

  return useMemo(
    () => ({
      generatedDatasets,
      onClick,
      onHover,
    }),
    [chartRef, chart],
  );
};
