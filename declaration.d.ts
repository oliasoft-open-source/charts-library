declare module '*.module.less' {
  const classes: { [className: string]: string };
  export default classes;
}

declare module '*.svg' {
  import * as React from 'react';

  export const ReactComponent: React.FunctionComponent<
    React.SVGProps<SVGSVGElement> & { title?: string }
  >;

  const src: string;
  export default src;
}

declare module 'chartjs-plugin-dragdata';

declare module 'react-base64-downloader' {
  export function triggerBase64Download(base64: string, filename: string): void;
}

declare module 'html-to-image';
